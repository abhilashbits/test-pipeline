# atm-framework
Arine Automation Framework

Used Technologies to develop this framework:
1. Java 8.0
2. Maven 3.6.0

Below are the steps to run this automation:

1. Install Java 8.0 and add JAVA_HOME in System/User Environment Variables.
2. Install Maven 3.6.0 and add MAVEN_HOME in System/User Environment Variables.
3. For Development purpose install IntelliJ Community version.
4. To run use command: **'mvn test'**
5. By default we have configured windows as operation system if we want to run it from mac-OS we need to add additional parameter at run time: **mvn test -DOS=MAC**
6. After execution we can find report directories with name cucumber-report & extent-report.
7. Also if we want to verify logs, we can get logs from logs directory where it will create logs file.
