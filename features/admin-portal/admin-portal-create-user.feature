@AdminPortal @CreateUser
Feature: Verify create user functionality

  @Setup @Regression @Smoke
  Scenario Outline: SETUP: Launch Browser and go to application
    Given User launched "chrome"
    Then User go to application "https://admin-dev.arine.io/"
    When Admin User login with "<username>" and "<password>"
    Then User is on Home Page
    Examples:
      | username                              | password  | message | type              |
      | abhilashgadekar@benchmarkit.solutions | Arine@123 | success | Valid Credentials |

#  @Regression @Smoke
#  Scenario Outline: verify_create_user_feature
#    Given User is on Home Page
#    When Create new user with data:
#      | firstName  | lastName | email                        | jobTitle    | account          | role       | credentials        | npi        | ipAddress   | phone      |
#      | Automation | User     | automationtimestamp@test.com | QA Engineer | Prospect Medical | Pharmacist | testautomationuser | 1234567890 | 10.10.10.10 | 8993484834 |
#    Then verify success message : "<message>"
#    And Search created user with email address.
#    Examples:
#      | message                   |
#      | User Created Successfully |

  @Regression @Smoke
  Scenario: verify_create_user_error_message_validation
    Given User is on Home Page
    And User clicks on reset button
    When Create new user with invalid data:
      | firstName  | lastName   | email                     | jobTitle    | account | role       | credentials        | npi        | ipAddress | phone    |
      | Peter Mark | Henry,cena | automation-timestamp@test | QA Engineer | PPOK    | Pharmacist | testautomationuser | 1234567890 | test      | 89934844 |
    Then Verify error message displayed is "FirstName is invalid, only alphabets are allowed here."
    And Verify error message displayed is "LastName is invalid, only alphabets are allowed here."
    And Verify error message displayed is "Email is not valid"
    And Verify error message displayed is "IpAddress is not valid"
    And Verify error message displayed is "PhoneNumber is invalid, please enter your 10 digit phone number excluding your country code"
    Then User clicks on "Cancel" button

  @Regression @Smoke
  Scenario: verify_create_user_form_create_button
    Given User is on Home Page
    And User clicks on reset button
    When Create new user with invalid data:
      | firstName | lastName | email | jobTitle | account | role | credentials | npi | ipAddress   | phone      |
      |           |          |       |          |         |      |             |     | 10.10.10.10 | 8983979090 |
    Then Verify "Create" button is disabled.
    And User clicks on "Cancel" button

  @Regression @Smoke
  Scenario: verify_fields_as_per_role
    Given User is on Home Page
    And User clicks on reset button
    And User clicks on "Create User" button
    When User selects role "Support"
    Then Verify textbox "Credentials" is disabled.
    Then Verify textbox "npi" is disabled.
    When User selects role "Pharmacist"
    Then Verify textbox "Credentials" is enabled.
    Then Verify textbox "npi" is enabled.
    And User clicks on "Cancel" button

  @Regression @Smoke
  Scenario Outline: verify_duplicate_create_user_feature
    Given User is on Home Page
    And User clicks on reset button
    When Create new user with data:
      | firstName  | lastName | email         | jobTitle    | account | role       | credentials        | npi        | ipAddress   | phone      |
      | Automation | User     | abhi@test.com | QA Engineer | PPOK    | Pharmacist | testautomationuser | 1234567890 | 10.10.10.10 | 8993484834 |
    Then Verify alert message "<message>"
    Then User clicks on "Cancel" button
    Examples:
      | message                                           |
      | [409] There is already an account with this email |

  @Regression @Smoke
  Scenario: verify_create_button_disabled_without_mandatory_data
    Given User is on Home Page
    And User clicks on reset button
    When User clicks on "Create User" button
    Then Verify "Create" button is disabled.

  @Regression @Smoke
  Scenario: verify_readonly_field_create_user_modal.
    Given User is on Home Page
    And User clicks on reset button
    When User clicks on "Create User" button
    Then Verify textbox "Created By" is readonly and textbox value is "Abhilash"
    #And Verify textbox "Created" is readonly and textbox value is "currentdate"
    #And Verify textbox "Updated" is readonly and textbox value is "currentdate"
    And Verify textbox "Updated By" is readonly and textbox value is "Abhilash"
    And User clicks on "Cancel" button

  @Regression @Smoke
  Scenario: verify_create_user_invalid_phoneno
    Given User is on Home Page
    And User clicks on reset button
    When Create new user with invalid data:
      | firstName  | lastName | email                        | jobTitle    | account          | role       | credentials        | npi        | ipAddress   | phone         |
      | Automation | User     | automationtimestamp@test.com | QA Engineer | Prospect Medical | Pharmacist | testautomationuser | 1234567890 | 10.10.10.10 | +918993484834 |
    And Verify error message displayed is "PhoneNumber is invalid, please enter your 10 digit phone number excluding your country code"
    And User clicks on "Cancel" button

  @Regression @Smoke
  Scenario: verify_edit_user_with_valid_data
    Given User is on Home Page
    And User clicks on reset button
    Given User enters "email" as "abhilash@test.com"
    When User clicks on search button
    And select first user from search result and open edit user modal
    When Edit user with data:
      | firstName        | lastName   | jobTitle           | account          | role       | credentials  | npi        | ipAddress       | status   | phone      |
      | AutomationUpdate | UserUpdate | QA Engineer Update | Prospect Medical | Pharmacist | updatedcreds | 1236412345 | 100.100.100.100 | Disabled | 8983979090 |
    Then verify success message : "User Updated Successfully"
    And User clicks on reset button
    Given User enters "email" as "abhilash@test.com"
    When User clicks on search button
    Then Verify searched data in column "firstName" contains "AutomationUpdate"
    Then Verify searched data in column "lastName" contains "UserUpdate"
    Then Verify searched data in column "email" contains "abhilash@test.com"
    Then Verify searched data in column "role" contains "Pharmacist"

  @Regression @Smoke
  Scenario: verify_edit_user_without_changing_fields
    Given User is on Home Page
    And User clicks on reset button
    And User enters "email" as "abhilash@test.com"
    When User clicks on search button
    And select first user from search result and open edit user modal
    Then User clicks on "Update" button and wait for message "User Updated Successfully"

  @Regression @Smoke
  Scenario: verify_edit_user_with_blank_data_in_mandatory_fields
    Given User is on Home Page
    And User clicks on reset button
    And User enters "email" as "abhilash@test.com"
    When User clicks on search button
    And select first user from search result and open edit user modal
    When Edit user with invalid data:
      | firstName | lastName | jobTitle | account | role | credentials | npi | ipAddress | status | phone |
      |           |          |          |         |      |             |     |           |        |       |
    And User clicks on "Cancel" button

  @Regression @Smoke
  Scenario: verify_readonly_field_edit_user_modal.
    Given User is on Home Page
    And User clicks on reset button
    And User enters "email" as "abhilash@test.com"
    When User clicks on search button
    And select first user from search result and open edit user modal
    Then Verify textbox "Created By" is readonly and textbox value is "Abhilash"
    #And Verify textbox "Created" is readonly and textbox value is "currentdate"
    #And Verify textbox "Updated" is readonly and textbox value is "currentdate"
    And Verify textbox "Updated By" is readonly and textbox value is "Abhilash"
    #And Verify textbox "Password Last Updated" is readonly and textbox value is "currentdate"
    And User clicks on "Cancel" button

  @Regression @Smoke
  Scenario: verify_enable_disable_functionality
    Given User is on Home Page
    And User clicks on reset button
    And User enters "email" as "disabledpharmacist@benchmarkit.solutions"
    When User clicks on search button
    And select first user from search result and open edit user modal
    When Edit user "status" with data "Enabled"
    Then User clicks on "Update" button and wait for message "User Updated Successfully"
    And User clicks on reset button
    Given User enters "email" as "disabledpharmacist@benchmarkit.solutions"
    When User clicks on search button
    Then Verify searched data in column "status" contains "Enabled"
    And select first user from search result and open edit user modal
    When Edit user "status" with data "Disabled"
    Then User clicks on "Update" button and wait for message "User Updated Successfully"
    And User clicks on reset button
    Given User enters "email" as "disabledpharmacist@benchmarkit.solutions"
    When User clicks on search button
    Then Verify searched data in column "status" contains "Disabled"

  @Regression @Smoke
  Scenario: verify_email_field_disabled_edit_user.
    Given User is on Home Page
    And User clicks on reset button
    And User enters "email" as "abhilash@test.com"
    When User clicks on search button
    And select first user from search result and open edit user modal
    Then Verify textbox "Email" is disabled.
    And User clicks on "Cancel" button

#  @Regression @Smoke
#  Scenario: verify_edit_user_with_invalid_phone
#    Given User is on Home Page
#    And User search "firstName" with "Automation"
#    And select first user from search result and open edit user modal
#    When Edit user with invalid data:
#      | firstName        | lastName   | jobTitle           | account          | role    | credentials  | npi       | ipAddress       | status   | phone      |
#      | AutomationUpdate | UserUpdate | QA Engineer Update | Prospect Medical | Support | updatedcreds | 123412345 | 100.100.100.100 | Disabled | +918983979090 |

  @Regression @Smoke
  Scenario: verify_pharmacist_user_default_permissions
    Given User is on Home Page
    And User clicks on reset button
    And User enters "email" as "automationpharmacist@benchmarkit.solutions"
    When User clicks on search button
    And select first user from search result and open edit permission modal
    And verify permissions
      | PermissionName                       | Status     |
      | ARCHIVE REPORTS                      | ON         |
      | ARCHIVE SENT RECOMMENDATIONS         | ON         |
      | CONFIRM REPORT SENT                  | ON         |
      | DELETE REPORTS                       | ON         |
      | DOWNLOAD REPORTS                     | ON         |
      | EDIT SENT RECOMMENDATION DRP         | ON         |
      | EDIT SENT RECOMMENDATION RESPONSE    | ON         |
      | GENERATE REPORTS                     | ON         |
      | PUBLISH REPORTS                      | ON         |
      | SEND FAX                             | ON         |
      | UPLOAD FILE                          | ON         |
      | VIEW PHARMACIST TASKS                | ON         |
      | VIEW SUPPORT TASKS                   | ON         |
      | PATIENT ADDRESS                      | READ/WRITE |
      | PATIENT ASSIGN RECOMMENDATIONS       | READ/WRITE |
      | PATIENT BIRTHDATE                    | READ       |
      | PATIENT CONTACT                      | READ/WRITE |
      | PATIENT GENDER                       | READ       |
      | PATIENT NAME                         | READ       |
      | PATIENT OTHERS                       | READ/WRITE |
      | PATIENT SET CONVERSATION REVIEW DATE | READ/WRITE |
      | PHARMACIST TASKS                     | READ/WRITE |
      | PHARMACY NAME                        | READ       |
      | PRACTITIONER FIRST NAME              | READ       |
      | PRACTITIONER LAST NAME               | READ       |
    Then User clicks on "Cancel" button

  @Regression @Smoke
  Scenario: verify_update_user_permission_for_disabled_user
    Given User is on Home Page
    And User clicks on reset button
    And User enters "email" as "disabledpharmacist@benchmarkit.solutions"
    When User clicks on search button
    And select first user from search result and open edit user modal
    When Edit user "status" with data "Disabled"
    And User clicks on "Update" button and wait for message "User Updated Successfully"
    And User clicks on reset button
    Given User enters "email" as "disabledpharmacist@benchmarkit.solutions"
    When User clicks on search button
    Then Verify searched data in column "status" contains "Disabled"
    And select first user from search result and open edit permission modal
    Then update selected user from search results with below permissions
      | PermissionName                 | Status |
      | CONFIRM REPORT SENT            | OFF    |
      | PATIENT ASSIGN RECOMMENDATIONS | READ   |
    And select first user from search result and open edit permission modal
    And verify permissions
      | PermissionName                 | Status |
      | CONFIRM REPORT SENT            | OFF    |
      | PATIENT ASSIGN RECOMMENDATIONS | READ   |
    Then User clicks on "Cancel" button
    And select first user from search result and open edit permission modal
    Then update selected user from search results with below permissions
      | PermissionName                 | Status     |
      | CONFIRM REPORT SENT            | ON         |
      | PATIENT ASSIGN RECOMMENDATIONS | READ/WRITE |
    And select first user from search result and open edit permission modal
    And verify permissions
      | PermissionName                 | Status     |
      | CONFIRM REPORT SENT            | ON         |
      | PATIENT ASSIGN RECOMMENDATIONS | READ/WRITE |
    Then User clicks on "Cancel" button

  @Regression @Smoke
  Scenario: verify_bulk_update_uncommon_permissions
    Given User is on Home Page
    And User clicks on reset button
    And User enters "email" as "bulkupdate*"
    When User clicks on search button
    And select all user from search result and open edit permission modal
    And verify permissions
      | PermissionName                       | Status          |
      | PUBLISH REPORTS                      | TOGGLE-DISABLED |
      | UPLOAD FILE                          | TOGGLE-DISABLED |
      | PATIENT ASSIGN RECOMMENDATIONS       | WRITE-DISABLED  |
      | PATIENT SET CONVERSATION REVIEW DATE | WRITE-DISABLED  |
    Then User clicks on "Cancel" button

  @Regression @Smoke
  Scenario: verify_disabled_permission_enabled_pharmacist_user_individual
    Given User is on Home Page
    And User clicks on reset button
    And User enters "email" as "bulkupdate-pharmacist@benchmarkit.solutions"
    When User clicks on search button
    And select all user from search result and open edit permission modal
    And verify permissions
      | PermissionName                       | Status     |
      | PUBLISH REPORTS                      | ON         |
      | UPLOAD FILE                          | ON         |
      | PATIENT ASSIGN RECOMMENDATIONS       | READ/WRITE |
      | PATIENT SET CONVERSATION REVIEW DATE | READ/WRITE |
    Then User clicks on "Cancel" button

  @Regression @Smoke
  Scenario: verify_disabled_permission_for_support_user
    Given User is on Home Page
    And User clicks on reset button
    And User enters "email" as "bulkupdate-support@benchmarkit.solutions"
    When User clicks on search button
    And select all user from search result and open edit permission modal
    And verify permissions
      | PermissionName                       | Status          |
      | PUBLISH REPORTS                      | TOGGLE-DISABLED |
      | UPLOAD FILE                          | TOGGLE-DISABLED |
      | PATIENT ASSIGN RECOMMENDATIONS       | WRITE-DISABLED  |
      | PATIENT SET CONVERSATION REVIEW DATE | WRITE-DISABLED  |
    Then User clicks on "Cancel" button

  @Regression @Smoke
  Scenario: verify_bulk_update_common_user_permission
    Given User is on Home Page
    And User clicks on reset button
    And User enters "email" as "role*"
    When User clicks on search button
    And select all user from search result and open edit permission modal
    Then update selected user from search results with below permissions
      | PermissionName      | Status |
      | CONFIRM REPORT SENT | OFF    |
    #Verifying at pharmacist user is permissions were updated after bulk update
    And User clicks on reset button
    And User enters "email" as "rolepharmacist@test.com"
    When User clicks on search button
    And select first user from search result and open edit permission modal
    And verify permissions
      | PermissionName      | Status |
      | CONFIRM REPORT SENT | OFF    |
    Then User clicks on "Cancel" button
    #Verifying at support user is permissions were updated after bulk update
    And User clicks on reset button
    And User enters "email" as "rolesupport@test.com"
    When User clicks on search button
    And select first user from search result and open edit permission modal
    And verify permissions
      | PermissionName      | Status |
      | CONFIRM REPORT SENT | OFF    |
    Then User clicks on "Cancel" button
    #Reverting changes done to both pharmacist & support user
    And User clicks on reset button
    And User enters "email" as "role*"
    When User clicks on search button
    And select all user from search result and open edit permission modal
    Then update selected user from search results with below permissions
      | PermissionName      | Status |
      | CONFIRM REPORT SENT | ON     |

  @Regression @Smoke
  Scenario: verify_update_permission_disabled_without_user_selection
    Given User is on Home Page
    And User clicks on reset button
    And User enters "email" as "bulkupdate*"
    When User clicks on search button
    Then Update permission icon is disabled

  @Regression @Smoke
  Scenario: verify_permission_not_updated_when_cancel_button_clicked
    Given User is on Home Page
    And User clicks on reset button
    And User enters "email" as "automationpharmacist@benchmarkit.solutions"
    When User clicks on search button
    And select first user from search result and open edit permission modal
    Then Change permissions as provided but instead of saving click on cancel button
      | PermissionName  | Status |
      | ARCHIVE REPORTS | OFF    |
    And select first user from search result and open edit permission modal
    And verify permissions
      | PermissionName  | Status |
      | ARCHIVE REPORTS | ON     |
    Then User clicks on "Cancel" button

  @Regression @Smoke
  Scenario: Remove_all_permissions_for_pharmacist_user
    Given User is on Home Page
    And User clicks on reset button
    And User enters "email" as "bulkupdate-pharmacist@benchmarkit.solutions"
    When User clicks on search button
    And select first user from search result and open edit permission modal
    Then update selected user from search results with below permissions
      | PermissionName                       | Status |
      | ARCHIVE REPORTS                      | OFF    |
      | ARCHIVE SENT RECOMMENDATIONS         | OFF    |
      | CONFIRM REPORT SENT                  | OFF    |
      | DELETE REPORTS                       | OFF    |
      | DOWNLOAD REPORTS                     | OFF    |
      | EDIT SENT RECOMMENDATION DRP         | OFF    |
      | EDIT SENT RECOMMENDATION RESPONSE    | OFF    |
      | GENERATE REPORTS                     | OFF    |
      | PUBLISH REPORTS                      | OFF    |
      | SEND FAX                             | OFF    |
      | UPLOAD FILE                          | OFF    |
      | VIEW PHARMACIST TASKS                | OFF    |
      | VIEW SUPPORT TASKS                   | OFF    |
      | PATIENT ADDRESS                      |        |
      | PATIENT ASSIGN RECOMMENDATIONS       |        |
      | PATIENT BIRTHDATE                    |        |
      | PATIENT CONTACT                      |        |
      | PATIENT GENDER                       |        |
      | PATIENT NAME                         |        |
      | PATIENT OTHERS                       |        |
      | PATIENT SET CONVERSATION REVIEW DATE |        |
      | PHARMACIST TASKS                     |        |
      | PHARMACY NAME                        |        |
      | PRACTITIONER FIRST NAME              |        |
      | PRACTITIONER LAST NAME               |        |
    And select first user from search result and open edit permission modal
    And verify permissions
      | PermissionName                       | Status |
      | ARCHIVE REPORTS                      | OFF    |
      | ARCHIVE SENT RECOMMENDATIONS         | OFF    |
      | CONFIRM REPORT SENT                  | OFF    |
      | DELETE REPORTS                       | OFF    |
      | DOWNLOAD REPORTS                     | OFF    |
      | EDIT SENT RECOMMENDATION DRP         | OFF    |
      | EDIT SENT RECOMMENDATION RESPONSE    | OFF    |
      | GENERATE REPORTS                     | OFF    |
      | PUBLISH REPORTS                      | OFF    |
      | SEND FAX                             | OFF    |
      | UPLOAD FILE                          | OFF    |
      | VIEW PHARMACIST TASKS                | OFF    |
      | VIEW SUPPORT TASKS                   | OFF    |
      | PATIENT ADDRESS                      |        |
      | PATIENT ASSIGN RECOMMENDATIONS       |        |
      | PATIENT BIRTHDATE                    |        |
      | PATIENT CONTACT                      |        |
      | PATIENT GENDER                       |        |
      | PATIENT NAME                         |        |
      | PATIENT OTHERS                       |        |
      | PATIENT SET CONVERSATION REVIEW DATE |        |
      | PHARMACIST TASKS                     |        |
      | PHARMACY NAME                        |        |
      | PRACTITIONER FIRST NAME              |        |
      | PRACTITIONER LAST NAME               |        |
    Then User clicks on "Cancel" button
    And select first user from search result and open edit permission modal
    Then update selected user from search results with below permissions
      | PermissionName                       | Status     |
      | ARCHIVE REPORTS                      | ON         |
      | ARCHIVE SENT RECOMMENDATIONS         | ON         |
      | CONFIRM REPORT SENT                  | ON         |
      | DELETE REPORTS                       | ON         |
      | DOWNLOAD REPORTS                     | ON         |
      | EDIT SENT RECOMMENDATION DRP         | ON         |
      | EDIT SENT RECOMMENDATION RESPONSE    | ON         |
      | GENERATE REPORTS                     | ON         |
      | PUBLISH REPORTS                      | ON         |
      | SEND FAX                             | ON         |
      | UPLOAD FILE                          | ON         |
      | VIEW PHARMACIST TASKS                | ON         |
      | VIEW SUPPORT TASKS                   | ON         |
      | PATIENT ADDRESS                      | READ/WRITE |
      | PATIENT ASSIGN RECOMMENDATIONS       | READ/WRITE |
      | PATIENT BIRTHDATE                    | READ       |
      | PATIENT CONTACT                      | READ/WRITE |
      | PATIENT GENDER                       | READ       |
      | PATIENT NAME                         | READ       |
      | PATIENT OTHERS                       | READ/WRITE |
      | PATIENT SET CONVERSATION REVIEW DATE | READ/WRITE |
      | PHARMACIST TASKS                     | READ/WRITE |
      | PHARMACY NAME                        | READ       |
      | PRACTITIONER FIRST NAME              | READ       |
      | PRACTITIONER LAST NAME               | READ       |
    And select first user from search result and open edit permission modal
    And verify permissions
      | PermissionName                       | Status     |
      | ARCHIVE REPORTS                      | ON         |
      | ARCHIVE SENT RECOMMENDATIONS         | ON         |
      | CONFIRM REPORT SENT                  | ON         |
      | DELETE REPORTS                       | ON         |
      | DOWNLOAD REPORTS                     | ON         |
      | EDIT SENT RECOMMENDATION DRP         | ON         |
      | EDIT SENT RECOMMENDATION RESPONSE    | ON         |
      | GENERATE REPORTS                     | ON         |
      | PUBLISH REPORTS                      | ON         |
      | SEND FAX                             | ON         |
      | UPLOAD FILE                          | ON         |
      | VIEW PHARMACIST TASKS                | ON         |
      | VIEW SUPPORT TASKS                   | ON         |
      | PATIENT ADDRESS                      | READ/WRITE |
      | PATIENT ASSIGN RECOMMENDATIONS       | READ/WRITE |
      | PATIENT BIRTHDATE                    | READ       |
      | PATIENT CONTACT                      | READ/WRITE |
      | PATIENT GENDER                       | READ       |
      | PATIENT NAME                         | READ       |
      | PATIENT OTHERS                       | READ/WRITE |
      | PATIENT SET CONVERSATION REVIEW DATE | READ/WRITE |
      | PHARMACIST TASKS                     | READ/WRITE |
      | PHARMACY NAME                        | READ       |
      | PRACTITIONER FIRST NAME              | READ       |
      | PRACTITIONER LAST NAME               | READ       |
    Then User clicks on "Cancel" button

  @Setup @Regression @Smoke
  Scenario: SETUP: Logout and Close Browser
    When Admin User logout from the application
    Then User close browser