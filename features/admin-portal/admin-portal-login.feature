@AdminPortal @Login
Feature: Verify Login Feature

  @Setup @Regression @Smoke
  Scenario: SETUP: Launch Browser and go to application
    Given User launched "chrome"
    Then User go to application "https://admin-dev.arine.io/"

  @Regression @Smoke
  Scenario Outline: verify_login_with_invalid_username
    When Admin User login with "<username>" and "<password>"
    Then Verify alert message "<message>"
    Examples:
      | username                       | password  | message        |
      | abhilash@benchmarkit.solutions | Arine@123 | User not found |

  @Regression @Smoke
  Scenario Outline: verify_login_with_invalid_password
    When Admin User login with "<username>" and "<password>"
    Then Verify alert message "<message>"
    Examples:
      | username                              | password | message                 |
      | abhilashgadekar@benchmarkit.solutions | Arine@12 | Wrong username/password |

  @Regression @Smoke
  Scenario Outline: verify_login_with_valid_credentials
    When Admin User login with "<username>" and "<password>"
    Then User is on Home Page
    Examples:
      | username                              | password  | message |
      | abhilashgadekar@benchmarkit.solutions | Arine@123 | success |

  @Setup @Regression @Smoke
  Scenario: SETUP: Logout and Close Browser
    When Admin User logout from the application
    Then User close browser