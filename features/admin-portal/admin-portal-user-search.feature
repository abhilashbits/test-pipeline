@AdminPortal @SearchUser
Feature: Verify user search functionality

  @Setup @Regression @Smoke
  Scenario Outline: SETUP: Launch Browser and go to application
    Given User launched "chrome"
    Then User go to application "https://admin-dev.arine.io/"
    When Admin User login with "<username>" and "<password>"
    Examples:
      | username                              | password  | message | type              |
      | abhilashgadekar@benchmarkit.solutions | Arine@123 | success | Valid Credentials |

  @Regression @Smoke
  Scenario: verify_user_search_page
    Given User is on Home Page
    Then Verify text on Home Page is "Please select an account to begin the search"

  @Regression @Smoke
  Scenario Outline: verify_search_button
    Given User is on Home Page
    And Verify "<button>" button is disabled.
    Examples:
      | button |
      | Search |

  @Regression @Smoke
  Scenario: verify_reset_button_enabled_when_all_field_blank
    Given User enters "firstName" as "Automation*"
    When User clicks on search button
    And  Verify Search results displayed
    And User enters "firstName" as ""
    And Verify searchbox "firstName" is cleared
    Then Verify "Reset" button is enabled.

  @Regression @Smoke
  Scenario: verify_search_user_with_valid_data
    Given User enters "firstName" as "Automation"
    Given User enters "email" as "automation-*"
    Given User enters "account" as "SoonerCare Test"
    When User clicks on search button
    Then Verify searched data in column "firstName" contains "Automation"
    Then Verify searched data in column "email" contains "automation-"
    Then Verify searched data in column "account" contains "SoonerCare Test"

  @Regression @Smoke
  Scenario: verify_search_data_reset_feature_on_refresh
    Given User is on Home Page
    When User refreshes page
    Then Verify searched data in column "firstName" contains "Automation"
    Then Verify searched data in column "email" contains "automation-"
    Then Verify searched data in column "account" contains "SoonerCare Test"

  @Regression @Smoke
  Scenario: verify_search_user_with_invalid_data
    Given User enters "firstName" as "ABC"
    Given User enters "email" as "ABC@GMAIL.COM"
    Given User enters "account" as "SoonerCare Test"
    When User clicks on search button
    Then Verify text on Home Page is "There are no users that meet the current search criteria."

  @Regression @Smoke
  Scenario: verify_search_with_first_name_filter
    Given User refreshes page
    And User enters "firstName" as "Automation"
    When User clicks on search button
    Then Verify searched data in column "firstName" contains "Automation"

  @Regression @Smoke
  Scenario: verify_search_user_with_invalid_email_filter
    Given User enters "email" as "ABC@gmail.com"
    When User clicks on search button
    Then Verify text on Home Page is "There are no users that meet the current search criteria."

  @Regression @Smoke
  Scenario: verify_search_user_with_multiple_filter
    Given User enters "firstName" as "Automation"
    Given User enters "email" as "automation-*"
    When User clicks on search button
    Then Verify searched data in column "firstName" contains "Automation"
    Then Verify searched data in column "email" contains "automation-"
    When User refreshes page
    Given User enters "email" as "automation-*"
    Given User enters "account" as "SoonerCare Test"
    When User clicks on search button
    Then Verify searched data in column "email" contains "automation-"
    Then Verify searched data in column "account" contains "SoonerCare Test"
    When User refreshes page
    Given User enters "firstName" as "Automation"
    Given User enters "account" as "SoonerCare Test"
    When User clicks on search button
    Then Verify searched data in column "firstName" contains "Automation"
    Then Verify searched data in column "account" contains "SoonerCare Test"

  @Regression @Smoke
  Scenario: verify_search_user_with_invalid_multiple_filter
    Given User enters "firstName" as "Automation1"
    Given User enters "email" as "ABC@gmail.com"
    When User clicks on search button
    Then Verify text on Home Page is "There are no users that meet the current search criteria."

  @Regression @Smoke
  Scenario: verify_search_user_with_valid_pattern
    Given User enters "firstName" as "Automa*"
    Given User enters "email" as "automation-*"
    When User clicks on search button
    Then Verify searched data in column "firstName" contains "Automa"
    Then Verify searched data in column "email" contains "automation-"

  @Regression @Smoke
  Scenario: verify_search_user_with_invalid_pattern
    Given User enters "firstName" as "ABC*"
    Given User enters "email" as "ABC@gmail.com"
    When User clicks on search button
    Then Verify text on Home Page is "There are no users that meet the current search criteria."

  @Regression @Smoke
  Scenario: verify_search_user_with_valid_name_pattern
    When User refreshes page
    Given User enters "firstName" as "Automa*"
    When User clicks on search button
    Then Verify searched data in column "firstName" contains "Automa"

  @Regression @Smoke
  Scenario: verify_search_user_with_valid_email_pattern
    Given User enters "email" as "automation-*"
    When User clicks on search button
    Then Verify searched data in column "email" contains "automation-"

  @Regression @Smoke
  Scenario: verify_search_user_with_invalid_multiple_input_pattern
    Given User enters "firstName" as "Automa*"
    Given User enters "email" as "Automation@gmail*"
    When User clicks on search button
    Then Verify text on Home Page is "There are no users that meet the current search criteria."

  @Regression @Smoke
  Scenario: verify_reset_button
    Given User enters "firstName" as "Automation"
    And User enters "email" as "automation-*"
    And User enters "account" as "SoonerCare Test"
    When User clicks on "Reset" button
    Then Verify text on Home Page is "Please select an account to begin the search"
    And Verify searchbox "firstName" is cleared
    And Verify searchbox "email" is cleared
    And Verify searchbox "email" is cleared

  @Regression @Smoke
  Scenario: verify_pagination
    Given User is on Home Page
    And User clicks on reset button
    Given User enters "email" as "gmail*"
    When User clicks on search button
    Then Verify Search results displayed
    And Verify Pagination

  @Regression @Smoke
  Scenario: verify_sorting
    Given User is on Home Page
    And User clicks on reset button
    And User enters "email" as "gmail*"
    When User clicks on search button
    Then Verify Search results displayed
    And verify "Ascending" for column "First Name"
    And verify "Descending" for column "First Name"
    And verify "Ascending" for column "Email"
    And verify "Descending" for column "Email"
    And verify "Ascending" for column "Role"
    And verify "Descending" for column "Role"
    And verify "Ascending" for column "created date"
    And verify "Descending" for column "created date"
    And verify "Ascending" for column "Last Login"
    And verify "Descending" for column "Last Login"
    And verify "Ascending" for column "Status"
    And verify "Descending" for column "Status"

  @Regression @Smoke
  Scenario: verify_casesensitive_search
    Given User enters "firstName" as "AUTOMATION"
    Given User enters "email" as "AUTOMATION-*"
    Given User enters "account" as "SoonerCare Test"
    When User clicks on search button
    Then Verify searched data in column "firstName" contains "Automation"
    Then Verify searched data in column "email" contains "automation-"
    Then Verify searched data in column "account" contains "SoonerCare Test"

  @Setup @Regression @Smoke
  Scenario: SETUP: Logout and Close Browser
    When Admin User logout from the application
    Then User close browser