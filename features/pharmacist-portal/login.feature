@PharmacistPortal @Login
Feature: Verify Login Feature

  @Setup @Regression @Smoke
  Scenario: SETUP: Launch Browser and go to application
    Given User launched "chrome"
    Then User go to application "https://pharmacist-dev.arine.io/"

  @Regression @Smoke
  Scenario Outline: verify_login_with_invalid_email_format
    When User enter email: "<username>" and password: "<password>"
    Then Verify validation message: "<message>"
    Examples:
      | username                 | password | message                                     |
      | invalid_email_format.com | 1234     | Please include an '@' in the email address. |

  @Regression @Smoke
  Scenario: verify_login_forgot_password_feature
    When User click on forgot password link
    Then Verify text on screen "Password Assistance"
    And Verify button: "Send Email"
    And Click on button: "Go Back to Login"

  @Regression @Smoke
  Scenario Outline: verify_login_with_invalid_credentials
    When User login with "<username>" and "<password>"
    Then Verify Login message: "<message>"
    Examples:
      | username                    | password | message        |
      | incorrect_id@mailinator.com | 1234     | User not found |

  @Regression @Smoke
  Scenario Outline: verify_login_with_valid_credentials
    When User login with "<username>" and "<password>"
    Then Verify Login message: "<message>"
    Examples:
      | username                            | password    | message |
      | pharmacist-automation11@mailinator.com | ArinePass3! | success |

  @Setup @Regression @Smoke
  Scenario: SETUP: Logout and Close Browser
    When User logout from the application
    Then User close browser