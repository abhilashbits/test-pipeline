@PharmacistPortal @PatientSearch
Feature: Verify Patient Search Feature

  @Setup @Regression @Smoke
  Scenario Outline: SETUP: Launch Browser and go to application
    Given User launched "chrome"
    Then User go to application "https://pharmacist-dev.arine.io/"
    When User login with "<username>" and "<password>"
    And Verify Login message: "<message>"
    Examples:
      | username                            | password    | message |
      | offshore-pharmacist1@mailinator.com | ArinePass3! | success |

  @Regression @Smoke
  Scenario Outline: verify_patient_tab_views
    When Click on Patient Tab
    And Search patient: "<searchString>"
    Then Verify patient's "Name": "<Name>"
    And Verify comments section
    And Verify patient medication section
    And Verify patient DRP section
    And Verify patient Medical Diagnosis section
    Examples:
      | searchString                         | Name        |
      | a67d600a-a676-483e-a17c-14440012c21f | Lucy Miller |

  @Regression @Smoke
  Scenario Outline: verify_patient_search
    When Click on Patient Tab
    And Search patient: "<searchString>"
    Then Verify Patient Name: "<patientName>"
    Examples:
      | searchString                         | patientName |
      | a67d600a-a676-483e-a17c-14440012c21f | Lucy Miller |

  @Regression @Smoke
  Scenario Outline: verify_load_patient_by_hpid
    When Click on Patient Tab
    And Search patient: "<HPID>"
    Then Verify Patient Name: "<patientName>"
    Examples:
      | HPID          | patientName |
      | XD13103620896 | Lucy Miller |

  @Regression @Smoke
  Scenario Outline: verify_load_patient_by_name
    When Click on Patient Tab
    And Search patient: "<Name>"
    Then Verify Patient Name: "<patientName>"
    Examples:
      | Name        | patientName |
      | lucy miller | Lucy Miller |

  @Regression @Smoke
  Scenario Outline: verify_patient_no_permission
    When Click on Patient Tab
    And Search patient: "<searchString>"
    Then Verify "<Message>" error popup
    Examples:
      | searchString                         | Message           |
      | 8ccd83ae-efcb-42d3-bb70-ea6614b863dd | Permission denied |

  @Regression @Smoke
  Scenario Outline: verify_patient_not_found
    When Click on Patient Tab
    And Search patient: "<searchString>"
    Then Verify "<Message>" error popup
    Examples:
      | searchString                         | Message   |
      | 68f17ddb-e3dd-49b2-ba5b-3c89003affc0 | not found |

  @Regression @Smoke
  Scenario Outline: verify_advance_search_filter_feature
    When Click on Patient Tab
    And User click on ADVANCE search
    Then Verify patient search popup "Advanced Patient Search"
    And Search patient by last name: "<searchString>"
    Then Verify patient record by id: "<PatientId>"
    Examples:
      | searchString | PatientId                            |
      | miller       | a67d600a-a676-483e-a17c-14440012c21f |

  @Regression @Smoke
  Scenario Outline: verify_patient_info_table
    When Click on Patient Tab
    Then Search patient: "<searchString>"
    And Verify patient's "Name": "<Name>"
    And Verify patient's "DOB": "<DOB>"
    And Verify patient's "Sex": "<Sex>"
    And Verify patient's "Age": "<Age>"
    And Verify patient's "30d Hospital": "<30d Hospital>"
    Examples:
      | searchString                         | Name        | DOB       | Sex    | Age | 30d Hospital | Medication Allergies |
      | a67d600a-a676-483e-a17c-14440012c21f | Lucy Miller | 1/23/1941 | female | 81  | Yes          | NKDA                 |

  @Setup @Regression @Smoke
  Scenario: SETUP: Logout and Close Browser
    When User logout from the application
    Then User close browser
