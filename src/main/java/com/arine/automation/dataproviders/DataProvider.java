package com.arine.automation.dataproviders;

abstract class DataProvider {

    public abstract void loadData(String filePath);
}
