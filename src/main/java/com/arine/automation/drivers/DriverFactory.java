package com.arine.automation.drivers;

import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.logging.Logger;

public class DriverFactory {
    private static final Logger LOGGER = Logger.getLogger(DriverFactory.class.getName());
    public String downloadPath = ((System.getProperty(OS)==null || System.getProperty(OS)==WINDOWS)?System.getProperty("user.dir"):System.getProperty("user.dir").replace("\\", "/"))+"/downloads";
    public static final String DEFAULT_BROWSER = "chrome";
    public static final String WINDOWS = "WINDOWS";
    public static final String linux = "linux";
    public static final String OS = "OS";
    public static ThreadLocal<WebDriver> drivers = new ThreadLocal<>();

    private DriverFactory() {
        if (System.getProperty(OS)== null)
            System.setProperty(OS,WINDOWS);
    }

    private static DriverFactory instance = new DriverFactory();

    public static DriverFactory getInstance() {
        return instance;
    }

    public WebDriver initDriver(String browser) {
//        try {
//            FileUtils.cleanDirectory(new File(downloadPath));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        WebDriver driver = drivers.get();
        if(driver==null) {
            if (browser == null || browser.isEmpty())
                browser = DEFAULT_BROWSER;
            File file = executableDriver(System.getProperty(OS), browser);
            switch (browser) {
                case "firefox":
                    System.setProperty("webdriver.gecko.driver", file.getAbsolutePath());
                    driver = new FirefoxDriver(firefoxOptions());
                case "ie":
                    System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
                    driver = new InternetExplorerDriver(ieOptions());
                default:
                    System.setProperty("webdriver.chrome.driver", "/usr/bin/google-chrome");
                    try{
                        driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"),chromeOptions());
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    System.out.println("Chrome browser launched");
            }
        }
        drivers.set(driver);
        return driver;
    }

    public static InternetExplorerOptions ieOptions() {
        InternetExplorerOptions options = new InternetExplorerOptions().requireWindowFocus().enableNativeEvents();
        options.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
        options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        options.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
        options.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.DISMISS);
        options.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true);
        options.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, false);
        options.setCapability(CapabilityType.SUPPORTS_JAVASCRIPT, true);
        options.setCapability("allowBlockedContent", true);
        options.setCapability("ignoreProtectedModeSettingd", true);
        options.setCapability("initialBrowserUrl", "http://google.co.in");
        options.setCapability("nativeEvents", false);
        File file = new File("./config/driver/iexplore.exe");
        String binaryPath = file.getAbsolutePath();
        options.setCapability("IE.binary", binaryPath);
        return options;
    }

    public FirefoxOptions firefoxOptions() {
        FirefoxProfile firefoxProfile = new FirefoxProfile();
        firefoxProfile.setPreference("browser.download.dir", downloadPath);
        firefoxProfile.setPreference("browser.download.folderList", 2);
        firefoxProfile.setPreference("browser.download.manager.showWhenStarting", false);
        firefoxProfile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/csv,application/x-msexcel,application/excel,application/x-excel,application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain,application/msword,application/xml");
        FirefoxOptions option = new FirefoxOptions();
        option.setProfile(firefoxProfile);
        return option;
    }

    public ChromeOptions chromeOptions() {
        HashMap<String, Object> chromePrefs = new HashMap<>();
        chromePrefs.put("profile.default_content_settings.popups", 0);
        chromePrefs.put( "profile.default_content_setting_values.automatic_downloads", 1 );
        chromePrefs.put("download.default_directory", downloadPath);
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", chromePrefs);
        if(System.getProperty("debug")==null || System.getProperty("debug").equalsIgnoreCase("false")) {
            if(linux.equalsIgnoreCase(System.getProperty(OS))) {
                System.out.println("Setting linux chrome version");
                options.setBinary("/usr/bin/google-chrome");
            }
            options.addArguments("--no-sandbox");
            options.addArguments("--headless");
            options.addArguments("--disable-gup");
            options.addArguments("--disable-dev-shm-usage");
            options.addArguments("--window-size=1400,600");
        }
        System.out.println("Returning chrome options");
        return options;
    }

    public File executableDriver(String os, String browser) {
        File file;
        switch (browser) {
            case "firefox":
                file = new File("src/test/resources/web-drivers/geckodriver.exe");
            case "ie":
                file = new File("src/test/resources/web-drivers/IEDriverServer.exe");
            default:
                if (os.equalsIgnoreCase(WINDOWS))
                    file = new File("src/test/resources/web-drivers/chromedriver.exe");
                else if (os.equalsIgnoreCase(linux))
                    file = new File("src/test/resources/web-drivers/linux/chromedriver");
                else
                    file = new File("src/test/resources/web-drivers/iOS/chromedriver");
        }
        return file;

    }

    public static void clearDriverSession() {
        drivers.set(null);
    }


}