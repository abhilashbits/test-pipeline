package com.arine.automation.exception;

interface ThrowException {
    void throwing(String message) throws AutomationException;
}
