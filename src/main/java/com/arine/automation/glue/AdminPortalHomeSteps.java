package com.arine.automation.glue;

import com.arine.automation.exception.AutomationException;
import com.arine.automation.pages.PageFactory;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class AdminPortalHomeSteps {
    CommonSteps common = new CommonSteps();

    @Given("^User enters \"([^\"]*)\" as \"([^\"]*)\"$")
    public void searchUserWith(String searchWith,String searchString) throws AutomationException {
        common.logInfo("Searching user "+searchWith+" with '"+searchString+"'");
        PageFactory.adminPortalHomePage().searchUser(searchWith,searchString);
    }

    @Then("^Verify searched data in column \"([^\"]*)\" contains \"([^\"]*)\"$")
    public void verifySearchData(String columnName,String searchString) throws AutomationException {
        common.logInfo("Verifying search data for  "+columnName+" with "+searchString);
        PageFactory.adminPortalHomePage().verifyColumnData(columnName,searchString);
    }

    @Given("^User enters name: \"([^\"]*)\"$")
    public void searchUserWithName(String name) throws AutomationException {
        common.logInfo("Search user with name: '"+name+"'");
        PageFactory.adminPortalHomePage().searchUserWithName(name);
    }

    @Given("^User enters email: \"([^\"]*)\"$")
    public void searchUserWithEmail(String email) throws AutomationException {
        common.logInfo("Search user with name: '"+email+"'");
        PageFactory.adminPortalHomePage().searchUserWithEmail(email);
    }

    @Given("^User selects account: \"([^\"]*)\"$")
    public void searchUserWithAccount(String account) throws AutomationException {
        common.logInfo("Select Account : '"+account+"'");
        PageFactory.adminPortalHomePage().selectAccount(account);
    }

    @And("^User clicks on search button$")
    public void userClicksOnSearchButton() throws AutomationException {
        common.logInfo("User clicks on search button");
        PageFactory.adminPortalHomePage().clickOnSearchButton();
        CommonSteps.takeScreenshot();
    }

    @And("^User clicks on reset button$")
    public void userClicksOnResetButton() throws AutomationException {
        common.logInfo("User clicks on reset button");
        PageFactory.adminPortalHomePage().clickOnResetButton();
        CommonSteps.takeScreenshot();
    }

    @And("^Verify First Name column contains searched name: \"([^\"]*)\"$")
    public void verifyFirstName(String name) throws AutomationException {
        common.logInfo("Verify first name column has searched name: '"+name+"'");
        PageFactory.adminPortalHomePage().searchUserWithName(name);
    }

    @Given("^User is on Home Page$")
    public void userIsOnHomePage() throws AutomationException {
        common.logInfo("Verify user is on home page");
        PageFactory.adminPortalHomePage().verifyHomePage();
    }

    @Given("^Search created user with email address.$")
    @And("^Search edited user with email address.$")
    public void searchCreatedUserWithEmail() throws AutomationException {
        common.logInfo("Searching created user with email address");
        PageFactory.adminPortalHomePage().searchCreatedUserWithEmail();
    }

    @Given("^Navigate to Home Page if not already$")
    public void navigateToHome() throws AutomationException {
        common.logInfo("Navigating to Home Page");
        PageFactory.adminPortalHomePage().verifyHomePage();
    }

    @Given("^User refreshes page$")
    public void userRefreshPage() throws AutomationException {
        common.logInfo("User refreshed Page");
        PageFactory.adminPortalHomePage().pageRefresh();
    }

    @Then("^Edit user with data:$")
    public void editUser(DataTable data) throws AutomationException {
        common.logInfo("Edit user"+data.toString());
        PageFactory.adminPortalHomePage().editUser(data,true);
    }

    @Then("^Edit user with invalid data:$")
    public void editUserWithInvalidData(DataTable data) throws AutomationException {
        common.logInfo("Edit user with data : "+data.toString());
        PageFactory.adminPortalHomePage().editUser(data,false);
        CommonSteps.takeScreenshot();
    }


    @Then("^Create new user with data:$")
    public void createNewValidUser(DataTable data) throws AutomationException {
        common.logInfo("Create new user "+data.toString());
        boolean valid=true;
        PageFactory.adminPortalHomePage().createUser(data,valid);
    }

    @Then("^update selected user from search results with below permissions$")
    public void updateUser(DataTable data) throws AutomationException {
        common.logInfo("Update user permissions"+data.toString());
        PageFactory.adminPortalHomePage().updateUserPermission(data,true);
    }

    @Then("^Change permissions as provided but instead of saving click on cancel button$")
    public void changePermissionAndClickCancel(DataTable data) throws AutomationException {
        common.logInfo("Changing user permissions & then clicking on cancel button"+data.toString());
        PageFactory.adminPortalHomePage().updateUserPermission(data,false);
    }

    @Then("^Create new user with below data if user is not present already for given emailAddress:$")
    public void createUserIfNotAlreadyPresent(DataTable data) throws AutomationException {
        common.logInfo("Create new user"+data.toString());
        PageFactory.adminPortalHomePage().createUser(data,true);
    }

    @Then("^Create new user with invalid data:$")
    public void createNewInvalidUser(DataTable data) throws AutomationException {
        common.logInfo("Create new user"+data.toString());
        PageFactory.adminPortalHomePage().createUser(data,false);
        CommonSteps.takeScreenshot();
    }

    @Then("^verify success message : \"([^\"]*)\"$")
    public void verifySuccessMessage(String message) throws AutomationException {
        common.logInfo("Verifying message : "+message);
        PageFactory.adminPortalHomePage().verifyMessage(message);
        CommonSteps.takeScreenshot();
    }

    @Then("^User clicks on \"([^\"]*)\" button$")
    public void clickOnButton(String buttonToClick) throws AutomationException {
        common.logInfo("Clicking on button : "+buttonToClick);
        PageFactory.adminPortalHomePage().clickOnButton(buttonToClick);
    }

    @Then("^User clicks on \"([^\"]*)\" button and wait for message \"([^\"]*)\"$")
    public void clickOnButtonAndWaitForMessage(String buttonToClick, String message) throws AutomationException {
        common.logInfo("Clicking on button : '"+buttonToClick + "' and wait for message: '"+message+"'");
        PageFactory.adminPortalHomePage().clickOnButtonAndWaitForMessage(buttonToClick, message);
    }

    @And("^select first user from search result and open edit permission modal$")
    public void openEditPermissionModal() throws AutomationException {
        common.logInfo("select first user from search result and open edit permission modal");
        PageFactory.adminPortalHomePage().selectFirstUserOnHomePage();
        PageFactory.adminPortalHomePage().openEditPermissionModal();
    }

    @And("^select first user from search result and open edit user modal$")
    public void openEditUserModal() throws AutomationException {
        common.logInfo("select first user from search result and open edit user modal");
        PageFactory.adminPortalHomePage().openEditUserModal();
    }

    @Then("^verify permissions$")
    public void verifyPermissions(DataTable data) throws AutomationException {
        common.logInfo("Verifying user permissions : "+data.toString());
        PageFactory.adminPortalHomePage().verifyPermission(data);
        CommonSteps.takeScreenshot();
    }

    @When("^Verify text on Home Page is \"([^\"]*)\"$")
    public void verifyTextOnHomePageIs(String textToVerify) throws AutomationException {
        common.logInfo("Verifying text on home Page: '"+textToVerify+"'");
        PageFactory.adminPortalHomePage().verifyText(textToVerify);
        CommonSteps.takeScreenshot();
    }

    @When("^Verify alert message \"([^\"]*)\"$")
    public void verifyAlertMessage(String textToVerify) throws AutomationException {
        common.logInfo("Verifying alert message: '"+textToVerify+"'");
        PageFactory.adminPortalHomePage().verifyText(textToVerify);
    }

    @When("^Verify \"([^\"]*)\" button is disabled.$")
    public void verifyButtonIsDisabled(String buttonToVerify) throws AutomationException {
        common.logInfo("Verify "+buttonToVerify+" is disabled!");
        PageFactory.adminPortalHomePage().verifyButtonIsDisabled(buttonToVerify);
    }

    @When("^Verify \"([^\"]*)\" button is enabled.$")
    public void verifyButtonIsEnabled(String buttonToVerify) throws AutomationException {
        common.logInfo("Verify "+buttonToVerify+" is enabled!");
        PageFactory.adminPortalHomePage().verifyButtonIsEnabled(buttonToVerify);
    }

    @When("^User selects role \"([^\"]*)\"")
    public void selectRole(String roleToSelect) throws AutomationException {
        common.logInfo("User selects role: '"+roleToSelect+"'");
        PageFactory.adminPortalHomePage().selectRole(roleToSelect);
    }

    @Then("^Verify textbox \"([^\"]*)\" is disabled.$")
    public void verifyTextboxDisabled(String textboxLabel) throws AutomationException {
        common.logInfo("Verifying textbox is disabled : "+textboxLabel);
        PageFactory.adminPortalHomePage().verifyTextboxIsDisabled(textboxLabel);
    }

    @Then("^Verify textbox \"([^\"]*)\" is readonly and textbox value is \"([^\"]*)\"$")
    public void verifyTextboxReadOnly(String textboxLabel,String textboxValue) throws AutomationException {
        common.logInfo("Verifying "+textboxLabel+" is read only & textbox value is "+textboxValue);
        PageFactory.adminPortalHomePage().verifyTextboxIsReadOnly(textboxLabel,textboxValue);
    }

    @Then("^Verify textbox \"([^\"]*)\" is enabled.$")
    public void verifyTextboxEnabled(String textboxLabel) throws AutomationException {
        common.logInfo("Verifying textbox is enabled : "+textboxLabel);
        PageFactory.adminPortalHomePage().verifyTextboxIsEnabled(textboxLabel);
    }

    @Then("^Verify error message displayed is \"([^\"]*)\"$")
    public void verifyErrorMessage(String textToVerify) throws AutomationException {
        common.logInfo("Verify error message displayed is '"+textToVerify+"'");
        PageFactory.adminPortalHomePage().verifyText(textToVerify);
    }

    @Given("^verify \"([^\"]*)\" for column \"([^\"]*)\"$")
    public void verifySort(String sortType,String columnNameToSort) throws AutomationException {
        common.logInfo("Performing "+sortType+" sort to column "+columnNameToSort);
        PageFactory.adminPortalHomePage().sortColumn(sortType,columnNameToSort);
    }

    @And("^Verify Search results displayed$")
    public void verifySearchResultsOnHomePage() throws AutomationException {
        common.logInfo("Verifying user search results on home page");
        PageFactory.adminPortalHomePage().verifySearchResultsAreDisplayed();
    }

    @Then("^Edit user \"([^\"]*)\" with data \"([^\"]*)\"$")
    public void editUserField(String fieldNameToUpdate,String value) throws AutomationException {
        common.logInfo("Updating '"+fieldNameToUpdate+"' with value '"+value+"'");
        PageFactory.adminPortalHomePage().editUserField(fieldNameToUpdate,value);
    }

    @And("^Verify Pagination$")
    public void verifyPagination() throws AutomationException {
        common.logInfo("Verifying pagination");
        PageFactory.adminPortalHomePage().verifyPagination();
    }

    @And("^Verify searchbox \"([^\"]*)\" is cleared$")
    public void verifySearchboxIsCleared(String searchBoxName) throws AutomationException {
        common.logInfo("Verify searchbox '"+searchBoxName+"' is cleared");
        PageFactory.adminPortalHomePage().verifySearchBoxIsCleared(searchBoxName);
    }

    @And("^select all user from search result and open edit permission modal$")
    public void selectAllUserAndOpenEditPermissionModal() throws AutomationException {
        common.logInfo("select all user from search result and open edit permission modal");
        PageFactory.adminPortalHomePage().selectAllUserOnHomePage();
        PageFactory.adminPortalHomePage().openEditPermissionModal();
    }

    @And("^Update permission icon is disabled$")
    public void verifyUpdatePermissionIcon() throws AutomationException {
        common.logInfo("Verifying update permission icon is disabled");
        PageFactory.adminPortalHomePage().updatePermissionIconDisabled();
        CommonSteps.takeScreenshot();
    }
}
