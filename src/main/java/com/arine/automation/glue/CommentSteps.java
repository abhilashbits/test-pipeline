package com.arine.automation.glue;

import com.arine.automation.exception.AutomationException;
import com.arine.automation.pages.PageFactory;
import cucumber.api.java.en.*;

import static com.arine.automation.glue.CommonSteps.takeScreenshot;

public class CommentSteps {
    CommonSteps common = new CommonSteps();
    @When("^Edit Existing Users Comment$")
    public void editComments() throws AutomationException {
        common.logInfo("Edit Existing Users Comment");
        PageFactory.commentsPage().editComments();
    }

    @When("^Update Own new comment: \"([^\"]*)\"$")
    public void updateNewComments(String newComment) throws AutomationException {
        common.logInfo("Update Own new comment: '"+newComment+"'");
        PageFactory.commentsPage().updateNewComments(newComment);
    }

    @Then("^Verify popup Message \"([^\"]*)\"$")
    public void verifyPopup(String title) throws AutomationException {
        common.logInfo("Verify popup: '"+title+"'");
        PageFactory.commentsPage().verifyPopupMessage(title);
        takeScreenshot();
    }

    @Then("^Verify comment is Disabled: \"([^\"]*)\"$")
    public void verifyCommentDisabled(String text) throws AutomationException {
        common.logInfo("Verify comment is Disabled: '"+text+"'");
        PageFactory.commentsPage().verifyCommentDisabled(text);
        takeScreenshot();
    }

    @Then("^User Update comment \"([^\"]*)\" with: \"([^\"]*)\"$")
    public void verifyPharmaUserCommentUpdateByAnotherPharmaUser(String previousComment,String afterupdateComment) throws AutomationException {
        common.logInfo("Verify Pharmacist User comment"+previousComment+"Updated By another Pharmacist User"+afterupdateComment);
        PageFactory.commentsPage().verifyPharmaUserCommentUpdateByAnotherPharmaUser(previousComment,afterupdateComment);
        PageFactory.patientPage().clickOnNameLink();
        takeScreenshot();
    }

    @When("^Verify Comment is not present: \"([^\"]*)\"$")
    public void verifyCommentNotPresent(String text) throws AutomationException {
        common.logInfo("Verify comment Not Present: '"+text+"'");
        PageFactory.commentsPage().verifyCommentNotPresent(text, true);
        takeScreenshot();
    }

    @Then ("^Verify user able to Add new multiline comment: \"([^\"]*)\" and \"([^\"]*)\"$")
    public void verifyUserAbleToAddMultilineComment(String comment1,String comment2) throws AutomationException {
        common.logInfo("Verify User is able to add multiline comment:'"+comment1+"'and'"+comment2+"'");
        PageFactory.commentsPage().verifyUserAbleToAddMultilineComment(comment1, comment2);
        PageFactory.patientPage().clickOnNameLink();
        takeScreenshot();
    }


    @Then ("^User Try to add blank comment$")
    public void addBlankComment() throws AutomationException {
        common.logInfo("user try to add blank comment:");
        PageFactory.commentsPage().addBlankComment(true);
        takeScreenshot();
    }

    @Then ("^Verify user is not able to add blank comment$")
    public void verifyUserNotAbleToAddBlankComment() throws AutomationException {
        common.logInfo("Verify user is not able to add blank comment:");
        PageFactory.commentsPage().verifyUserNotAbleToAddBlankComment(true);
        takeScreenshot();
    }

    @Then ("^Verify comment \"([^\"]*)\" whith updated username \"([^\"]*)\"$")
    public void verifyUserNameWhenHeAddorUpdateComment(String userName, String comment) throws AutomationException {
        common.logInfo("Verify user name is displayed when he add or update comment:");
        PageFactory.commentsPage().verifyUserNameWhenAddUpdateComment(userName,comment);
        takeScreenshot();
    }
}
