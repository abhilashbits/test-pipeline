package com.arine.automation.glue;

import com.arine.automation.MainApplication;
import com.arine.automation.constants.Constants;
import com.arine.automation.drivers.DriverFactory;
import com.arine.automation.exception.AutomationException;
import com.arine.automation.models.TestScenario;
import com.arine.automation.pages.HomePage;
import com.arine.automation.pages.LoginPage;
import com.arine.automation.pages.PageFactory;
import com.arine.automation.reports.ExtentTestManager;
import com.arine.automation.util.*;
import com.aventstack.extentreports.ExtentTest;
import cucumber.api.PendingException;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.runtime.ScenarioImpl;
import gherkin.formatter.model.Result;
import org.apache.commons.lang.reflect.FieldUtils;
import org.apache.log4j.PropertyConfigurator;
import org.hamcrest.MatcherAssert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.SkipException;

import javax.print.attribute.standard.Media;
import java.io.File;
import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class CommonSteps {

    private static final Logger LOGGER = Logger.getLogger("Arine Test Automation Logger-");
    protected String logPropertyFileName = null;
    public static ReportLogger REPORT_LOGGER = new ReportLogger();
    private static Boolean SETUP_COMPLETED = false;
    public static WebDriverUtil driverUtil;
    public static final int WAIT_10_SECOND = 10;
    public static final int WAIT_30_SECOND = 30;
    public static final int WAIT_60_SECOND = 60;
    public static final int WAIT_120_SECOND = 120;
    public static final String METHOD_NAME = "NAME";
    public static ThreadLocal<Scenario> CURRENT_SCENARIO = new ThreadLocal<>();
    public static ThreadLocal<String> CURRENT_SCENARIO_MESSAGE = new ThreadLocal<>();
    public static ThreadLocal<String> CURRENT_STEP_MESSAGE = new ThreadLocal<>();
    public static ThreadLocal<Integer> currentStepIndex = new ThreadLocal<>();
    public static ThreadLocal<String> currentStep = new ThreadLocal<>();
    public static ThreadLocal<String> stepScreenshot = new ThreadLocal<>();
    public static ThreadLocal<List<String>> stepScreenshots = new ThreadLocal<>();
    public static ThreadLocal<Boolean> SETUP_FAILED = new ThreadLocal<>();

    public static void logError(String error) {
        takeScreenshot();
        REPORT_LOGGER.log(ExtentTestManager.featureFileName.get()+" => " + error);
        CURRENT_SCENARIO_MESSAGE.set(error);
    }

    public static void logError(String error, List<String> screenshot) {
        stepScreenshots.set(screenshot);
        REPORT_LOGGER.log(ExtentTestManager.featureFileName.get()+" => " + error);
        CURRENT_SCENARIO_MESSAGE.set(error);
    }

    public void logInfo(String... data) {
        REPORT_LOGGER.log(ExtentTestManager.featureFileName.get()+" => " + data[0]);
        beforeStep(data);
    }

    static {
        cleanUP();
        setup();
    }

    public static void cleanUP(){
        File logs = new File("./logs");
        if(logs.exists())
            PackageUtil.recursiveDelete(logs);
        File extentReports = new File("./extent-reports");
        if(extentReports.exists())
            PackageUtil.recursiveDelete(extentReports);
    }

    public static void setup() {
        try {
            MainApplication.init();
            String logPropertyFileName = Constants.PROPERTY_FILE_PATH + "config/log4j.properties";
            PropertyConfigurator.configure(logPropertyFileName);
            PropertyReader.loadProperties(PropertyReader.CONFIG_PROPERTIES_FILE);
            PropertyReader.loadProperties(PropertyReader.ENV_PROPERTIES_FILE);
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    @Then("^User go to application \"([^\"]*)\"$")
    public void launch(String url) {
        logInfo("User go to application "+url);
        launchApplication(url);
    }

    public void verifyHome() throws AutomationException {
        if(DriverFactory.drivers.get().getCurrentUrl().contains("eula")) {
            WebElement acceptCheckbox = driverUtil.getWebElementAndScroll("//input[@id='agreeToEula']");
            if(acceptCheckbox==null)
                throw new AutomationException("We supposed to have End User License Agreement checkbox should be display but not found!");
            acceptCheckbox.click();
            WebElement updateUserPolicyButton = driverUtil.getWebElementAndScroll("//button[contains(@class,'UpdatePolicies')]");
            if(updateUserPolicyButton==null)
                throw new AutomationException("Unable to find Update user policy 'Next' Button!");
            updateUserPolicyButton.click();
            driverUtil.waitForLoadingPage();
        }
        WebElement tasksTab = PageFactory.homePage().getMenu("Tasks");
        MatcherAssert.assertThat("Tasks tab is not being display!", tasksTab!=null);
    }

    @Then("^Verify Home Page tabs$")
    public void verifyHomePageContents() throws AutomationException {
        logInfo("Verify Home Page tabs");
        WebElement tasksTab = PageFactory.homePage().getMenu("Tasks");
        WebElement patientTab = PageFactory.homePage().getMenu("Patient");
        WebElement practitionersTab = PageFactory.homePage().getMenu("Practitioners");
        WebElement pharmaciesTab = PageFactory.homePage().getMenu("Pharmacies");
        WebElement reportsTab = PageFactory.homePage().getMenu("Reports");
        REPORT_LOGGER.log("Verify Tasks tab is being displayed to user..");
        MatcherAssert.assertThat("Tasks tab is not being display!", tasksTab!=null);
        REPORT_LOGGER.log("Verify Patient tab is being displayed to user..");
        MatcherAssert.assertThat("Patient tab is not being display!", patientTab!=null);
        REPORT_LOGGER.log("Verify Practitioners tab is being displayed to user..");
        MatcherAssert.assertThat("Practitioners tab is not being display!", practitionersTab!=null);
        REPORT_LOGGER.log("Verify Pharmacies tab is being displayed to user..");
        MatcherAssert.assertThat("Pharmacies tab is not being display!", pharmaciesTab!=null);
        REPORT_LOGGER.log("Verify Reports tab is being displayed to user..");
        MatcherAssert.assertThat("Reports tab is not being display!", reportsTab!=null);
    }

    @Given("^User launched \"([^\"]*)\"$")
    public void launchBrowser(String browser) throws AutomationException {
        logInfo("User launched "+browser);
        System.out.println("Inside the step defs ");
        if(DriverFactory.drivers.get()==null) {
            System.out.println("Verify driver is null, if yes then initiate brower");
            WebDriver driver = DriverFactory.getInstance().initDriver(browser);
            System.out.println("Browser initiated");
            driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
            driver.manage().window().maximize();
            System.out.println("Browser Maximized");
            PageFactory.init();
            driverUtil = new WebDriverUtil();
        }
    }

    @Before
    public void beforeScenario(Scenario scenario) throws AutomationException {
        REPORT_LOGGER.log(ExtentTestManager.featureFileName.get()+" => " + scenario.getName());
        REPORT_LOGGER.log("----------------------- TEST STARTED -----------------------");
        currentStepIndex.set(0);
        CURRENT_SCENARIO.set(scenario);
        CURRENT_SCENARIO_MESSAGE.set(null);
        CURRENT_STEP_MESSAGE.set(null);
        String featureName = ExtentTestManager.featureFileName.get();
        String scenarioName = scenario.getName();
        Collection<String> tags = scenario.getSourceTagNames();
        Map<String, TestScenario> mappings = TestDataExcelUtil.PHARMACIST_PORTAL_TEST_SCENARIO_MAPPING;
        if(tags.contains("@AdminPortal")) {
            mappings = TestDataExcelUtil.ADMIN_PORTAL_TEST_SCENARIO_MAPPING;
        }
        if(!tags.contains("@Setup") && PropertyReader.groupingBy().equalsIgnoreCase(Constants.GROUPING_BY_EXCEL_MAPPING_DATA)) {
            TestScenario test = mappings.get(scenarioName);
             if(test!=null && test.isDisable(PropertyReader.suiteType())) {
                throw new PendingException("As per the mapping in TestData.xlsx, we are skipping this scenario!");
            }
        } else if(tags.contains("@Setup") && PropertyReader.groupingBy().equalsIgnoreCase(Constants.GROUPING_BY_EXCEL_MAPPING_DATA)) {
            if(!TestDataExcelUtil.isFeatureIncluded(mappings,PropertyReader.suiteType(), featureName)) {
                throw new PendingException("As per the mapping in TestData.xlsx, we are skipping this scenario!");
            }
        }
        ExtentTest test = ExtentTestManager.startTest(scenario.getName(), scenario.getName());
        ExtentTestManager.assignCategory(test);
        if(tags.contains("@Setup") && scenarioName.toLowerCase().contains("launch browser")) {
            SETUP_FAILED.set(Boolean.FALSE);
        } else if(!tags.contains("@Setup") && SETUP_FAILED.get()!=null && SETUP_FAILED.get()) {
            throw new SkipException("Skipping this scenario as setup scenario got failed!");
        } else if(tags.contains("@Setup") && SETUP_FAILED.get()!=null && SETUP_FAILED.get() && scenarioName.toLowerCase().contains("close browser")) {
            throw new SkipException("Skipping this scenario as setup scenario got failed!");
        }
    }

    public void beforeStep(String... stepName) {
        int index = currentStepIndex.get();
        if(index>0) {
            afterStep();
        }
        stepScreenshot.set(null);
        currentStep.set(stepName[0]);
        currentStepIndex.set(index+1);
        CURRENT_STEP_MESSAGE.set(null);
        Scenario scenario = CURRENT_SCENARIO.get();
        Collection<String> tags = scenario.getSourceTagNames();
        if(!tags.contains("@AdminPortal") && DriverFactory.drivers.get()!=null)
            PageFactory.homePage().checkInfoPopupAndClose();
    }

    public void afterStep() {
        try {
            //WebDriverUtil.waitForAWhile(1);
            String step = currentStep.get();
            Media m = null;
            if(CURRENT_SCENARIO.get().isFailed())
                ReportUtil.writeReportLog(false,currentStep.get(),CURRENT_SCENARIO_MESSAGE.get()!=null?CURRENT_SCENARIO_MESSAGE.get():step,stepScreenshot.get());
            else
                ReportUtil.writeReportLog(true,currentStep.get(),CURRENT_STEP_MESSAGE.get()!=null?CURRENT_STEP_MESSAGE.get():step,stepScreenshot.get());
            stepScreenshot.set(null);
            currentStep.set(null);
            CURRENT_STEP_MESSAGE.set(null);
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    @After
    public void afterScenario(Scenario scenario) {
        try {
            String status = scenario.getStatus();
            String scenarioName = scenario.getName();
            Collection<String> tags = scenario.getSourceTagNames();
            if(tags.contains("@Setup") && SETUP_FAILED.get()!=null && SETUP_FAILED.get() && scenarioName.toLowerCase().contains("close browser")) {
                if(DriverFactory.drivers.get()!=null)
                    takeScreenshot(true);
                currentStepIndex.set(0);
                REPORT_LOGGER.log(ExtentTestManager.featureFileName.get()+" => " + scenario.getName());
                REPORT_LOGGER.log("---------------------- TEST COMPLETED ----------------------");
                if(DriverFactory.drivers.get()!=null)
                    DriverFactory.drivers.get().quit();
                DriverFactory.clearDriverSession();
            } else if(tags.contains("@Setup") && status.equalsIgnoreCase("failed") && !scenarioName.toLowerCase().contains("close browser")) {
                SETUP_FAILED.set(Boolean.TRUE);
                if(DriverFactory.drivers.get()!=null)
                    takeScreenshot(true);
                currentStepIndex.set(0);
                REPORT_LOGGER.log(ExtentTestManager.featureFileName.get()+" => " + scenario.getName());
                REPORT_LOGGER.log("---------------------- TEST COMPLETED ----------------------");
            } else if(!tags.contains("@Setup") && SETUP_FAILED.get()!=null && SETUP_FAILED.get()) {
                REPORT_LOGGER.log(ExtentTestManager.featureFileName.get()+" => " + scenario.getName() + " Execution has been skipped!");
                ReportUtil.writeReportSkipLog(CURRENT_SCENARIO.get().getName(),scenario.getName() + " Execution has been skipped as login failed!");
                REPORT_LOGGER.log("---------------------- TEST COMPLETED ----------------------");
            } else {
                if(status.equalsIgnoreCase("passed"))
                    afterStep();
                else if(status.equalsIgnoreCase("failed"))
                    takeScreenshot(true);
                currentStepIndex.set(0);
                if(!status.equalsIgnoreCase("pending")) {
                    REPORT_LOGGER.log(ExtentTestManager.featureFileName.get()+" => " + scenario.getName());
                    REPORT_LOGGER.log("---------------------- TEST COMPLETED ----------------------");
                }
            }
        } catch(Exception ex) {
            ex.printStackTrace();
            logErrorInReport(scenario);
        } finally {
            CURRENT_SCENARIO_MESSAGE.set(null);
            CURRENT_STEP_MESSAGE.set(null);
            Collection<String> tags = scenario.getSourceTagNames();
            if(!tags.contains("@AdminPortal") && DriverFactory.drivers.get()!=null)
                PageFactory.homePage().checkAnyPopupAndClose();
            else if(DriverFactory.drivers.get()!=null)
                PageFactory.homePage().checkAdminPortalErrorPopup();
        }
    }

    private static void logErrorInReport(Scenario scenario) {
        Field field = FieldUtils.getField(((ScenarioImpl) scenario).getClass(), "stepResults", true);
        field.setAccessible(true);
        try {
            ArrayList<Result> results = (ArrayList<Result>) field.get(scenario);
            for (Result result : results) {
                if (result.getError() != null)
                    ReportUtil.writeReportLog(false,currentStep.get(),CURRENT_SCENARIO_MESSAGE.get()!=null?(CURRENT_SCENARIO_MESSAGE.get()+", "):""+result.getError(),true);
            }
        } catch (Exception e) {
            e.printStackTrace();
            ReportUtil.writeReportLog(false,currentStep.get(),e.getMessage()+", Error while logging error: "+ Arrays.toString(e.getStackTrace()),true);
        }
    }

    private static void logErrorInReport(Scenario scenario, List<String> screenshots) {
        String errorMessage = CURRENT_STEP_MESSAGE.get();
        if(errorMessage==null)
            errorMessage = CURRENT_SCENARIO_MESSAGE.get();
        Field field = FieldUtils.getField(((ScenarioImpl) scenario).getClass(), "stepResults", true);
        field.setAccessible(true);
        try {
            ArrayList<Result> results = (ArrayList<Result>) field.get(scenario);
            for (Result result : results) {
                if (result.getError() != null)
                    ReportUtil.writeReportLog(false,currentStep.get(),errorMessage!=null?errorMessage+", ":""+result.getError(),screenshots);
            }
        } catch (Exception e) {
            e.printStackTrace();
            ReportUtil.writeReportLog(false,currentStep.get(),e.getMessage()+", Error while logging error: "+ Arrays.toString(e.getStackTrace()),screenshots);
        }
    }

    public static void takeScreenshot() {
        takeScreenshot(false);
    }

    public static void takeScreenshot(boolean isFailedScenario) {
        String stepMessage = CURRENT_STEP_MESSAGE.get();
        if(stepMessage==null)
            stepScreenshot.set(ReportUtil.takeScreenshot());
        if(isFailedScenario){
            if(stepMessage!=null)
                ReportUtil.writeReportLog(false,currentStep.get(),stepMessage,stepScreenshot.get());
            else if(stepScreenshots.get()!=null && !stepScreenshots.get().isEmpty())
                logErrorInReport(CURRENT_SCENARIO.get(), stepScreenshots.get());
            else
                logErrorInReport(CURRENT_SCENARIO.get());
            stepScreenshot.set(null);
            stepScreenshots.set(null);
        }
    }

    @Then("^User close browser$")
    public void closeBrowser() throws AutomationException {
        logInfo("User close browser");
        if(DriverFactory.drivers.get()!=null)
            DriverFactory.drivers.get().quit();
        DriverFactory.clearDriverSession();
    }

    public static void launchApplication(String url) {
        DriverFactory.drivers.get().get(url);
    }

    public static WebDriver getDriver() {
        return DriverFactory.drivers.get();
    }

    @Then("^Verify Account Selection$")
    public void verifyAccountSelection() throws AutomationException {
        logInfo("Verify Account Selection");
        PageFactory.homePage().selectAccount("Oklahoma Health Care Authority Test");
        WebDriverUtil.waitForAWhile(3);
    }

    public static void clickOnSearchIcon() throws AutomationException {
        WebElement searchIcon = driverUtil.findElementAndScroll(HomePage.SEARCH_BUTTON);
        if(searchIcon==null)
            throw new AutomationException("No search icon is being displayed!");
        searchIcon.click();
        driverUtil.waitForLoadingPage();
    }

    @Then("^Wait to page load$")
    public void waitToPageLoad() {
        logInfo("Wait to page load");
        PageFactory.homePage().waitForLoadingPage();
    }

    @Then("^Verify text on screen \"([^\"]*)\"$")
    public void verifyTextOnScreen(String text) throws AutomationException {
        logInfo("Verify text on screen '"+text+"'");
        WebElement element = driverUtil.findElementAndScroll(String.format(LoginPage.VERIFY_TEXT,text));
        if(element==null)
            throw new AutomationException("Unable to find '"+text+"' on screen!");
        takeScreenshot();
    }

    @Then("^Verify button: \"([^\"]*)\"$")
    public void verifyButton(String text) throws AutomationException {
        logInfo("Verify button: '"+text+"'");
        WebElement element = driverUtil.findElementAndScroll(String.format(LoginPage.VERIFY_BUTTON,text));
        if(element==null)
            throw new AutomationException("Unable to find button with text '"+text+"' on screen!");
    }
}
