package com.arine.automation.glue;

import com.arine.automation.exception.AutomationException;
import com.arine.automation.pages.PageFactory;
import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;
import java.io.IOException;

import static com.arine.automation.glue.CommonSteps.takeScreenshot;


public class LogActionDefs {
    CommonSteps common = new CommonSteps();

    @Then("^Verify log Action pop-up hase Open Task column$")
    public void verifyOpenTaskColumnInLogActionPopUp() throws AutomationException {
        common.logInfo("Verify log Action pop-up hase Open Task column");
        PageFactory.logActionPage().verifyOpenTaskcolumninLogActionPopup();
        takeScreenshot();
    }


    @Then("^Verify all Open Task present in open task table$")
    public void verifyOpenTaskPresentIntaskTable() throws AutomationException {
        common.logInfo("Verify all open tasks from task table is displayed in Open Task column");
        PageFactory.logActionPage().verifyOpenTaskcolumnIncludeListOfOpenTask();
        takeScreenshot();
    }

    @Then("^Verify NA is added in open task column in log action popup$")
    public void verifyNAisAddedinOpenTasksColumn() throws AutomationException {
        common.logInfo("Verify NA is displayed in Open Task column");
        PageFactory.logActionPage().verifyNAisPresentinOpenTaskcolumn();
        takeScreenshot();
    }

    @Then("^Verify User is able to log action for selected task \"([^\"]*)\" and Verify \"([^\"]*)\"$")
    public void verifyUserAbleTologActionFroSelectedTask(String task,String note) throws AutomationException {
        common.logInfo(String.format("Verify User is able to log action for selected task '%s' and Verify note '%s'",task,note));
        PageFactory.logActionPage().verifyUserIsAbleToLogActionForSelectedTask(task,note);
        takeScreenshot();
    }

    @Then("^Verify notes edit section is updated with \"([^\"]*)\" for created \"([^\"]*)\" in task details$")
    public void verifyLogLastAttempedDateAndUserNameInNotesBox(String task,String note) throws AutomationException {
        common.logInfo(String.format("Verify notes edit section is updated with %s for created %s in task details",note,task));
        PageFactory.logActionPage().verifyLastAttemptDateAndUserNameInNoteBoxTaskDetail(note,task);
        takeScreenshot();
    }

    @Then("^Verify log action is not displayed with outcome as \"([^\"]*)\"$")
    public void verifyLogActionIsNotDisplayedFor(String identifier) throws AutomationException {
        common.logInfo("Verify log action is not displayed with outcome as "+identifier);
        PageFactory.logActionPage().verifyLogActionNotDisplayed(identifier);
    }

    @Then("^Verify notes column is updated with \"([^\"]*)\" for created \"([^\"]*)\" in task table$")
    public void verifyLastAttemptDateAndUserNameInNoteBox(String notesColumn, String taskColumn) throws AutomationException {
        common.logInfo(String.format("Verify notes column is updated with %s for created %s in task table",notesColumn,taskColumn));
        PageFactory.logActionPage().verifyLastAttemptDateAndUserNameInNoteBoxTaskTable(taskColumn,notesColumn);
    }

    @Then("Verify task details displays created by user and date as \"([^\"]*)\" for task \"([^\"]*)\"$")
    public void verifyTaskDetailsDisplaysUpdatedDateAndUsernameAs(String createdByUserAndDate,String task) throws AutomationException {
        common.logInfo(String.format("Verify task details displays created by user and date as %s for task %s",createdByUserAndDate,task));
        PageFactory.logActionPage().verifyCreatedByUserAndDate(createdByUserAndDate,task);
    }
}
