package com.arine.automation.glue;

import com.arine.automation.exception.AutomationException;
import com.arine.automation.pages.PageFactory;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static com.arine.automation.glue.CommonSteps.driverUtil;
import static com.arine.automation.glue.CommonSteps.takeScreenshot;

public class LoginSteps {
    CommonSteps common = new CommonSteps();
    @Then("^User login with \"([^\"]*)\" and \"([^\"]*)\"$")
    public void login(String userName, String password) throws Exception {
        common.logInfo("User login with user: "+userName+" and password: *********");
        PageFactory.loginPage().doLogin(userName, password);
    }

    @Then("^User enter email: \"([^\"]*)\" and password: \"([^\"]*)\"$")
    public void enterCredentials(String userName, String password) throws Exception {
        common.logInfo("User enter email: "+userName+" and password: *********");
        PageFactory.loginPage().validateLogin(userName, password);
        takeScreenshot();
    }

    @Then("^Verify Login message: \"([^\"]*)\"$")
    public void verifyError(String message) throws AutomationException {
        if(!message.contains("success")) {
            common.logInfo("Verify Login message: "+message);
            takeScreenshot();
            if(!PageFactory.loginPage().verifyErrorMessage(message))
                throw new AutomationException(String.format("Error message is not being match: %s",message));
        } else if(PageFactory.loginPage().verifyError()) {
            throw new AutomationException("Unable to login, Please provide valid credentials!");
        } else {
            verifyLoginSuccess();
            takeScreenshot();
        }
    }

    @Then("^Verify validation message: \"([^\"]*)\"$")
    public void validationHTML5Message(String message) throws AutomationException {
        common.logInfo("Verify validation message: '"+message+"'");
        PageFactory.loginPage().verifyHTML5Message(message);
    }

    @Then("^User click on forgot password link$")
    public void clickOnForgotPassword() throws AutomationException {
        common.logInfo("User click on forgot password link");
        PageFactory.loginPage().clickOnForgotPasswordLink();
        driverUtil.waitForAWhile(2);
    }

    @Then("^Verify Login Success$")
    public void verifyLoginSuccess() throws AutomationException {
        common.logInfo("Verify Login Success");
        common.verifyHome();
    }

    @Then("^User logout from the application$")
    public void logout() throws AutomationException {
        common.logInfo("User logout from the application");
        PageFactory.loginPage().doLogout();
        driverUtil.waitForAWhile(1);
    }

    @When("Admin User login with \"([^\"]*)\" and \"([^\"]*)\"$")
    public void adminUserLoginWithAnd(String userName, String password) throws AutomationException {
        common.logInfo("Admin User login with user: "+userName+" and password: *********");
        PageFactory.loginPage().doAdminLogin(userName, password);
    }

    @Then("^Admin User logout from the application$")
    public void adminUserlogout() throws AutomationException {
        common.logInfo("Admin User logout from the application");
        PageFactory.loginPage().doAdminLogout();
        driverUtil.waitForAWhile(1);
    }
}
