package com.arine.automation.glue;

import com.arine.automation.exception.AutomationException;
import com.arine.automation.pages.PageFactory;
import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.io.IOException;

public class MedListStepDefs {
    CommonSteps common = new CommonSteps();

    @Then("^User add new medication$")
    public void addNewMed(DataTable dataTable) throws AutomationException {
        common.logInfo("User add new medication: "+dataTable.toString());
        PageFactory.medListPage().addMed(dataTable);
    }

    @Then("^User update medication details$")
    public void updateMedDetails(DataTable dataTable) throws AutomationException {
        common.logInfo("User update medication details: "+dataTable.toString());
        PageFactory.medListPage().updateMed(dataTable);
    }

    @Then("^Verify medication \"([^\"]*)\" record in med list$")
    public void verifyMedRecord(String medName) throws AutomationException {
        common.logInfo("Verify medication "+medName+" record in med list");
        PageFactory.medListPage().verifyMed(medName,true);
        CommonSteps.takeScreenshot();
    }

    @Then("^Select medication \"([^\"]*)\" record in med list$")
    public void selectMedRecord(String medName) throws AutomationException {
        common.logInfo("Select medication "+medName+" record in med list");
        PageFactory.medListPage().selectMed(medName);
    }

    @Then("^User delete medication \"([^\"]*)\" record from med list if present$")
    public void deleteMedRecordIfPresent(String medName) throws AutomationException {
        common.logInfo("Delete medication "+medName+" record from med list");
        PageFactory.medListPage().deleteMedIfPresent(medName);
    }

    @Then("^User delete medication \"([^\"]*)\" record from med list$")
    public void deleteMedRecord(String medName) throws AutomationException {
        common.logInfo("Delete medication "+medName+" record from med list");
        PageFactory.medListPage().deleteMed(medName);
    }

    @Then("^User update all medications sig text with different random sig$")
    public void updateAllMedicationsSigTextWithDifferentSig() throws AutomationException, IOException {
        common.logInfo("User update all medications sig text with different random sig");
        PageFactory.medListPage().updateAllMedicationsSigTextWithDifferentSig();
    }

    @Then("^User update mtm patient all medications sig text with different random sig$")
    public void updateAllMedicationsSigTextWithDifferentSigMTMPatient() throws AutomationException, IOException {
        common.logInfo("User update mtm patient all medications sig text with different random sig");
        PageFactory.medListPage().updateAllMedicationsSigTextWithDifferentSigMTMPatient();
    }

    @When("^medication is not present then add new medication$")
    public void ifMedicationNotPresentInTableThenAddNewMedication(DataTable dataTable) throws AutomationException {
        common.logInfo("medication is not present then add new medication");
        PageFactory.medListPage().addNewMedicationRecordIfNotPresent(dataTable);
    }

    @Then("^Verify Prescriber Name \"([^\"]*)\" for medication \"([^\"]*)\"$")
    public void verifyPrescribeName(String prescribeName, String medication) throws AutomationException {
        common.logInfo("Verify prescribeName " + prescribeName + " record in med list");
        PageFactory.medListPage().verifyPrescriberName(prescribeName, medication);
    }

    @When("^User Select Prescriber: \"([^\"]*)\"$")
    public void userSelectPrescriber(String Prescriber) throws AutomationException {
        common.logInfo("Select Prescriber: '" + Prescriber + "'");
        PageFactory.medListPage().selectPrescriber(Prescriber);
    }

    @Then("^User Enter Notes is Notes to MD: \"([^\"]*)\"$")
    public void userEnterNotesInNotesToMDBox(String Notes) throws AutomationException {
        common.logInfo("User Enter Note In Note MD Box: '" + Notes + "'");
        PageFactory.medListPage().userEnterNotesInNotesToMDBox(Notes);
    }

    @Then("^Verify Note in Notes to MD \"([^\"]*)\" for medication \"([^\"]*)\"$")
    public void VerifyEnteredNotesInNotesToMDBox(String Note, String medication) throws AutomationException {
        common.logInfo("Verify Note " + Note + " record in Notes to MD column in Medication table");
        PageFactory.medListPage().verifyNoteFromNoteToMDBox(Note, medication);
    }

    @Then("^User Enter Condition in Condition Box: \"([^\"]*)\"$")
    public void userEnterConditionInConditionBox(String Condition) throws AutomationException {
        common.logInfo("User Enter Condition In Condition Box: '" + Condition + "'");
        PageFactory.medListPage().userEnterConditionInConditionsBox(Condition);
    }

    @Then("^Verify Condition in Condition column to \"([^\"]*)\" for medication \"([^\"]*)\"$")
    public void VerifyEnteredConditionInConditionsColumn(String Condition, String medication) throws AutomationException {
        common.logInfo("Verify Condition " + Condition + " record in Condition column in Medication table");
        PageFactory.medListPage().verifyConditionFromConditioncolumn(Condition, medication);
    }
}
