package com.arine.automation.glue;

import com.arine.automation.exception.AutomationException;
import com.arine.automation.models.SigTranslation;
import com.arine.automation.pages.PageFactory;
import com.arine.automation.pages.PatientPage;
import com.arine.automation.util.JSONUtil;
import com.arine.automation.util.WebDriverUtil;
import com.aspose.pdf.Page;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.hamcrest.MatcherAssert;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.util.ArrayList;

import static com.arine.automation.glue.CommonSteps.driverUtil;
import static com.arine.automation.glue.CommonSteps.takeScreenshot;


public class PatientSteps {
    CommonSteps common = new CommonSteps();
    @Then("^Click on Patient Tab$")
    public void gotoPatientTab() throws AutomationException {
        common.logInfo("Click on Patient Tab");
        PageFactory.homePage().gotoMenu("Patient");
    }

    @And("^Verify Patient Tab Functionality$")
    public void verifyPatientFunctionality() throws AutomationException {
        common.logInfo("Verify Patient Tab Functionality");
        PageFactory.homePage().gotoMenu("Patient");
    }

    @And("^Search patient: \"([^\"]*)\"$")
    public void searchPatient(String patientId) throws AutomationException {
        common.logInfo("Search patient: '"+patientId+"'");
        PageFactory.patientPage().searchPatient(patientId);
    }

    @And("^User click on ADVANCE search$")
    public void clickOnAdvanceSearch() throws AutomationException {
        common.logInfo("User click on ADVANCE search");
        PageFactory.patientPage().clickOnAdvanceSearch();
    }

    @And("^Verify Patient Name: \"([^\"]*)\"$")
    public void verifyPatientName(String name) throws AutomationException {
        common.logInfo("Verify Patient Name: '"+name+"'");
        String patientName = PageFactory.patientPage().getPatientDetail(PatientPage.PATIENT_NAME_INDEX);
        MatcherAssert.assertThat("Patient Name is not being match!", name.equalsIgnoreCase(patientName));
    }

    @And("^User update patient preferred language \"([^\"]*)\"$")
    public void updatePatientPreferredLanguage(String language) throws AutomationException {
        common.logInfo("User update patient preferred language '"+language+"'");
        WebDriverUtil.waitForAWhile();
        clickOnPatientNameLink();
        WebDriverUtil.waitForAWhile();
        PageFactory.patientPage().updatePatientPreferredLanguage(language);
    }

    @And("^User update patient report language \"([^\"]*)\"$")
    public void updatePatientReportLanguage(String language) throws AutomationException {
        common.logInfo("User update patient report language '"+language+"'");
        WebDriverUtil.waitForAWhile();
        clickOnPatientNameLink();
        WebDriverUtil.waitForAWhile(5);
        PageFactory.patientPage().updatePatientReportLanguage(language);
    }

    @And("^Verify \"([^\"]*)\" error popup$")
    public void verifyPopupMessage(String message) throws AutomationException {
        common.logInfo("Verify '"+message+ "' error popup");
        WebDriverUtil.waitForAWhile(1);
        driverUtil.waitForLoadingPage();
        WebElement messagePopup = driverUtil.getWebElement(String.format(PatientPage.INFORMATION_DIALOG,message));
        if(messagePopup==null)
            throw new AutomationException("Unable to find any error message popup with:"+message);
        takeScreenshot();
        PageFactory.patientPage().clickOnButton("OK");
    }

    @Then("^Click on patient name link$")
    public void clickOnPatientNameLink() throws AutomationException {
        common.logInfo("Click on patient name link");
        PageFactory.patientPage().clickOnNameLink();
    }

    @Then("^Click on patient allergies input$")
    public void clickOnPatientAllergiesInput() throws AutomationException {
        common.logInfo("Click on patient allergies input");
        PageFactory.patientPage().clickOnPatientAllergiesInput();
    }

    @Then("^Click on practitioner name link$")
    public void clickOnPractitionerNameLink() throws AutomationException {
        common.logInfo("Click on practitioner name link");
        PageFactory.practitionersPage().clickOnNameLink();
    }

    @Then("^Verify patient details section$")
    public void verifyPatientDetailsIsDisplaying() throws AutomationException {
        common.logInfo("Verify patient details section");
        PageFactory.patientPage().verifyPatientDetailsSection();
    }

    @Then("^Verify patient \"([^\"]*)\" as \"([^\"]*)\"$")
    public void verifyPatientPersonaDetails(String attribute, String value) throws AutomationException {
        common.logInfo("Verify patient '"+attribute+"' as '"+value+"'");
        PageFactory.patientPage().veryPatientPersonalDetail(attribute, value);
        takeScreenshot();
    }

    @Then("Update patient \"([^\"]*)\" as \"([^\"]*)\"")
    public void updatePatientPersonaDetails(String attribute, String value) throws AutomationException {
        common.logInfo("Update patient '"+attribute+"' as '"+value+"'");
        PageFactory.patientPage().updatePatientPersonalDetail(attribute, value);
        common.clickOnSearchIcon();

    }
    
    @When("^Click on patient first medicine record$")
    public void clickOnFirstMedicineRecord() throws AutomationException {
        common.logInfo("Click on patient first medicine record");
        PageFactory.patientPage().clickOnMedicineFirstRecord();
    }

    @When("^Verify SIG details section$")
    public void verifySigDetailsSection() throws AutomationException {
        common.logInfo("Verify SIG details section");
        PageFactory.patientPage().verifySigDetails();
        takeScreenshot();
    }

    @When("^Verify all sig abbreviations$")
    public void verifyAllSigAbbreviations() throws AutomationException {
        common.logInfo("Verify all sig abbreviations");
        PageFactory.patientPage().verifyAllSigAbbreviations();
    }

    @When("^Verify all sig translations in english and spanish")
    public void verifyAllSigEnglishTranslations() throws AutomationException, IOException {
        common.logInfo("Verify all sig translations in english and spanish");
        PageFactory.patientPage().verifySIGEnglishSpanishTextForNonMtmPatient(JSONUtil.readSigText(JSONUtil.SIG_TEXT_FILE_PATH), true,0);
    }

    @When("^Verify random \"([^\"]*)\" sig translations in english and spanish")
    public void verifySomeSigEnglishSpanishTranslations(int recordCount) throws AutomationException, IOException {
        common.logInfo("Verify random "+recordCount+" sig translations in english and spanish");
        PageFactory.patientPage().verifySIGEnglishSpanishTextForNonMtmPatient(JSONUtil.readSigText(JSONUtil.SIG_TEXT_FILE_PATH), false, recordCount);
    }

    @When("^Updated SIG as \"([^\"]*)\"$")
    public void updateSIG(String sig) throws AutomationException {
        common.logInfo("Updated SIG as '"+sig+"'");
        PageFactory.patientPage().updateSigDetails(sig);
    }

    @When("^Verify SIG Text as \"([^\"]*)\"$")
    public void verifySIGText(String expected) throws AutomationException {
        common.logInfo("Verify SIG Text as '"+expected+"'");
        PageFactory.patientPage().verifySigText(expected);
        takeScreenshot();
    }

    @When("^Clear SIG information$")
    public void clearSigInformation() throws AutomationException {
        common.logInfo("Clear SIG information");
        PageFactory.patientPage().clearSigInformation();
        common.clickOnSearchIcon();
    }

    @When("^Click on tab: \"([^\"]*)\"$")
    public void clickOnTab(String name) throws AutomationException {
        common.logInfo("Click on tab: '"+name+"'");
        PageFactory.patientPage().clickOnGlobalTab(name);
    }

    @When("^Verify comments section$")
    public void verifyCommentSection() throws AutomationException {
        common.logInfo("Verify comments section");
        PageFactory.patientPage().verifyCommentsTable();
    }
    @When("^Verify patient medication section$")
    public void verifyPatientMedicationSection() throws AutomationException {
        common.logInfo("Verify patient medication section");
        PageFactory.patientPage().verifyPatientMedicationSection();
    }
    @When("^Verify patient DRP section$")
    public void verifyPatientDRPSection() throws AutomationException {
        common.logInfo("Verify patient DRP section");
        PageFactory.patientPage().verifyPatientDRPSection();
    }
    @When("^Verify patient Medical Diagnosis section$")
    public void verifyPatientMedicalDiagnosisSection() throws AutomationException {
        common.logInfo("Verify patient Medical Diagnosis section");
        PageFactory.patientPage().verifyPatientMedicalDiagnosisSection();
    }

    @When("^Add new comment: \"([^\"]*)\"$")
    public void addNewComment(String text) throws AutomationException {
        common.logInfo("Add new comment: '"+text+"'");
        PageFactory.patientPage().addNewComment(text);
        PageFactory.patientPage().clickOnNameLink();

    }

    @When("^Verify comment \"([^\"]*)\"$")
    public void verifyComment(String text) throws AutomationException {
        common.logInfo("Verify comment: '"+text+"'");
        PageFactory.patientPage().verifyComment(text, true);
        takeScreenshot();
    }

    @When("^Delete comment: \"([^\"]*)\"$")
    public void deleteComment(String text) throws AutomationException {
        common.logInfo("Delete comment: '"+text+"'");
        PageFactory.patientPage().deleteComment(text);
        takeScreenshot();
        PageFactory.patientPage().clickOnNameLink();
    }

    @When("^Delete comment if present: \"([^\"]*)\"$")
    public void deleteCommentIfPresent(String text) throws AutomationException {
        common.logInfo("Delete comment if present: '"+text+"'");
        PageFactory.patientPage().deleteCommentIfPresent(text);
    }

    @Then("^Verify patient task details$")
    public void verifyPatientTaskDetailsSection() throws AutomationException {
        common.logInfo("Verify patient task details");
        PageFactory.patientPage().verifyPatientTaskDetailsSection();
        takeScreenshot();
    }

    @Then("^Verify patient timeline details$")
    public void verifyPatientTimelineDetailsSection() throws AutomationException {
        common.logInfo("Verify patient timeline details");
        PageFactory.patientPage().verifyPatientTimelineDetailsSection();
        takeScreenshot();
    }

    @When("^Click on button: \"([^\"]*)\"$")
    public void clickOnButton(String name) throws AutomationException {
        common.logInfo("Click on button: '" + name + "'");
        PageFactory.patientPage().clickOnButton(name);
    }

    @When("^Click on tab button: \"([^\"]*)\"$")
    public void clickOnTabButton(String name) throws AutomationException {
        common.logInfo("Click on button: '"+name+"'");
        PageFactory.patientPage().clickOnTabButton(name);
    }

    @Then("^Verify create new task popup$")
    public void verifyCreateNewTaskPopup() throws AutomationException {
        common.logInfo("Verify create new task popup");
        PageFactory.patientPage().verifyCreateNewTaskPopup();
        takeScreenshot();
    }

    @Then("^Verify create new log action popup$")
    public void verifyCreateNewLogActionPopup() throws AutomationException {
        common.logInfo("Verify create new log action popup");
        PageFactory.patientPage().verifyCreateNewLogActionPopup();
        takeScreenshot();
    }

    @Then("^Create new Task with below information:$")
    public void createNewTask(DataTable data) throws AutomationException {
        common.logInfo("Create new Task with below information: "+data.toString());
        PageFactory.patientPage().createNewTask(data);
    }

    @Then("^Create new log action with below information:$")
    public void createNewLogAction(DataTable data) throws AutomationException {
        common.logInfo("Create new log action with below information: "+data.toString());
        PageFactory.patientPage().createNewLogAction(data);
    }

    @Then("^Update log action with below information:$")
    public void updateLogAction(DataTable data) throws AutomationException {
        common.logInfo("Update log action with below information: "+data.toString());
        PageFactory.patientPage().createNewLogAction(data);
    }

    @Then("^Verify newly created task for \"([^\"]*)\"$")
    public void verifyNewlyCreatedTask(String patientName) throws AutomationException {
        common.logInfo("Verify newly created task for '"+patientName+"'");
        PageFactory.patientPage().verifyNewlyCreatedTask(patientName);
    }

    @Then("^Click on newly created task for \"([^\"]*)\"$")
    public void clickOnNewlyCreatedTask(String patientName) throws AutomationException {
        common.logInfo("Click on newly created task for '"+patientName+"'");
        PageFactory.patientPage().clickOnNewlyCreatedTask(patientName);
    }

    @Then("^Edit newly create task with below information:$")
    public void editNewlyCreatedTask(DataTable data) throws AutomationException {
        common.logInfo("Edit newly create task with below information: "+data.toString());
        PageFactory.patientPage().editNewlyCreatedTask(data);
    }

    @Then("^Verify the task is updated with below information:$")
    public void verifyUpdatedTaskDetails(DataTable data) throws AutomationException {
        common.logInfo("Verify the task is updated with below information: "+data.toString());
        PageFactory.patientPage().verifyUpdatedTaskDetails(data);
    }

    @Then("^Delete newly created task \"([^\"]*)\"$")
    public void deleteNewlyCreatedTask(String finder) throws AutomationException {
        common.logInfo("Delete newly created task "+finder);
        PageFactory.patientPage().deleteNewlyCreatedTask(finder);
    }

    @Then("^Delete task \"([^\"]*)\" if present$")
    public void deleteTaskIfPresent(String finder) throws AutomationException {
        common.logInfo("Delete task '"+finder+"' if present");
        PageFactory.patientPage().deleteTaskIfPresent(finder);
    }

    @Then("^Verify newly created log action for \"([^\"]*)\"$")
    public void verifyNewlyCreatedLogAction(String identifier) throws AutomationException {
        common.logInfo("Verify newly created log action for '"+identifier+"'");
        PageFactory.patientPage().verifyNewlyCreatedLogAction(identifier);
        takeScreenshot();
    }

    @Then("^Delete newly created log action$")
    public void deleteNewlyCreatedLogAction() throws AutomationException {
        common.logInfo("Delete newly created log action");
        PageFactory.patientPage().deleteNewlyCreatedLogAction();
        common.takeScreenshot();
    }

    @Then("^Select report type: \"([^\"]*)\"$")
    public void selectReportType(String types) throws AutomationException {
        common.logInfo("Select report type: '"+types+"'");
        PageFactory.patientPage().selectReportTypes(types.split(","));
        WebDriverUtil.waitForAWhile();
        PageFactory.patientPage().selectFirstPractitionerToSendReport();
    }

    @Then("^Click on set call date button$")
    public void clickOnSetCallDateButton() throws AutomationException {
        common.logInfo("Click on set call date button");
        PageFactory.patientPage().clickOnSetCallDateButton();
    }

    @Then("^User select DRP \"([^\"]*)\"$")
    public void selectReportDRP(String drpName) throws AutomationException {
        common.logInfo("User select DRP '"+drpName+"'");
        PageFactory.patientPage().selectReportDRP(drpName);
    }

    @Then("^User verify DRP \"([^\"]*)\"$")
    public void verifyReportDRP(String drpName) throws AutomationException {
        common.logInfo("User verify DRP '"+drpName+"'");
        PageFactory.profileActionPlanPage().verifyDrpRecord(drpName);
    }

    @Then("^User delete DRP \"([^\"]*)\"$")
    public void deleteReportDRP(String drpName) throws AutomationException {
        common.logInfo("User delete DRP '"+drpName+"'");
        PageFactory.patientPage().deleteReportDRP(drpName);
    }

    @Then("^Verify generated report for: \"([^\"]*)\"$")
    public void verifyGeneratedReport(String patientName) throws AutomationException {
        common.logInfo("Verify generated report for: '"+patientName+"'");
        PageFactory.patientPage().verifyGeneratedReport(patientName);
    }

    @Then("^Click on view report link \"([^\"]*)\"$")
    public void clickOnViewReportLink(String name) throws AutomationException {
        common.logInfo("Click on view report link '"+name+"'");
        PageFactory.patientPage().clickOnViewReportLink(name);
    }

    @Then("^Click on view report with role \"([^\"]*)\" & link \"([^\"]*)\"$")
    public void clickOnViewReportLinkWithRole(String role, String name) throws AutomationException {
        common.logInfo("Click on view report with role '"+role+"' & link: '"+name+"'");
        PageFactory.patientPage().clickOnViewReportLink(role, name);
    }

    @Then("^Verify report viewer$")
    public void verifyReportViewer() throws AutomationException {
        common.logInfo("Verify report viewer");
        PageFactory.patientPage().verifyReportViewer();
    }

    @Then("^Verify report contains: \"([^\"]*)\"$")
    public void verifyReportContains(String text) throws AutomationException {
        common.logInfo("Verify report contains: '"+text+"'");
        PageFactory.patientPage().verifyReportContains(text);
    }

    @Then("^Verify report's all updated medication sig text$")
    public void verifyReportMedicationSigNotes() throws AutomationException {
        common.logInfo("Verify report's all updated medication sig text");
        PageFactory.patientPage().verifyReportAllUpdatedMedicationSIGText();
    }

    @Then("^Verify report's all updated medication sig text for MTM patient$")
    public void verifyReportMedicationSigNotesForMTM() throws AutomationException {
        common.logInfo("Verify report's all updated medication sig text for MTM patient");
        PageFactory.patientPage().verifyReportMedicationSigNotesForMTM();
    }

    @Then("^Verify report's all updated medication spanish sig text$")
    public void verifyReportMedicationSpanishSigNotes() throws AutomationException {
        common.logInfo("Verify report's all updated medication spanish sig text");
        PageFactory.patientPage().verifyReportMedicationSpanishSigNotes();
    }

    @Then("^Verify report's all updated medication spanish sig text for MTM patient$")
    public void verifyReportMedicationSpanishSigNotesForMTM() throws AutomationException {
        common.logInfo("Verify report's all updated medication spanish sig text for MTM patient");
        PageFactory.patientPage().verifyReportMedicationSpanishSigNotesForMTM();
    }

    @Then("^User verify \"([^\"]*)\" logo on the report$")
    public void verifyReportLogo(String organization) throws AutomationException, IOException {
        common.logInfo("User verify "+organization+" logo on the report");
        PageFactory.patientPage().verifyReportLogo(organization);
    }

    @Then("^Verify task details section$")
    public void verifyTaskDetailsSection() throws AutomationException {
        common.logInfo("Verify task details section");
        PageFactory.patientPage().verifyTaskDetails();
    }

    @Then("^Click on task action button: \"([^\"]*)\"$")
    public void clickOnTaskAction(String action) throws AutomationException {
        common.logInfo("Click on task action button: '"+action+"'");
        PageFactory.patientPage().clickOnTaskAction(action);
    }

    @Then("^Verify Log new Action popup$")
    public void verifyLogNewActionPopup() throws AutomationException {
        common.logInfo("Verify Log new Action popup");
        PageFactory.patientPage().verifyLogNewActionPopup();
    }

    @Then("^Close Log new Action popup$")
    public void closeLogNewActionPopup() throws AutomationException {
        common.logInfo("Close Log new Action popup");
        PageFactory.patientPage().closeLogNewActionPopup();
    }

    @Then("^Verify patient task is not closed: \"([^\"]*)\"$")
    public void verifyTaskIsNotClosed(String taskName) throws AutomationException {
        common.logInfo("Verify patient task is not closed: '"+taskName+"'");
        PageFactory.patientPage().verifyNewlyCreatedTask(taskName);
    }

    @Then("^Perform log action$")
    public void performLogAction() throws AutomationException {
        common.logInfo("Perform log action");
        PageFactory.patientPage().performLogNewAction();
    }

    @Then("^Verify patient task is closed: \"([^\"]*)\"$")
    public void verifyPatientTaskClosed(String patientName) throws AutomationException {
        common.logInfo("Verify patient task is closed: '"+patientName+"'");
        PageFactory.patientPage().verifyNewlyCreatedTaskDeleted(patientName);
    }

    @Then("^Click on add new lab icon$")
    public void clickOnAddNewLabIcon() throws AutomationException {
        common.logInfo("Click on add new lab icon");
        PageFactory.patientPage().clickOnAddNewLabIcon();
    }

    @Then("^Verify create new lab popup$")
    public void verifyCreateNewLabPopup() throws AutomationException {
        common.logInfo("Verify create new lab popup");
        PageFactory.patientPage().verifyCreateNewLabPopup();
        takeScreenshot();
    }

    @Then("^Create new lab \"([^\"]*)\" and \"([^\"]*)\"$")
    public void createNewLab(String name, String value) throws AutomationException {
        common.logInfo("Create new lab name: "+name+" and value: "+value);
        PageFactory.patientPage().createNewLab(name, value);
    }

    @Then("^Verify newly created lab for \"([^\"]*)\"$")
    public void verifyNewlyCreatedLab(String labName) throws AutomationException {
        common.logInfo("Verify newly created lab for '"+labName+"'");
        PageFactory.patientPage().verifyNewlyCreatedLab(labName);
    }

    @Then("^Delete newly created lab for \"([^\"]*)\"$")
    public void deleteNewlyCreatedLab(String labName) throws AutomationException {
        common.logInfo("Delete newly created lab for '"+labName+"'");
        PageFactory.patientPage().deleteNewlyCreatedLab(labName);
    }

    @When("^User \"([^\"]*)\" the comment: \"([^\"]*)\"$")
    public void performOperationOnComment(String status, String text) throws AutomationException {
        common.logInfo("User "+status+" the comment: '"+text+"'");
        switch(status.toLowerCase()) {
            case "lock":
                PageFactory.patientPage().updateCommentLockStatus(text,true);
                break;
            case "unlock":
                PageFactory.patientPage().updateCommentLockStatus(text,false);
                break;
            case "pin":
                PageFactory.patientPage().updateCommentPinStatus(text,true);
                break;

            case "unpin":
                PageFactory.patientPage().updateCommentPinStatus(text,false);
                break;
            case "highlight":
                PageFactory.patientPage().updateCommentHighlightStatus(text,true);
                break;
            case "un-highlight":
                PageFactory.patientPage().updateCommentHighlightStatus(text,false);
                break;
        }
    }

    @When("^Verify comment: \"([^\"]*)\" status \"([^\"]*)\"$")
    public void verifyCommentStatus(String text, String status) throws AutomationException {
        common.logInfo("Verify comment: '"+text+"' status '"+status+"'");
        takeScreenshot();
        switch(status.toLowerCase()) {
            case "locked":
                PageFactory.patientPage().verifyCommentLockedStatus(text,true);
                break;
            case "unlocked":
                PageFactory.patientPage().verifyCommentLockedStatus(text,false);
                break;
            case "pinned":
                PageFactory.patientPage().verifyCommentPinnedStatus(text,true);
                break;
            case "unpinned":
                PageFactory.patientPage().verifyCommentPinnedStatus(text,false);
                break;
            case "highlighted":
                PageFactory.patientPage().verifyCommentHighlightStatus(text,true);
                break;
            case "un-highlighted":
                PageFactory.patientPage().verifyCommentHighlightStatus(text,false);
                break;
        }
    }

    @When("^User \"([^\"]*)\" Show only toggle button$")
    public void switchShowOnlyToggleButton(String status) throws AutomationException {
        common.logInfo("User "+status+" Show only toggle button");
        switch (status.toLowerCase()) {
            case "on":
                PageFactory.patientPage().performShowHideOperation(true);
                break;
            case "off":
                PageFactory.patientPage().performShowHideOperation(false);
                break;
        }
    }

    @When("^Verify comment \"([^\"]*)\" is \"([^\"]*)\"$")
    public void verifyCommentIsDisplayed(String text, String status) throws AutomationException {
        common.logInfo("Verify comment: '"+text + "' is '"+ status+"'");
        switch (status.toLowerCase()) {
            case "visible":
                PageFactory.patientPage().verifyComment(text, true);
                break;
            case "invisible":
                PageFactory.patientPage().verifyComment(text, false);
                break;
        }
        takeScreenshot();
    }

    @When("^Verify medication \"([^\"]*)\" should be \"([^\"]*)\"$")
    public void verifyMedicationIsDisplayed(String text, String status) throws AutomationException {
        common.logInfo("Verify medication: '"+text+"' should be '"+status+"'");
        switch (status.toLowerCase()) {
            case "visible":
                PageFactory.patientPage().verifyMedicationRecordIsDisplayed(text, true);
                break;
            case "invisible":
                PageFactory.patientPage().verifyMedicationRecordIsDisplayed(text, false);
                break;
        }
        takeScreenshot();
    }

    @When("^User discontinue medication \"([^\"]*)\"$")
    public void selectPatientMedicationRecord(String text) throws AutomationException {
        common.logInfo("User discontinue medication '"+text+"'");
        PageFactory.patientPage().discontinueMedication(text);
    }

    @When("^User verify patient medical diagnosis data table$")
    public void verifyPatientMedicalDiagnosis() throws AutomationException {
        common.logInfo("User verify patient medical diagnosis data table");
        PageFactory.patientPage().verifyPatientMedicalDiagnosis();
    }

    @When("^User verify medical diagnosis data table column \"([^\"]*)\"$")
    public void verifyPatientMedicalDiagnosisTableColumn(String columnName) throws AutomationException {
        common.logInfo("User verify patient medical diagnosis data table column '"+columnName+"'");
        PageFactory.patientPage().verifyPatientMedicalDiagnosisTableColumn(columnName);
    }

    @When("^User verify diagnosis records order$")
    public void getAllDiagnosisRecords() throws AutomationException {
        common.logInfo("User verify diagnosis records order");
        PageFactory.patientPage().verifyAllDiagnosisRecordsOrder();
    }

    @Then("^User click on add new practitioner icon$")
    public void clickOnAddNewPractitionerIcon() throws AutomationException {
        common.logInfo("User click on add new practitioner icon");
        PageFactory.patientPage().clickOnAddNewPractitionerIcon();
    }

    @Then("^Verify practitioner popup \"([^\"]*)\"$")
    public void verifyAddNewPractitionerPopup(String title) throws AutomationException {
        common.logInfo("Verify popup '"+title+"'");
        PageFactory.patientPage().verifyAddNewPractitionerPopup(title);
    }

    @Then("^Wait to close popup \"([^\"]*)\"$")
    public void waitToClosePopup(String title) throws AutomationException {
        common.logInfo("Wait to close popup '"+title+"'");
        PageFactory.patientPage().waitToClosePractitionerPopup(title);
    }

    @Then("^User verify that the practitioner \"([^\"]*)\" is added to the table$")
    public void verifyPractitionerRecordInCareTeam(String name) throws AutomationException {
        common.logInfo("User verify that the practitioner '"+name+"' is added to the table");
        PageFactory.patientPage().verifyCareTeamPractitionerRecord(name);
    }

    @Then("^User click on practitioner \"([^\"]*)\" record$")
    public void clickPractitionerRecordInCareTeam(String name) throws AutomationException {
        common.logInfo("User click on practitioner '"+name+"' record");
        PageFactory.patientPage().clickOnCareTeamPractitionerRecord(name);
    }

    @Then("^User delete the practitioner \"([^\"]*)\"$")
    public void deletePractitionerRecordInCareTeam(String name) throws AutomationException {
        common.logInfo("User delete the practitioner '"+name+"'");
        PageFactory.patientPage().deleteCareTeamPractitionerRecord(name);
    }

    @Then("^User search practitioner by first name \"([^\"]*)\"$")
    @And("^User search pharmacy by first name \"([^\"]*)\"$")
    public void searchPractitionerByFirstName(String name) throws AutomationException {
        common.logInfo("User search practitioner by first name '"+name+"'");
        PageFactory.patientPage().searchPractitionerByFirstName(name);
    }

    @Then("^User search practitioner by last name \"([^\"]*)\"$")
    public void searchPractitionerByLastName(String name) throws AutomationException {
        common.logInfo("User search practitioner by last name '"+name+"'");
        PageFactory.patientPage().searchPractitionerByLastName(name);
    }

    @Then("^User search practitioner by phone \"([^\"]*)\"$")
    public void searchPractitionerByPhone(String phone) throws AutomationException {
        common.logInfo("User search practitioner by phone '"+phone+"'");
        PageFactory.patientPage().searchPractitionerByPhone(phone);
    }

    @Then("^User select practitioner \"([^\"]*)\"$")
    public void selectPractitioner(String name) throws AutomationException {
        common.logInfo("User select practitioner '"+name+"'");
        PageFactory.patientPage().selectPractitioner(name);
    }

    @Then("^User update practitioner role \"([^\"]*)\"$")
    public void updatePractitionerRole(String roleName) throws AutomationException {
        common.logInfo("User update practitioner role '"+roleName+"'");
        PageFactory.patientPage().updatePractitionerRole(roleName);
    }

    @Then("^User verify practitioner role \"([^\"]*)\"$")
    public void verifyPractitionerRole(String roleName) throws AutomationException {
        common.logInfo("User verify practitioner role '"+roleName+"'");
        PageFactory.patientPage().verifyPractitionerRole(roleName);
    }

    @Then("^Verify log story table \"([^\"]*)\" filter$")
    public void verifyLogFilter(String filterName) throws AutomationException {
        common.logInfo("Verify log story table '"+filterName+"' filter");
        PageFactory.patientPage().verifyLogFilter(filterName);
    }

    @Then("^Verify task table \"([^\"]*)\" filter$")
    public void verifyTaskFilter(String filterName) throws AutomationException {
        common.logInfo("Verify task table '"+filterName+"' filter");
        PageFactory.patientPage().verifyTaskFilter(filterName);
    }

    @Then("^User select file to upload: \"([^\"]*)\"$")
    public void uploadFile(String fileName) throws AutomationException {
        common.logInfo("User select file to upload: '"+fileName+"'");
        PageFactory.patientPage().uploadFile(fileName);
    }

    @Then("^User update file name \"([^\"]*)\" with current timestamp$")
    public void updateUploadedFileNameWithTimestamp(String fileName) throws AutomationException {
        common.logInfo("User update file name '"+fileName+"' with current timestamp");
        PageFactory.patientPage().updateUploadedFileNameWithTimestamp(fileName);
    }

    @Then("^User verify uploaded text file and text: \"([^\"]*)\"$")
    public void verifyUploadedTextFileAndData(String text) throws AutomationException {
        common.logInfo("User verify uploaded text file and data: '"+text+"'");
        PageFactory.patientPage().verifyUploadedTextFileAndData(text);
    }

    @Then("^User verify uploaded pdf file and text: \"([^\"]*)\"$")
    public void verifyUploadedPdfFileAndData(String text) throws AutomationException {
        common.logInfo("User verify uploaded pdf file and data: '"+text+"'");
        PageFactory.patientPage().verifyUploadedPdfFileAndData(text);
    }

    @Then("^Verify patient search popup \"([^\"]*)\"$")
    public void verifyPatientSearchPopup(String title) throws AutomationException {
        common.logInfo("Verify patient search popup: '"+title+"'");
        PageFactory.patientPage().verifyPopup(title);
        takeScreenshot();
    }

    @Then("^Verify popup \"([^\"]*)\"$")
    public void verifyPopup(String title) throws AutomationException {
        common.logInfo("Verify popup: '"+title+"'");
        PageFactory.patientPage().verifyPopup(title);
        takeScreenshot();
    }

    @Then("^Verify search practitioner popup \"([^\"]*)\"$")
    public void verifySearchPractitionerPopup(String title) throws AutomationException {
        common.logInfo("Verify search practitioner popup: '"+title+"'");
        PageFactory.patientPage().verifySearchPractitionerPopup(title);
        takeScreenshot();
    }

    @Then("^Search patient by first name: \"([^\"]*)\"$")
    public void advanceSearchByFirstName(String input) throws AutomationException {
        common.logInfo("Search patient by first name: '"+input+"'");
        PageFactory.patientPage().advanceSearchByFirstName(input);
    }

    @Then("^Search patient by last name: \"([^\"]*)\"$")
    public void advanceSearchByLastName(String input) throws AutomationException {
        common.logInfo("Search patient by last name: '"+input+"'");
        PageFactory.patientPage().advanceSearchByLastName(input);
    }

    @Then("^Verify patient record by id: \"([^\"]*)\"$")
    public void verifyAdvanceSearchPatientRecord(String patientId) throws AutomationException {
        common.logInfo("Verify patient record by id: '"+patientId+"'");
        PageFactory.patientPage().verifyAdvanceSearchPatientRecord(patientId);
    }

    @Then("^Verify patient's \"([^\"]*)\": \"([^\"]*)\"$")
    public void verifyPatientOtherDetails(String title, String value) throws AutomationException {
        common.logInfo("Verify patient "+title+": "+value);
        PageFactory.patientPage().verifyPatientInfo(title,value);
    }

    @Then("^User add appointment with next available slot$")
    public void scheduleNextDayAvailableAppointment() throws AutomationException {
        common.logInfo("User add appointment with next available slot");
        PageFactory.patientPage().scheduleNextDayAvailableAppointment();
    }

    @Then("^User reschedule appointment with next available slot$")
    public void rescheduleNextDayAvailableAppointment() throws AutomationException {
        common.logInfo("User reschedule appointment with next available slot");
        PageFactory.patientPage().rescheduleNextDayAvailableAppointment();
    }

    @Then("^Verify newly added appointment$")
    public void verifyAddedAppointment() throws AutomationException {
        common.logInfo("Verify newly added appointment");
        PageFactory.patientPage().verifyAddedAppointment();
    }

    @Then("^Verify rescheduled appointment$")
    public void verifyRescheduledAppointment() throws AutomationException {
        common.logInfo("Verify rescheduled appointment");
        PageFactory.patientPage().verifyRescheduledAppointment();
    }

    @Then("^User click on rescheduled appointment$")
    public void clickOnRescheduledAppointmentButton() throws AutomationException {
        common.logInfo("User click on rescheduled appointment");
        PageFactory.patientPage().clickOnRescheduledAppointmentButton();
    }

    @Then("^User click on cancel scheduled appointment$")
    public void clickOnCancelAppointmentButton() throws AutomationException {
        common.logInfo("User click on cancel scheduled appointment");
        PageFactory.patientPage().clickOnCancelAppointmentButton();
    }

    @Then("^User cancel scheduled appointment$")
    public void cancelAppointment() throws AutomationException {
        common.logInfo("User cancel scheduled appointment");
        PageFactory.patientPage().cancelAppointment();
    }

    @Then("^Cancel scheduled appointment if any scheduled$")
    public void verifyCancelAppointment() throws AutomationException {
        common.logInfo("Cancel scheduled appointment if any scheduled");
        PageFactory.patientPage().verifyCancelAppointment();
    }

    @Then("^User update patient allergy: \"([^\"]*)\"$")
    public void updatePatientAllergy(String allergyName) throws AutomationException {
        common.logInfo("User update patient allergy: '"+allergyName+"'");
        PageFactory.patientPage().setPatientAllergyOption(allergyName);
    }

    @Then("^Verify patient allergy: \"([^\"]*)\"$")
    public void verifyPatientAllergy(String allergyName) throws AutomationException {
        common.logInfo("Verify patient allergy: '"+allergyName+"'");
        PageFactory.patientPage().verifyPatientAllergy(allergyName);
    }

    @Then("^Verify patient timeline auto log \"([^\"]*)\"$")
    public void verifyPatientAutoLog(String logMessage) throws AutomationException {
        logMessage = logMessage.replaceAll("double-quote","\"");
        common.logInfo("Verify patient timeline auto log '"+logMessage+"'");
        PageFactory.patientPage().verifyPatientAutoLog(logMessage);
        takeScreenshot();
    }

    @And("^Verify search results contains first name \"([^\"]*)\"$")
    public void verifySearchResultsContainsFirstName(String firstName) throws AutomationException {
        common.logInfo("Verify search result has first name : '"+firstName+"'");
        PageFactory.patientPage().verifySearchResultContainsName(firstName);
    }

    @And("^Verify search results contains last name \"([^\"]*)\"$")
    public void verifySearchResultsContainsLastName(String lastName) throws AutomationException {
        common.logInfo("Verify search result has last name : '"+lastName+"'");
        PageFactory.patientPage().verifySearchResultContainsName(lastName);
    }

    @And("^Verify search results contains phone \"([^\"]*)\"$")
    public void verifySearchResultsContainsPhone(String phone) throws AutomationException {
        common.logInfo("Verify search result has phone number : '"+phone+"'");
        PageFactory.careTeamPage().verifySearchResultContainsPhone(phone);
    }

    @Then("^Verify Add Practitioner to Patient$")
    public void verifyAddPractitionerToPatient(DataTable dataTable) throws AutomationException {
        common.logInfo("Adding Practitioner to Patient : "+dataTable.toString());
        PageFactory.careTeamPage().addPractitionerToPatient(dataTable);
    }

    @Then("^User updates practitioner address : \"([^\"]*)\"$")
    public void userUpdatesPractitionerAddress(String addressToUpdate) throws AutomationException {
        common.logInfo("Updating practitioner address as : "+addressToUpdate);
        PageFactory.careTeamPage().updatePractitionerAddress(addressToUpdate);
    }

    @And("^Verify \"([^\"]*)\" for practitioner \"([^\"]*)\" is updated as \"([^\"]*)\"")
    public void verifyAddressForPractitionerIsUpdatedAs(String columnName,String practitionerName, String practitionerAddress) throws AutomationException {
        common.logInfo("Verify Address of practitioner named '"+practitionerName+"' is displayed as "+practitionerAddress);
        PageFactory.careTeamPage().verifyPractitionerTable(practitionerName,columnName,practitionerAddress);
    }

    @Then("^User updates practitioner role : \"([^\"]*)\"$")
    public void userUpdatesPractitionerRole(String roleToSelect) throws AutomationException {
        common.logInfo("Updating practitioner role as : "+roleToSelect);
        PageFactory.careTeamPage().updatePractitionerRole(roleToSelect);
    }

    @When("^User click on add new pharmacy icon$")
    public void userClickOnAddNewPharmacyIcon() throws AutomationException {
        common.logInfo("Clicking on Add new Pharmacy Icon");
        PageFactory.patientPage().clickOnAddNewPharmacyIcon();
    }

    @Then("^Verify Add Pharmacy to Patient$")
    public void verifyAddPharmacyToPatient(DataTable dataTable) throws AutomationException {
        common.logInfo("Adding Pharmacy to Patient : "+dataTable.toString());
        PageFactory.careTeamPage().addPharmacyToPatient(dataTable);
    }

    @Then("^User verify that the pharmacy \"([^\"]*)\" is added to the table$")
    public void verifyPharmacyRecordInCareTeam(String name) throws AutomationException {
        common.logInfo("User verify that the pharmacy '"+name+"' is added to the table");
        PageFactory.patientPage().verifyCareTeamPharmacyRecord(name);
    }

    @Then("^Verify pharmacy popup \"([^\"]*)\"$")
    public void verifyAddNewPharmacyPopup(String title) throws AutomationException {
        common.logInfo("Verify popup '"+title+"'");
        PageFactory.patientPage().verifyAddNewPharmacyPopup(title);
    }

    @Then("^User click on pharmacy \"([^\"]*)\" record$")
    public void clickPharmacyRecordInCareTeam(String name) throws AutomationException {
        common.logInfo("User click on pharmacy '"+name+"' record");
        PageFactory.patientPage().clickOnCareTeamPharmacyRecord(name);
    }

    @Then("^User updates pharmacy address : \"([^\"]*)\"$")
    public void userUpdatesPharmacyAddress(String addressToUpdate) throws AutomationException {
        common.logInfo("Updating pharmacy address as : "+addressToUpdate);
        PageFactory.careTeamPage().updatePractitionerAddress(addressToUpdate);
    }

    @And("^Verify \"([^\"]*)\" for pharmacy \"([^\"]*)\" is updated as \"([^\"]*)\"")
    public void verifyAddressForPharmacyIsUpdatedAs(String columnName,String pharmacyName, String practitionerAddress) throws AutomationException {
        common.logInfo("Verify Address of pharmacy named '"+pharmacyName+"' is displayed as "+practitionerAddress);
        PageFactory.careTeamPage().verifyPharmacyTable(pharmacyName,columnName,practitionerAddress);
    }

    @Then("^User delete the pharmacy \"([^\"]*)\"$")
    public void deletePharmacyRecordInCareTeam(String name) throws AutomationException {
        common.logInfo("User delete the pharmacy '"+name+"'");
        PageFactory.patientPage().deleteCareTeamPharmacyRecord(name);
    }

    @Then("^User archive all existing reports$")
    public void archiveAllExistingReports() throws AutomationException {
        common.logInfo("User archive all existing reports");
        PageFactory.patientPage().archiveAllExistingReports();
    }

    @And("^Verify DRP ordering on make reports tab & Profile and Action plan tab is same$")
    public void verifyDRPInformationOnProfileAndActionPlan() throws AutomationException {
        common.logInfo("Verify DRP Information ordering on make reports tab & Profile and Action plan tab is same");
        PageFactory.profileActionPlanPage().verifyDRPInfoOrdering();
    }
}
