package com.arine.automation.glue;

import com.arine.automation.exception.AutomationException;
import com.arine.automation.pages.PageFactory;
import cucumber.api.java.en.Then;

public class PersonalNoteSteps {
    CommonSteps common = new CommonSteps();
    @Then("^User create a new personal note: \"([^\"]*)\"$")
    public void addPersonalNote(String noteText) throws AutomationException {
        common.logInfo("User create a new personal note: '"+noteText+"'");
        PageFactory.homePage().addPersonalNote(noteText);
    }

    @Then("^User verify personal note: \"([^\"]*)\"$")
    public void verifyPersonalNote(String noteText) throws AutomationException {
        common.logInfo("User verify personal note: '"+noteText+"'");
        PageFactory.homePage().verifyPersonalNote(noteText);
    }

    @Then("^User verify personal note: \"([^\"]*)\" should not display$")
    public void verifyPersonalNoteNotDisplay(String noteText) throws AutomationException {
        common.logInfo("User verify personal note: '"+noteText+"' should not display");
        PageFactory.homePage().verifyPersonalNoteNotDisplay(noteText);
    }
}
