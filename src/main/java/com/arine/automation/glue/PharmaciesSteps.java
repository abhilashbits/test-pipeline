package com.arine.automation.glue;

import com.arine.automation.exception.AutomationException;
import com.arine.automation.pages.PageFactory;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class PharmaciesSteps {
    CommonSteps common = new CommonSteps();
    @Then("^Click on Pharmacies Tab$")
    public void gotoPatientTab() throws AutomationException {
        common.logInfo("Click on Pharmacies Tab");
        PageFactory.homePage().gotoMenu("Pharmacies");
    }

    @Given("^User enters \"([^\"]*)\" as \"([^\"]*)\" on pharmacies tab$")
    public void searchPharmaciesWith(String searchWith,String searchString) throws AutomationException {
        common.logInfo("Searching Pharmacies with "+searchWith+" as '"+searchString+"'");
        PageFactory.pharmaciesPage().search(searchWith,searchString);
    }

    @Then("^Verify searched data in column \"([^\"]*)\" contains \"([^\"]*)\" on pharmacies tab$")
    public void verifySearchData(String columnName,String searchString) throws AutomationException {
        common.logInfo("Verifying search data for  "+columnName+" with "+searchString);
        PageFactory.pharmaciesPage().verifyColumnData(columnName,searchString);
    }

    @When("^User clicks on pharmacies tab search icon$")
    public void userClicksOnPharmaciesTabSearchIcon() throws AutomationException {
        common.logInfo("User clicks on pharmacies tab search icon");
        PageFactory.pharmaciesPage().userClickOnSearchIcon();
    }
}
