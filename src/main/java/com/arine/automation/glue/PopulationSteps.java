package com.arine.automation.glue;

import com.arine.automation.exception.AutomationException;
import com.arine.automation.pages.PageFactory;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class PopulationSteps {
    CommonSteps common = new CommonSteps();

    @Then("^Click on Population Tab$")
    public void gotoPatientTab() throws AutomationException {
        common.logInfo("Click on Population Tab");
        PageFactory.homePage().gotoMenu("Population");
    }

    @And("^Verify dashboard not displayed$")
    public void verify_Dashboard_Not_Displayed() throws AutomationException {
        common.logInfo("Verify dashboard not displayed");
        PageFactory.populationPage().verifyDashboardNotDisplayed();
    }

    @When("^User selects dashboard: \"([^\"]*)\"$")
    public void user_selects_dashboard(String dashboardName) throws AutomationException {
        common.logInfo("User selects dashboard : '"+dashboardName+"'");
        PageFactory.populationPage().selectDashboard(dashboardName);
    }

    @Then("^Verify dashboard is displayed$")
    public void verifyDashboardIsDisplayed() throws AutomationException {
        common.logInfo("Verify dashboard is displayed");
        PageFactory.populationPage().verifyDashboardIsDisplayed();
    }
}
