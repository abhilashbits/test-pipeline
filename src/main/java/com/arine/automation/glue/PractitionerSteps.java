package com.arine.automation.glue;

import com.arine.automation.exception.AutomationException;
import com.arine.automation.pages.PageFactory;
import com.arine.automation.pages.PractitionersPage;
import com.arine.automation.util.WebDriverUtil;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.hamcrest.MatcherAssert;

import static com.arine.automation.glue.CommonSteps.driverUtil;
import static com.arine.automation.glue.CommonSteps.takeScreenshot;

public class PractitionerSteps {
    CommonSteps common = new CommonSteps();

    @Then("^Click on Practitioners Tab$")
    public void gotoPatientTab() throws AutomationException {
        common.logInfo("Click on Practitioners Tab");
        PageFactory.homePage().gotoMenu("Practitioners");
    }

    @Then("^User search practitioner by npi: \"([^\"]*)\"$")
    public void searchPractitionerByNPI(String npi) throws AutomationException {
        common.logInfo("User search practitioner by npi: "+npi);
        PageFactory.practitionersPage().searchPractitioner(npi);
    }

    @Then("^User search practitioner by name: \"([^\"]*)\"$")
    public void searchPractitionerByName(String name) throws AutomationException {
        common.logInfo("User search practitioner by name: "+name);
        PageFactory.practitionersPage().searchPractitioner(name);
    }

    @And("^Verify practitioner Name: \"([^\"]*)\"$")
    public void verifyPractitionerName(String name) throws AutomationException {
        common.logInfo("Verify practitioner Name: "+name);
        WebDriverUtil.waitForAWhile(5);
        driverUtil.waitForLoadingPage();
        String practitionerName = PageFactory.practitionersPage().getPractitionerDetail(PractitionersPage.PRACTITIONER_NAME_INDEX);
        MatcherAssert.assertThat("Practitioner Name is not being match!", name.equalsIgnoreCase(practitionerName));
        takeScreenshot();
    }

    @And("^Verify practitioner NPI: \"([^\"]*)\"$")
    public void verifyPractitionerNPI(String npi) throws AutomationException {
        common.logInfo("Verify practitioner NPI: "+npi);
        WebDriverUtil.waitForAWhile(5);
        driverUtil.waitForLoadingPage();
        String practitionerNPI = PageFactory.practitionersPage().getPractitionerDetail(PractitionersPage.PRACTITIONER_NPI_INDEX);
        MatcherAssert.assertThat("Practitioner NPI is not being match!", npi.equalsIgnoreCase(practitionerNPI));
        takeScreenshot();
    }

    @Then("^Verify practitioner details section$")
    public void verifyPractitionerDetailsIsDisplaying() throws AutomationException {
        common.logInfo("Verify practitioner details section");
        PageFactory.practitionersPage().verifyPractitionerDetailsSection();
    }

    @Then("^Verify practitioner \"([^\"]*)\" as \"([^\"]*)\"$")
    public void verifyPractitionerPersonaDetails(String attribute, String value) throws AutomationException {
        common.logInfo("Verify practitioner '"+attribute+"' as '"+value+"'");
        PageFactory.practitionersPage().veryPractitionerPersonalDetail(attribute, value);
        takeScreenshot();
    }

    @Then("Update practitioner \"([^\"]*)\" as \"([^\"]*)\"")
    public void updatePractitionerPersonaDetails(String attribute, String value) throws AutomationException {
        common.logInfo("Update practitioner '"+attribute+"' as '"+value+"'");
        PageFactory.practitionersPage().updatePractitionerPersonalDetail(attribute, value);
        common.clickOnSearchIcon();

    }

}
