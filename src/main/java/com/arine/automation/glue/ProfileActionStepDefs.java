package com.arine.automation.glue;

import com.arine.automation.exception.AutomationException;
import com.arine.automation.pages.PageFactory;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class ProfileActionStepDefs {
    CommonSteps common = new CommonSteps();

    @Then("^User add new DRP record$")
    public void addNewMed(DataTable dataTable) throws AutomationException {
        common.logInfo("User add new DRP record: "+dataTable.toString());
        PageFactory.profileActionPlanPage().addDRP(dataTable);
    }

    @Then("^User add new DRP record, if not present$")
    public void addNewDRP(DataTable dataTable) throws AutomationException {
        common.logInfo("User add new DRP record, if not present: "+dataTable.toString());
        PageFactory.profileActionPlanPage().addDRPIfNotPresent(dataTable);
    }

    @Then("^User delete DRP record \"([^\"]*)\"$")
    public void deleteDRP(String drpName) throws AutomationException {
        common.logInfo("User delete DRP record'"+drpName+"'");
        PageFactory.profileActionPlanPage().deleteDRP(drpName);
    }

    @Then("^User verify DRP not displayed \"([^\"]*)\"$")
    public void verifyDRPNotDisplayed(String drpName) throws AutomationException {
        common.logInfo("User verify DRP not displayed '"+drpName+"'");
        PageFactory.profileActionPlanPage().verifyDrpRecordNotDisplayed(drpName);
    }

    @And("^User clicks on show DRP$")
    public void userClicksOnShowDRP() throws AutomationException {
        common.logInfo("User clicks on show DRP");
        PageFactory.profileActionPlanPage().showDRP();
    }

    @And("^User clicks on hide DRP$")
    public void userClicksOnHideDRP() throws AutomationException {
        common.logInfo("User clicks on hide DRP");
        PageFactory.profileActionPlanPage().hideDRP();
    }

    @And("^User restore DRP record \"([^\"]*)\"$")
    public void userRestoreDRPRecord(String drpName) throws AutomationException {
        common.logInfo("User restore DRP record : "+drpName);
        PageFactory.profileActionPlanPage().restoreDRP(drpName);
    }

    @And("^Verify Personal Medication List$")
    public void verifyPersonalMedicationList() throws AutomationException {
        common.logInfo("Verify Personal Medication List");
        PageFactory.profileActionPlanPage().verifyPersonalMedicationList();
    }

    @And("^User restore DRP record \"([^\"]*)\" if deleted$")
    public void userRestoreDRPRecordIfDeleted(String drpName) throws AutomationException {
        common.logInfo("User restore DRP record "+drpName+" if deleted");
        PageFactory.profileActionPlanPage().restoreDRPIfDeleted(drpName);
    }
}
