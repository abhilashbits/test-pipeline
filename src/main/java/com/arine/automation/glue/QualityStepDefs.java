package com.arine.automation.glue;

import com.arine.automation.exception.AutomationException;
import com.arine.automation.pages.PageFactory;
import com.aspose.pdf.Page;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import gherkin.lexer.Pa;

import java.util.List;

public class QualityStepDefs {
    CommonSteps common = new CommonSteps();

    @Then("^Verify \"([^\"]*)\" tab displayed$")
    public void verifyTabDisplayed(String expectedTabName) throws AutomationException {
        common.logInfo(String.format("Verify %s tab displayed", expectedTabName));
        PageFactory.patientPage().verifyGlobalTab(expectedTabName);
    }

    @Then("^Verify \"([^\"]*)\" tab not displayed$")
    public void verifyTabNotDisplayed(String tabName) throws AutomationException {
        common.logInfo(String.format("Verify %s tab not displayed", tabName));
        PageFactory.patientPage().verifyGlobalTabNotDisplayed(tabName);
    }

    @And("^User select first medication from medication table$")
    public void userIsAbleToSelectMedicationFromMedicationTable() throws AutomationException {
        common.logInfo("User is able to select medication from medication table");
        PageFactory.qualityPage().selectFirstMedRecord();
    }

    @And("^User selects first smart question on quality tab$")
    public void userSelectsFirstSmartQuestion() throws AutomationException {
        common.logInfo("User selects first smart question on quality tab");
        PageFactory.qualityPage().selectFirstSmartQuestion();
    }

    @And("^Verify Smart question form is displayed and first question is selected$")
    public void verifySmartQuestionFormDisplayedAndFirstQuestionSelected() throws AutomationException {
        common.logInfo("Verify Smart question form is displayed and first question is selected");
        PageFactory.qualityPage().verifySmartQuestionFormDisplayedAndFirstQuestionSelected();
    }

    @And("^User updates answer for first smart question on quality tab$")
    public void userUpdatesAnswerForFirstSmartQuestion() throws AutomationException {
        common.logInfo("User updates answer for first smart question on quality tab");
        PageFactory.qualityPage().updateFirstSmartQuestionAnswer();
    }

    @Then("^Verify adherence status table is displayed having measures$")
    public void verifyAdherenceStatusTableIsDisplayedHavingMeasures(List<String> measures) throws AutomationException {
        common.logInfo("Verify adherence status table is displayed having measures");
        PageFactory.qualityPage().verifyAdherenceStatusTable(measures);
    }

    @Then("^Update adherence status of \"([^\"]*)\" as \"([^\"]*)\"$")
    public void updateAdherenceStatusOfAs(String measure, String status) throws AutomationException {
        common.logInfo(String.format("Update adherence status of %s as %s", measure, status));
        PageFactory.qualityPage().updateAdherenceStatusOfMeasures(measure, status);
    }

    @And("^Verify first medication's status of \"([^\"]*)\" column is \"([^\"]*)\"$")
    public void verifyFirstMedicationSStatusOfColumnIs(String columnName, String status) throws AutomationException {
        common.logInfo(String.format("Verify first medication's status of %s column is %s", columnName, status));
        PageFactory.qualityPage().verifyFirstMedRecordColumnValue(columnName, status);
    }

    @Then("^verify today's date is updated for measure \"([^\"]*)\"$")
    public void verifyTodaySDateIsUpdatedForMeasure(String measureName) throws AutomationException {
        common.logInfo(String.format("verify today's date is updated for measure %s", measureName));
        PageFactory.qualityPage().verifyUpdatedDateForMeasure(measureName);
    }

    @And("^Verify medication table has column \"([^\"]*)\"$")
    public void verifyMedicationTableHasColumn(String columnName) throws AutomationException {
        common.logInfo("Verify medication table has column " + columnName);
        PageFactory.qualityPage().verifyMedicationTableHasColumn(columnName);
    }

    @And("^Verify medication table does not have column \"([^\"]*)\"$")
    public void verifyMedicationTableNotHasColumn(String columnName) throws AutomationException {
        common.logInfo("Verify medication table not has column " + columnName);
        PageFactory.qualityPage().verifyMedicationTableNotHasColumn(columnName);
    }

    @When("^User apply filter on med detail table$")
    public void userApplyFilterOnMedDetailTable() throws AutomationException {
        common.logInfo("User apply filter on med detail table");
        PageFactory.qualityPage().applyFilterMedDetailTable();
    }

    @Then("^Verify med records with PDC value are displayed$")
    public void verifyMedRecordsWithPDCValueAreDisplayed() throws AutomationException {
        common.logInfo("Verify med records with PDC value are displayed");
        PageFactory.qualityPage().verifyMedRecordsWithOnlyPDCValueDisplayed();
    }

    @When("^User remove filter on med detail table$")
    public void userRemoveFilterOnMedDetailTable() throws AutomationException {
        common.logInfo("User remove filter on med detail table");
        PageFactory.qualityPage().removeFilterMedDetailTable();
    }
}
