package com.arine.automation.glue;

import com.arine.automation.drivers.DriverFactory;
import com.arine.automation.exception.AutomationException;
import com.arine.automation.pages.PageFactory;
import com.arine.automation.util.FileUtil;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import gherkin.lexer.Pa;

import java.io.File;

public class ReportSteps {
    CommonSteps common = new CommonSteps();
    @Then("^Click on Reports Tab$")
    public void gotoPatientTab() throws AutomationException {
        common.logInfo("Click on Reports Tab");
        PageFactory.homePage().gotoMenu("Reports");
    }

    @And("^Verify Complete CMR button is disabled$")
    public void verifyCompleteCMRButtonIsDisabled() throws AutomationException {
        common.logInfo("Verifying Complete CMR button is disabled");
        PageFactory.homePage().verifyCompleteCMRButtonIsDisabled();
    }

    @And("^Click on set call date button and verify message \"([^\"]*)\"$")
    public void clickOnSetCallDateButtonAndVerifyMessage(String message) throws AutomationException {
        common.logInfo("User clicks on Set call date & verify message : "+message);
        PageFactory.patientPage().clickOnSetCallDateButtonAndVerifyMessage(message);
    }

    @Then("^Verify no DRP Records displayed and Rerun Analysis button is displayed")
    public void verifyRerunAnalysisButton() throws AutomationException {
        common.logInfo("Verifying no DRP Records displayed and Rerun Analysis button displayed");
        PageFactory.patientPage().verifyRerunAnalysisButton();
    }

    @Then("^User uploads file: \"([^\"]*)\" if not already present and update filename as \"([^\"]*)\"$")
    public void userUploadsFileIfNotAlreadyPresent(String filenameToUpload,String updateFileName) throws AutomationException {
        common.logInfo("User uploads file: "+filenameToUpload+" if not already present and update filename as "+updateFileName);
        PageFactory.patientPage().uploadFileIfNotPresent(filenameToUpload,updateFileName);
    }

    @Then("^Select report: \"([^\"]*)\"$")
    public void selectReport(String reportName) throws AutomationException {
        common.logInfo("Select report: "+reportName);
        PageFactory.patientPage().selectReport(reportName);
    }

    @Then("^Select multiple report$")
    public void selectMultipleReport(DataTable reportName) throws AutomationException {
        common.logInfo("Select multiple report");
        PageFactory.patientPage().selectReport(reportName);
    }

    @And("^Verify report not displayed: \"([^\"]*)\"$")
    public void verifyReportNotDisplayed(String reportName) throws AutomationException {
        common.logInfo("Verify report not displayed: "+reportName);
        PageFactory.patientPage().verifyReportNotDisplayed(reportName);
    }

    @And("^Verify report is displayed: \"([^\"]*)\"$")
    public void verifyReportDisplayed(String reportName) throws AutomationException {
        common.logInfo("Verify report is displayed: "+reportName);
        PageFactory.patientPage().verifyReportDisplayed(reportName);
    }

    @And("^User clicks on show Archived Link$")
    public void userClicksOnShowArchivedLink() throws AutomationException {
        common.logInfo("User clicks on show Archived Link");
        PageFactory.patientPage().showArchivedReports();
    }

    @And("^User clicks on hide Archived Link$")
    public void userClicksOnHideArchivedLink() throws AutomationException {
        common.logInfo("User clicks on hide Archived Link");
        PageFactory.patientPage().hideArchivedReports();
    }

    @Then("^Verify MTM report preview is displayed$")
    public void verifyReportPreviewIsDisplayed() throws AutomationException{
        common.logInfo("Verify report preview is displayed");
        PageFactory.patientPage().verifyMTMReportPreviewIsDisplayed();
    }

    @And("^Verify Downloaded file has file name: \"([^\"]*)\"$")
    public void verifyDownloadedFileHasFileName(String fileName) throws AutomationException {
        common.logInfo("Verify Downloaded file has file name: "+fileName);
        PageFactory.patientPage().verifyDownloadedFileHasFileName(fileName);
    }

    @And("^Get DRP Information$")
    public void getDRPInformation() throws AutomationException {
        common.logInfo("get DRP Information");
        PageFactory.patientPage().getDRPInformation();
    }

    @And("^Verify PCP_MTM Report$")
    public void verifyPCPMTMReport() throws AutomationException {
        common.logInfo("Verify PCP_MTM Report");
        PageFactory.patientPage().verifyPCPMTMReportInfo();
    }

    @And("^Select all cardiologist and pcp reports$")
    public void selectAllCardiologistAndPcpReports() throws AutomationException {
        common.logInfo("Select all cardiologist and pcp reports");
        PageFactory.patientPage().selectAllCardiologistAndPCPReport();
    }

    @And("^Verify MAP report$")
    public void verifyMAPReport() throws AutomationException {
        common.logInfo("Verify MAP report");
        PageFactory.patientPage().verifyMAPReportInfo();
    }

    @Then("^Restore archived report \"([^\"]*)\"$")
    public void restoreArchivedReportReportName(String reportName) throws AutomationException {
        common.logInfo("Restore archived report");
        PageFactory.reportPage().unarchiveReportIfArchived(reportName);
    }

    @When("^User add new DRP record on make reports tab$")
    public void userAddNewDRPRecordOnMakeReportsTab(DataTable dataTable) throws AutomationException {
        common.logInfo("User add new DRP record on make reports tab");
        PageFactory.reportPage().addNewDrp(dataTable);
    }

    @And("^User deselect all existing reports$")
    public void userDeselectAllReports() throws AutomationException {
        common.logInfo("User deselect all reports");
        PageFactory.reportPage().deselectAllExistingReports();
    }

    @And("^User deletes existing files with name \"([^\"]*)\"$")
    public void userDeletesExistingFilesWithName(String nameOfFile) {
        common.logInfo("User deletes existing files with name combined_reports");
        FileUtil.deleteFile(((System.getProperty(DriverFactory.OS)==null || System.getProperty(DriverFactory.OS)==DriverFactory.WINDOWS)?System.getProperty("user.dir"):System.getProperty("user.dir").replace("\\", "/"))+ File.separator+"downloads",nameOfFile);
    }

    @And("^User click on Generate Selected Reports button$")
    public void userClickOnGenerateSelectedReportsButton() throws AutomationException {
        common.logInfo("User click on Generate Selected Reports button");
        PageFactory.patientPage().clickOnButton("Generate Selected Reports");
        PageFactory.homePage().waitForLoadingPage();
        boolean errorFound=PageFactory.patientPage().checkAnyPopupAndClose();
        if(errorFound) {
            PageFactory.patientPage().clickOnButton("Generate Selected Reports");
            PageFactory.homePage().waitForLoadingPage();
        }
    }
}
