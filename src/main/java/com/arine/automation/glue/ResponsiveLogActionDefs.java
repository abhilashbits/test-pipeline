package com.arine.automation.glue;

import com.arine.automation.exception.AutomationException;
import com.arine.automation.pages.PageFactory;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static com.arine.automation.glue.CommonSteps.takeScreenshot;

public class ResponsiveLogActionDefs {
    CommonSteps common = new CommonSteps();

    @Then("^Verify in log Action pop-up by default only action column displayed$")
    public void verifyDefaultOnlyActionColumnDisplayed() throws AutomationException {
        common.logInfo("Verify when click on LogAction log Action pop-up has Action column by default");
        PageFactory.responsiveLogActionPage().verifyByDefaultActionColumnInLogActionPopup();
        takeScreenshot();
    }

    @Then("^Verify according to selected action other column should displayed$")
    public void verifyacordingToSelectedActionOtherColumnDisplayed() throws AutomationException {
        common.logInfo("Verify according to selected action other column should displayed");
        PageFactory.responsiveLogActionPage().verifyWhenSelectActionOtherColumnDisplayed();
        takeScreenshot();
    }

    @And("^Create log action without selecting all attribute$")
    public void createLogActionWithoutSelectingAttribute() throws AutomationException {
        common.logInfo("Not able to Create log Action without selecting all attribute");
        PageFactory.responsiveLogActionPage().verifyNotAbleToCreateLogWithoutSelectingAnyAttribute();
        takeScreenshot();
    }

    @Then("^Create log action with only comment: \"([^\"]*)\"$")
    public void createLogActionWithOnlyComment(String comment) throws AutomationException {
        common.logInfo("Create log action with only comment");
        PageFactory.responsiveLogActionPage().createLogActionWithOnlyComment(comment);
        takeScreenshot();
    }

    @And("^Close Log New Action error popUp$")
    public void closeLogNewActionErrorPopUp() throws AutomationException {
        common.logInfo("Close Log New Action Error Pop-Up");
        PageFactory.responsiveLogActionPage().closeLogNewActionErrorPopUp();
        takeScreenshot();
    }

    @Then("^Verify when PCP_MTM report is archived then PCP_MTM option not displayed in Log Action popUp$")
    public void verifyPCP_MTMoptionIsNotDisplayedInLogActionPopUp(DataTable data) throws AutomationException {
        common.logInfo("Verify when PCP_MTM Report is archived then PCP_MTM option is not displayed in LogAction PopUp");
        PageFactory.responsiveLogActionPage().selectDataForVerifyPCP_MTMAndCasesPresentInLogActionPopUp(data);
        PageFactory.responsiveLogActionPage().verifyNoPCP_MTMOptionDisplayedinLogActionPopUp();
        takeScreenshot();
    }

    @Then("^Verify when PCP_MTM report is present in report tab then PCP_MTM and cases option is displayed in Log Action popUp$")
    public void verifyPCP_MTMAndCasesOptionIsDisplayedInLogActionPopUp(DataTable data) throws AutomationException {
        common.logInfo("Verify when PCP_MTM Report is present in Report tab then PCP_MTM and cases option is displayed in LogAction PopUp");
        PageFactory.responsiveLogActionPage().selectDataForVerifyPCP_MTMAndCasesPresentInLogActionPopUp(data);
        PageFactory.responsiveLogActionPage().verifyPCP_MTMOAndClassptionDisplayedinLogActionPopUp();
//        PageFactory.responsiveLogActionPage().verifyPCP_MTMOptionDisplayedinLogActionPopUp();
        takeScreenshot();
    }

    @Then("^Verify practitioners name displayed in log action pop Up: \"([^\"]*)\"$")
    public void verifyPractitionersNameDisplayedInLogActionPopUp(String name,DataTable data) throws AutomationException {
        common.logInfo("Verify Practitioners name in LogAction PopUp");
        PageFactory.responsiveLogActionPage().selectDataForVerifyPCP_MTMAndCasesPresentInLogActionPopUp(data);
        PageFactory.responsiveLogActionPage().verifypractitionersNameDisplayedILogAction(name);
        takeScreenshot();
    }

    @Then("^Verify practitioners name displayed in Care Team Tab: \"([^\"]*)\"$")
    public void verifyPractitionersNameDisplayedInCareTeamTab(String name) throws AutomationException {
        common.logInfo("Verify Verify Practitioners name in Care Team Tab");
        PageFactory.responsiveLogActionPage().verifypractitionersNameDisplayedILogAction(name);
        takeScreenshot();
    }

    @Then("^Click on add new Log Action icon$")
    public void clickOnAddNewLabIcon() throws AutomationException {
        common.logInfo("Click on add new lab icon");
        PageFactory.responsiveLogActionPage().clickOnAddNewLogActionIcon();
    }
    @When("^Click on  new  Log Action tab button: \"([^\"]*)\"$")
    public void clickOnTabButton(String name) throws AutomationException {
        common.logInfo("Click on new Log Action  button: '" + name + "'");
        PageFactory.responsiveLogActionPage().clickOnTabButton(name);
    }

    @Then("^Select newly created task on tasks table for \"([^\"]*)\"$")
    public void verifyNewlyCreatedTask(String finder) throws AutomationException {
        common.logInfo("select newly created task for '"+finder+"'");
        PageFactory.responsiveLogActionPage().selectNewlycreatedTask(finder);
    }
}
