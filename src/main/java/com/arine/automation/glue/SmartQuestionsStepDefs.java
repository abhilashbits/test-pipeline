package com.arine.automation.glue;

import com.arine.automation.exception.AutomationException;
import com.arine.automation.pages.PageFactory;
import com.arine.automation.pages.SmartQuestionsPage;
import com.arine.automation.util.WebDriverUtil;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class SmartQuestionsStepDefs {
    CommonSteps common = new CommonSteps();

    @Then("^Verify Med-Diagnosis Table is displayed$")
    public void verifyMedDiagnosisTable() throws AutomationException {
        common.logInfo("Verify Med-Diagnosis Table is displayed");
        PageFactory.smartQuestionsPage().verifyMedDiagnosisTableDisplayed();
    }

    @Then("^User verify med \"([^\"]*)\" should not displayed$")
    public void verifyMedNotDisplayed(String medName) throws AutomationException {
        common.logInfo("User verify med " + medName + " should not displayed");
        PageFactory.smartQuestionsPage().verifyMedicationIsDisplayed(medName, false);
    }

    @Then("^User verify med \"([^\"]*)\" should displayed$")
    public void verifyMedShouldDisplayed(String medName) throws AutomationException {
        common.logInfo("User verify med " + medName + " should displayed");
        PageFactory.smartQuestionsPage().verifyMedicationIsDisplayed(medName, true);
    }

    @Then("^User verify other assessment \"([^\"]*)\" should displayed$")
    public void verifyOtherAssessmentShouldDisplayed(String medName) throws AutomationException {
        common.logInfo("User verify other assessment " + medName + " should displayed");
        PageFactory.smartQuestionsPage().verifyOtherAssessmentIsDisplayed(medName, true);
    }

    @Then("^User click on medication \"([^\"]*)\"$")
    public void clickOnSmartQuestionMedication(String medName) throws AutomationException {
        common.logInfo("User click on medication " + medName);
        PageFactory.smartQuestionsPage().clickOnSmartQuestionMedication(medName);
    }

    @Then("^User verify smart question \"([^\"]*)\"$")
    public void verifySmartQuestion(String smartQuestion) throws AutomationException {
        common.logInfo("User verify smart question " + smartQuestion);
        PageFactory.smartQuestionsPage().verifySmartQuestion(smartQuestion);
    }

    @Then("^Click on smart question \"([^\"]*)\"$")
    public void clickOnSmartQuestion(String smartQuestion) throws AutomationException {
        common.logInfo("User click on smart question " + smartQuestion);
        PageFactory.smartQuestionsPage().clickOnSmartQuestion(smartQuestion);
    }

    @When("^User click on update smart questions$")
    public void clickOnUpdateSmartQuestions() throws AutomationException {
        common.logInfo("User click on update smart questions");
        PageFactory.smartQuestionsPage().clickOnUpdateSmartQuestionsButton();
    }

    @Then("^User verify health condition \"([^\"]*)\" should be displayed$")
    public void verifyHealthConditionsShouldBeDisplayed(String healthCondition) throws AutomationException {
        common.logInfo("User verify health condition " + healthCondition + " should displayed");
        PageFactory.smartQuestionsPage().verifyHealthConditionsAreDisplayed(healthCondition, true);
    }

    @Then("^Verify first smart question answer for current date historical column$")
    public void verifySmartQuestionAndAnswerCurrentDateHistoricalColumn() throws AutomationException {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("MM/dd/yy");
        String currentDate = LocalDateTime.now().format(format);
        common.logInfo("Verify first smart question answer for current date historical column");
        PageFactory.smartQuestionsPage().verifySmartQuestionAndAnswer(SmartQuestionsPage.firstSmartQuestionUpdatedAnswer, currentDate);
    }

    @Then("^Verify first smart question answer for past date historical column$")
    public void verifySmartQuestionAndAnswerPastDateHistoricalColumn() throws AutomationException {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("MM/dd/yy");
        String PastDate = LocalDateTime.now().minusDays(1).format(format);
        common.logInfo("Verify first smart question answer for past date historical column");
        PageFactory.smartQuestionsPage().verifySmartQuestionAndAnswer(SmartQuestionsPage.firstSmartQuestionUpdatedAnswer, PastDate);
    }

    @When("^Click on complete CMR$")
    public void clickOnCompleteCMR() throws AutomationException {
        common.logInfo("Click on complete CMR");
        PageFactory.smartQuestionsPage().clickOnCompleteCMR();
    }

    @And("^User set past call date$")
    public void userSetPastCallDate() throws AutomationException {
        common.logInfo("User sets past call date");
        PageFactory.smartQuestionsPage().userSetPastCallDate();
    }

    @Then("^Verify historical date column not displayed$")
    public void verifyHistoricalDateColumnNotDisplayed() throws AutomationException {
        common.logInfo("Verify no historical column displayed");
        PageFactory.smartQuestionsPage().verifyHistoricalColumnNotDisplayed();
    }

//    @And("^User update \"([^\"]*)\" with answer$")
//    public void userUpdateWithAnswer(String question) throws AutomationException {
//        common.logInfo("User update " + question + " with answer");
//        PageFactory.smartQuestionsPage().userUpdateWithAnswer(question);
//    }

    @And("^User selects first smart question on smart questions tab$")
    public void userSelectsFirstSmartQuestion() throws AutomationException {
        common.logInfo("User selects first smart question on smart questions tab");
        PageFactory.smartQuestionsPage().selectFirstSmartQuestion();
    }

    @And("^User updates answer for first smart question on smart questions tab$")
    public void userUpdatesAnswerForFirstSmartQuestionOnSmartQuestionsTab() throws AutomationException {
        common.logInfo("User updates answer for first smart question on smart questions tab");
        PageFactory.smartQuestionsPage().updateFirstSmartQuestionAnswer();
    }

    @And("^User answers other assessment for \"([^\"]*)\" and validate score$")
    public void userAnswersOtherAssessmentForAndValidateScore(String assessmentGroup) throws AutomationException {
        common.logInfo(String.format("User answers other assessment for %s and validate score",assessmentGroup));
        PageFactory.smartQuestionsPage().answerQuestionForOtherAssessmentAndValidateScore(assessmentGroup);
    }

    @Then("^User answers other assessment \"([^\"]*)\" such that score tally's to \"([^\"]*)\"$")
    public void userAnswersOtherAssessmentSuchThatScoreTallySTo(String otherAssessmentGroup, String expectedScoreTally) throws AutomationException {
        common.logInfo(String.format("User answers other assessment %s such that score tally's to %s",otherAssessmentGroup,expectedScoreTally));
        PageFactory.smartQuestionsPage().userAnswerOtherAssessmentToScore(otherAssessmentGroup,expectedScoreTally);
    }

    @And("^User verify score scale for PHQ-9 points to \"([^\"]*)\"$")
    public void userVerifyScoreScaleForPHQ9Is(String risk) throws AutomationException{
        common.logInfo(String.format("User verify score scale for PHQ-9 points to %s",risk));
        PageFactory.smartQuestionsPage().userVerifyScoreScaleForPHQ9(risk);
    }

    @And("^User verify score scale for PHQ-2 points to \"([^\"]*)\"$")
    public void userVerifyScoreScaleForPHQPointsTo(String risk) throws AutomationException{
        common.logInfo(String.format("User verify score scale for PHQ-2 points to %s",risk));
        PageFactory.smartQuestionsPage().userVerifyScoreScaleForPHQ2(risk);
    }

    @And("^User verify other assessment \"([^\"]*)\" should be disabled$")
    public void userVerifyOtherAssessmentShouldBeDisabled(String otherAssessmentGroup) throws AutomationException {
        common.logInfo(String.format("User verify other assessment %s should be disabled",otherAssessmentGroup));
        PageFactory.smartQuestionsPage().verifyOtherAssessmentIsDisabled(otherAssessmentGroup);
    }

    @And("^User verify other assessment \"([^\"]*)\" should be enabled$")
    public void userVerifyOtherAssessmentShouldBeEnabled(String otherAssessmentGroup) throws AutomationException {
        common.logInfo(String.format("User verify other assessment %s should be enabled",otherAssessmentGroup));
        PageFactory.smartQuestionsPage().verifyOtherAssessmentIsEnabled(otherAssessmentGroup);
    }

    @Then("^User verify answers for current date historic columns for other assessment for \"([^\"]*)\"$")
    public void userVerifyAnswersForCurrentDateHistoricColumnsForOtherAssessmentFor(String otherAssessmentGroup) throws AutomationException {
        common.logInfo(String.format("User verify answers for current date historic columns for other assessment for %s",otherAssessmentGroup));
        DateTimeFormatter format = DateTimeFormatter.ofPattern("MM/dd/yy");
        String currentDate = LocalDateTime.now().format(format);
        PageFactory.smartQuestionsPage().verifyAnswersForOtherAssessmentWithHistoricalColumnAnswers(otherAssessmentGroup,currentDate);
    }

    @Then("^User click on other assessment for \"([^\"]*)\"$")
    public void userClickOnOtherAssessmentFor(String otherAssessmentGroup) throws AutomationException {
        common.logInfo(String.format("User click on other assessment for %s",otherAssessmentGroup));
        PageFactory.smartQuestionsPage().clickOnOtherAssessment(otherAssessmentGroup);
    }

    @And("^Verify tooltip contains score, updated date and \"([^\"]*)\" details$")
    public void verifyTooltipContainsScoreUpdatedDateAndDetails(String updatedBy) throws AutomationException {
        common.logInfo(String.format("Verify tooltip contains score, updated date and %s details",updatedBy));
        PageFactory.smartQuestionsPage().verifyToolTip(updatedBy);
    }
}
