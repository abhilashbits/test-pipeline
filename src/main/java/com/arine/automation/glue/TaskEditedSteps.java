package com.arine.automation.glue;

import com.arine.automation.exception.AutomationException;
import com.arine.automation.models.TaskInfo;
import com.arine.automation.pages.PageFactory;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class TaskEditedSteps {
    CommonSteps common = new CommonSteps();
    public static TaskInfo info = new TaskInfo();

    @Then("^Verify Select multiple tasks by selecting checkbox$")
    public void SelecctMultipleTaskBySelectingCheckbox() throws AutomationException {
        common.logInfo("Verify Select All Task Filter is present and clickable");
        PageFactory.taskEditedPage().selectAllTasksCheckBox();
    }

    @And("^Deselect all existing tasks$")
    public void DeselectAllExistingTasksBySelectingCheckbox() throws AutomationException {
        common.logInfo("Deselect Select All Existing Tasks ");
        PageFactory.taskEditedPage().DeselectAllTasks();
    }

    @Then("^Verify Edit Selected button is disabled$")
    public void verifyEditSelectedButtonIsDisabled() throws AutomationException {
        common.logInfo("Verifying Edit Selected button is disabled");
        PageFactory.taskEditedPage().verifyEditSelectedButtonIsDisabled();
    }

    @Then("^Verify Edit Selected button is enable$")
    public void verifyEditSelectedButtonIsEnable() throws AutomationException {
        common.logInfo("Verifying Edit Selected button is Enable");
        PageFactory.taskEditedPage().verifyEditSelectedButtonIsEnable();
    }

//    @Then("^Select newly created task on tasks table for \"([^\"]*)\"$")
//    public void verifyNewlyCreatedTask(String finder) throws AutomationException {
//        common.logInfo("select newly created task for '"+finder+"'");
//        PageFactory.taskEditedPage().selectNewlycreatedTask(finder);
//    }

    @And("^Verify newly created task on tasks table is not present for \"([^\"]*)\"$")
    public void verifyNewlyCreatedTaskisNotDisplayed(String finder) throws AutomationException {
        common.logInfo("Verify task is not Present for '"+finder+"'");
        PageFactory.taskEditedPage().verifyTaskNotDisplayed(finder);
    }


//    @And("^close the Selected task form tasks table \"([^\"]*)\"$")
//    public void CloseTask(String finder) throws AutomationException {
//        common.logInfo("Close newly created task '"+finder+"'");
//        PageFactory.taskEditedPage().closeTask(finder);
//    }
//
//    @Then("^Delete created task from tasks tab \"([^\"]*)\"$")
//    public void deleteNewlyCreatedTask(String finder) throws AutomationException {
//        common.logInfo("Delete newly created task '"+finder+"'");
//        PageFactory.taskEditedPage().deleteTask(finder);
//    }

    @Then("^Task \"([^\"]*)\" from tasks tab \"([^\"]*)\"$")
    public void Perform_delete_CloseOperationsNewlyCreatedTask(String OperationName ,String finder) throws AutomationException {
        common.logInfo("'"+OperationName+"' newly created task '"+finder+"'");
        PageFactory.taskEditedPage().Delete_Close_Operations_On_task(finder,OperationName);
    }


    @Then("^Multiple Tasks \"([^\"]*)\" from tasks tab \"([^\"]*)\" and \"([^\"]*)\"$")
    public void Perform_multiple_tasks_delete_CloseOperationsNewlyCreatedTask(String OperationName ,String task_1,String task_2) throws AutomationException {
        common.logInfo(String.format("Multiple Tasks %s from tasks tab %s and %s",OperationName,task_1,task_2));
        PageFactory.taskEditedPage().Delete_Close_Operations_On_Multiple_tasks(task_1,task_2,OperationName);
    }


    @Then("^Set Task In-Progress created task from tasks tab \"([^\"]*)\"$")
    public void taskStatusMakeInProgress(String Finder) throws AutomationException {
        common.logInfo(String.format("Set Task In-Progress created task from tasks tab %s",Finder));
        PageFactory.taskEditedPage().Task_InProgress(Finder);
    }

    @Then("^Set Multiple tasks In-Progress for tasks tab \"([^\"]*)\" and \"([^\"]*)\"$")
    public void multipletaskStatusMakeInProgress(String noteOfTask_1,String noteOfTask_2) throws AutomationException {
        common.logInfo("Set Multiple tasks In-Progress for tasks tab '"+noteOfTask_1+"'and"+noteOfTask_2);
        PageFactory.taskEditedPage().Multiple_Task_InProgress(noteOfTask_1,noteOfTask_2);
    }


    @Then("^Verify newly created task \"([^\"]*)\" task status is: \"([^\"]*)\"$")
    public void VerifyTaskStatus(String task,String Finder) throws AutomationException {
        common.logInfo("Verify newly created task "+task+" task status is: "+Finder);
        PageFactory.taskEditedPage().verifyTaskStatus(task,Finder);
    }

    @Then("^Verify newly created task \"([^\"]*)\" task Priority is: \"([^\"]*)\"$")
    public void VerifyTaskPriority(String task,String Priority) throws AutomationException {
        common.logInfo(String.format("Verify newly created task %s task Priority is: %s",task,Priority));
        PageFactory.taskEditedPage().verifyTaskPriority(task,Priority);
    }


    @Then("^Set task Priority for created task from tasks tab \"([^\"]*)\"$")
    public void taskPriorityMakeUrgent(String noteOfTask) throws AutomationException {
        common.logInfo("Set task Priority for created task from tasks tab '"+noteOfTask+"'");
        PageFactory.taskEditedPage().Set_Task_Priority(noteOfTask);
    }

    @Then("^Set task Priority for Multiple created task from tasks tab \"([^\"]*)\" and for \"([^\"]*)\"$")
    public void taskPriorityMakeUrgentForMultipletasks(String noteOfTask_1, String noteOfTask_2) throws AutomationException {
        common.logInfo(String.format("Set task Priority for Multiple created task from tasks tab %s and for %s",noteOfTask_1,noteOfTask_2));
        PageFactory.taskEditedPage().Set_Task_Priority_for_Multiple_tasks(noteOfTask_1,noteOfTask_2);
    }


    @Then("^Select Due date and time and Verify it from table for task: \"([^\"]*)\"$")
    public void SetDateAndTimeAndVerify(String task) throws AutomationException {
        common.logInfo(String.format("Select Due date and time and Verify it from table for task: %s",task));
        PageFactory.taskEditedPage().SetDueDateAndTimeAndVerifyFromTab(task);
    }

    @Then("^Select Due date and time for multiple tasks and Verify it from table for task: \"([^\"]*)\" and \"([^\"]*)\"$")
    public void SetDateAndTimeAndVerify(String task_1,String task_2) throws AutomationException {
        common.logInfo(String.format("Select Due date and time for multiple tasks and Verify it from table for task: %s and %s",task_1,task_2));
        PageFactory.taskEditedPage().SetDueDateAndTimeforMultipletaskAndVerifyFromTab(task_1,task_2);
    }

    @Then("^verify notes from text box which is present below the table \"([^\"]*)\"$")
    public void updateNotesInNotesTextBox(String TaskNote) throws AutomationException {
        common.logInfo("verify notes from text box which is present below the table %s"+TaskNote);
        PageFactory.taskEditedPage().verifyUpdatedTaskReflectInBelowTable(TaskNote);
    }
}
