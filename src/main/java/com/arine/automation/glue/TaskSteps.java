package com.arine.automation.glue;

import com.arine.automation.exception.AutomationException;
import com.arine.automation.models.TaskInfo;
import com.arine.automation.pages.PageFactory;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class TaskSteps {
    CommonSteps common = new CommonSteps();
    public static TaskInfo info = new TaskInfo();

    @Then("^Click on Tasks Tab$")
    public void gotoPatientTab() throws AutomationException {
        common.logInfo("Click on Tasks Tab");
        PageFactory.homePage().gotoMenu("Tasks");
    }

    @Then("^Verify task details$")
    public void verifyTaskDetails() throws AutomationException {
        common.logInfo("Verify task details");
        info = PageFactory.taskPage().getTaskInfo();
    }

    @And("^Verify Tasks Tab Functionality$")
    public void verifyTasksFunctionality() throws AutomationException {
        common.logInfo("Verify Tasks Tab Functionality");
        PageFactory.homePage().gotoMenu("Tasks");
    }

    @And("^Click on first task record$")
    public void clickOnFirstTaskRecord() throws AutomationException {
        common.logInfo("Click on first task record");
        PageFactory.taskPage().getTaskRecord(1).click();
    }

    @And("^Select task filter: \"([^\"]*)\"$")
    public void selectTaskFilter(String filterName) throws AutomationException {
        common.logInfo("Select task filter: '"+filterName+"'");
        PageFactory.taskPage().selectTaskFilter(filterName);
    }

   @And("^Click on tasks sub tab: \"([^\"]*)\"$")
    public void selectTasksSubTab(String tabName) throws AutomationException {
        common.logInfo("CLick on tasks sub tab: '"+tabName+"'");
        PageFactory.taskPage().selectTasksSubTab(tabName);
    }

    @Then("^Verify newly created task on tasks tab for \"([^\"]*)\"$")
    public void verifyNewlyCreatedTask(String finder) throws AutomationException {
        common.logInfo("Verify newly created task for '"+finder+"'");
        PageFactory.taskPage().verifyTask(finder);
    }

    @Then("^Delete newly created task from tasks tab \"([^\"]*)\"$")
    public void deleteNewlyCreatedTask(String finder) throws AutomationException {
        common.logInfo("Delete newly created task '"+finder+"'");
        PageFactory.taskPage().deleteTask(finder);
    }

    @Then("^User select organization: \"([^\"]*)\"$")
    public void verifyAccountSelection(String organization) throws AutomationException {
        common.logInfo("User select organization: '"+organization+"'");
        PageFactory.homePage().selectAccount(organization);
    }

    @And("^Select task having \"([^\"]*)\"$")
    public void selectTaskHavingNotes(String notes) throws AutomationException {
        common.logInfo("Select task having "+notes);
        PageFactory.taskEditedPage().selectTask(notes);
    }

    @And("Verify User is able to log action for selected task and Verify \"([^\"]*)\"$")
    public void verifyUserIsAbleToLogActionForSelectedTaskAndVerify(String userNameAndDate) throws AutomationException {
        common.logInfo("Verify User is able to log action for selected task and Verify");
        PageFactory.taskEditedPage().logActionForSelectedTaskAndVerifyInNotesBox(userNameAndDate);
    }
}
