package com.arine.automation.listeners;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.gherkin.model.Feature;
import com.aventstack.extentreports.gherkin.model.Given;
import com.aventstack.extentreports.gherkin.model.Scenario;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import cucumber.api.HookTestStep;
import cucumber.api.PickleStepTestStep;
import cucumber.api.event.*;
import java.util.HashMap;
import java.util.Map;


public class CustomReportListener implements ConcurrentEventListener {
    private ExtentSparkReporter spark;
    private ExtentReports extent;
    Map<String, ExtentTest> feature = new HashMap<String, ExtentTest>();
    ExtentTest scenario;
    ExtentTest step;
    public CustomReportListener() {
    };

    @Override
    public void setEventPublisher(EventPublisher eventPublisher) {
        eventPublisher.registerHandlerFor(TestRunStarted.class, this::runStarted);
        eventPublisher.registerHandlerFor(TestRunFinished.class, this::runFinished);
        eventPublisher.registerHandlerFor(TestSourceRead.class, this::featureRead);
        eventPublisher.registerHandlerFor(TestCaseStarted.class, this::ScenarioStarted);
        eventPublisher.registerHandlerFor(TestStepStarted.class, this::stepStarted);
        eventPublisher.registerHandlerFor(TestStepFinished.class, this::stepFinished);
    };

    private void runStarted(TestRunStarted event) {
        spark = new ExtentSparkReporter("./ExtentReportResults.html");
        extent = new ExtentReports();
        spark.config().setTheme(Theme.DARK);
        // Create extent report instance with spark reporter
        extent.attachReporter(spark);
    };

    private void runFinished(TestRunFinished event) {
        extent.flush();
    };

    private void featureRead(TestSourceRead event) {
        String featureSource = event.toString();
        String featureName = featureSource.split(".*/")[1];
        if (feature.get(featureSource) == null) {
            feature.putIfAbsent(featureSource, extent.createTest(featureName));
        }
    };

    private void ScenarioStarted(TestCaseStarted event) {
        String featureName = event.getTestCase().getUri().toString();
        scenario = feature.get(featureName).createNode(event.getTestCase().getName());
    };

    private void stepStarted(TestStepStarted event) {
        String stepName = " ";
        if (event.testStep instanceof PickleStepTestStep) {
            PickleStepTestStep steps = (PickleStepTestStep) event.testStep;
            stepName = steps.getStepText();
        } else {
            HookTestStep hoo = (HookTestStep) event.testStep;
            stepName = hoo.getHookType().name();
        }
        step = scenario.createNode(Given.class, stepName);
    };

    private void stepFinished(TestStepFinished event) {
        if (event.result.getStatus().toString() == "PASSED") {
            step.log(Status.PASS, "This passed");
        } else if (event.result.getStatus().toString() == "SKIPPED") {
            step.log(Status.SKIP, "This step was skipped ");
        } else {
            step.log(Status.FAIL, "This failed");
        }
    };

}