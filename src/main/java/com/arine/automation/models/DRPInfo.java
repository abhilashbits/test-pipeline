package com.arine.automation.models;

public class DRPInfo {
    private String drpName;

    public String getDrpName() {
        return drpName;
    }

    public void setDrpName(String drpName) {
        this.drpName = drpName;
    }

    public String getProviderAssessment() {
        return providerAssessment;
    }

    public void setProviderAssessment(String providerAssessment) {
        this.providerAssessment = providerAssessment;
    }

    public String getProviderRecommendation() {
        return providerRecommendation;
    }

    public void setProviderRecommendation(String providerRecommendation) {
        this.providerRecommendation = providerRecommendation;
    }

    private String providerAssessment;
    private String providerRecommendation;

    public String getPatientAssessment() {
        return patientAssessment;
    }

    public void setPatientAssessment(String patientAssessment) {
        this.patientAssessment = patientAssessment;
    }

    public String getPatientRecommendation() {
        return patientRecommendation;
    }

    public void setPatientRecommendation(String patientRecommendation) {
        this.patientRecommendation = patientRecommendation;
    }

    private String patientAssessment;
    private String patientRecommendation;


    public DRPInfo(String drpName, String providerAssessment, String providerRecommendation, String patientAssessment, String patientRecommendation) {
        this.drpName = drpName;
        this.providerAssessment = providerAssessment;
        this.providerRecommendation = providerRecommendation;
        this.patientAssessment=patientAssessment;
        this.patientRecommendation=patientRecommendation;
    }

}
