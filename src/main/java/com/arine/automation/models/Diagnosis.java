package com.arine.automation.models;

import java.util.Date;
import java.util.List;

public class Diagnosis implements Comparable {
    public String title;
    public String dxDate;
    public int ip;
    public int ed;
    public List<String> ipDates;
    public List<String> edDates;
    public Date latest;

    @Override
    public int compareTo(Object o) {
        if(o instanceof Diagnosis) {
            Diagnosis other = (Diagnosis) o;
            if(other.latest.before(this.latest))
                return -1;
             else if(other.latest.after(this.latest))
                return 1;
        }
        return 0;
    }
}
