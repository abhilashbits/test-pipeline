package com.arine.automation.models;

import com.arine.automation.constants.Constants;
import com.arine.automation.util.ExcelUtil;
import com.arine.automation.util.JSONUtil;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.IOException;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SigTranslation extends ExcelMapper {
    public static final String SIG_MAPPING_FILE_PATH = Constants.TEST_DATA_FILE_PATH + "sig-mapping.xlsx";
    public static final String SIG_MAPPING_FILE_ALTERNATE_PATH = Constants.TEST_DATA_FILE_PATH + "sig-mapping1.xlsx";
    public static final String SIG_MAPPING_SHEET = "Mapping";
    public static final String SIG_TEXT_EXECUTED_COUNTER_INDEX = "SIG_TEXT_EXECUTED_COUNTER_INDEX";
    public static final String REPORT_SIG_TEXT_EXECUTED_COUNTER_INDEX = "REPORT_SIG_TEXT_EXECUTED_COUNTER_INDEX";
    public static final String REPORT_SIG_TEXT_EXECUTED_COUNTER_INDEX_MTM_PATIENT = "REPORT_SIG_TEXT_EXECUTED_COUNTER_INDEX_MTM_PATIENT";

    public String sig;
    public String sigTextEnglish;
    public String sigTextSpanish;
    public String sigEnglishTextForMTM;
    public String sigSpanishTextForMTM;

    public String getSig() {
        return sig;
    }

    public void setSig(String sig) {
        this.sig = sig;
    }

    public String getSigTextEnglish() {
        return sigTextEnglish;
    }

    public void setSigTextEnglish(String sigTextEnglish) {
        this.sigTextEnglish = sigTextEnglish;
    }

    public String getSigTextSpanish() {
        return sigTextSpanish;
    }

    public void setSigTextSpanish(String sigTextSpanish) {
        this.sigTextSpanish = sigTextSpanish;
    }

    public String getSigShortForm() {
        return sig;
    }

    public void setSigShortForm(String sigShortForm) {
        this.sig = sigShortForm;
    }

    public String getSigEnglishTextForNonMTM() {
        return sigTextEnglish;
    }

    public void setSigEnglishTextForNonMTM(String sigEnglishTextForNonMTM) {
        this.sigTextEnglish = sigEnglishTextForNonMTM;
    }

    public String getSigSpanishTextForNonMTM() {
        return sigTextSpanish;
    }

    public void setSigSpanishTextForNonMTM(String sigSpanishTextForNonMTM) {
        this.sigTextSpanish = sigSpanishTextForNonMTM;
    }

    public List<SigTranslation> getSigTranslations() throws IOException {
        return JSONUtil.readSigText(JSONUtil.SIG_TEXT_FILE_PATH);
    }

    @Override
    public SigTranslation setData(List<String> dataArray) {
        SigTranslation sigTranslation = new SigTranslation();
        sigTranslation.setSigShortForm(dataArray.get(0));
        sigTranslation.setSigEnglishTextForNonMTM(dataArray.get(1));
        sigTranslation.setSigSpanishTextForNonMTM(dataArray.get(2));
        return sigTranslation;
    }

    public static void main(String[] args) {
        //List<ExcelMapper> translations = SigTranslation.getSigTranslations();
    }

    public String getSigEnglishTextForMTM() {
        return sigEnglishTextForMTM;
    }

    public void setSigEnglishTextForMTM(String sigEnglishTextForMTM) {
        this.sigEnglishTextForMTM = sigEnglishTextForMTM;
    }

    public String getSigSpanishTextForMTM() {
        return sigSpanishTextForMTM;
    }

    public void setSigSpanishTextForMTM(String sigSpanishTextForMTM) {
        this.sigSpanishTextForMTM = sigSpanishTextForMTM;
    }

    @Override
    public String toString() {
        return "SigTranslation{" +
                "sigShortForm='" + sig + '\'' +
                ", sigEnglishTextForNonMTM='" + sigTextEnglish + '\'' +
                ", sigSpanishTextForNonMTM='" + sigTextSpanish + '\'' +
                ", sigEnglishTextForMTM='" + sigEnglishTextForMTM + '\'' +
                ", sigSpanishTextForMTM='" + sigSpanishTextForMTM + '\'' +
                '}';
    }
}
