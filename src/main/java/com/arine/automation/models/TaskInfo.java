package com.arine.automation.models;

public class TaskInfo {
    public String taskName;
    public String patientName;
    public String patientId;

    @Override
    public String toString() {
        return "TaskInfo{" +
                "taskName='" + taskName + '\'' +
                ", patientName='" + patientName + '\'' +
                ", patientId='" + patientId + '\'' +
                '}';
    }
}
