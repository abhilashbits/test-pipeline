package com.arine.automation.pages;

import com.arine.automation.drivers.DriverFactory;
import com.arine.automation.exception.AutomationException;
import com.arine.automation.glue.CommonSteps;
import com.arine.automation.models.UserPermissionMapper;
import com.arine.automation.util.WebDriverUtil;
import com.google.common.collect.Ordering;
import cucumber.api.DataTable;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.logging.Logger;

public class AdminPortalHomePage {
    public static final String HOMEPAGE_HEADER_LABEL = "//div[@class='MuiCardHeader-content']//h5[text()='Users']";
    public static final String USER_SEARCH_NAME_INPUT = "//div[@title='Name']//input";
    public static final String USER_SEARCH_EMAIL_INPUT = "//div[@title='Email']//input";
    public static final String USER_ACCOUNTS_DROPDOWN = "//div[contains(@class,'dropdown')]";
    public static final String SEARCH_BUTTON = "//span[text()='Search']/parent::button";
    public static final String RESET_BUTTON = "//span[text()='Reset']/parent::button";
    public static final String USER_SEARCH_RESULT_NAME_COLUMN = "//div[@col-id='firstName' and @role!='columnheader']";
    public static final String USER_SEARCH_RESULT_NAME_COLUMN_HEADER = "//div[@col-id='firstName' and @role='columnheader']";
    public static final String USER_SEARCH_RESULT_LAST_NAME_COLUMN = "//div[@col-id='lastName' and @role!='columnheader']";
    public static final String USER_SEARCH_RESULT_EMAIL_COLUMN = "//div[@col-id='email' and @role!='columnheader']";
    public static final String USER_SEARCH_RESULT_EMAIL_COLUMN_HEADER = "//div[@col-id='email' and @role='columnheader']";
    public static final String USER_SEARCH_RESULT_ACCOUNT_COLUMN = "//div[@col-id='account' and @role!='columnheader']";
    public static final String USER_SEARCH_RESULT_STATUS_COLUMN = "//div[@col-id='status' and @role!='columnheader']";
    public static final String USER_SEARCH_RESULT_ROLE_COLUMN = "//div[@col-id='role' and @role!='columnheader']";
    public static final String CREATE_USER_BUTTON = "//button/span[text()='Create User']";
    public static final String CREATE_USER_POP_UP = "//*[@id='max-width-dialog-title']/h2[text()='Create User']";
    public static final String EDIT_USER_POP_UP = "//*[@id='max-width-dialog-title']/h2[text()='Edit User']";
    public static final String CREATE_USER_FIRST_NAME_INPUT = "//div[@title='FirstName*']//input";
    public static final String CREATE_USER_LAST_NAME_INPUT = "//div[@title='LastName*']//input";
    public static final String CREATE_USER_EMAIL_INPUT = "//div[@title='Email*']//input";
    public static final String CREATE_USER_JOB_TITLE_INPUT = "//*[text()='Job Title']/..//input";
    public static final String CREATE_USER_ORG_DROPDOWN = "//label[text()='MTM Org(s)*']/parent::div";
    public static final String CREATE_USER_ROLE_DROPDOWN = "//label[text()='Role*']/parent::div";
    public static final String CREATE_USER_STATUS_DROPDOWN = "//label[text()='Status*']/parent::div";
    public static final String CREATE_USER_CREDENTIALS_INPUT = "//*[text()='Credentials']/..//input";
    public static final String CREATE_USER_NPI_INPUT = "//div[@title='npi*']//input";
    public static final String CREATE_USER_IP_ADDRESS_INPUT = "//div[@title='Ip Address']//input";
    public static final String CREATE_USER_PHONE_INPUT = "//div[@title='Phone Number']//input";
    public static final String CREATE_USER_SAVE_BUTTON = "//button[not(contains(@class,'Mui-disabled'))]/span[text()='Create']";
    public static final String EDIT_USER_UPDATE_BUTTON = "//button[not(contains(@class,'Mui-disabled'))]/span[text()='Update']";
    public static final String ALERT_MESSAGE = "//div[@class='MuiAlert-message']";
    public static final String TIMESTAMP_FORMAT = "MMddHHmm";
    public static final String UPDATE_PERMISSION_BUTTON = "//span[@aria-label='Updates permissions of selected user(s)']/button[not(contains(@class,'disabled'))]";
    public static final String UPDATE_PERMISSION_BUTTON_DISABLED = "//span[@aria-label='Updates permissions of selected user(s)']/button[contains(@class,'disabled')]";
    public static final String USER_SELECT_ALL_RECORDS = "//div[@col-id='selectUsers']//*[@aria-label='Press Space to toggle all rows selection (unchecked)']";
    public static final String USER_DESELECT_ALL_RECORDS = "//div[@col-id='selectUsers']//*[@aria-label='Press Space to toggle all rows selection (checked)']";
    public static final String USER_FIRST_RECORD_CHECKBOX_SELECT = "//div[@class='ag-center-cols-viewport']//div[@row-index='0']//*[@aria-label='Press Space to toggle row selection (unchecked)']";
    public static final String USER_FIRST_RECORD_CHECKBOX_DESELECT = "//div[@class='ag-center-cols-viewport']//div[@row-index='0']//*[@aria-label='Press Space to toggle row selection (checked)']";
    public static final String USER_SEARCH_RECORD_COUNT = "//span[@ref='lbRecordCount']";
    public static final String USER_SEARCH_RESULTS = "//div[@ref='eHeaderViewport']";
    public static final String USER_SEARCH_PAGINATION_NEXT = "//div[@ref='btNext' and not(contains(@class,'disabled'))]";
    public static final String USER_SEARCH_PAGINATION_PREVIOUS = "//div[@ref='btPrevious' and not(contains(@class,'disabled'))]";
    public static final String USER_SEARCH_PAGINATION_LAST = "//div[@ref='btLast' and not(contains(@class,'disabled'))]";
    public static final String USER_SEARCH_PAGINATION_FIRST = "//div[@ref='btFirst' and not(contains(@class,'disabled'))]";
    public static final String USER_SEARCH_PAGINATION_CURRENT_PAGE = "//span[@ref='lbCurrent']";
    public static final String USER_SEARCH_PAGINATION_TOTAL_PAGE_COUNT = "//span[@ref='lbTotal']";
    private static final Logger LOGGER = Logger.getLogger(AdminPortalHomePage.class.getName());
    public static WebDriverUtil driverUtil = new WebDriverUtil();
    Map<String, String> userDetails = new HashMap<>();

    public void searchUserWithName(String name) throws AutomationException {
        WebElement searchInput = driverUtil.getWebElement(USER_SEARCH_NAME_INPUT);
        if (searchInput == null)
            throw new AutomationException("Unable to locate name search input!");
        String existing = searchInput.getAttribute("value");
        if (existing == null || !existing.equalsIgnoreCase(name)) {
            searchInput.click();
            searchInput.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
            searchInput.sendKeys(name);
        }
    }

    public void searchUser(String searchWith, String searchString) throws AutomationException {
        switch (searchWith.toLowerCase()) {
            case "firstname":
                searchUserWithName(searchString);
                break;
            case "email":
                searchUserWithEmail(searchString);
                break;
            case "account":
                selectAccount(searchString);
                break;
            default:
                throw new AutomationException("Invalid search with option. Valid options are FirstName, EmailAddress or Account");
        }
    }

    public void searchUserWithEmail(String email) throws AutomationException {
        WebElement searchInput = driverUtil.getWebElement(USER_SEARCH_EMAIL_INPUT);
        if (searchInput == null)
            throw new AutomationException("Unable to locate email search input!");
        String existing = searchInput.getAttribute("value");
        if (existing == null || !existing.equalsIgnoreCase(email)) {
            searchInput.click();
            searchInput.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
            searchInput.sendKeys(email);
        }
    }

    public void selectAccount(String account) throws AutomationException {
        WebElement dropdown = driverUtil.getWebElement(USER_ACCOUNTS_DROPDOWN);
        if (dropdown == null)
            throw new AutomationException("Unable to locate account selection dropdown!");
        dropdown.click();
        WebElement element = driverUtil.getWebElement("//li[" + driverUtil.ignoreCase(account) + "]", 3);
        if (element == null)
            throw new AutomationException("Account :" + account + " not found in drop down or drop down not open");
        element.click();
    }

    public void clickOnSearchButton() throws AutomationException {
        WebElement searchBtn = driverUtil.getWebElement(SEARCH_BUTTON);
        if (searchBtn == null)
            throw new AutomationException("Unable to locate search button");
        driverUtil.waitForElementToBeClickable(SEARCH_BUTTON);
        driverUtil.moveToElementAndClick(searchBtn);
        driverUtil.waitForLoaderToDisappear();
    }

    public void clickOnResetButton() throws AutomationException {
        WebElement resetBtn = driverUtil.getWebElement(RESET_BUTTON);
        if (resetBtn == null)
            throw new AutomationException("Unable to locate reset button");
        if (!resetBtn.getAttribute("class").contains("disabled")) {
            driverUtil.waitForElementToBeClickable(RESET_BUTTON);
            driverUtil.moveToElementAndClick(resetBtn);
            driverUtil.waitForLoaderToDisappear();
        }
    }

    public void verifyHomePage() throws AutomationException {
        WebElement home = driverUtil.getWebElement(HOMEPAGE_HEADER_LABEL);
        if (home == null)
            throw new AutomationException("Unable to locate Users label on Home Page");
        CommonSteps.takeScreenshot();
    }

    public void createUser(DataTable data, boolean validUser) throws AutomationException {

        WebElement createUserButton = driverUtil.getWebElement(CREATE_USER_BUTTON);
        if (createUserButton == null)
            throw new AutomationException("Unable to locate create user button on Home Page");
        createUserButton.click();

        WebElement createUserPopup = driverUtil.getWebElement(CREATE_USER_POP_UP);
        if (createUserPopup == null)
            throw new AutomationException("Create User Pop was not displayed when user clicked Create User button");

        String todayDateTime = (new SimpleDateFormat(TIMESTAMP_FORMAT)).format(new Date());

        List<List<String>> rows = data.asLists(String.class);
        for (int i = 1; i < rows.size(); i++) {
            List<String> row = rows.get(i);
            for (int j = 0; j < row.size(); j++) {
                userDetails.put(rows.get(0).get(j), row.get(j).replace("timestamp", todayDateTime));
            }
        }

        enterFirstName(userDetails.get("firstName"));
        enterLastName(userDetails.get("lastName"));
        enterEmailAddress(userDetails.get("email"));
        enterJobTitle(userDetails.get("jobTitle"));

        if (userDetails.get("account").length() > 1) {
            selectAccountCreateUserModal(userDetails.get("account"));
        }

        if (userDetails.get("role").length() > 1) {
            selectRole(userDetails.get("role"));
        }
//
//        if (userDetails.get("status").length() > 1) {
//            selectStatus(userDetails.get("status"));
//        }

        enterCredentials(userDetails.get("credentials"));
        enterNPI(userDetails.get("npi"));
        enterIPAddress(userDetails.get("ipAddress"));
        enterPhone(userDetails.get("phone"));

        if (validUser) {
            WebElement saveBtn = driverUtil.getWebElement(CREATE_USER_SAVE_BUTTON);
            if (saveBtn == null)
                throw new AutomationException("Save button not displayed");
            saveBtn.click();
            driverUtil.waitForLoaderToDisappear();
        }

    }

    // Enter first name on Create or Edit user modal
    public void enterFirstName(String firstName) throws AutomationException {
        WebElement element = driverUtil.getWebElement(CREATE_USER_FIRST_NAME_INPUT);
        if (firstName == null)
            throw new AutomationException("First Name input box not displayed");
        element.click();
        element.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
        element.sendKeys(firstName);
    }

    // Enter last name on Create or Edit user modal
    public void enterLastName(String lastName) throws AutomationException {
        WebElement element = driverUtil.getWebElement(CREATE_USER_LAST_NAME_INPUT);
        if (element == null)
            throw new AutomationException("Last Name input box not displayed");
        element.click();
        element.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
        element.sendKeys(lastName);
    }

    // Enter emailaddress on Create user modal
    public void enterEmailAddress(String textToEnter) throws AutomationException {
        WebElement element = driverUtil.getWebElement(CREATE_USER_EMAIL_INPUT);
        if (element == null)
            throw new AutomationException("Email address input box not displayed");
        element.clear();
        element.sendKeys(textToEnter);
    }

    // Enter credentials on Create/Edit user modal
    public void enterCredentials(String textToEnter) throws AutomationException {
        WebElement element = driverUtil.getWebElement(CREATE_USER_CREDENTIALS_INPUT);
        if (element == null)
            throw new AutomationException("Credentials input box not displayed");
        element.click();
        element.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
        element.sendKeys(textToEnter);
    }

    // Enter npi on Create/Edit user modal
    public void enterNPI(String textToEnter) throws AutomationException {
        WebElement element = driverUtil.getWebElement(CREATE_USER_NPI_INPUT);
        if (element == null)
            throw new AutomationException("NPI input box not displayed");
        element.click();
        element.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
        element.sendKeys(textToEnter);
    }

    // Enter IP Address on Create/Edit user modal
    public void enterIPAddress(String textToEnter) throws AutomationException {
        WebElement element = driverUtil.getWebElement(CREATE_USER_IP_ADDRESS_INPUT);
        if (element == null)
            throw new AutomationException("IP Address input box not displayed");
        element.click();
        element.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
        element.sendKeys(textToEnter);
    }

    // Enter IP Address on Create/Edit user modal
    public void enterPhone(String textToEnter) throws AutomationException {
        WebElement element = driverUtil.getWebElement(CREATE_USER_PHONE_INPUT);
        if (element == null)
            throw new AutomationException("Phone input box not displayed");
        element.click();
        element.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
        element.sendKeys(textToEnter);
    }

    // Enter job title on Create/Edit user modal
    public void enterJobTitle(String textToEnter) throws AutomationException {
        WebElement element = driverUtil.getWebElement(CREATE_USER_JOB_TITLE_INPUT);
        if (element == null)
            throw new AutomationException("Job title input box not displayed");
        element.click();
        element.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
        element.sendKeys(textToEnter);
    }

    public void selectAccountCreateUserModal(String accountToSelect) throws AutomationException {
        WebElement accountDropdown = driverUtil.getWebElement(CREATE_USER_ORG_DROPDOWN);
        if (accountDropdown == null)
            throw new AutomationException("Account dropdown not displayed");
        accountDropdown.click();
        WebElement accountDropdownValues = driverUtil.getWebElement("//li[" + driverUtil.ignoreCase(accountToSelect) + "]", 3);
        if (accountDropdownValues == null)
            throw new AutomationException("Account :" + userDetails.get("Account") + " not found in drop down or drop down not open");
        driverUtil.clickUsingJavaScript("//li[" + driverUtil.ignoreCase(accountToSelect) + "]");
    }

    public void selectRole(String roleToSelect) throws AutomationException {
        WebElement roleDropdown = driverUtil.getWebElement(CREATE_USER_ROLE_DROPDOWN);
        if (roleDropdown == null)
            throw new AutomationException("Role dropdown not displayed");
        roleDropdown.click();
        WebElement roleDropdownValues = driverUtil.getWebElement("//li[" + driverUtil.ignoreCase(roleToSelect) + "]", 3);
        if (roleDropdownValues == null)
            throw new AutomationException("Role :" + roleToSelect + " not found in drop down or drop down not open");
        roleDropdownValues.click();
    }

    public void selectStatus(String statusToSelect) throws AutomationException {
        WebElement statusDropdown = driverUtil.getWebElement(CREATE_USER_STATUS_DROPDOWN);
        if (statusDropdown == null)
            throw new AutomationException("Status dropdown not displayed");
        statusDropdown.click();
        WebElement statusDropdownValues = driverUtil.getWebElement("//li[" + driverUtil.ignoreCase(statusToSelect) + "]", 3);
        if (statusDropdownValues == null)
            throw new AutomationException("Status :" + statusToSelect + " not found in drop down or drop down not open");
        statusDropdownValues.click();
    }

    public void verifyMessage(String messageToVerify) throws AutomationException {
        WebElement element = driverUtil.getWebElement(ALERT_MESSAGE);
        if (element == null)
            throw new AutomationException("No alert message displayed");
        String actualMessage = element.getText();
        if (!actualMessage.contains(messageToVerify)) {
            throw new AutomationException("We expected message : " + messageToVerify + " but message displayed was " + actualMessage);
        }
        CommonSteps.takeScreenshot();
        driverUtil.waitForLoaderToDisappear();
    }

    public void selectAllUserRecords() throws AutomationException {
        WebElement element = driverUtil.getElementUsingJavascript(USER_SELECT_ALL_RECORDS);
        if (element != null) {
            new Actions(DriverFactory.drivers.get()).moveToElement(element).perform();
            element.click();
        }
    }

    public void deselectAllUserRecords() throws AutomationException {
        WebElement element = driverUtil.getElementUsingJavascript(USER_DESELECT_ALL_RECORDS);
        if (element != null) {
            new Actions(DriverFactory.drivers.get()).moveToElement(element).perform();
            element.click();
        }
    }

    public void selectFirstUserCheckbox() throws AutomationException {
        WebElement element = driverUtil.getElementUsingJavascript(USER_FIRST_RECORD_CHECKBOX_SELECT);
        if (element != null) {
            new Actions(DriverFactory.drivers.get()).moveToElement(element).perform();
            element.click();
        }
    }

    public void clickOnUpdatePermissionButton() throws AutomationException {
        WebElement updatePermission = driverUtil.getWebElement(UPDATE_PERMISSION_BUTTON);
        if (updatePermission == null)
            throw new AutomationException("Update Permission button disabled");
        driverUtil.waitForElementToBeClickable(UPDATE_PERMISSION_BUTTON);
        updatePermission.click();
    }

    public void verifyEditPermissionModalDisplayed() throws AutomationException {
        WebElement element = driverUtil.findElementByText("Edit Permissions");
        if (element == null)
            throw new AutomationException("Edit Permission modal not displayed");
    }

    public void selectFirstUserOnHomePage() throws AutomationException {
        selectAllUserRecords();
        deselectAllUserRecords();
        selectFirstUserCheckbox();
    }

    public void selectAllUserOnHomePage() throws AutomationException {
        selectAllUserRecords();
    }

    public void openEditPermissionModal() throws AutomationException {
        clickOnUpdatePermissionButton();
        verifyEditPermissionModalDisplayed();
    }

    public void updateUserPermission(DataTable data, boolean saveUpdates) throws AutomationException {
        for (Map<String, String> entry : data.asMaps(String.class, String.class)) {
            String permission = entry.get("PermissionName");
            String status = entry.get("Status");
            switch (status.toUpperCase()) {
                case "ON":
                case "OFF":
                    WebElement toggle = driverUtil.getElementUsingJavascript(UserPermissionMapper.mapping.get(permission.toUpperCase()));
                    if (toggle == null)
                        throw new AutomationException(permission + " toggle not displayed");
                    String currentStatus = toggle.getAttribute("value");
                    LOGGER.info("Status of permission '" + permission + "' before change on UI is : " + currentStatus);
                    if (status.equalsIgnoreCase(currentStatus))
                        toggle.click();
                    break;
                case "READ":
                    WebElement readCheckbox = driverUtil.getElementUsingJavascript(UserPermissionMapper.mapping.get(permission.toUpperCase() + "-READ"));
                    if (readCheckbox == null)
                        throw new AutomationException(permission + " read checkbox not displayed");
                    boolean isReadCheckboxSelected = readCheckbox.getAttribute("class").contains("Mui-checked");
                    LOGGER.info("Before changes Read checkbox for " + permission + " is selected : " + isReadCheckboxSelected);
                    if (!isReadCheckboxSelected)
                        readCheckbox.click();

                    WebElement writeCheckbox = driverUtil.getElementUsingJavascript(UserPermissionMapper.mapping.get(permission.toUpperCase() + "-WRITE"));
                    if (writeCheckbox == null)
                        throw new AutomationException(permission + " write checkbox not displayed");
                    boolean isWriteCheckboxSelected = writeCheckbox.getAttribute("class").contains("Mui-checked");
                    LOGGER.info("Before changes Write checkbox for " + permission + " is selected : " + isWriteCheckboxSelected);
                    if (isWriteCheckboxSelected)
                        writeCheckbox.click();
                    break;
                case "WRITE":

                    writeCheckbox = driverUtil.getElementUsingJavascript(UserPermissionMapper.mapping.get(permission.toUpperCase() + "-WRITE"));
                    if (writeCheckbox == null)
                        throw new AutomationException(permission + " write checkbox not displayed");
                    isWriteCheckboxSelected = writeCheckbox.getAttribute("class").contains("Mui-checked");
                    LOGGER.info("Before changes Write checkbox for " + permission + " is selected : " + isWriteCheckboxSelected);
                    if (!isWriteCheckboxSelected)
                        writeCheckbox.click();

                    readCheckbox = driverUtil.getWebElement(UserPermissionMapper.mapping.get(permission.toUpperCase() + "-READ"));
                    if (readCheckbox == null)
                        throw new AutomationException(permission + " read checkbox not displayed");
                    isReadCheckboxSelected = readCheckbox.getAttribute("class").contains("Mui-checked");
                    LOGGER.info("Before changes Read checkbox for " + permission + " is selected : " + isReadCheckboxSelected);
                    if (isReadCheckboxSelected)
                        readCheckbox.click();
                    break;

                case "READ/WRITE":
                    readCheckbox = driverUtil.getElementUsingJavascript(UserPermissionMapper.mapping.get(permission.toUpperCase() + "-READ"));
                    if (readCheckbox == null)
                        throw new AutomationException(permission + " read checkbox not displayed");
                    isReadCheckboxSelected = readCheckbox.getAttribute("class").contains("Mui-checked");
                    LOGGER.info("Before changes Read checkbox for " + permission + " is selected : " + isReadCheckboxSelected);
                    if (!isReadCheckboxSelected)
                        readCheckbox.click();

                    writeCheckbox = driverUtil.getElementUsingJavascript(UserPermissionMapper.mapping.get(permission.toUpperCase() + "-WRITE"));
                    if (writeCheckbox == null)
                        throw new AutomationException(permission + " write checkbox not displayed");
                    isWriteCheckboxSelected = writeCheckbox.getAttribute("class").contains("Mui-checked");
                    LOGGER.info("Before changes Write checkbox for " + permission + " is selected : " + isWriteCheckboxSelected);
                    if (!isWriteCheckboxSelected)
                        writeCheckbox.click();
                    break;

                case "":
                    readCheckbox = driverUtil.getElementUsingJavascript(UserPermissionMapper.mapping.get(permission.toUpperCase() + "-READ"));
                    if (readCheckbox == null)
                        throw new AutomationException(permission + " read checkbox not displayed");
                    isReadCheckboxSelected = readCheckbox.getAttribute("class").contains("Mui-checked");
                    if (isReadCheckboxSelected)
                        readCheckbox.click();

                    writeCheckbox = driverUtil.getElementUsingJavascript(UserPermissionMapper.mapping.get(permission.toUpperCase() + "-WRITE"));
                    if (writeCheckbox == null)
                        throw new AutomationException(permission + " write checkbox not displayed");
                    isWriteCheckboxSelected = writeCheckbox.getAttribute("class").contains("Mui-checked");
                    if (isWriteCheckboxSelected)
                        writeCheckbox.click();
                    break;
            }
        }
        if (saveUpdates) {
            clickOnButton("Update");
            verifyMessage("Permissions updated successfully");
        } else {
            clickOnButton("Cancel");
        }
    }

    public void verifyPermission(DataTable data) throws AutomationException {
        for (Map<String, String> entry : data.asMaps(String.class, String.class)) {
            String permission = entry.get("PermissionName");
            String status = entry.get("Status");
            switch (status.toUpperCase()) {
                case "ON":
                case "OFF":
                    WebElement toggle = driverUtil.getElementUsingJavascript(UserPermissionMapper.mapping.get(permission.toUpperCase()));
                    if (toggle == null)
                        throw new AutomationException(permission + " toggle not displayed");
                    String currentStatus = toggle.getAttribute("value").trim();
                    if (status.trim().equalsIgnoreCase(currentStatus))
                        throw new AutomationException("We expected status of permission '" + permission + "' to be " + status);
                    toggle = driverUtil.getElementUsingJavascript(UserPermissionMapper.mapping.get(permission.toUpperCase()) + "/../..");
                    String value = toggle.getAttribute("aria-disabled");
                    if (value.equalsIgnoreCase("true"))
                        throw new AutomationException(permission + " toggle is probably disabled but we expected it to be enabled");
                    break;

                case "TOGGLE-DISABLED":
                    toggle = driverUtil.getElementUsingJavascript(UserPermissionMapper.mapping.get(permission.toUpperCase()) + "/../..");
                    if (toggle == null)
                        throw new AutomationException(permission + " toggle not displayed");
                    value = toggle.getAttribute("aria-disabled");
                    if (!value.equalsIgnoreCase("true"))
                        throw new AutomationException(permission + " toggle is probably enabled but we expected it to be disabled");
                    break;

                case "READ":
                    WebElement readCheckbox = driverUtil.getElementUsingJavascript(UserPermissionMapper.mapping.get(permission.toUpperCase() + "-READ"));
                    if (readCheckbox == null)
                        throw new AutomationException(permission + " read checkbox not displayed");
                    boolean isReadCheckboxSelected = readCheckbox.getAttribute("class").contains("Mui-checked");
                    if (!isReadCheckboxSelected)
                        throw new AutomationException("We expected read checkbox of permission '" + permission + "' should be checked but found unchecked");
                    value = readCheckbox.getAttribute("aria-disabled");
                    if (value.equalsIgnoreCase("true"))
                        throw new AutomationException(permission + " read checkbox is disabled but we expected it to be enabled");

                    WebElement writeCheckbox = driverUtil.getElementUsingJavascript(UserPermissionMapper.mapping.get(permission.toUpperCase() + "-WRITE"));
                    if (writeCheckbox == null)
                        throw new AutomationException(permission + " write checkbox not displayed");
                    boolean isWriteCheckboxSelected = writeCheckbox.getAttribute("class").contains("Mui-checked");
                    LOGGER.info("Before changes Write checkbox for " + permission + " is selected : " + isWriteCheckboxSelected);
                    if (isWriteCheckboxSelected)
                        throw new AutomationException("We expected write checkbox of permission '" + permission + "' should be unchecked but found checked");
                    value = readCheckbox.getAttribute("aria-disabled");
                    if (value.equalsIgnoreCase("true"))
                        throw new AutomationException(permission + " write checkbox is disabled but we expected it to be enabled");
                    break;

                case "WRITE":
                    writeCheckbox = driverUtil.getElementUsingJavascript(UserPermissionMapper.mapping.get(permission.toUpperCase() + "-WRITE"));
                    if (writeCheckbox == null)
                        throw new AutomationException(permission + " write checkbox not displayed");
                    isWriteCheckboxSelected = writeCheckbox.getAttribute("class").contains("Mui-checked");
                    LOGGER.info("Before changes Write checkbox for " + permission + " is selected : " + isWriteCheckboxSelected);
                    if (!isWriteCheckboxSelected)
                        throw new AutomationException("We expected write checkbox of permission '" + permission + "' should be checked but found unchecked");
                    value = writeCheckbox.getAttribute("aria-disabled");
                    if (value.equalsIgnoreCase("true"))
                        throw new AutomationException(permission + " write checkbox is disabled but we expected it to be enabled");

                    readCheckbox = driverUtil.getElementUsingJavascript(UserPermissionMapper.mapping.get(permission.toUpperCase() + "-READ"));
                    if (readCheckbox == null)
                        throw new AutomationException(permission + " read checkbox not displayed");
                    isReadCheckboxSelected = readCheckbox.getAttribute("class").contains("Mui-checked");
                    if (isReadCheckboxSelected)
                        throw new AutomationException("We expected read checkbox of permission '" + permission + "' should be unchecked but found checked");
                    value = readCheckbox.getAttribute("aria-disabled");
                    if (value.equalsIgnoreCase("true"))
                        throw new AutomationException(permission + " read checkbox is disabled but we expected it to be enabled");
                    break;

                case "READ/WRITE":
                    readCheckbox = driverUtil.getElementUsingJavascript(UserPermissionMapper.mapping.get(permission.toUpperCase() + "-READ"));
                    if (readCheckbox == null)
                        throw new AutomationException(permission + " read checkbox not displayed");
                    isReadCheckboxSelected = readCheckbox.getAttribute("class").contains("Mui-checked");
                    if (!isReadCheckboxSelected)
                        throw new AutomationException("We expected read checkbox of permission '" + permission + "' should be checked but found unchecked");

                    writeCheckbox = driverUtil.getElementUsingJavascript(UserPermissionMapper.mapping.get(permission.toUpperCase() + "-WRITE"));
                    if (writeCheckbox == null)
                        throw new AutomationException(permission + " write checkbox not displayed");
                    isWriteCheckboxSelected = writeCheckbox.getAttribute("class").contains("Mui-checked");
                    if (!isWriteCheckboxSelected)
                        throw new AutomationException("We expected write checkbox of permission '" + permission + "' should be checked but found unchecked");
                    break;

                case "READ-DISABLED":
                    readCheckbox = driverUtil.getElementUsingJavascript(UserPermissionMapper.mapping.get(permission.toUpperCase() + "-READ"));
                    if (readCheckbox == null)
                        throw new AutomationException(permission + " read checkbox not displayed");
                    value = readCheckbox.getAttribute("aria-disabled");
                    if (!value.equalsIgnoreCase("true"))
                        throw new AutomationException(permission + " read checkbox is enabled but we expected it to be disabled");
                    break;

                case "WRITE-DISABLED":
                    writeCheckbox = driverUtil.getElementUsingJavascript(UserPermissionMapper.mapping.get(permission.toUpperCase() + "-WRITE"));
                    if (writeCheckbox == null)
                        throw new AutomationException(permission + " read checkbox not displayed");
                    value = writeCheckbox.getAttribute("aria-disabled");
                    if (!value.equalsIgnoreCase("true"))
                        throw new AutomationException(permission + " write checkbox is enabled but we expected it to be disabled");
                    break;

                case "":
                    readCheckbox = driverUtil.getElementUsingJavascript(UserPermissionMapper.mapping.get(permission.toUpperCase() + "-READ"));
                    if (readCheckbox == null)
                        throw new AutomationException(permission + " read checkbox not displayed");
                    isReadCheckboxSelected = readCheckbox.getAttribute("class").contains("Mui-checked");
                    if (isReadCheckboxSelected)
                        throw new AutomationException("We expected read checkbox of permission '" + permission + "' should be unchecked but found checked");
                    value = readCheckbox.getAttribute("aria-disabled");
                    if (value.equalsIgnoreCase("true"))
                        throw new AutomationException(permission + " read checkbox is disabled but we expected it to be enabled");

                    writeCheckbox = driverUtil.getElementUsingJavascript(UserPermissionMapper.mapping.get(permission.toUpperCase() + "-WRITE"));
                    if (writeCheckbox == null)
                        throw new AutomationException(permission + " write checkbox not displayed");
                    isWriteCheckboxSelected = writeCheckbox.getAttribute("class").contains("Mui-checked");
                    LOGGER.info("Before changes Write checkbox for " + permission + " is selected : " + isWriteCheckboxSelected);
                    if (isWriteCheckboxSelected)
                        throw new AutomationException("We expected write checkbox of permission '" + permission + "' should be unchecked but found checked");
                    value = readCheckbox.getAttribute("aria-disabled");
                    if (value.equalsIgnoreCase("true"))
                        throw new AutomationException(permission + " write checkbox is disabled but we expected it to be enabled");
                    break;
            }
        }
    }

    public void verifyText(String textToVerify) throws AutomationException {
        WebElement element = driverUtil.findElementByText(textToVerify);
        if (element == null)
            throw new AutomationException("Expected text is not displayed : " + textToVerify);
        CommonSteps.takeScreenshot();
    }

    public void verifyColumnData(String columnName, String searchString) throws AutomationException {
        switch (columnName.toLowerCase()) {
            case "firstname":
                List<WebElement> webElements = driverUtil.getWebElements(USER_SEARCH_RESULT_NAME_COLUMN);
                if (webElements.size() == 0)
                    throw new AutomationException("We expected " + columnName + " column's to have searched string : " + searchString);
                for (WebElement element : webElements
                ) {
                    if (!element.getText().toLowerCase().contains(searchString.toLowerCase()))
                        throw new AutomationException("We expected firstName  column to have searched string but found " + element.getText());
                }
                break;
            case "lastname":
                webElements = driverUtil.getWebElements(USER_SEARCH_RESULT_LAST_NAME_COLUMN);
                if (webElements.size() == 0)
                    throw new AutomationException("We expected " + columnName + " column's to have searched string : " + searchString);
                for (WebElement element : webElements
                ) {
                    if (!element.getText().toLowerCase().contains(searchString.toLowerCase()))
                        throw new AutomationException("We expected lastname column to have searched string but found " + element.getText());
                }
                break;
            case "email":
                webElements = driverUtil.getWebElements(USER_SEARCH_RESULT_EMAIL_COLUMN);
                if (webElements.size() == 0)
                    throw new AutomationException("We expected " + columnName + " column's to have searched string : " + searchString);
                for (WebElement element : webElements
                ) {
                    if (!element.getText().toLowerCase().contains(searchString.toLowerCase()))
                        throw new AutomationException("We expected email column to have searched string but found " + element.getText());
                }
                break;
            case "account":
                webElements = driverUtil.getWebElements(USER_SEARCH_RESULT_ACCOUNT_COLUMN);
                if (webElements.size() == 0)
                    throw new AutomationException("We expected " + columnName + " column's to have searched string : " + searchString);
                for (WebElement element : webElements
                ) {
                    if (!element.getText().toLowerCase().contains(searchString.toLowerCase()))
                        throw new AutomationException("We expected account column to have searched string but found " + element.getText());
                }
                break;
            case "role":
                webElements = driverUtil.getWebElements(USER_SEARCH_RESULT_ROLE_COLUMN);
                if (webElements.size() == 0)
                    throw new AutomationException("We expected " + columnName + " column's to have searched string : " + searchString);
                for (WebElement element : webElements
                ) {
                    if (!element.getText().toLowerCase().contains(searchString.toLowerCase()))
                        throw new AutomationException("We expected role column to have searched string but found " + element.getText());
                }
                break;
            case "status":
                webElements = driverUtil.getWebElements(USER_SEARCH_RESULT_STATUS_COLUMN);
                if (webElements.size() == 0)
                    throw new AutomationException("We expected " + columnName + " column's to have searched string : " + searchString);
                for (WebElement element : webElements
                ) {
                    if (!element.getText().toLowerCase().contains(searchString.toLowerCase()))
                        throw new AutomationException("We expected status column to have searched string but found " + element.getText());
                }
                break;
            default:
                throw new AutomationException("Invalid Column Name. Valid options are FirstName, EmailAddress, Account,Role or Status");
        }
    }

    public void verifyButtonIsDisabled(String buttonToVerify) throws AutomationException {
        WebElement element = driverUtil.getWebElement("//span[" + driverUtil.ignoreCase(buttonToVerify) + "]/parent::button[contains(@class,'Mui-disabled')]");
        if (element == null)
            throw new AutomationException(buttonToVerify + " Button is enabled");
        CommonSteps.takeScreenshot();
    }

    public void verifyButtonIsEnabled(String buttonToVerify) throws AutomationException {
        WebElement element = driverUtil.getWebElement("//span[" + driverUtil.ignoreCase(buttonToVerify) + "]/parent::button[not(contains(@class,'Mui-disabled'))]");
        if (element == null)
            throw new AutomationException(buttonToVerify + " Button is disabled");
        CommonSteps.takeScreenshot();
    }

    public void verifyTextboxIsDisabled(String textBoxToVerify) throws AutomationException {
        WebElement element = driverUtil.getWebElement("//label[" + driverUtil.containsIgnoreCase(textBoxToVerify) + " and contains(@class,'Mui-disabled')]");
        if (element == null)
            throw new AutomationException(textBoxToVerify + " textbox is enabled");
        CommonSteps.takeScreenshot();
    }

    public void verifyTextboxIsEnabled(String textBoxToVerify) throws AutomationException {
        WebElement element = driverUtil.getWebElement("//label[" + driverUtil.containsIgnoreCase(textBoxToVerify) + " and not(contains(@class,'Mui-disabled'))]");
        if (element == null)
            throw new AutomationException(textBoxToVerify + " textbox is disabled");
        CommonSteps.takeScreenshot();
    }

    public void pageRefresh() throws AutomationException {
        DriverFactory.drivers.get().navigate().refresh();
        driverUtil.waitForLoaderToDisappear();
    }

    public void clickOnButton(String buttonToClick) throws AutomationException {
        WebElement btn = driverUtil.getWebElement("//*[" + driverUtil.ignoreCase(buttonToClick) + "]/parent::button[not(contains(@class,'Mui-disabled'))]");
        if (btn == null)
            throw new AutomationException("Unable to locate " + buttonToClick + " button or it is disabled state");
        btn.click();
        driverUtil.waitForLoaderToDisappear();
    }

    public void clickOnButtonAndWaitForMessage(String buttonToClick, String message) throws AutomationException {
        WebElement btn = driverUtil.getWebElement("//span[" + driverUtil.ignoreCase(buttonToClick) + "]/parent::button[not(contains(@class,'Mui-disabled'))]");
        if (btn == null)
            throw new AutomationException("Unable to locate " + buttonToClick + " button or it is disabled state");
        btn.click();
        verifyMessage(message);
        CommonSteps.takeScreenshot();
        driverUtil.waitForLoaderToDisappear();
    }

    public void searchCreatedUserWithEmail() throws AutomationException {
        searchUserWithEmail(userDetails.get("email"));
        clickOnSearchButton();
        verifyColumnData("firstName", userDetails.get("firstName"));
        verifyColumnData("lastName", userDetails.get("lastName"));
        verifyColumnData("email", userDetails.get("email"));
        verifyColumnData("role", userDetails.get("role"));
    }

    public void openEditUserModal() throws AutomationException {
        WebElement element = driverUtil.findElement("//*[@role='rowgroup']/div[@row-id='0']/div[@col-id='id']");
        if (element == null)
            throw new AutomationException("Edit icon not displayed for user records on home page");
        element.click();
    }

    public void editUser(DataTable data, boolean validUser) throws AutomationException {

        WebElement editUserPopup = driverUtil.getWebElement(EDIT_USER_POP_UP);
        if (editUserPopup == null)
            throw new AutomationException("Edit User Pop was not displayed");

        String todayDateTime = (new SimpleDateFormat(TIMESTAMP_FORMAT)).format(new Date());

        List<List<String>> rows = data.asLists(String.class);
        for (int i = 1; i < rows.size(); i++) {
            List<String> row = rows.get(i);
            for (int j = 0; j < row.size(); j++) {
                userDetails.put(rows.get(0).get(j), row.get(j).replace("timestamp", todayDateTime));
            }
        }

        enterFirstName(userDetails.get("firstName"));
        enterLastName(userDetails.get("lastName"));
        enterJobTitle(userDetails.get("jobTitle"));

        if (userDetails.get("account").length() > 1) {
            selectAccountCreateUserModal(userDetails.get("account"));
        }

        if (userDetails.get("role").length() > 1) {
            selectRole(userDetails.get("role"));
        }

        if (userDetails.get("status").length() > 1) {
            selectStatus(userDetails.get("status"));
        }
        if (userDetails.get("role").equalsIgnoreCase("Pharmacist")) {
            enterCredentials(userDetails.get("credentials"));
            enterNPI(userDetails.get("npi"));
        }
        enterIPAddress(userDetails.get("ipAddress"));
        userDetails.put("email", driverUtil.findElement(CREATE_USER_EMAIL_INPUT).getText());
        //enterPhone(userDetails.get("phone"));
        if (validUser) {
            WebElement editBtn = driverUtil.getWebElement(EDIT_USER_UPDATE_BUTTON);
            if (editBtn == null)
                throw new AutomationException("Save button not displayed");
            editBtn.click();
            //driverUtil.waitForLoaderToDisappear();
        } else
            verifyButtonIsDisabled("Update");
    }

    public List<String> getFirstNamesFromSearchResult() throws AutomationException {
        WebElement rowCount = driverUtil.getWebElement(USER_SEARCH_RECORD_COUNT);
        if (rowCount == null)
            throw new AutomationException("Total User record count not displayed or page is taking long time to load");
        int noOfRecords = Integer.parseInt(rowCount.getText());
        List<String> firstNames = new ArrayList<>();
        for (int i = 2; i <= noOfRecords + 1 && i <= 26; i++) {
            WebElement element = driverUtil.findElementAndScroll("//div[@aria-rowindex='" + i + "']/div[@col-id='firstName' and not(@role='columnheader')]");
            firstNames.add(element.getText().trim().toLowerCase());
        }
        return firstNames;
    }

    public List<String> getEmailFromSearchResult() throws AutomationException {
        WebElement rowCount = driverUtil.getWebElement(USER_SEARCH_RECORD_COUNT);
        if (rowCount == null)
            throw new AutomationException("Total User record count not displayed or page is taking long time to load");
        int noOfRecords = Integer.parseInt(rowCount.getText());
        List<String> emailList = new ArrayList<>();
        for (int i = 2; i <= noOfRecords + 1 && i <= 26; i++) {
            WebElement element = driverUtil.findElementAndScroll("//div[@aria-rowindex='" + i + "']/div[@col-id='email' and not(@role='columnheader')]");
            emailList.add(element.getText().trim().toLowerCase());
        }
        return emailList;
    }

    public List<String> getRoleFromSearchResult() throws AutomationException {
        WebElement rowCount = driverUtil.getWebElement(USER_SEARCH_RECORD_COUNT);
        if (rowCount == null)
            throw new AutomationException("Total User record count not displayed or page is taking long time to load");
        int noOfRecords = Integer.parseInt(rowCount.getText());
        List<String> roleList = new ArrayList<>();
        for (int i = 2; i <= noOfRecords + 1 && i <= 26; i++) {
            WebElement element = driverUtil.findElementAndScroll("//div[@aria-rowindex='" + i + "']/div[@col-id='role' and not(@role='columnheader')]");
            roleList.add(element.getText().trim().toLowerCase());
        }
        return roleList;
    }

    public List<String> getCreatedDateFromSearchResult() throws AutomationException {
        WebElement rowCount = driverUtil.getWebElement(USER_SEARCH_RECORD_COUNT);
        if (rowCount == null)
            throw new AutomationException("Total User record count not displayed or page is taking long time to load");
        int noOfRecords = Integer.parseInt(rowCount.getText());
        List<String> createdDateList = new ArrayList<>();
        for (int i = 2; i <= noOfRecords + 1 && i <= 26; i++) {
            WebElement element = driverUtil.findElementAndScroll("//div[@aria-rowindex='" + i + "']/div[@col-id='created' and not(@role='columnheader')]");
            createdDateList.add(element.getText().trim().toLowerCase());
        }
        return createdDateList;
    }

    public List<String> getLastLoginFromSearchResult() throws AutomationException {
        WebElement rowCount = driverUtil.getWebElement(USER_SEARCH_RECORD_COUNT);
        if (rowCount == null)
            throw new AutomationException("Total User record count not displayed or page is taking long time to load");
        int noOfRecords = Integer.parseInt(rowCount.getText());
        List<String> lastLoginDateList = new ArrayList<>();
        for (int i = 2; i <= noOfRecords + 1 && i <= 26; i++) {
            WebElement element = driverUtil.findElementAndScroll("//div[@aria-rowindex='" + i + "']/div[@col-id='lastSuccessfulLogin' and not(@role='columnheader')]");
            lastLoginDateList.add(element.getText().trim().toLowerCase());
        }
        return lastLoginDateList;
    }

    public List<String> getDisableDateFromSearchResult() throws AutomationException {
        WebElement rowCount = driverUtil.getWebElement(USER_SEARCH_RECORD_COUNT);
        if (rowCount == null)
            throw new AutomationException("Total User record count not displayed or page is taking long time to load");
        int noOfRecords = Integer.parseInt(rowCount.getText());
        List<String> disabledDateList = new ArrayList<>();
        for (int i = 2; i <= noOfRecords + 1 && i <= 26; i++) {
            WebElement element = driverUtil.findElementAndScroll("//div[@aria-rowindex='" + i + "']/div[@col-id='disabledDate' and not(@role='columnheader')]");
            disabledDateList.add(element.getText().trim().toLowerCase());
        }
        return disabledDateList;
    }

    public List<String> getStatusFromSearchResult() throws AutomationException {
        WebElement rowCount = driverUtil.getWebElement(USER_SEARCH_RECORD_COUNT);
        if (rowCount == null)
            throw new AutomationException("Total User record count not displayed or page is taking long time to load");
        int noOfRecords = Integer.parseInt(rowCount.getText());
        List<String> statusList = new ArrayList<>();
        for (int i = 2; i <= noOfRecords + 1 && i <= 26; i++) {
            WebElement element = driverUtil.findElementAndScroll("//div[@aria-rowindex='" + i + "']/div[@col-id='status' and not(@role='columnheader')]");
            statusList.add(element.getText().trim().toLowerCase());
        }
        return statusList;
    }

    public List<String> getAccountFromSearchResult() throws AutomationException {
        WebElement rowCount = driverUtil.getWebElement(USER_SEARCH_RECORD_COUNT);
        if (rowCount == null)
            throw new AutomationException("Total User record count not displayed or page is taking long time to load");
        int noOfRecords = Integer.parseInt(rowCount.getText());
        List<String> accountList = new ArrayList<>();
        for (int i = 2; i <= noOfRecords + 1 && i <= 26; i++) {
            WebElement element = driverUtil.findElementAndScroll("//div[@aria-rowindex='" + i + "']/div[@col-id='account' and not(@role='columnheader')]");
            accountList.add(element.getText().trim().toLowerCase());
        }
        return accountList;
    }

    public void sortColumn(String sortType, String columnNameToSort) throws AutomationException {
        boolean ordered;
        WebElement element;
        if (sortType.equalsIgnoreCase("Ascending")) {
            element = driverUtil.findElement("//*[" + driverUtil.ignoreCase(columnNameToSort) + "]/ancestor::div[contains(@class,'sorted-asc')]");
            if (element == null) {
                driverUtil.findElement("//*[" + driverUtil.ignoreCase(columnNameToSort) + " and contains(@class,'header')]").click();
                driverUtil.waitForLoaderToDisappear();
                element = driverUtil.findElement("//*[" + driverUtil.ignoreCase(columnNameToSort) + "]/ancestor::div[contains(@class,'sorted-asc')]");
                if (element == null) {
                    driverUtil.findElement("//*[" + driverUtil.ignoreCase(columnNameToSort) + " and contains(@class,'header')]").click();
                    driverUtil.waitForLoaderToDisappear();
                }
            }
        } else {
            element = driverUtil.findElement("//*[" + driverUtil.ignoreCase(columnNameToSort) + "]/ancestor::div[contains(@class,'sorted-desc')]");
            if (element == null) {
                driverUtil.findElement("//*[" + driverUtil.ignoreCase(columnNameToSort) + " and contains(@class,'header')]").click();
                driverUtil.waitForLoaderToDisappear();
                element = driverUtil.findElement("//*[" + driverUtil.ignoreCase(columnNameToSort) + "]/ancestor::div[contains(@class,'sorted-desc')]");
                if (element == null) {
                    driverUtil.findElement("//*[" + driverUtil.ignoreCase(columnNameToSort) + " and contains(@class,'header')]").click();
                    driverUtil.waitForLoaderToDisappear();
                }
            }
        }
        CommonSteps.takeScreenshot();
        switch (columnNameToSort.toUpperCase()) {
            case "FIRST NAME":
                if (sortType.equalsIgnoreCase("ASCENDING")) {
                    ordered = Ordering.natural().isOrdered(getFirstNamesFromSearchResult());
                    if (!ordered)
                        throw new AutomationException(columnNameToSort + " was expected to be sorted in ascending order but its not");
                } else {
                    ordered = Ordering.natural().reverse().isOrdered(getFirstNamesFromSearchResult());
                    if (!ordered)
                        throw new AutomationException(columnNameToSort + " was expected to be sorted in descending order but its not");
                }
                break;
            case "EMAIL":
                if (sortType.equalsIgnoreCase("ASCENDING")) {
                    ordered = Ordering.natural().isOrdered(getEmailFromSearchResult());
                    if (!ordered)
                        throw new AutomationException(columnNameToSort + " was expected to be sorted in ascending order but its not");
                } else {
                    ordered = Ordering.natural().reverse().isOrdered(getEmailFromSearchResult());
                    if (!ordered)
                        throw new AutomationException(columnNameToSort + " was expected to be sorted in descending order but its not");
                }
                break;
            case "ROLE":
                if (sortType.equalsIgnoreCase("ASCENDING")) {
                    ordered = Ordering.natural().isOrdered(getRoleFromSearchResult());
                    if (!ordered)
                        throw new AutomationException(columnNameToSort + " was expected to be sorted in ascending order but its not");
                } else {
                    ordered = Ordering.natural().reverse().isOrdered(getRoleFromSearchResult());
                    if (!ordered)
                        throw new AutomationException(columnNameToSort + " was expected to be sorted in descending order but its not");
                }
                break;
            case "CREATED DATE":
                if (sortType.equalsIgnoreCase("ASCENDING")) {
                    ordered = Ordering.natural().isOrdered(getCreatedDateFromSearchResult());
                    if (!ordered)
                        throw new AutomationException(columnNameToSort + " was expected to be sorted in ascending order but its not");
                } else {
                    ordered = Ordering.natural().reverse().isOrdered(getCreatedDateFromSearchResult());
                    if (!ordered)
                        throw new AutomationException(columnNameToSort + " was expected to be sorted in descending order but its not");
                }
                break;
            case "LAST LOGIN":
                if (sortType.equalsIgnoreCase("ASCENDING")) {
                    ordered = Ordering.natural().isOrdered(getLastLoginFromSearchResult());
                    if (!ordered)
                        throw new AutomationException(columnNameToSort + " was expected to be sorted in ascending order but its not");
                } else {
                    ordered = Ordering.natural().reverse().isOrdered(getLastLoginFromSearchResult());
                    if (!ordered)
                        throw new AutomationException(columnNameToSort + " was expected to be sorted in descending order but its not");
                }
                break;
            case "DISABLE DATE":
                if (sortType.equalsIgnoreCase("ASCENDING")) {
                    ordered = Ordering.natural().isOrdered(getDisableDateFromSearchResult());
                    if (!ordered)
                        throw new AutomationException(columnNameToSort + " was expected to be sorted in ascending order but its not");
                } else {
                    ordered = Ordering.natural().reverse().isOrdered(getDisableDateFromSearchResult());
                    if (!ordered)
                        throw new AutomationException(columnNameToSort + " was expected to be sorted in descending order but its not");
                }
                break;
            case "STATUS":
                if (sortType.equalsIgnoreCase("ASCENDING")) {
                    ordered = Ordering.natural().isOrdered(getStatusFromSearchResult());
                    if (!ordered)
                        throw new AutomationException(columnNameToSort + " was expected to be sorted in ascending order but its not");
                } else {
                    ordered = Ordering.natural().reverse().isOrdered(getStatusFromSearchResult());
                    if (!ordered)
                        throw new AutomationException(columnNameToSort + " was expected to be sorted in descending order but its not");
                }
                break;
            case "ACCOUNT(S)":
                if (sortType.equalsIgnoreCase("ASCENDING")) {
                    ordered = Ordering.natural().isOrdered(getAccountFromSearchResult());
                    if (!ordered)
                        throw new AutomationException(columnNameToSort + " was expected to be sorted in ascending order but its not");
                } else {
                    ordered = Ordering.natural().reverse().isOrdered(getAccountFromSearchResult());
                    if (!ordered)
                        throw new AutomationException(columnNameToSort + " was expected to be sorted in descending order but its not");
                }
                break;

            default:
                throw new AutomationException(String.format("No column name found with %s, Please send valid column name!", columnNameToSort));
        }
        pageRefresh();
        driverUtil.scrollPageUp();
    }

    public void verifyTextboxIsReadOnly(String textboxLabel, String textboxValue) throws AutomationException {
        Format f = new SimpleDateFormat("MM/dd/yy");
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        String currentdate = f.format(calendar.getTime());
        textboxValue = textboxValue.replace("currentdate", currentdate);
        String locator = "//*[@title='" + textboxLabel + "']//input[@readonly]";
        WebElement element = driverUtil.getWebElement(locator);
        if (element == null)
            throw new AutomationException(textboxLabel + " textbox is not readonly");
        String actualTextboxValue = element.getAttribute("value");
        driverUtil.getWebElementAndScroll(locator);
        if (!textboxValue.equals(actualTextboxValue))
            throw new AutomationException("We expected " + textboxLabel + "should be set to : " + textboxValue + " but found " + actualTextboxValue);
        CommonSteps.takeScreenshot();
    }

    public void verifySearchResultsAreDisplayed() throws AutomationException {
        WebElement element = driverUtil.findElement(USER_SEARCH_RESULTS);
        if (element == null) {
            WebElement alertMessage = driverUtil.findElement(ALERT_MESSAGE);
            if (alertMessage == null)
                throw new AutomationException("We expected user search results to be displayed on home page");
            else
                throw new AutomationException("We expected user search results to be displayed on home page but getting error : '" + alertMessage.getText() + "'");
        }
        CommonSteps.takeScreenshot();
    }

    public void editUserField(String fieldNameToUpdate, String value) throws AutomationException {
        if (fieldNameToUpdate.equalsIgnoreCase("status"))
            selectStatus(value);
    }

    public void clickOnPaginationNextPage() throws AutomationException {
        WebElement nextPage = driverUtil.getWebElement(USER_SEARCH_PAGINATION_NEXT);
        if (nextPage == null)
            throw new AutomationException("We were expecting pagination for next page to be enabled");
        nextPage.click();
    }

    public void clickOnPaginationPreviousPage() throws AutomationException {
        WebElement nextPage = driverUtil.getWebElement(USER_SEARCH_PAGINATION_PREVIOUS);
        if (nextPage == null)
            throw new AutomationException("We were expecting pagination for next page to be enabled");
        nextPage.click();
    }

    public void clickOnPaginationLastPage() throws AutomationException {
        WebElement nextPage = driverUtil.getWebElement(USER_SEARCH_PAGINATION_LAST);
        if (nextPage == null)
            throw new AutomationException("We were expecting pagination for next page to be enabled");
        nextPage.click();
    }

    public void clickOnPaginationFirstPage() throws AutomationException {
        WebElement nextPage = driverUtil.getWebElement(USER_SEARCH_PAGINATION_FIRST);
        if (nextPage == null)
            throw new AutomationException("We were expecting pagination for next page to be enabled");
        nextPage.click();
    }

    public int getCurrentPageOfPagination() throws AutomationException {
        WebElement currentPage = driverUtil.getWebElement(USER_SEARCH_PAGINATION_CURRENT_PAGE);
        if (currentPage == null)
            throw new AutomationException("Current page not displayed in pagination");
        int currentPageNumber = Integer.parseInt(currentPage.getText());
        return currentPageNumber;
    }

    public int getNoOfPagesOfPagination() throws AutomationException {
        WebElement noOfPages = driverUtil.getWebElement(USER_SEARCH_PAGINATION_TOTAL_PAGE_COUNT);
        if (noOfPages == null)
            throw new AutomationException("Number of pages not displayed in pagination");
        int totalNoOfPages = Integer.parseInt(noOfPages.getText());
        return totalNoOfPages;
    }

    public void verifyPagination() throws AutomationException {
        WebElement element = driverUtil.findElementAndScroll(USER_SEARCH_RECORD_COUNT);
        int currentPageNumber;
        int lastPageNumber;
        if (element == null)
            throw new AutomationException("No of records not displayed in pagination");
        int noOfRecords = Integer.parseInt(element.getText());
        if (noOfRecords > 25) {
            clickOnPaginationNextPage();
            currentPageNumber = getCurrentPageOfPagination();
            if (currentPageNumber != 2)
                throw new AutomationException("We were expecting to be on page '2' but current page displayed is '" + currentPageNumber + "'");
            clickOnPaginationPreviousPage();
            currentPageNumber = getCurrentPageOfPagination();
            if (currentPageNumber != 1)
                throw new AutomationException("We were expecting to be on page '1' but current page displayed is '" + currentPageNumber + "'");
            clickOnPaginationLastPage();
            currentPageNumber = getCurrentPageOfPagination();
            lastPageNumber = getNoOfPagesOfPagination();
            if (currentPageNumber != lastPageNumber)
                throw new AutomationException("We were expecting to be on page '" + lastPageNumber + "' but current page displayed is '" + currentPageNumber + "'");
            clickOnPaginationFirstPage();
            currentPageNumber = getCurrentPageOfPagination();
            if (currentPageNumber != 1)
                throw new AutomationException("We were expecting to be on page '1' but current page displayed is '" + currentPageNumber + "'");
        }
    }

    public void verifySearchBoxIsCleared(String searchBoxName) throws AutomationException {
        WebElement element;
        String value;
        if (searchBoxName.equalsIgnoreCase("firstName")) {
            element = driverUtil.getWebElement(USER_SEARCH_NAME_INPUT);
            if (element == null)
                throw new AutomationException("First Name search input box not displayed");
            value = element.getAttribute("value");
            if (!value.equalsIgnoreCase(""))
                throw new AutomationException("First Name search input box has value :" + value);
        } else if (searchBoxName.equalsIgnoreCase("email")) {
            element = driverUtil.getWebElement(USER_SEARCH_EMAIL_INPUT);
            if (element == null)
                throw new AutomationException("First Name search input box not displayed");
            value = element.getAttribute("value");
            if (!value.equalsIgnoreCase(""))
                throw new AutomationException("First Name search input box has value :" + value);
        } else if (searchBoxName.equalsIgnoreCase("account")) {
            element = driverUtil.getWebElement("//div[contains(@class,'dropdown')]/input");
            if (element == null)
                throw new AutomationException("Account search input box not displayed");
            value = element.getAttribute("value");
            if (!value.equalsIgnoreCase(""))
                throw new AutomationException("Account search input box has value :" + value);
        }
    }

    public void updatePermissionIconDisabled() throws AutomationException {
        WebElement updatePermission = driverUtil.getWebElement(UPDATE_PERMISSION_BUTTON_DISABLED);
        if (updatePermission == null)
            throw new AutomationException("Update Permission icon is enabled");
    }
}
