package com.arine.automation.pages;

import com.arine.automation.exception.AutomationException;
import com.arine.automation.util.WebDriverUtil;
import cucumber.api.DataTable;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.List;
import java.util.Map;

public class CareTeamPage extends PatientPage {

    public static final String CREATE_PRACTITIONER_LAST_NAME_INPUT = "//input[@name='lastName']";
    public static final String CREATE_PRACTITIONER_ADDRESS1_INPUT = "//input[@name='address1']";
    public static final String CREATE_PRACTITIONER_CITY_INPUT = "//input[@name='city']";
    public static final String CREATE_PRACTITIONER_STATE_INPUT = "//*[@name='statecode']";
    public static final String CREATE_PRACTITIONER_ZIP_INPUT = "//input[@name='zipcode']";
    public static final String CREATE_PRACTITIONER_PHONE_INPUT = "//input[@name='phone1']";
    public static final String PRACTITIONER_NAMES = "//div[contains(@class,'PractitionerTable-__scrollbox')]//td[1]";
    public static final String PHARMACY_NAMES = "//div[contains(@class,'PharmacyTable-__scrollbox')]//td[2]";
    public static final String PRACTITIONER_ADDRESS = "//div[contains(@class,'PractitionerTable-__scrollbox')]//tr[%s]/td[3]";
    public static final String PHARMACY_ADDRESS = "//div[contains(@class,'PharmacyTable-__scrollbox')]//tr[%s]/td[3]";
    public static final String PRACTITIONER_ROLE = "//div[contains(@class,'PractitionerTable-__scrollbox')]//tr[%s]/td[2]";
    public static final String PRACTITIONER_SEARCH_RESULT_FIRST_RECORD = "(//*[contains(@class,'SelectPractitioner')]//tbody/tr/td[2])[1]";
    public static final String PHARMACY_SEARCH_RESULT_FIRST_RECORD = "(//*[contains(@class,'SelectPharmacy')]//tbody/tr/td[2])[1]";
    public static final String CREATE_PRACTITIONER_POPUP_HEADER = "//*[contains(text(),'Create New Practitioner')]";
    public static final String CREATE_PHARMACY_POPUP_HEADER = "//*[contains(text(),'Create New Pharmacy')]";

    public void addPractitionerToPatient(DataTable data) throws AutomationException {
        WebElement recordsFound = driverUtil.findElementByText("*Showing 0 result");
        Map<String, String> inputDataMap = convertDataTableIntoMap(data);
        if (recordsFound != null) {
            clickCreateDoctorButton();
            verifyPopup("Create New Practitioner");
            enterLastName(inputDataMap.get("lastName"));
            enterAddress(inputDataMap.get("address1"));
            enterCity(inputDataMap.get("city"));
            selectState(inputDataMap.get("state"));
            enterZip(inputDataMap.get("zip"));
            enterPhone(inputDataMap.get("phone"));
            clickOnButton("Create");
            WebDriverUtil.waitForInvisibleElement(By.xpath(CREATE_PRACTITIONER_POPUP_HEADER));
        }else
        {
            WebElement firstRecord=driverUtil.getWebElement(PRACTITIONER_SEARCH_RESULT_FIRST_RECORD);
            firstRecord.click();
            WebDriverUtil.waitForAWhile(1);
            driverUtil.findElementByTextAndClick("Add");
        }
    }

    //Create New Practitioner
    public void clickCreateDoctorButton() throws AutomationException {
        WebElement createDoctorBtn = driverUtil.findElementByText("Create Doctor");
        if (createDoctorBtn == null)
            throw new AutomationException("Create Doctor button not displayed");
        createDoctorBtn.click();
    }

    public void enterLastName(String lastName) throws AutomationException {
        WebElement lastNameTextBox = driverUtil.getWebElement(CREATE_PRACTITIONER_LAST_NAME_INPUT);
        if (lastName == null)
            throw new AutomationException("Last Name input box not displayed");
        lastNameTextBox.click();
        lastNameTextBox.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
        lastNameTextBox.sendKeys(lastName);
    }

    public void enterAddress(String address) throws AutomationException {
        WebElement addressTextBox = driverUtil.getWebElement(CREATE_PRACTITIONER_ADDRESS1_INPUT);
        if (addressTextBox == null)
            throw new AutomationException("Address1 input box not displayed");
        addressTextBox.clear();
        addressTextBox.sendKeys(address);
    }

    public void enterCity(String city) throws AutomationException {
        WebElement cityTextBox = driverUtil.getWebElement(CREATE_PRACTITIONER_CITY_INPUT);
        if (cityTextBox == null)
            throw new AutomationException("City input box not displayed");
        cityTextBox.clear();
        cityTextBox.sendKeys(city);
    }

    public void selectState(String state) throws AutomationException {
        WebElement stateDropdown = driverUtil.getWebElement(CREATE_PRACTITIONER_STATE_INPUT);
        if (stateDropdown == null)
            throw new AutomationException("State dropdown not displayed");
        stateDropdown.click();
        Select states=new Select(stateDropdown);
        states.selectByVisibleText(state);
    }

    public void enterZip(String zip) throws AutomationException {
        WebElement zipTextBox = driverUtil.getWebElement(CREATE_PRACTITIONER_ZIP_INPUT);
        if (zipTextBox == null)
            throw new AutomationException("Zip input box not displayed");
        zipTextBox.clear();
        zipTextBox.sendKeys(zip);
    }

    public void enterPhone(String phone) throws AutomationException {
        WebElement phoneTextBox = driverUtil.getWebElement(CREATE_PRACTITIONER_PHONE_INPUT);
        if (phoneTextBox == null)
            throw new AutomationException("Phone input box not displayed");
        phoneTextBox.clear();
        phoneTextBox.sendKeys(phone);
    }

    public void verifyPractitionerNameIsAssignedToPatient(String expectedPractitionerName) throws AutomationException {
        List<WebElement> webElements = driverUtil.getWebElements(PRACTITIONER_NAMES);
        boolean recordFound=false;
        if(webElements==null)
            throw new AutomationException("We expected practitioner '"+expectedPractitionerName+"' to be displayed but no records were found");
        for (WebElement webElement:webElements
             ) {
            driverUtil.findElementAndScroll(webElement);
            if(webElement.getText().contains(expectedPractitionerName)){
                recordFound=true;
                break;
            }
        }
        if(!recordFound)
            throw new AutomationException("Expected Practitioner was not assigned to patient : "+expectedPractitionerName);
    }

    public void selectPractitioner(String expectedPractitionerName) throws AutomationException {
        List<WebElement> webElements = driverUtil.getWebElements(PRACTITIONER_NAMES);
        boolean recordFound=false;
        if(webElements==null)
            throw new AutomationException("We expected practitioner '"+expectedPractitionerName+"' to be displayed but no records were found");
        for (WebElement webElement:webElements
        ) {
            driverUtil.findElementAndScroll(webElement);
            if(webElement.getText().contains(expectedPractitionerName)){
                recordFound=true;
                webElement.click();
                break;
            }
        }
        if(!recordFound)
            throw new AutomationException("Expected Practitioner was not assigned to patient : "+expectedPractitionerName);
    }

    public void updatePractitionerAddress(String addressToUpdate) throws AutomationException {
        WebElement webElement=driverUtil.getWebElement(CREATE_PRACTITIONER_ADDRESS1_INPUT);
        if(webElement==null)
            throw new AutomationException("Address field not displayed or not editable");
        webElement.click();
        webElement.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
        webElement.sendKeys(addressToUpdate);
        webElement.sendKeys(Keys.TAB);
    }

    public void updatePractitionerRole(String roleName) throws AutomationException {
        Select roleDropDown = new Select(driverUtil.getWebElement(PRACTITIONER_ROLE_SELECT));
        roleDropDown.selectByVisibleText(roleName);
        WebDriverUtil.waitForAWhile(WebDriverUtil.WAIT_2_SEC);
    }

    public void verifyPractitionerTable(String practitionerName,String columnToVerify,String expectedColumnValue) throws AutomationException {
        List<WebElement> webElements = driverUtil.getWebElements(PRACTITIONER_NAMES);
        int row=1;
        boolean recordFound=false;
        if(webElements==null)
            throw new AutomationException("We expected practitioner '"+practitionerName+"' to be displayed but no records were found");
        for (WebElement webElement:webElements) {
            driverUtil.findElementAndScroll(webElement);
            if(webElement.getText().contains(practitionerName)){
                recordFound=true;
                break;
            }
            row++;
        }
        if(!recordFound)
            throw new AutomationException("Expected Practitioner was not assigned to patient : "+practitionerName);

        switch (columnToVerify.toUpperCase()){
            case "ADDRESS" :
                WebElement address=driverUtil.getWebElement(String.format(PRACTITIONER_ADDRESS,row));
                if(address==null)
                    throw new AutomationException("Couldn't locate element for address of practitioner : '"+practitionerName+"'");
                if(!address.getText().contains(expectedColumnValue))
                    throw new AutomationException("We expected address to be '"+expectedColumnValue+"' but found "+address.getText());
                break;
            case "ROLE" :
                WebElement element=driverUtil.getWebElement(String.format(PRACTITIONER_ROLE,row));
                if(element==null)
                    throw new AutomationException("Couldn't locate element for role of practitioner : '"+practitionerName+"'");
                if(!element.getText().contains(expectedColumnValue))
                    throw new AutomationException("We expected role to be '"+expectedColumnValue+"' but found "+element.getText());
                break;
        }
    }

    public void verifyPharmacyTable(String pharmacyName,String columnToVerify,String expectedColumnValue) throws AutomationException {
        List<WebElement> webElements = driverUtil.getWebElements(PHARMACY_NAMES);
        int row=1;
        boolean recordFound=false;
        if(webElements==null)
            throw new AutomationException("We expected pharmacy '"+pharmacyName+"' to be displayed but no records were found");
        for (WebElement webElement:webElements
        ) {
            driverUtil.findElementAndScroll(webElement);
            if(webElement.getText().contains(pharmacyName)){
                recordFound=true;
                break;
            }
            row++;
        }
        if(!recordFound)
            throw new AutomationException("Expected Practitioner was not assigned to patient : "+pharmacyName);

        switch (columnToVerify.toUpperCase()){
            case "ADDRESS" :
                WebElement address=driverUtil.getWebElement(String.format(PHARMACY_ADDRESS,row));
                if(address==null)
                    throw new AutomationException("Couldn't locate element for address of practitioner : '"+pharmacyName+"'");
                if(!address.getText().contains(expectedColumnValue))
                    throw new AutomationException("We expected address to be '"+expectedColumnValue+"' but found "+address.getText());
                break;
        }
    }

    public void addPharmacyToPatient(DataTable data) throws AutomationException {
        WebElement recordsFound = driverUtil.findElementByText("*Showing 0 result");
        Map<String, String> inputDataMap = convertDataTableIntoMap(data);
        if (recordsFound != null) {
            driverUtil.findElementByTextAndClick("Create Pharmacy");
            verifyPopup("Create New Pharmacy");
            enterAddress(inputDataMap.get("address1"));
            enterCity(inputDataMap.get("city"));
            selectState(inputDataMap.get("state"));
            enterZip(inputDataMap.get("zip"));
            enterPhone(inputDataMap.get("phone"));
            clickOnButton("Create");
            WebDriverUtil.waitForInvisibleElement(By.xpath(CREATE_PHARMACY_POPUP_HEADER));
        }else
        {
            WebElement firstRecord=driverUtil.getWebElement(PHARMACY_SEARCH_RESULT_FIRST_RECORD);
            firstRecord.click();
            WebDriverUtil.waitForAWhile(1);
            driverUtil.findElementByTextAndClick("Add");
        }
    }
}
