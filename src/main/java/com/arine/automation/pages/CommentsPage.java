package com.arine.automation.pages;

import com.arine.automation.exception.AutomationException;
import com.arine.automation.util.WebDriverUtil;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class CommentsPage extends PatientPage {
    public static final String FIRST_COMMENT_NOTE = "(//textarea[contains(@class,'Notes')])[1]";
    public static final String EDIT_POPUP_COMMENT_BUTTON = "//*[@type='button'][text()='Yes, edit it']";
    public static final String VERIFY_POPUP_MESSAGE = "//*[contains(@class,'swal2-html-container') and contains(text(),'%s')]";
    public static final String EDIT_PERMISSION_POPUP = "//*[contains(@id,'swal2-content')]";
    public static final String DISABLED_COMMENT = "//textarea[contains(@class,'Notes') and contains(text(),'%s')]/..//*[@disabled]";
    public static final String CANCEL_BUTTON = "//*[@type='button'][text()='Cancel']";
    public static final String YES_EDIT_BUTTON = "//*[@type='button'][text()='Yes, edit it']";
    public static final String BLANK_COMMENT_UNSAVED_TAG = "//*[contains(@class,'alignTextAreaHead')]/..//*[contains(text(),'UNSAVED')]";
    public static final String USER_NAME_UPDATE_FOR_COMMENT = "//*[contains(@class,'normalNotes') and contains(text(),'%s')]/../..//*[@data-for='Full' and contains(@data-tip,'%s')]";


    public void editComments() throws AutomationException {
        WebElement editNote = driverUtil.getWebElement(FIRST_COMMENT_NOTE);
        if (editNote == null)
            throw new AutomationException("Comment text area is not being displayed after clicking!");
        editNote.click();
        WebElement Note = driverUtil.getWebElement(FIRST_COMMENT_NOTE);
        if (Note == null)
            throw new AutomationException("Comment text area is not being edited after enter!");
        Note.sendKeys(Keys.ENTER);
        WebDriverUtil.waitForAWhile(3);
    }


    public void updateNewComments(String newComment) throws AutomationException {
        WebElement editNote = driverUtil.getWebElement(FIRST_COMMENT_NOTE);
        if (editNote == null)
            throw new AutomationException("Comment text area is not being displayed after clicking!");
        editNote.click();
        if (editNote == null)
            throw new AutomationException("Comment text area is not being cleared!");
        editNote.clear();
        WebElement commentNote = driverUtil.getWebElement(FIRST_COMMENT_NOTE);
        if (commentNote == null)
            throw new AutomationException("Comment text area is not being displayed after clicking on add new button!");
        commentNote.sendKeys(newComment);
    }

    public static void verifyPopupMessage(String name) throws AutomationException {
        WebElement popup = driverUtil.getWebElement(String.format(VERIFY_POPUP_MESSAGE, name));
        if (popup == null)
            throw new AutomationException(String.format("Unable to find popup Message:%s", name));
    }

    public void verifyCommentDisabled(String text) throws AutomationException {
        WebElement disabledComment = driverUtil.getWebElement(String.format(DISABLED_COMMENT, text));
        if (disabledComment != null)
            throw new AutomationException(String.format("Unable to Disable Comment:%s", text));
    }

    public void verifyPharmaUserCommentUpdateByAnotherPharmaUser(String previousComment, String updatedComment) throws AutomationException {
        WebElement previousCommentRecord = driverUtil.getWebElement(String.format(FIND_COMMENT, previousComment));
        if (previousCommentRecord == null)
            throw new AutomationException(String.format("No Comment Record found with the given text: %s but we supposed it should be visible", previousComment));
        previousCommentRecord.click();
        previousCommentRecord.sendKeys(Keys.CONTROL+"a");
        previousCommentRecord.sendKeys(Keys.DELETE);
        WebElement editPermissionPopup = driverUtil.getWebElement(EDIT_PERMISSION_POPUP);
        WebElement editItButton = null;
        if (editPermissionPopup != null) {
            editItButton = driverUtil.getWebElement(YES_EDIT_BUTTON);
            editItButton.click();
        } else
            WebDriverUtil.waitForAWhile(3);
//        WebElement commentNote = driverUtil.getWebElement(FIRST_COMMENT_NOTE);
//        if (commentNote == null)
//            throw new AutomationException("Comment text area is not being displayed after clicking on add new button!");
//        commentNote.clear();
//        WebDriverUtil.waitForAWhile(3);
//        commentNote.sendKeys(updatedComment);
        previousCommentRecord.sendKeys(Keys.CONTROL+"a");
        previousCommentRecord.sendKeys(Keys.DELETE);
        previousCommentRecord.sendKeys(updatedComment);
        WebDriverUtil.waitForAWhile(3);
    }

    public void verifyCommentNotPresent(String text, boolean isVisible) throws AutomationException {
        WebElement comment = driverUtil.getWebElement(String.format(FIND_COMMENT, text));
        if (!isVisible && comment != null)
            throw new AutomationException(String.format("Comment found with the given text: %s but we supposed it should not visible", text));
    }


    public void verifyUserNameWhenAddUpdateComment(String username, String comment) throws AutomationException {
        WebElement userName = driverUtil.getWebElement(String.format(USER_NAME_UPDATE_FOR_COMMENT, username, comment));
        if (userName == null)
            throw new AutomationException(String.format("User Name not displayed when add or update comment:"));
    }

    public void addBlankComment(boolean isVisible) throws AutomationException {
        WebElement newLogActionPopup = driverUtil.getWebElement(CREATE_NEW_LOG_ACTION_POPUP);
        WebElement addButton;
        if (newLogActionPopup != null)
            addButton = driverUtil.getWebElement(LOG_ACTION_POPUP_ADD_NEW_COMMENT_BUTTON);
        else
            addButton = driverUtil.getWebElement(STORY_TAB_ADD_NEW_COMMENT_BUTTON);
        if (addButton == null)
            throw new AutomationException("Add new comment icon is not being displayed or it might taking too long time to load!");
        addButton.click();
        WebDriverUtil.waitForAWhile(2);
        WebElement commentNote = driverUtil.getWebElement(FIRST_COMMENT_NOTE);
        if (commentNote == null)
            throw new AutomationException("Comment text area is not being displayed after clicking on add new button!");

        WebElement blankComment = driverUtil.getWebElement(String.format(BLANK_COMMENT_UNSAVED_TAG));
        if (isVisible && blankComment == null)
            throw new AutomationException(String.format("New Note Message Not found above Blank comment but we supposed it should not visible"));
    }

    public void verifyUserNotAbleToAddBlankComment(boolean isVisible) throws AutomationException {
        WebElement blankComment = driverUtil.getWebElement(String.format(BLANK_COMMENT_UNSAVED_TAG));
        if (!isVisible && blankComment != null)
            throw new AutomationException(String.format("New Note Message Not found above Blank comment but we supposed it should not visible"));
    }

    public void verifyUserAbleToAddMultilineComment(String comment1, String comment2) throws AutomationException {
        WebElement newLogActionPopup = driverUtil.getWebElement(CREATE_NEW_LOG_ACTION_POPUP);
        WebElement addButton;
        if (newLogActionPopup != null)
            addButton = driverUtil.getWebElement(LOG_ACTION_POPUP_ADD_NEW_COMMENT_BUTTON);
        else
            addButton = driverUtil.getWebElement(STORY_TAB_ADD_NEW_COMMENT_BUTTON);
        if (addButton == null)
            throw new AutomationException("Add new comment icon is not being displayed or it might taking too long time to load!");
        addButton.click();
        WebDriverUtil.waitForAWhile(2);
        WebElement commentNote = driverUtil.getWebElement(FIRST_COMMENT_NOTE);
        if (commentNote == null)
            throw new AutomationException("Comment text area is not being displayed after clicking on add new button!");
        commentNote.sendKeys(comment1);
        WebDriverUtil.waitForAWhile(5);
        commentNote.sendKeys(Keys.ENTER, comment2);
        WebDriverUtil.waitForAWhile(3);
    }
}
