package com.arine.automation.pages;

import com.arine.automation.drivers.DriverFactory;
import com.arine.automation.exception.AutomationException;
import com.arine.automation.glue.CommonSteps;
import com.arine.automation.util.WebDriverUtil;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.List;
import java.util.logging.Logger;

public class HomePage extends BasePage {
    private static final Logger LOGGER = Logger.getLogger(HomePage.class.getName());

    public static final String ACCOUNT_DROPDOWN = "//*[@id='insurance_org_id']";
    public static final String TABS_XPATH = "//button[@type='button' and text()='%s']";
    public static final String SELECTED_TAB_XPATH = "//button[contains(@class,'activeButton') and @type='button' and text()='%s']";
    public static final String SEARCH_BUTTON = "//*[@data-icon='search']";
    public static final String PERSONAL_NOTE_BUTTON = "//span[contains(@class,'MenuBar-__scratchboard')]";
    public static final String PERSONAL_NOTEPAD = "//*[contains(@class,'ScratchBoard-__scratchBoard') and .//*[text()='Your Personal Notepad']]";
    public static final String PERSONAL_NOTEPAD_TEXTAREA = PERSONAL_NOTEPAD+"//textarea";
    public static final String PERSONAL_NOTEPAD_SAVE = PERSONAL_NOTEPAD+"//*[contains(@class,'ScratchBoard-__rightButton')]";
    public static final String COMPLETE_CMR_DISABLED_BUTTON="//*[text()='Complete  CMR' and @disabled]";

    public WebElement getMenu(String menu) throws AutomationException {
        WebElement element = driverUtil.getWebElementAndScroll(String.format(TABS_XPATH,menu));
        if(element!=null)
            return element;
        WebDriver driver = DriverFactory.drivers.get();
        driver.get(driver.getCurrentUrl());
        return driverUtil.getWebElement(String.format(TABS_XPATH,menu));
    }

    public void gotoMenu(String menu) throws AutomationException {
        if(!isMenuSelected(menu))
            getMenu(menu).click();
    }

    public boolean isMenuSelected(String menu) throws AutomationException {
        WebElement element = driverUtil.getWebElementWithoutWait(String.format(SELECTED_TAB_XPATH,menu));
        if(element==null)
            return false;
        return true;
    }

    public void scrollToMenu(String menu) throws AutomationException {
        driverUtil.getWebElementAndScroll(String.format(TABS_XPATH,menu));
    }

    public void selectAccount(String accountName) throws AutomationException {
        WebElement accountDropdown = driverUtil.getWebElement(ACCOUNT_DROPDOWN);
        Select select = new Select(accountDropdown);
        List<WebElement> options = select.getOptions();
        for(WebElement option: options) {
            String optionText = option.getText();
            if(optionText.contains(accountName)) {
                option.click();
                break;
            }
        }
        driverUtil.waitForLoadingPage();
    }

    public void addPersonalNote(String noteText) throws AutomationException {
        driverUtil.getWebElement(PERSONAL_NOTE_BUTTON, WebDriverUtil.NO_WAIT,
                "Unable to find create personal note icon on home page!").click();
        driverUtil.getWebElement(PERSONAL_NOTEPAD, WebDriverUtil.WAIT_2_SEC,
                "Unable to find Notepad!");
        WebElement textarea = driverUtil.getWebElement(PERSONAL_NOTEPAD_TEXTAREA, WebDriverUtil.NO_WAIT,
                "Unable to find Textarea to write note!");
        textarea.click();
        textarea.sendKeys(Keys.chord(Keys.CONTROL,"a",Keys.DELETE));
        WebDriverUtil.waitForAWhile(1);
        textarea.sendKeys(noteText);
        driverUtil.getWebElement(PERSONAL_NOTEPAD_SAVE, WebDriverUtil.NO_WAIT,
                "Unable to find right button on scratch board!").click();
    }

    public void verifyPersonalNote(String noteText) throws AutomationException {
        driverUtil.getWebElement(PERSONAL_NOTE_BUTTON, WebDriverUtil.NO_WAIT,
                "Unable to find create personal note icon on home page!").click();
        driverUtil.getWebElement(PERSONAL_NOTEPAD, WebDriverUtil.WAIT_2_SEC,
                "Unable to find Notepad!");
        WebElement textarea = driverUtil.getWebElement(PERSONAL_NOTEPAD_TEXTAREA, WebDriverUtil.NO_WAIT,
                "Unable to find Textarea to write note!");
        String existingNote = textarea.getText();
        CommonSteps.takeScreenshot();
        driverUtil.getWebElement(PERSONAL_NOTEPAD_SAVE, WebDriverUtil.NO_WAIT,
                "Unable to find right button on scratch board!").click();
        if(!noteText.equalsIgnoreCase(existingNote))
            throw new AutomationException("Found personal note: ["+existingNote+"] and it is not being matched!");
    }

    public void verifyPersonalNoteNotDisplay(String noteText) throws AutomationException {
        driverUtil.getWebElement(PERSONAL_NOTE_BUTTON, WebDriverUtil.NO_WAIT,
                "Unable to find create personal note icon on home page!").click();
        driverUtil.getWebElement(PERSONAL_NOTEPAD, WebDriverUtil.WAIT_2_SEC,
                "Unable to find Notepad!");
        WebElement textarea = driverUtil.getWebElement(PERSONAL_NOTEPAD_TEXTAREA, WebDriverUtil.NO_WAIT,
                "Unable to find Textarea to write note!");
        String existingNote = textarea.getText();
        CommonSteps.takeScreenshot();
        driverUtil.getWebElement(PERSONAL_NOTEPAD_SAVE, WebDriverUtil.NO_WAIT,
                "Unable to find right button on scratch board!").click();
        if(noteText.equalsIgnoreCase(existingNote))
            throw new AutomationException("Found personal note: ["+existingNote+"] and it should not display!");
    }

    public static void verifyCompleteCMRButtonIsDisabled() throws AutomationException {
        WebElement setCallDateButton= driverUtil.findElementByText("Set Call Date");
        if(setCallDateButton!=null){
            WebElement element=driverUtil.findElement(COMPLETE_CMR_DISABLED_BUTTON);
            if(element==null)
                throw new AutomationException("Complete CMR button is not displayed, else it is enabled but we expected it to disabled");
        }
    }

    @Override
    public String getName() {
        return "Home";
    }
}
