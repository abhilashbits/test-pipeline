package com.arine.automation.pages;

import com.arine.automation.exception.AutomationException;
import com.arine.automation.util.WebDriverUtil;
import org.openqa.selenium.WebElement;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class LogActionPage extends PatientPage {
    public static final String OPEN_TASKS_COLUMN = "//*[contains(@class,'PatientStoryInput-__tableHeader') and text()='OPEN TASKS']";
    public static final String OPEN_TASK_COUNT="//td[text()='Open']/..//td[4]";
    public static final String LOG_ACTION_TAB_DEFAULT_BUTTON = "//*[contains(@class,'defaultButton') and text()='Log Action']";
    public static final String OPEN_TASK_COUNT_IN_LOG_ACTION="//*[contains(@class,'PatientStoryInput-__tableHeader') and text()='OPEN TASKS']/..//label";

    public static final String NA_OPTION_IN_OPEN_TASK_COLUMN="//*[contains(@class,'PatientStoryInput-__options')]/..//label[contains(text(),'N/A')]";
    public static final String NOTES_TEXT_BOX_AREA="//*[contains(@class,'textBoxInputWrapper')]//../textarea[@name='note']";
    private static final String CREATEDBY = "//*[contains(@class,'createdBy')][2]";

    public void verifyOpenTaskcolumninLogActionPopup() throws AutomationException {
        WebElement openTaskColumn = driverUtil.getWebElement(OPEN_TASKS_COLUMN);
        if (openTaskColumn == null)
            throw new AutomationException("No Open Task column is being displayed in log action popUp or it might taking too long time to load!");
    }

    public void verifyOpenTaskcolumnIncludeListOfOpenTask() throws AutomationException {
        WebDriverUtil.waitForAWhile(5);
        List <WebElement> listofItems = driverUtil.getWebElements(OPEN_TASK_COUNT);
            if (listofItems == null)
            throw new AutomationException("No Open Tasks is being displayed in task table or it might taking too long time to load!");
        int countOfOpenTaskFromTaskTable= 0;
        for (int i=1; i<=listofItems.size(); i++)
        {
            countOfOpenTaskFromTaskTable++;
         }
        System.out.println("Total Open tasks= "+countOfOpenTaskFromTaskTable);
        WebDriverUtil.waitForAWhile(5);

        WebElement button = driverUtil.getWebElement(LOG_ACTION_TAB_DEFAULT_BUTTON);
        if (button == null)
            throw new AutomationException("Unable to find log Action button on patient tab or it might taking too long time to load!");
        //driverUtil.waitForElementClickable(By.xpath(String.format(PATIENT_TAB_BUTTON, text)));
        button.click();

        WebDriverUtil.waitForAWhile(5);
        List <WebElement> listofItemsfromlogActin = driverUtil.getWebElements(OPEN_TASK_COUNT_IN_LOG_ACTION);
        if (listofItemsfromlogActin == null)
            throw new AutomationException("No Open Tasks is being displayed in log action popUp or it might taking too long time to load!");
        int countofopentaskfromlogAction= 0;
        for (int i=2; i<=listofItemsfromlogActin.size(); i++)
        {
            countofopentaskfromlogAction++;
        }
        System.out.println("Total Open tasks= "+countofopentaskfromlogAction);

        if(countOfOpenTaskFromTaskTable!=countofopentaskfromlogAction)
            throw new AutomationException("Open Tasks from task table and log action is Equal or it might taking too long time to load!");
    }

    public void verifyNAisPresentinOpenTaskcolumn() throws AutomationException {
        WebElement NAOptionInopenTaskColumn = driverUtil.getWebElement(NA_OPTION_IN_OPEN_TASK_COLUMN);
        if (NAOptionInopenTaskColumn == null)
            throw new AutomationException("No NA Present in Open Task column is being displayed or it might taking too long time to load!");
    }

    public void verifyUserIsAbleToLogActionForSelectedTask(String task,String note) throws AutomationException {
        WebElement SelectedTaskNote = driverUtil.getWebElement("//td[text()='"+task+"']/..//td[8]");
        String noteLogUpdateBy=SelectedTaskNote.getText();
        System.out.println("Note Text ====="+noteLogUpdateBy);
        if (SelectedTaskNote == null)
            throw new AutomationException("Unable to find task's Note or it might taking too long time to load!");

        DateTimeFormatter format = DateTimeFormatter.ofPattern("M/d/yy");
        String currentDate = LocalDateTime.now().format(format);
        String CurrentNoteWithDate = null;
        CurrentNoteWithDate = (note+" "+( getCurrentDate())+".");
        System.out.println("Current Date ====="+CurrentNoteWithDate);
//        System.out.println("Current Date ====="+note+" "+( currentDate));

//        if(!(note+" "+currentDate+".").equals(noteLogUpdateBy))
        if(!CurrentNoteWithDate.equals(noteLogUpdateBy))
            throw new AutomationException("Created log action is not being displayed or it might be taking too long time to load!");
    }


    public void verifyLastAttemptDateAndUserNameInNoteBoxTaskTable(String task,String note) throws AutomationException {
        WebElement selectedTaskNote = driverUtil.getWebElement("//td[text()='"+task+"']/..//td[8]");
        selectedTaskNote.click();
        if (selectedTaskNote == null)
            throw new AutomationException("Unable to find task's Note or it might taking too long time to load!");
        WebDriverUtil.waitForAWhile();

        String taskTableNotesColumn=selectedTaskNote.getText();

        DateTimeFormatter format = DateTimeFormatter.ofPattern("M/d/yy");
        String currentDate = LocalDateTime.now().format(format);
        String CurrentNoteWithDate = null;
        CurrentNoteWithDate = (note+" "+( getCurrentDate())+".");
        System.out.println("Current Date ====="+CurrentNoteWithDate);

        if(!CurrentNoteWithDate.equals(taskTableNotesColumn))
            throw new AutomationException("No Last Attempted date and User name is being displayed in Note Text Box or it might taking too long time to load!");
    }

    public void verifyLastAttemptDateAndUserNameInNoteBoxTaskDetail(String task,String note) throws AutomationException {
        WebElement SelectedtaskNote = driverUtil.getWebElement("//td[text()='"+task+"']/..//td[8]");
        SelectedtaskNote.click();
        if (SelectedtaskNote == null)
            throw new AutomationException("Unable to find task's Note or it might taking too long time to load!");
        WebDriverUtil.waitForAWhile();

        WebElement NotesTextBox = driverUtil.getWebElement(NOTES_TEXT_BOX_AREA);
        String noteLogUpdateInNotesTextBox=NotesTextBox.getText();
        System.out.println("Note Text ====="+noteLogUpdateInNotesTextBox);
        if (NotesTextBox == null)
            throw new AutomationException("Unable to find NotesTextBox or it might taking too long time to load!");

        DateTimeFormatter format = DateTimeFormatter.ofPattern("M/d/yy");
        String currentDate = LocalDateTime.now().format(format);
        String CurrentNoteWithDate = null;
        CurrentNoteWithDate = (note+" "+( getCurrentDate())+".");
        System.out.println("Current Date ====="+CurrentNoteWithDate);

        if(!CurrentNoteWithDate.equals(noteLogUpdateInNotesTextBox))
            throw new AutomationException("No Last Attempted date and User name is being displayed in Note Text Box or it might taking too long time to load!");
    }

    public void verifyLogActionNotDisplayed(String identifier) throws AutomationException {
        WebElement taskRecord = driverUtil.getWebElement(String.format(VERIFY_PATIENT_STORY_FIRST_RECORD, identifier));
        if (taskRecord != null)
            throw new AutomationException(String.format("We were not supposed to get newly created log action on first index : %s", identifier));
    }

    public void verifyCreatedByUserAndDate(String createdByUser,String task) throws AutomationException {
        WebElement SelectedtaskNote = driverUtil.getWebElement("//td[text()='"+task+"']/..//td[8]");
        SelectedtaskNote.click();
        if (SelectedtaskNote == null)
            throw new AutomationException("Unable to find task or it might taking too long time to load!");
        WebDriverUtil.waitForAWhile();

        DateTimeFormatter format = DateTimeFormatter.ofPattern("M/d/yy");
        String currentDate = LocalDateTime.now().format(format);
        String createdByUserWithDate = null;
        createdByUserWithDate = (createdByUser+" "+( getCurrentDate()));
        System.out.println("Current Date ====="+createdByUserWithDate);

        WebElement element = driverUtil.getWebElement(CREATEDBY);
        if(element==null)
            throw new AutomationException("Created by user and date is not displayed");
        if(!element.getText().equals(createdByUserWithDate))
            throw new AutomationException(String.format("Created by is not displayed as expected, we expected it to be %s but it was displayed as %s",createdByUserWithDate,element.getText()));
    }

    private static String getCurrentDate() {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("M/d/yy");
        String currentDate = LocalDateTime.now().format(format);
        return currentDate;
    }


}
