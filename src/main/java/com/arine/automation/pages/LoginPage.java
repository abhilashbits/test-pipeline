package com.arine.automation.pages;

import com.arine.automation.exception.AutomationException;
import com.arine.automation.util.WebDriverUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import java.util.logging.Logger;

public class LoginPage  extends BasePage {
    public static final Logger LOGGER = Logger.getLogger(LoginPage.class.getName());
    public static final String USER_NAME_INPUT = "//input[@name='email']";
    public static final String ADMIN_USER_NAME_INPUT = "//input[@id='filled-adornment-username']";

    public static final String PASSWORD_INPUT = "//input[@name='password']";
    public static final String ADMIN_PASSWORD_INPUT = "//input[@id='filled-adornment-password']";
    public static final String LOGIN_BUTTON = "//button[@type='submit']";
    public static final String FORGOT_PASSWORD_LINK = "//*[text()='Forgot Password?']";
    public static final String ADMIN_LOGIN_BUTTON = "//button[contains(@class,'loginButton')]";
    public static final String FORGOT_LINK = "//button[@type='button' and text()='Forgot Password?']";
    public static final String VERIFY_TEXT = "//*[contains(text(),\"%s\")]";
    public static final String VERIFY_BUTTON = "//button[text()='%s']";
    public static final String LOGOUT_BUTTON = "//button[contains(@class,'signOutButton')]";
    public static final String ADMIN_MENU_OPTION = "//header//*[@class='MuiSvgIcon-root']/../..";
    public static final String ADMIN_LOGOUT_BUTTON = "//button[contains(@class,'signOutButton')]";
    public static String ERROR = "//*[contains(@class,'textError')]";
    public static String ERROR_MESSAGE_XPATH = "//*[contains(@class,'textError') and contains(text(),'%s')]";

    /**
     * This function is used to login.
     * @param userName
     * @param password
     * @throws AutomationException
     */
    public void doLogin(String userName, String password) throws AutomationException {
        WebElement userNameInput = driverUtil.getWebElement(USER_NAME_INPUT);
        if(userNameInput!=null) {
            WebElement passwordInput = driverUtil.getWebElement(PASSWORD_INPUT);
            WebElement login = driverUtil.getWebElement(LOGIN_BUTTON);
            userNameInput.clear();
            userNameInput.sendKeys(userName);
            passwordInput.clear();
            passwordInput.sendKeys(password);
            login.click();
            driverUtil.waitForLoadingPage();
            verifyError();
        }
    }

    public void validateLogin(String userName, String password) throws AutomationException {
        WebElement userNameInput = driverUtil.getWebElement(USER_NAME_INPUT);
        WebElement passwordInput = driverUtil.getWebElement(PASSWORD_INPUT);
        WebElement login = driverUtil.getWebElement(LOGIN_BUTTON);
        userNameInput.clear();
        userNameInput.sendKeys(userName);
        passwordInput.clear();
        passwordInput.sendKeys(password);
        login.click();
    }

    public void doAdminLogin(String userName, String password) throws AutomationException {
        WebElement userNameInput = driverUtil.getWebElement(ADMIN_USER_NAME_INPUT);
        if(userNameInput!=null) {
            WebElement passwordInput = driverUtil.getWebElement(ADMIN_PASSWORD_INPUT);
            WebElement login = driverUtil.getWebElement(ADMIN_LOGIN_BUTTON);
            userNameInput.click();
            userNameInput.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
            userNameInput.sendKeys(userName);
            passwordInput.click();
            passwordInput.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
            passwordInput.sendKeys(password);
            login.click();
            driverUtil.waitForLoaderToDisappear();
        }
    }

    /**
     * This method is to written to check sny error message popup present.
     * @return boolean
     */
    public boolean verifyError() throws AutomationException {
        WebElement errorMessage = driverUtil.getWebElement(ERROR,WAIT_1_SECOND);
        if(errorMessage==null)
            return false;
        return true;
    }

    public boolean verifyErrorMessage(String message) {
        WebElement errorMessage = driverUtil.findElement(String.format(ERROR_MESSAGE_XPATH,message));
        if(errorMessage==null)
            return false;
        return true;
    }

   public void verifyHTML5Message(String message) throws AutomationException {
       WebElement userNameInput = driverUtil.getWebElement(USER_NAME_INPUT);
       String displayMessage = userNameInput.getAttribute("validationMessage");
       if(displayMessage==null)
           throw new AutomationException("No validation message is being displayed!");
       if(!displayMessage.contains(message))
           throw new AutomationException("We supposed to see message: '"+message+"' but getting: '"+displayMessage+"'");
    }

    public void doLogout() throws AutomationException {
        boolean isSuccess = true;
        try {
            driverUtil.getWebElementAndScroll(LOGOUT_BUTTON, WebDriverUtil.NO_WAIT).click();
            driverUtil.waitForElement(By.xpath(LOGIN_BUTTON));
        } catch(Exception ex) {
            isSuccess = false;
        }
        if(!isSuccess) {
            try {
                PageFactory.homePage().checkAnyPopupAndClose();
                checkAndCloseErrorPopup();
                driverUtil.waitForLoadingPage();
                WebElement logout = driverUtil.getWebElementAndScroll(LOGOUT_BUTTON, WebDriverUtil.WAIT_2_SEC);
                if(logout!=null)
                    logout.click();
                driverUtil.waitForElement(By.xpath(LOGIN_BUTTON));
            } catch(Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void doAdminLogout() throws AutomationException {
        boolean isSuccess = true;
        try {
            driverUtil.getWebElementAndScroll(ADMIN_MENU_OPTION, WebDriverUtil.NO_WAIT).click();
            driverUtil.findElementByText("Logout").click();
            driverUtil.waitForElement(By.xpath(ADMIN_LOGIN_BUTTON),10);
        } catch(Exception ex) {
            isSuccess = false;
        }
        if(!isSuccess) {
            try {
                PageFactory.adminPortalHomePage().clickOnButton("Cancel");
                driverUtil.getWebElementAndScroll(ADMIN_MENU_OPTION, WebDriverUtil.NO_WAIT).click();
                driverUtil.findElementByText("Logout").click();
                driverUtil.waitForElement(By.xpath(ADMIN_LOGIN_BUTTON));
            } catch(Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void clickOnForgotPasswordLink() throws AutomationException {
        WebElement forgotPasswordLink = driverUtil.getWebElementAndScroll(FORGOT_PASSWORD_LINK, WebDriverUtil.WAIT_2_SEC);
        if(forgotPasswordLink==null)
            throw new AutomationException("Unable to find forgot password link on login page!");
        forgotPasswordLink.click();
    }

    @Override
    public String getName() {
        return"Login";
    }
}
