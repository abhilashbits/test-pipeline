package com.arine.automation.pages;

import com.arine.automation.drivers.DriverFactory;
import com.arine.automation.exception.AutomationException;
import com.arine.automation.glue.CommonSteps;
import com.arine.automation.models.ExcelMapper;
import com.arine.automation.models.SigTranslation;
import com.arine.automation.util.CommonUtil;
import com.arine.automation.util.JSONUtil;
import com.arine.automation.util.PersistentData;
import com.arine.automation.util.WebDriverUtil;
import cucumber.api.DataTable;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.io.IOException;
import java.util.*;

public class MedListPage extends PatientPage {

    public static final String MED_LIST_TAB = "Med List";
    public static final String ADD_NEW_MED_POPUP_TITLE = "Add New Medication to Patient";
    public static final String MED_RECORD = "//tr[contains(@class,'MedDetailTable') and .//td[text()='%s']]";
    public static final String MEDICATION_DOSE_INPUT = "//*[@name='doseForm']";
    public static final String MEDICATION_DOSE_DROPDOWN = "//datalist[@id='doseForm']";
    public static final String MEDICATION_DOSE_TABLET_OPTION = "//datalist[@id='doseForm']/option[@value='Tablet']";
    public static final String MEDICATION_DOSE_ERROR_POPUP_BUTTON_OK = "//*[@role='dialog']//button[contains(@style,'inline-block') and text()='OK']";
    public static final String ADD_MED_ICON = "//th[@id='brandName']/*[1]";
    public static final String SEARCH_DRUG_INPUT = "//input[@name='brandName']";
    public static final String CHOOSE_MED_RECORD = "//tr[contains(@class,'SelectMed-__choiceWrapper') and .//*[text()='%s'] and .//*[text()='%s']]";
    public static final String DELETE_MED_BUTTON = "//tr[contains(@class,'MedDetailTable') and .//td[text()='%s']]//td[last()]";
    public static final String PATIENT_MEDICATION_RECORDS = "//*[contains(@class,'MedDetailTable-__tableContainer')]//tr[contains(@class,'MedDetailTable')]";
    public static final String SELECTED_MEDICATION_NAME = "//*[contains(@class,'DetailedMedInput-__rx')]";
    public static final String SELECTED_MEDICATION_NAME_INPUT = "//*[@name='brandName']";
    public static final String SELECTED_MEDICATION_DOSAGE_INPUT = "//*[@name='dosageStrength']";
    public static final String SELECTED_MEDICATION_UNIT_INPUT = "//*[@name='dosageUnit']";

    public static final String MEDLIST_TAB = "Med List";
    public static final String PATIENT_MEDICIN_TABLE = "//*[contains(@class,'MiddleTable') and contains(@class,'tableContainer')]//tr//td[1]";
    public static final String PRESCRIBER_DROPDOWN = "//select[@name='prescriber']";
    public static final String NOTE_TO_MD_TEXBOX = "//textarea[@name='question']";
    public static final String ADD_NEW_MEDRECORD = "//*[contains(@class,'MedDetailTable-__iconAdd')]";
    public static final String MEDLIST_RECORD = "//tr[contains(@class,'MedDetailTable') and .//td[text()='%s']]";
    public static final String MEDICATION_DRUG_NAME = "//input[@name='otherDrugName']";
    public static final String MEDICATION_DRUG_DOSE = "//input[@name='otherDrugDose']";
    public static final String MEDICATION_DRUG_UNIT = "//input[@name='otherDrugUnit']";
    public static final String MEDLIST_DOSE_FORM_DROPDOWN = "//input[@name='otherDoseForm']";
    public static final String MEDLIST_DOSE_FORM = "//option[contains(text(),'%s')]";
    public static final String GENERIC_NAME_INPUT = "//input[@name='otherGenericName']";
    public static final String ADD_MED_BUTTON = "//button[contains(text(),'Add Med')]";
    public static final String CONDITION_BOX = "//input[@name='condition']";


    public static ThreadLocal<Map<String, SigTranslation>> updatedMedicationSigTextMapping = new ThreadLocal<>();

    public void addMed(DataTable data) throws AutomationException {
        Map<String, String> inputDataMap = convertDataTableIntoMap(data);
        String patient = inputDataMap.get(MedData.PatientId.toString());
        searchPatient(patient);
        clickOnGlobalTab(MED_LIST_TAB);
        driverUtil.getWebElement(ADD_MED_ICON, WebDriverUtil.WAIT_2_SEC, "Unable to locate create new med icon!").click();
        verifyPopup(ADD_NEW_MED_POPUP_TITLE);
        String drugName = inputDataMap.get(MedData.DrugName.toString());
        String dose = inputDataMap.get(MedData.Dose.toString());
        driverUtil.getWebElement(SEARCH_DRUG_INPUT, WebDriverUtil.NO_WAIT, "Unable to locate drug search input!").sendKeys(drugName, Keys.ENTER);
        driverUtil.getWebElement(String.format(CHOOSE_MED_RECORD, drugName.toLowerCase(), dose),
                WebDriverUtil.MAX_ELEMENT_WAIT,
                String.format("Unable to find drug with the name:%s , dose: %s", drugName, dose)).click();
        clickOnButton("Add Med");
        WebDriverUtil.waitForInvisibleElement(By.xpath(String.format(VERIFY_POPUP, ADD_NEW_MED_POPUP_TITLE)));
    }

    public void updateMed(DataTable data) throws AutomationException {
        Map<String, String> inputDataMap = convertDataTableIntoMap(data);
        String patient = inputDataMap.get(MedData.PatientId.toString());
        searchPatient(patient);
        clickOnGlobalTab(MED_LIST_TAB);
        String drugName = inputDataMap.get(MedData.DrugName.toString());
        WebElement medRecord = driverUtil.getWebElement(String.format(MED_RECORD, drugName.toUpperCase()));
        if (medRecord == null)
            throw new AutomationException(String.format("Unable to find drug record for med: %s", drugName));
        medRecord.click();
        String sig = inputDataMap.get(MedData.SIG.toString());
        updateSigDetails(sig);
    }

    public void deleteMedIfPresent(String drugName) throws AutomationException {
        clickOnGlobalTab(MED_LIST_TAB);
        WebElement medRecord = driverUtil.getWebElement(String.format(MED_RECORD, drugName.toUpperCase()));
        if (medRecord != null) {
            List<WebElement> medicationRecords = driverUtil.getWebElements(String.format(DELETE_MED_BUTTON, drugName.toUpperCase()));
            for (WebElement medication : medicationRecords) {
                medication.click();
                WebElement deleteConfirmationButton = driverUtil.getWebElement(String.format(DELETE_CONFIRMATION_POPUP_DELETE_BUTTON, "Delete this medication"));
                if (deleteConfirmationButton == null)
                    throw new AutomationException("Unable to find delete med button on confirmation popup or it might taking too long time to load!");
                deleteConfirmationButton.click();
            }
        }
    }

    public void deleteMed(String drugName) throws AutomationException {
        clickOnGlobalTab(MED_LIST_TAB);
        WebElement medRecord = driverUtil.getWebElement(String.format(MED_RECORD, drugName.toUpperCase()));
        if (medRecord == null)
            throw new AutomationException(String.format("Unable to find drug record for med: %s", drugName));
        List<WebElement> medicationRecords = driverUtil.getWebElements(String.format(DELETE_MED_BUTTON, drugName.toUpperCase()));
        for (WebElement medication : medicationRecords) {
            medication.click();
            WebElement deleteConfirmationButton = driverUtil.getWebElement(String.format(DELETE_CONFIRMATION_POPUP_DELETE_BUTTON, "Delete this medication"));
            if (deleteConfirmationButton == null)
                throw new AutomationException("Unable to find delete med button on confirmation popup or it might taking too long time to load!");
            deleteConfirmationButton.click();
        }

    }

    public void verifyMed(String name, boolean isDisplayed) throws AutomationException {
        WebElement medRecord = driverUtil.getWebElement(String.format(MED_RECORD, name.toUpperCase()));
        if (isDisplayed && medRecord == null)
            throw new AutomationException(String.format("Unable to find drug record for med: %s", name));
        else if (!isDisplayed && medRecord != null)
            throw new AutomationException(String.format("We supposed drug record for med: %s should not be available!", name));
    }

    public void selectMed(String name) throws AutomationException {
        WebElement medRecord = driverUtil.getWebElement(String.format(MED_RECORD, name));
        if (medRecord == null)
            throw new AutomationException(String.format("Unable to find drug record for med: %s", name));
        medRecord.click();
    }

    private static enum MedData {
        PatientId,
        DrugName,
        Dose,
        SIG,
        Note,
        medication,
        Unit,
        DoseForm,
        RxGeneric;

        public String getName(MedData med) {
            return med.toString();
        }
    }

    public void updateAllMedicationsSigTextWithDifferentSig() throws AutomationException, IOException {
        List<WebElement> patientMedications = driverUtil.getWebElements(PATIENT_MEDICATION_RECORDS);
        if (patientMedications != null && !patientMedications.isEmpty()) {
            int recordCount = patientMedications.size();
            List<SigTranslation> sigTranslations = JSONUtil.readSigText(JSONUtil.SIG_TEXT_FILE_PATH);
            Map<String, SigTranslation> updatedMedicationSigTextMap = new HashMap<>();
            int index = 0;
            int count;
            index = CommonUtil.getIntFromObject(PersistentData.getProperty(SigTranslation.REPORT_SIG_TEXT_EXECUTED_COUNTER_INDEX));
            count = index + recordCount;
            int executedCount = 0;
            try {
                CommonSteps.REPORT_LOGGER.log("Update all patient medications sig");
                CommonSteps.REPORT_LOGGER.log("--------------------------------------------------------------");
                for (; index < count && index < sigTranslations.size(); index++) {
                    executedCount++;
                    SigTranslation sigTranslation = (SigTranslation) sigTranslations.get(index);
                    CommonSteps.REPORT_LOGGER.log(String.format("SIG Text: [%s : %s]", sigTranslation.getSigShortForm(), sigTranslation.getSigEnglishTextForNonMTM()));
                    int medicationIndex = ((index % recordCount) + 1);
                    WebElement medicationRecord = driverUtil.getWebElementAndScroll(PATIENT_MEDICATION_RECORDS + "[" + medicationIndex + "]");
                    WebDriverUtil.waitForAWhile();
                    medicationRecord.click();
                    WebDriverUtil.waitForAWhile();
                    WebElement medicationNameElement = driverUtil.getWebElement(SELECTED_MEDICATION_NAME);
                    if (medicationNameElement == null) {
                        verifyAndUpdateDose(true);
                        String brand = driverUtil.getWebElement(SELECTED_MEDICATION_NAME_INPUT).getAttribute("value");
                        String dosage = driverUtil.getWebElement(SELECTED_MEDICATION_DOSAGE_INPUT).getAttribute("value");
                        String unit = driverUtil.getWebElement(SELECTED_MEDICATION_UNIT_INPUT).getAttribute("value");
                        updatedMedicationSigTextMap.put(brand + " " + dosage + " " + unit, sigTranslation);
                    } else {
                        String medicationName = medicationNameElement.getText();
                        if (medicationName.contains(" MG"))
                            medicationName = medicationName.substring(0, medicationName.indexOf(" MG") + 3);
                        if (medicationName.contains(" MEQ"))
                            medicationName = medicationName.substring(0, medicationName.indexOf(" MEQ") + 4);
                        if (medicationName.contains(" MCG"))
                            medicationName = medicationName.substring(0, medicationName.indexOf(" MCG") + 4);
                        if (medicationName.contains(" IU"))
                            medicationName = medicationName.substring(0, medicationName.indexOf(" IU") + 3);
                        updatedMedicationSigTextMap.put(medicationName, sigTranslation);
                    }
                    WebElement sig = driverUtil.findElementAndScroll(SIG_INPUT);
                    if (sig == null)
                        throw new AutomationException("Unable to locate SIG input box!");
                    sig.click();
                    sig.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
                    sig.sendKeys(sigTranslation.getSigShortForm(), Keys.ENTER);
                    WebDriverUtil.waitForAWhile();
                    WebElement sigTextEnglish = driverUtil.getWebElementAndScroll(MEDICATION_SIG_TEXT_VALUE, WebDriverUtil.NO_WAIT);
                    if (sigTextEnglish == null)
                        throw new AutomationException("SIG English Text details is not being displayed for: " + sigTranslation.getSigShortForm() + "!");
                    sigTextEnglish.click();
                }
                CommonSteps.REPORT_LOGGER.log("--------------------------------------------------------------");
            } catch (Exception ex) {
                throw new AutomationException(ex.getMessage());
            }
            if (index < sigTranslations.size() - 1)
                PersistentData.updatePersistentProperty(SigTranslation.REPORT_SIG_TEXT_EXECUTED_COUNTER_INDEX, Integer.toString(index));
            else
                PersistentData.updatePersistentProperty(SigTranslation.REPORT_SIG_TEXT_EXECUTED_COUNTER_INDEX, "0");
            updatedMedicationSigTextMapping.set(updatedMedicationSigTextMap);
        }
    }

    private void verifyAndUpdateDose(boolean retry) {
        try {
            WebElement doseElement = driverUtil.getWebElement(MEDICATION_DOSE_INPUT);
            if (doseElement.getAttribute("value").isEmpty()) {
                doseElement.click();
                doseElement.sendKeys("Tablet");
                ((JavascriptExecutor) DriverFactory.drivers.get()).executeScript("arguments[0].value='Tablet';", doseElement);
                WebDriverUtil.waitForAWhile();
                WebDriverUtil.waitForAWhile(WebDriverUtil.WAIT_2_SEC);
            }
        } catch (Exception ex1) {
            try {
                WebElement errorPopup = driverUtil.getWebElement(MEDICATION_DOSE_ERROR_POPUP_BUTTON_OK);
                if (errorPopup != null)
                    errorPopup.click();
                if (retry)
                    verifyAndUpdateDose(false);
            } catch (Exception ex2) {
                //DO Nothing..
            }
        }
    }

    public void updateAllMedicationsSigTextWithDifferentSigMTMPatient() throws AutomationException, IOException {
        List<WebElement> patientMedications = driverUtil.getWebElements(PATIENT_MEDICATION_RECORDS);
        if (patientMedications != null && !patientMedications.isEmpty()) {
            int recordCount = patientMedications.size();
            List<SigTranslation> sigTranslations = JSONUtil.readSigText(JSONUtil.SIG_TEXT_FILE_PATH);
            Map<String, SigTranslation> updatedMedicationSigTextMap = new HashMap<>();
            int index = 0;
            int count;
            index = CommonUtil.getIntFromObject(PersistentData.getProperty(SigTranslation.REPORT_SIG_TEXT_EXECUTED_COUNTER_INDEX_MTM_PATIENT));
            count = index + recordCount;
            int executedCount = 0;
            try {
                CommonSteps.REPORT_LOGGER.log("Update all patient medications sig");
                CommonSteps.REPORT_LOGGER.log("--------------------------------------------------------------");
                for (; index < count && index < sigTranslations.size(); index++) {
                    executedCount++;
                    SigTranslation sigTranslation = (SigTranslation) sigTranslations.get(index);
                    CommonSteps.REPORT_LOGGER.log(String.format("SIG Text: [%s : %s]", sigTranslation.getSigShortForm(), sigTranslation.getSigEnglishTextForNonMTM()));
                    int medicationIndex = ((index % recordCount) + 1);
                    WebElement medicationRecord = driverUtil.getWebElementAndScroll(PATIENT_MEDICATION_RECORDS + "[" + medicationIndex + "]");
                    WebDriverUtil.waitForAWhile();
                    medicationRecord.click();
                    WebDriverUtil.waitForAWhile();
                    WebElement medicationNameElement = driverUtil.getWebElement(SELECTED_MEDICATION_NAME);
                    if (medicationNameElement == null) {
                        String brand = driverUtil.getWebElement(SELECTED_MEDICATION_NAME_INPUT).getAttribute("value");
                        String dosage = driverUtil.getWebElement(SELECTED_MEDICATION_DOSAGE_INPUT).getAttribute("value");
                        String unit = driverUtil.getWebElement(SELECTED_MEDICATION_UNIT_INPUT).getAttribute("value");
                        updatedMedicationSigTextMap.put(brand + " " + dosage + " " + unit, sigTranslation);
                    } else {
                        String medicationName = medicationNameElement.getText();
                        if (medicationName.contains(" MG"))
                            medicationName = medicationName.substring(0, medicationName.indexOf(" MG") + 3);
                        if (medicationName.contains(" MEQ"))
                            medicationName = medicationName.substring(0, medicationName.indexOf(" MEQ") + 4);
                        if (medicationName.contains(" MCG"))
                            medicationName = medicationName.substring(0, medicationName.indexOf(" MCG") + 4);
                        if (medicationName.contains(" IU"))
                            medicationName = medicationName.substring(0, medicationName.indexOf(" IU") + 3);
                        updatedMedicationSigTextMap.put(medicationName, sigTranslation);
                    }
                    WebElement sig = driverUtil.findElementAndScroll(SIG_INPUT);
                    if (sig == null)
                        throw new AutomationException("Unable to locate SIG input box!");
                    sig.click();
                    sig.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
                    sig.sendKeys(sigTranslation.getSigShortForm(), Keys.ENTER);
                    WebDriverUtil.waitForAWhile();
                    WebElement sigTextEnglish = driverUtil.getWebElementAndScroll(MEDICATION_SIG_TEXT_VALUE, WebDriverUtil.NO_WAIT);
                    if (sigTextEnglish == null)
                        throw new AutomationException("SIG English Text details is not being displayed for: " + sigTranslation.getSigShortForm() + "!");
                    sigTextEnglish.click();
                    sigTranslation.setSigEnglishTextForMTM(sigTextEnglish.getText());
                    WebElement sigTextSpanish = driverUtil.getWebElementAndScroll(MEDICATION_SIG_TEXT_SPANISH_INPUT, WebDriverUtil.NO_WAIT);
                    if (sigTextSpanish != null) {
                        String spanishTranslate = sigTextSpanish.getAttribute("value");
                        sigTranslation.setSigSpanishTextForMTM(spanishTranslate);
                    }
                }
                CommonSteps.REPORT_LOGGER.log("--------------------------------------------------------------");
            } catch (Exception ex) {
                throw new AutomationException(ex.getMessage());
            }
            if (index < sigTranslations.size() - 1)
                PersistentData.updatePersistentProperty(SigTranslation.REPORT_SIG_TEXT_EXECUTED_COUNTER_INDEX_MTM_PATIENT, Integer.toString(index));
            else
                PersistentData.updatePersistentProperty(SigTranslation.REPORT_SIG_TEXT_EXECUTED_COUNTER_INDEX_MTM_PATIENT, "0");
            updatedMedicationSigTextMapping.set(updatedMedicationSigTextMap);
        }
    }

    public void selectPrescriber(String Prescriber) throws AutomationException {
        WebElement dropdown = driverUtil.getWebElement(PRESCRIBER_DROPDOWN);
        if (dropdown == null)
            throw new AutomationException("Unable to locate Prescriber dropdown!");
        dropdown.click();
        driverUtil.waitForAWhile(2);
        WebElement element = driverUtil.getWebElement("//option[contains(text(),'" + Prescriber + "')]", 3);
        if (element == null)
            throw new AutomationException("Prescriber :" + Prescriber + " not found in drop down or drop down not open");
        driverUtil.waitForAWhile(4);
        element.click();
        driverUtil.waitForAWhile(4);

    }

    public void verifyPrescriberName(String prescriber, String medication) throws AutomationException {
        driverUtil.waitForAWhile(WAIT_3_SECOND);
        WebElement element = driverUtil.findElement("//tr[contains(@class,'MedDetailTable') and .//td[text()='" + medication + "']]//td[contains(text(),'" + prescriber + "')]");
        if (element == null)
            throw new AutomationException("verify Prescriber Name Not Displayed : " + prescriber);

    }

    public void userEnterNotesInNotesToMDBox(String Note) throws AutomationException {
        WebElement NotesInput = driverUtil.getWebElement(NOTE_TO_MD_TEXBOX);
        if (NotesInput == null)
            throw new AutomationException("Note Box Not Displayed! ");
        NotesInput.clear();
        NotesInput.sendKeys(Note);
        driverUtil.waitForAWhile(5);
    }

    public void verifyNoteFromNoteToMDBox(String Note, String medication) throws AutomationException {
        driverUtil.waitForAWhile(WAIT_5_SECOND);
        WebElement element = driverUtil.findElement("//tr[contains(@class,'MedDetailTable') and .//td[text()='" + medication + "']]//td[contains(text(),'" + Note + "')]");
        if (element == null)
            throw new AutomationException("verify Note Not Displayed : " + Note);
    }

    public void userEnterConditionInConditionsBox(String Condition) throws AutomationException {
        WebElement ConditionInput = driverUtil.getWebElement(CONDITION_BOX);
        if (ConditionInput == null)
            throw new AutomationException("Condition Box Not Displayed! ");
        ConditionInput.clear();
        ConditionInput.sendKeys(Condition);
        driverUtil.waitForAWhile(5);
    }

    public void verifyConditionFromConditioncolumn(String Condition, String medication) throws AutomationException {
        driverUtil.waitForAWhile(WAIT_5_SECOND);
        WebElement element = driverUtil.findElement("//tr[contains(@class,'MedDetailTable') and .//td[text()='" + medication + "']]//td[contains(text(),'" + Condition + "')]");
        if (element == null)
            throw new AutomationException("verify Condition Not Displayed : " + Condition);
    }

    public void addNewMedicationRecordIfNotPresent(DataTable dataTable) throws AutomationException {
        try{
            Map<String, String> inputDataMap = convertDataTableIntoMap(dataTable);
            String patient = inputDataMap.get(MedListPage.MedData.PatientId.toString());
            searchPatient(patient);
            clickOnGlobalTab(MEDLIST_TAB);

            WebElement medRecord = driverUtil.getWebElementAndScroll(String.format(MEDLIST_RECORD, inputDataMap.get(MedListPage.MedData.medication.toString())), WebDriverUtil.WAIT_5_SEC);
            if (medRecord == null) {
                driverUtil.getWebElement(ADD_NEW_MEDRECORD, WebDriverUtil.WAIT_2_SEC, "Unable to locate create new med icon!").click();
                driverUtil.getWebElement(MEDICATION_DRUG_NAME, WebDriverUtil.WAIT_2_SEC, "Unable to find Medication name input!").click();

                driverUtil.getWebElement(MEDICATION_DRUG_NAME, WebDriverUtil.WAIT_2_SEC, "Unable to find Medication name input!")
                        .sendKeys(inputDataMap.get(MedListPage.MedData.medication.toString()));
                driverUtil.getWebElement(MEDICATION_DRUG_DOSE, WebDriverUtil.WAIT_1_SEC, "Unable to find Drug Dose input!")
                        .sendKeys(inputDataMap.get(MedListPage.MedData.Dose.toString()));
                driverUtil.getWebElement(MEDICATION_DRUG_UNIT, WebDriverUtil.WAIT_1_SEC, "Unable to find Drug unit input!")
                        .sendKeys(inputDataMap.get(MedListPage.MedData.Unit.toString()));

                driverUtil.getWebElement(MEDLIST_DOSE_FORM_DROPDOWN, WebDriverUtil.WAIT_1_SEC, "Unable to find Med List Dose input!")
                        .sendKeys(inputDataMap.get(MedListPage.MedData.DoseForm.toString()));

                driverUtil.getWebElementAndScroll(GENERIC_NAME_INPUT, WebDriverUtil.WAIT_1_SEC, "Unable to find RXGeneric input!")
                        .sendKeys(inputDataMap.get(MedListPage.MedData.RxGeneric.toString()));

                driverUtil.getWebElement(ADD_MED_BUTTON, WebDriverUtil.WAIT_1_SEC, "Unable to find Add med Button!").click();
                WebDriverUtil.waitForAWhile(5);
            } else {
                medRecord.click();
            }
        }catch (Exception e){
            //do nothing
        }
    }

}
