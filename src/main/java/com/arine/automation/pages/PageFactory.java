package com.arine.automation.pages;

public class PageFactory {
    private static ThreadLocal<PageFactory> factory = new ThreadLocal<>();
    public LoginPage loginPage;
    public HomePage homePage;
    public TaskPage taskPage;
    public PatientPage patientPage;
    public MedListPage medListPage;
    public AdminPortalHomePage adminPortalHomePage;
    private ProfileActionPlanPage profileActionPlanPage;
    private SmartQuestionsPage smartQuestionsPage;
    private PractitionersPage practitionersPage;
    private PharmaciesPage pharmaciesPage;
    private CareTeamPage careTeamPage;
    private PopulationPage populationPage;
    private ReportPage reportPage;
    private QualityPage qualityPage;
    private CommentsPage commentsPage;
    private TaskEditedPage taskEditedPage;
    private LogActionPage logActionPage;
    private ResponsiveLogActionPage responsiveLogActionPage;

    private PageFactory() {
        loginPage = new LoginPage();
        homePage = new HomePage();
        taskPage = new TaskPage();
        patientPage = new PatientPage();
        medListPage = new MedListPage();
        profileActionPlanPage = new ProfileActionPlanPage();
        smartQuestionsPage = new SmartQuestionsPage();
        practitionersPage = new PractitionersPage();
        adminPortalHomePage = new AdminPortalHomePage();
        pharmaciesPage = new PharmaciesPage();
        careTeamPage = new CareTeamPage();
        populationPage = new PopulationPage();
        reportPage = new ReportPage();
        qualityPage = new QualityPage();
        responsiveLogActionPage = new ResponsiveLogActionPage();
        taskEditedPage = new TaskEditedPage();
        logActionPage = new LogActionPage();
        commentsPage=new CommentsPage();
    }

    public static void init() {
        if (factory.get() == null) {
            PageFactory pageFactory = new PageFactory();
            factory.set(pageFactory);
        }
    }

    public static LoginPage loginPage() {
        return factory.get().loginPage;
    }

    public static HomePage homePage() {
        return factory.get().homePage;
    }

    public static TaskPage taskPage() {
        return factory.get().taskPage;
    }

    public static PatientPage patientPage() {
        return factory.get().patientPage;
    }

    public static MedListPage medListPage() {
        return factory.get().medListPage;
    }

    public static ProfileActionPlanPage profileActionPlanPage() {
        return factory.get().profileActionPlanPage;
    }

    public static SmartQuestionsPage smartQuestionsPage() {
        return factory.get().smartQuestionsPage;
    }

    public static PractitionersPage practitionersPage() {
        return factory.get().practitionersPage;
    }

    public static AdminPortalHomePage adminPortalHomePage() {
        return factory.get().adminPortalHomePage;
    }

    public static PharmaciesPage pharmaciesPage() {
        return factory.get().pharmaciesPage;
    }

    public static CareTeamPage careTeamPage() {
        return factory.get().careTeamPage;
    }

    public static PopulationPage populationPage() {
        return factory.get().populationPage;
    }

    public static ReportPage reportPage() {
        return factory.get().reportPage;
    }

    public static QualityPage qualityPage() {
        return factory.get().qualityPage;
    }

    public static TaskEditedPage taskEditedPage() {
        return factory.get().taskEditedPage;
    }

    public static LogActionPage logActionPage() {
        return factory.get().logActionPage;
    }

    public static ResponsiveLogActionPage responsiveLogActionPage() {
        return factory.get().responsiveLogActionPage;
    }

    public static CommentsPage commentsPage(){return factory.get().commentsPage;}
}
