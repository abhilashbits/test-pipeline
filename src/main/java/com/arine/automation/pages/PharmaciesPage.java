package com.arine.automation.pages;

import com.arine.automation.exception.AutomationException;
import com.arine.automation.glue.CommonSteps;
import com.arine.automation.util.WebDriverUtil;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.Locale;

public class PharmaciesPage {

    public static final String NAME_SEARCH_INPUT = "//input[@name='searchFirstName']";
    public static final String CITY_SEARCH_INPUT = "//input[@name='searchCity']";
    public static final String STATE_SEARCH_INPUT = "//input[@name='searchState']";
    public static final String PHONE_SEARCH_INPUT = "//input[@name='searchPhone']";
    public static final String NPI_SEARCH_INPUT = "//input[@name='searchNPI']";
    public static final String ZIP_SEARCH_INPUT = "//input[@name='searchZip']";
    public static final String SEARCH_BUTTON = "//*[@aria-label='search button']";
    public static final String USER_SEARCH_RESULT_NAME_COLUMN = "//tbody/tr/td[1]";
    public static final String USER_SEARCH_RESULT_ADDRESS_COLUMN = "//tbody/tr/td[2]";
    public static final String USER_SEARCH_RESULT_PHONE_COLUMN = "//tbody/tr/td[4]";
    public static final String USER_SEARCH_RESULT_NPI_COLUMN = "//tbody/tr/td[7]";
    public static WebDriverUtil driverUtil = new WebDriverUtil();

    public static void clearsearchInput(String searchInput) throws AutomationException {
        WebElement searchInputWebElement;
        switch (searchInput.toLowerCase()){
            case "name":
                searchInputWebElement=driverUtil.getWebElement(NAME_SEARCH_INPUT);
                if (searchInput == null)
                    throw new AutomationException("Unable to locate name search input!");
                break;
            case "city":
                searchInputWebElement=driverUtil.getWebElement(CITY_SEARCH_INPUT);
                if (searchInput == null)
                    throw new AutomationException("Unable to locate CITY search input!");
                break;
            case "state":
                searchInputWebElement=driverUtil.getWebElement(STATE_SEARCH_INPUT);
                if (searchInput == null)
                    throw new AutomationException("Unable to locate STATE search input!");
                break;
            case "phone":
                searchInputWebElement=driverUtil.getWebElement(PHONE_SEARCH_INPUT);
                if (searchInput == null)
                    throw new AutomationException("Unable to locate PHONE search input!");
                break;
            case "npi":
                searchInputWebElement=driverUtil.getWebElement(NPI_SEARCH_INPUT);
                if (searchInput == null)
                    throw new AutomationException("Unable to locate NPI search input!");
                break;
            default:
                throw new AutomationException("Invalid Search Input. Valid options are Name, City, State,Phone,NPI");
        }
        searchInputWebElement.click();
        searchInputWebElement.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
    }

    public static void search(String searchInput,String searchString) throws AutomationException {
        WebElement searchInputWebElement;
        switch (searchInput.toLowerCase()){
            case "name":
                searchInputWebElement=driverUtil.getWebElement(NAME_SEARCH_INPUT);
                if (searchInput == null)
                    throw new AutomationException("Unable to locate name search input!");
                break;
            case "city":
                searchInputWebElement=driverUtil.getWebElement(CITY_SEARCH_INPUT);
                if (searchInput == null)
                    throw new AutomationException("Unable to locate CITY search input!");
                break;
            case "state":
                searchInputWebElement=driverUtil.getWebElement(STATE_SEARCH_INPUT);
                if (searchInput == null)
                    throw new AutomationException("Unable to locate STATE search input!");
                break;
            case "phone":
                searchInputWebElement=driverUtil.getWebElement(PHONE_SEARCH_INPUT);
                if (searchInput == null)
                    throw new AutomationException("Unable to locate PHONE search input!");
                break;
            case "npi":
                searchInputWebElement=driverUtil.getWebElement(NPI_SEARCH_INPUT);
                if (searchInput == null)
                    throw new AutomationException("Unable to locate NPI search input!");
                break;
            default:
                throw new AutomationException("Invalid Search Input. Valid options are Name, City, State,Phone,NPI");
        }
        String existing = searchInputWebElement.getAttribute("value");
        if (existing == null || !existing.equalsIgnoreCase(searchString)) {
            searchInputWebElement.click();
            searchInputWebElement.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
            searchInputWebElement.sendKeys(searchString);
        }
    }

    public static void userClickOnSearchIcon() throws AutomationException {
        WebElement searchBtn=driverUtil.getWebElement(SEARCH_BUTTON);
        if(searchBtn==null)
            throw new AutomationException("Search button not displayed");
        searchBtn.click();
        WebDriverUtil.waitForAWhile(10);
        CommonSteps.takeScreenshot();
    }

    public void verifyColumnData(String columnName, String searchString) throws AutomationException {
        switch (columnName.toLowerCase()) {
            case "name":
                List<WebElement> webElements = driverUtil.getWebElements(USER_SEARCH_RESULT_NAME_COLUMN);
                if (webElements.size() == 0)
                    throw new AutomationException("We expected " + columnName + " column's to have searched string : " + searchString);
                for (WebElement element : webElements
                ) {
                    if (!element.getText().toLowerCase().contains(searchString.toLowerCase()))
                        throw new AutomationException("We expected name column to have searched string but found " + element.getText());
                }
                break;
            case "address":
                webElements = driverUtil.getWebElements(USER_SEARCH_RESULT_ADDRESS_COLUMN);
                if (webElements.size() == 0)
                    throw new AutomationException("We expected address column's to have searched string : " + searchString);
                for (WebElement element : webElements
                ) {
                    if (!element.getText().toLowerCase().contains(searchString.toLowerCase()))
                        throw new AutomationException("We expected address column to have searched string but found " + element.getText());
                }
                break;
            case "phone":
                webElements = driverUtil.getWebElements(USER_SEARCH_RESULT_PHONE_COLUMN);
                if (webElements.size() == 0)
                    throw new AutomationException("We expected " + columnName + " column's to have searched string : " + searchString);
                for (WebElement element : webElements
                ) {
                    if (!element.getText().toLowerCase().contains(searchString.toLowerCase()))
                        throw new AutomationException("We expected Pref Phone column to have searched string but found " + element.getText());
                }
                break;
            case "npi":
                webElements = driverUtil.getWebElements(USER_SEARCH_RESULT_NPI_COLUMN);
                if (webElements.size() == 0)
                    throw new AutomationException("We expected " + columnName + " column's to have searched string : " + searchString);
                for (WebElement element : webElements
                ) {
                    if (!element.getText().toLowerCase().contains(searchString.toLowerCase()))
                        throw new AutomationException("We expected account column to have searched string but found " + element.getText());
                }
                break;
            default:
                throw new AutomationException("Invalid Column Name. Valid options are Name, City, State,Phone,NPI");
        }
    }

}
