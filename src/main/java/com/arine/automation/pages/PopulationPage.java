package com.arine.automation.pages;

import com.arine.automation.exception.AutomationException;
import com.arine.automation.glue.CommonSteps;
import org.openqa.selenium.WebElement;

import java.util.logging.Logger;

public class PopulationPage extends BasePage {
    private static final Logger LOGGER = Logger.getLogger(PopulationPage.class.getName());
    public static final String DASHBOARD_IFRAME = "//*[@id='dashboardContainer']//iframe";
    public static final String DASHBOARD_SELECTION_DROPDOWN = "//*[@id='currentDashboardView']";

    @Override
    public String getName() {
        return "Population";
    }

    public void verifyDashboardNotDisplayed() throws AutomationException {
        WebElement dashboard=driverUtil.getWebElement(DASHBOARD_IFRAME,10);
        if(dashboard!=null)
            throw new AutomationException("Dashboard displayed but we expected it to be not displayed");
        CommonSteps.takeScreenshot();
    }

    public void verifyDashboardIsDisplayed() throws AutomationException {
        WebElement dashboard=driverUtil.getWebElement(DASHBOARD_IFRAME,20);
        if(dashboard==null)
            throw new AutomationException("Dashboard not displayed but we expected it to be displayed");
        CommonSteps.takeScreenshot();
    }

    public void selectDashboard(String dashboardName) throws AutomationException {
        WebElement dropdown=driverUtil.getWebElement(DASHBOARD_SELECTION_DROPDOWN,10);
        if(dropdown==null)
            throw new AutomationException("Dashboard selection dropdown not displayed");
        dropdown.click();
        CommonSteps.takeScreenshot();
        WebElement element = driverUtil.getWebElement("//option[" + driverUtil.ignoreCase(dashboardName) + "]", 3);
        if (element == null)
            throw new AutomationException("Dashboard :" + dashboardName + " not found in drop down or drop down not open");
        element.click();
        waitForLoadingPage();
        CommonSteps.takeScreenshot();
    }


}
