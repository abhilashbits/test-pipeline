package com.arine.automation.pages;

import com.arine.automation.exception.AutomationException;
import com.arine.automation.util.WebDriverUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.HashMap;
import java.util.Map;

public class PractitionersPage extends BasePage {
    public static final String PRACTITIONERS_SEARCH_INPUT = "//input[@name='nameInput']";
    public static final String PRACTITIONER_DETAILS = "//*[contains(@class,'Practitioners-__topTable')]//td";
    public static final String PRACTITIONER_NAME_INDEX = "[1]";
    public static final String PRACTITIONER_NPI_INDEX = "[2]";
    public static final String PRACTITIONER_SPECIALITY_INDEX = "[3]";
    public static final String PRACTITIONER_ADDRESS_INDEX = "[4]";
    public static final String PRACTITIONER_LAST_CONSULTATION_INDEX = "[5]";
    public static final String PRACTITIONER_PROFILE_DISPLAY = "//*[contains(@class,'LandingPage-components-units-PractitionerInput-__container')]//form";
    public static final String PRACTITIONER_PERSONAL_INFO_LABEL = "//*[contains(@class,'PractitionerInput')]//*[text()='%s']";
    public static final String PRACTITIONER_PERSONAL_INFO_INPUT = "//*[contains(@class,'PractitionerInput')]//input[@name='%s']";
    public static final String PRACTITIONER_GLOBAL_DETAILS = "//*[contains(@class,'__topTable')]//td[%s]";


    public static Map<String, Integer> GLOBAL_DETAILS_TAB_MAPPING = new HashMap<>();
    static {
        GLOBAL_DETAILS_TAB_MAPPING.put("Name", 1);
        GLOBAL_DETAILS_TAB_MAPPING.put("NPI", 2);
        GLOBAL_DETAILS_TAB_MAPPING.put("Speciality", 3);
        GLOBAL_DETAILS_TAB_MAPPING.put("Address", 4);
        GLOBAL_DETAILS_TAB_MAPPING.put("Last Consultation", 5);
    }

    public static Map<String, String> PRACTITIONER_DETAILS_INPUT_MAPPING = new HashMap<>();
    static {
        PRACTITIONER_DETAILS_INPUT_MAPPING.put("Address 1", "address1");
        PRACTITIONER_DETAILS_INPUT_MAPPING.put("Address 2", "address2");
        PRACTITIONER_DETAILS_INPUT_MAPPING.put("City", "city");
        PRACTITIONER_DETAILS_INPUT_MAPPING.put("State", "statecode");
        PRACTITIONER_DETAILS_INPUT_MAPPING.put("Zip Code", "zipcode");
        PRACTITIONER_DETAILS_INPUT_MAPPING.put("Preferred Phone", "phone1");
        PRACTITIONER_DETAILS_INPUT_MAPPING.put("Second Phone", "phone2");
        PRACTITIONER_DETAILS_INPUT_MAPPING.put("Fax", "faxValue");
        PRACTITIONER_DETAILS_INPUT_MAPPING.put("E-mail", "email");
    }

    public void searchPractitioner(String id) throws AutomationException {
        PageFactory.homePage().gotoMenu("Practitioners");
        WebElement searchInput = driverUtil.getWebElement(PRACTITIONERS_SEARCH_INPUT);
        if(searchInput==null)
            throw new AutomationException("Unable to locate practitioners search input!");
        String existing = searchInput.getAttribute("value");
        if(existing==null || !existing.equalsIgnoreCase(id)) {
            searchInput.clear();
            searchInput.sendKeys(id, Keys.ENTER);
            if(!checkErrorPopup()) {
                WebDriverUtil.waitForVisibleElement(By.xpath(HomePage.SEARCH_BUTTON));
                driverUtil.waitForLoadingPage();
            }
        }

    }

    public String getPractitionerDetail(String type) throws AutomationException {
        return driverUtil.getWebElementAndScroll(PRACTITIONER_DETAILS+type, WebDriverUtil.MAX_ELEMENT_WAIT).getText();
    }

    public void verifyPractitionerDetailsSection() throws AutomationException {
        WebElement patientDetailsSection = driverUtil.getWebElementAndScroll(PRACTITIONER_PROFILE_DISPLAY,WebDriverUtil.NO_WAIT);
        if(patientDetailsSection==null)
            clickOnNameLink();
        patientDetailsSection = driverUtil.getWebElementAndScroll(PRACTITIONER_PROFILE_DISPLAY);
        if(patientDetailsSection==null)
            throw new AutomationException("Practitioner details section is not being displayed!");
    }

    public void veryPractitionerPersonalDetail(String label, String expectedValue) throws AutomationException {
        verifyPractitionerDetailsSection();
        String nameAttribute = PRACTITIONER_DETAILS_INPUT_MAPPING.get(label);
        WebElement infoInput = driverUtil.getWebElementAndScroll(String.format(PRACTITIONER_PERSONAL_INFO_INPUT,nameAttribute));
        if(infoInput==null)
            throw new AutomationException(String.format("%s input box is not being displayed!",label));
        String actualValue = infoInput.getAttribute("value");
        if(!actualValue.toLowerCase().contains(expectedValue.toLowerCase()))
            throw new AutomationException(String.format("%s details is not being matched! Expected value: %s but found: %s",label,expectedValue,actualValue));
    }

    public void updatePractitionerPersonalDetail(String label, String value) throws AutomationException {
        String nameAttribute = PRACTITIONER_DETAILS_INPUT_MAPPING.get(label);
        WebElement infoInput = driverUtil.getWebElementAndScroll(String.format(PRACTITIONER_PERSONAL_INFO_INPUT,nameAttribute));
        if(infoInput==null)
            throw new AutomationException(String.format("%s input box is not being displayed!",label));
        String tagName = infoInput.getTagName();
        switch(tagName) {
            case "input":
                infoInput.click();
                infoInput.sendKeys(Keys.chord(Keys.CONTROL,"a",Keys.DELETE));
                infoInput.sendKeys(value,Keys.ENTER);
                infoInput.clear();
                break;
            case "select":
                Select select = new Select(infoInput);
                select.selectByVisibleText(value);
                break;
            default:
                break;
        }
        WebDriverUtil.waitForAWhile(10);
        clickOnNameLink();
    }

    public void clickOnNameLink() throws AutomationException {
        int index = GLOBAL_DETAILS_TAB_MAPPING.get("Name");
        WebElement patientInfo = driverUtil.findElementAndScroll(String.format(PRACTITIONER_GLOBAL_DETAILS, index));
        if(patientInfo==null)
            throw new AutomationException("Unable to find name link!");
        patientInfo.click();
        WebDriverUtil.waitForAWhile(2);
    }


    @Override
    String getName() {
        return "Practitioners";
    }
}
