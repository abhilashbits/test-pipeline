package com.arine.automation.pages;

import com.arine.automation.exception.AutomationException;
import com.arine.automation.glue.CommonSteps;
import com.arine.automation.util.WebDriverUtil;
import cucumber.api.DataTable;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.arine.automation.glue.CommonSteps.takeScreenshot;

public class ProfileActionPlanPage extends PatientPage {
    public static final String PROFILE_ACTION_TAB = "Profile and Action Plan";

    private static final String DRP_TABLE = "//*[contains(@class,'headerTable') and .//*[text()='DRP']]/..";
    private static final String ADD_DRP_ICON = "//*[contains(@class,'headerTable') and .//*[text()='DRP']]//*[contains(@class,'iconAdd')]";
    private static final String DRP_INPUT = "//textarea[@id='drp']";
    private static final String PAT_ASMT_INPUT = "//textarea[@id='patAsmt']";
    private static final String PAT_REC_INPUT = "//textarea[@id='patRec']";
    private static final String PROV_ASMT_INPUT = "//textarea[@id='provAsmt']";
    private static final String PROV_REC_INPUT = "//textarea[@id='provRec']";

    private static final String DRP_RECORD = "//tr[.//*[contains(@class,'drp') and text()='%s']]";
    private static final String ALL_DRP_RECORD = "//tr[.//*[contains(@class,'drp')]]//span[contains(@class,'RecommendationTable-__drp')]";
    private static final String FIRST_DRP_RECORD = "//tr[.//*[contains(@class,'drp')]][1]//span[2]";
    private static String FIRST_DRP_RECORD_NAME = "";
    private static final String DRP_RECORD_RADIOBUTTON = "//tr[.//*[contains(@class,'drp') and text()='%s']]//span[contains(@class,'checkbox')]";
    private static final String DELETE_DRP_ICON = "//*[@data-tip='Delete selected DRP']";
    private static final String SHOW_DELETED_DRP_ICON = "//*[@data-tip='click to show deleted DRP']";
    private static final String HIDE_DELETED_DRP_ICON = "//*[@data-tip='click to hide deleted DRP']";
    private static final String RESTORE_DELETED_DRP_ICON = "//tr[.//*[contains(@class,'drp') and text()='%s']]//*[@data-icon='trash-restore']";
    private static final String MEDICATION_LIST_PML_POPUP = "//*[contains(@class,'PreviewReportMulti')]//table//tr[contains(@class,'PreviewReportMulti')]//td[1]";
    private static final String MEDICATION_LIST_LANDING_PAGE = "//*[contains(@class,'MedDetailTable') and contains(@class,'tableContainer')]//tr//td[1]";

    public void addDRP(DataTable data) throws AutomationException {
        Map<String, String> inputDataMap = convertDataTableIntoMap(data);
        String patient = inputDataMap.get(ProfileActionPlanPage.DRPData.PatientId.toString());
        searchPatient(patient);
        clickOnGlobalTab(PROFILE_ACTION_TAB);

        if(PageFactory.profileActionPlanPage().verifyDrpRecordExist(inputDataMap.get(DRPData.DrugRelatedProblem.toString()))) {
            return;
        }
        WebElement element = driverUtil.getWebElement(ADD_DRP_ICON, WebDriverUtil.WAIT_2_SEC,"Unable to locate create new med icon!");
        if(element==null)
            throw new AutomationException("Unable to find add new DRP icon!");
        element.click();
        element = driverUtil.getWebElementAndScroll(DRP_INPUT,WebDriverUtil.WAIT_2_SEC,"Unable to find Drug Related Problem input!");
        WebDriverUtil.waitForAWhile();
        element.sendKeys(inputDataMap.get(DRPData.DrugRelatedProblem.toString()));
        element = driverUtil.getWebElementAndScroll(PAT_ASMT_INPUT, WebDriverUtil.WAIT_1_SEC, "Unable to find Patient Assessment input!");
        WebDriverUtil.waitForAWhile();
        element.sendKeys(inputDataMap.get(DRPData.PatientAssessment.toString()));
        element = driverUtil.getWebElementAndScroll(PAT_REC_INPUT, WebDriverUtil.WAIT_1_SEC, "Unable to find Patient Recommendation input!");
        WebDriverUtil.waitForAWhile();
        element.sendKeys(inputDataMap.get(DRPData.PatientRecommendation.toString()));
        element = driverUtil.getWebElementAndScroll(PROV_ASMT_INPUT, WebDriverUtil.WAIT_1_SEC, "Unable to find Provider Assessment input!");
        WebDriverUtil.waitForAWhile();
        element.sendKeys(inputDataMap.get(DRPData.ProviderAssessment.toString()));
        element = driverUtil.getWebElementAndScroll(PROV_REC_INPUT, WebDriverUtil.WAIT_1_SEC, "Unable to find Provider Recommendation input!");
        WebDriverUtil.waitForAWhile();
        element.sendKeys(inputDataMap.get(DRPData.ProviderRecommendation.toString()));
        WebDriverUtil.waitForAWhile(5);
    }

    public void addDRPIfNotPresent(DataTable data) throws AutomationException {
        Map<String, String> inputDataMap = convertDataTableIntoMap(data);
        String patient = inputDataMap.get(ProfileActionPlanPage.DRPData.PatientId.toString());
        searchPatient(patient);
        clickOnGlobalTab(PROFILE_ACTION_TAB);
        showDRP();
        WebElement restoreDRPIcon = driverUtil.getWebElementAndScroll(String.format(RESTORE_DELETED_DRP_ICON, inputDataMap.get(DRPData.DrugRelatedProblem.toString())));
        if (restoreDRPIcon != null) {
            WebDriverUtil.waitForAWhile();
            restoreDRPIcon.click();
        }
        WebElement drpRecord = driverUtil.getWebElementAndScroll(String.format(DRP_RECORD, inputDataMap.get(DRPData.DrugRelatedProblem.toString())), WebDriverUtil.WAIT_5_SEC);
        try {
            if (drpRecord == null) {
                WebElement element = driverUtil.getWebElement(ADD_DRP_ICON, WebDriverUtil.WAIT_2_SEC,"Unable to locate create new med icon!");
                if(element==null)
                    throw new AutomationException("Unable to find add new DRP icon!");
                element.click();
                element = driverUtil.getWebElementAndScroll(DRP_INPUT,WebDriverUtil.WAIT_2_SEC,"Unable to find Drug Related Problem input!");
                WebDriverUtil.waitForAWhile();
                element.sendKeys(inputDataMap.get(DRPData.DrugRelatedProblem.toString()));
                element = driverUtil.getWebElementAndScroll(PAT_ASMT_INPUT, WebDriverUtil.WAIT_1_SEC, "Unable to find Patient Assessment input!");
                WebDriverUtil.waitForAWhile();
                element.sendKeys(inputDataMap.get(DRPData.PatientAssessment.toString()));
                element = driverUtil.getWebElementAndScroll(PAT_REC_INPUT, WebDriverUtil.WAIT_1_SEC, "Unable to find Patient Recommendation input!");
                WebDriverUtil.waitForAWhile();
                element.sendKeys(inputDataMap.get(DRPData.PatientRecommendation.toString()));
                element = driverUtil.getWebElementAndScroll(PROV_ASMT_INPUT, WebDriverUtil.WAIT_1_SEC, "Unable to find Provider Assessment input!");
                WebDriverUtil.waitForAWhile();
                element.sendKeys(inputDataMap.get(DRPData.ProviderAssessment.toString()));
                element = driverUtil.getWebElementAndScroll(PROV_REC_INPUT, WebDriverUtil.WAIT_1_SEC, "Unable to find Provider Recommendation input!");
                WebDriverUtil.waitForAWhile();
                element.sendKeys(inputDataMap.get(DRPData.ProviderRecommendation.toString()));
                WebDriverUtil.waitForAWhile(5);
            }
        } finally {
            hideDRP();
        }
    }

    public void verifyDrpRecord(String drp) throws AutomationException {
        driverUtil.getWebElementAndScroll(String.format(DRP_RECORD, drp), WebDriverUtil.WAIT_5_SEC, "Unable to locate DRP record!");
        CommonSteps.takeScreenshot();
    }

    public void verifyDrpRecordNotDisplayed(String drp) throws AutomationException {
        WebElement element = driverUtil.getWebElementAndScroll(String.format(DRP_RECORD, drp), WebDriverUtil.WAIT_5_SEC);
        if (element != null)
            throw new AutomationException("We expected DRP Record : " + drp + " should not be displayed but its displayed");
        CommonSteps.takeScreenshot();
    }

    public boolean verifyDrpRecordExist(String drp) throws AutomationException {
        WebElement drpRecord = driverUtil.getWebElementAndScroll(String.format(DRP_RECORD,drp),WebDriverUtil.WAIT_5_SEC);
        return (drpRecord!=null);
    }

    //Profile And Action Plan
    public void deleteDRP(String drpName) throws AutomationException {
        WebElement radioBtn = driverUtil.getWebElementAndScroll(String.format(DRP_RECORD_RADIOBUTTON, drpName));
        if (radioBtn == null)
            throw new AutomationException(String.format("Unable to find DRP: %s or it might taking too long time to load!", drpName));
        WebDriverUtil.waitForAWhile();
        radioBtn.click();
        WebElement deleteDRPIcon = driverUtil.getWebElement(DELETE_DRP_ICON, 5);
        if (deleteDRPIcon == null)
            throw new AutomationException("Delete DRP Icon not displayed");
        deleteDRPIcon.click();
        takeScreenshot();
    }

    //Profile And Action Plan
    public void restoreDRP(String drpName) throws AutomationException {
        WebElement restoreDRPIcon = driverUtil.getWebElementAndScroll(String.format(RESTORE_DELETED_DRP_ICON, drpName));
        if (restoreDRPIcon == null)
            throw new AutomationException(String.format("Unable to find Restore DRP Icon: %s or it might taking too long time to load!", drpName));
        WebDriverUtil.waitForAWhile();
        restoreDRPIcon.click();
        takeScreenshot();
    }

    public void showDRP() throws AutomationException {
        WebElement showDeletedDRP = driverUtil.getWebElement(SHOW_DELETED_DRP_ICON, 5);
        if (showDeletedDRP != null)
            showDeletedDRP.click();
        takeScreenshot();
    }

    public void hideDRP() throws AutomationException {
        WebElement hideDeletedDRP = driverUtil.getWebElement(HIDE_DELETED_DRP_ICON, 5);
        if (hideDeletedDRP != null)
            hideDeletedDRP.click();
        takeScreenshot();
    }

    public void verifyPersonalMedicationList() throws AutomationException {
        List<WebElement> webElementListOnPopup = driverUtil.getWebElements(MEDICATION_LIST_PML_POPUP);
        List<String> listOfMedicationsOnPMLPopup = new ArrayList<>();
        if (webElementListOnPopup != null) {
            for (WebElement element : webElementListOnPopup
            ) {
                listOfMedicationsOnPMLPopup.add(element.getText());
            }
        }
        closeLogNewActionPopup();
        List<WebElement> webElementListOnLandingPage = driverUtil.getWebElements(MEDICATION_LIST_LANDING_PAGE);
        List<String> listOfMedicationsLandingPage = new ArrayList<>();
        if (webElementListOnLandingPage != null) {
            for (WebElement element : webElementListOnLandingPage) {
                if (!element.getText().trim().isEmpty()) {
                    listOfMedicationsLandingPage.add(element.getText());
                }
            }
        }
        if (listOfMedicationsOnPMLPopup.size()!=listOfMedicationsLandingPage.size())
            throw new AutomationException("List of Medication on PML Pop up & Profile And Action plan doesn't match");
    }

    public void verifyDRPInfoOrdering() throws AutomationException {
        List<WebElement> drpListElements=driverUtil.getWebElements(ALL_DRP_RECORD);
        if(drpListElements==null)
            throw new AutomationException("DRP Records not displayed or xpath to get all drp's from profile & action plan changed ");
        if(drpListElements.size()!= drpInfoList.size())
            throw new AutomationException("Count of DRP's on Profile & action plan is not matching with the count of DRP's on make report tab");

      for(int i=0;i<drpListElements.size();i++){
          String drpProfileAndActionPlanTab=drpListElements.get(i).getText();
          String drpMakeReportTab=drpInfoList.get(i).getDrpName();
          if(!drpProfileAndActionPlanTab.equals(drpMakeReportTab))
              throw new AutomationException("Ordering of DRP not same on Profile And Action Plan and Make report tab");
      }
    }

    public void restoreDRPIfDeleted(String drpName) throws AutomationException {
        showDRP();
        WebElement restoreDRPIcon = driverUtil.getWebElementAndScroll(String.format(RESTORE_DELETED_DRP_ICON, drpName));
        if (restoreDRPIcon != null) {
            WebDriverUtil.waitForAWhile();
            restoreDRPIcon.click();
            takeScreenshot();
        }
        hideDRP();
    }

    private static enum DRPData {
        PatientId,
        DrugRelatedProblem,
        PatientAssessment,
        PatientRecommendation,
        ProviderAssessment,
        ProviderRecommendation;

        public String getName(ProfileActionPlanPage.DRPData drp) {
            return drp.toString();
        }
    }
}
