package com.arine.automation.pages;

import com.arine.automation.drivers.DriverFactory;
import com.arine.automation.exception.AutomationException;
import com.arine.automation.glue.CommonSteps;
import com.arine.automation.util.WebDriverUtil;
import cucumber.api.DataTable;
import io.cucumber.java.bs.A;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.internal.thread.graph.IThreadWorkerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class QualityPage extends PatientPage {
    private static final String QUALITY_TAB = "Quality";
    private static final String MED_DIAGNOSIS_TABLE = "//table[.//thead[.//*[contains(text(),'Med-Diagnosis')]]]";
    private static final String MED_RECORD = "//table[.//thead[.//*[contains(text(),'Smart Questions')]]]//td[text()='%s']";
    private static final String SMART_QUESTION_YES_CHECKBOX = "//*[text()='%s']/../div//input[@value=1]";
    private static final String SMART_QUESTION_NO_CHECKBOX = "//*[text()='%s']/../div//input[@value=2]";
    private static final String HEALTH_CONDITIONS = "//table[.//thead[.//*[contains(text(),'Health Conditions')]]]//td[text()='%s']";
    private static final String UPDATE_SMART_QUESTIONS_BUTTON = "//button[text()='Update Smart Questions']";
    private static final String REPORT_TAB_CONVERSATION_DATE = "//input[@name='conversationDate']";
    private static final String REPORT_TAB_PREVIOUS_DAY = "//div[contains(@class,'today')]/preceding-sibling::div[1]";
    public static String firstSmartQuestionCurrentAnswer = "";
    private static final String MED_RECORDS = "//tr[contains(@class,'MedDetailTable')]";
    private static final String MED_RECORD_COLUMN_HEADER = "//*[contains(@class,'MedDetailTable')]//th[text()='%s']";
    private static final String FIRST_MED_RECORD_COLUMN_VALUE = "(//tr[contains(@class,'MedDetailTable')])[1]//td[%s]/span";
    private static final String SMART_QUESTIONS = "//*[text()='Smart Questions']/..//tbody//tr";
    private static final String SMART_QUESTIONS_FIRST_RECORD_ANSWER = "//*[text()='Smart Questions']/..//tbody//tr[1]/td[2]";
    private static final String FIRST_RECORD_SMART_QUESTIONS_FORM_SELECTED_STATUS = "(//*[contains(@class,'SmartQuestionForm-__scrollbox')]//p)[1]/parent::div[contains(@class,'selectedQuestion')]";
    private static final String FIRST_RECORD_SMART_QUESTIONS_FORM_ANSWER_OPTIONS = "(//*[contains(@class,'SmartQuestionForm-__scrollbox')]//p)[1]/../div//input";
    private static final String ADHERENCE_STATUS_MEASURES = "//*[contains(@class,'MedAdherenceStatusTable')]//tbody//tr//td[text()='%s']";
    private static final String SELECT_STATUS_OF_MEASURE = "//*[text()='%s']/..//*[@id='status']";
    private static final String UPDATED_DATE_OF_MEASURE = "//*[text()='%s']/..//td[3]";
    private static final String MED_DETAIL_TABLE_FILTER_APPLIED = "//*[contains(@class,'MedDetailTable') and contains(@style,'lightgray')]";
    private static final String MED_DETAIL_TABLE_FILTER_NOT_APPLIED = "//*[contains(@class,'MedDetailTable') and contains(@style,'white')]";
    private static final String ALL_MED_RECORD_PDC_COLUMN_DATA = "(//tr[contains(@class,'MedDetailTable')])//td[13]";

    public void selectFirstMedRecord() throws AutomationException {
        List<WebElement> elementList = driverUtil.getWebElements(MED_RECORDS);
        if (elementList.size() < 1)
            throw new AutomationException("No medications displayed");
        elementList.get(0).click();
        elementList = driverUtil.getWebElements(MED_RECORDS);
        if (!elementList.get(0).getAttribute("class").contains("selected"))
            throw new AutomationException("We expected first medication record to be selected but it was probably not selected");
        CommonSteps.takeScreenshot();
    }

    public void selectFirstSmartQuestion() throws AutomationException {
        List<WebElement> elementList = driverUtil.getWebElements(SMART_QUESTIONS);
        if (elementList.size() < 1)
            throw new AutomationException("No smart questions displayed");
        elementList.get(0).click();
        elementList = driverUtil.getWebElements(SMART_QUESTIONS);
        if (!elementList.get(0).getAttribute("class").contains("selected"))
            throw new AutomationException("We expected first smart question to be selected but it was probably not selected");
        CommonSteps.takeScreenshot();
    }

    public void verifySmartQuestionFormDisplayedAndFirstQuestionSelected() throws AutomationException {
        WebElement element = driverUtil.getWebElement(FIRST_RECORD_SMART_QUESTIONS_FORM_SELECTED_STATUS);
        if (element == null)
            throw new AutomationException("Smart Question form not displayed or expected smart question was not selected on the form");
    }

    public void updateFirstSmartQuestionAnswer() throws AutomationException {
        WebElement element = driverUtil.getWebElement(SMART_QUESTIONS_FIRST_RECORD_ANSWER);
        if (element == null)
            throw new AutomationException("No smart questions displayed");
        firstSmartQuestionCurrentAnswer = element.getText();

        List<WebElement> webElementList = driverUtil.getWebElements(FIRST_RECORD_SMART_QUESTIONS_FORM_ANSWER_OPTIONS);
        if (webElementList == null)
            throw new AutomationException("Could not find answer options for selected smart question");

        for (WebElement answerOption : webElementList
        ) {
            String type = answerOption.getAttribute("type");
            if (type.equalsIgnoreCase("radio")) {
                if (!answerOption.isSelected()) {
                    answerOption.click();
                    break;
                }
            } else if (type.equalsIgnoreCase("text")) {
                answerOption.clear();
                answerOption.sendKeys("Updated by automation_" + System.currentTimeMillis());
                answerOption.sendKeys(Keys.ENTER);
                break;
            } else if (type.equalsIgnoreCase("checkbox")) {
                answerOption.click();
                break;
            } else {
                throw new AutomationException("New input type added to smart questions, needs to be handled in automation");
            }
        }

        WebElement firstSmartQuestionUpdatedAnswer = driverUtil.getWebElement(SMART_QUESTIONS_FIRST_RECORD_ANSWER);
        if (firstSmartQuestionUpdatedAnswer == null)
            throw new AutomationException("No smart questions displayed");

        if (firstSmartQuestionCurrentAnswer.equals(firstSmartQuestionUpdatedAnswer.getText()))
            throw new AutomationException("We expected the answer for first smart question to be updated, but it was probably not updated");

    }

    public void verifyAdherenceStatusTable(List<String> measures) throws AutomationException {
        for (String measure : measures
        ) {
            WebElement element = driverUtil.getWebElement(String.format(ADHERENCE_STATUS_MEASURES, measure));
            if (element == null)
                throw new AutomationException(String.format("Measure %s not displayed in adherence status table", measure));
        }
    }

    public void updateAdherenceStatusOfMeasures(String measure, String status) throws AutomationException {
        WebElement selectDropdown = driverUtil.getWebElementAndScroll(String.format(SELECT_STATUS_OF_MEASURE, measure));
        if (selectDropdown == null)
            throw new AutomationException("Its taking long time to load or Select dropdown for not displayed for : " + measure);
        driverUtil.moveToElementAndClick(selectDropdown);
        Select dropdownValue = new Select(selectDropdown);
        dropdownValue.selectByVisibleText(status);
    }

    public void verifyFirstMedRecordColumnValue(String columnName, String status) throws AutomationException {
        WebElement webElement = driverUtil.getWebElement(FIRST_MED_RECORD_COLUMN_VALUE);
        switch (columnName.toUpperCase()) {

            case "AUTO":
                // column index in table is 15
                webElement = driverUtil.getWebElement(String.format(FIRST_MED_RECORD_COLUMN_VALUE, "14"));
                if (webElement == null)
                    throw new AutomationException(String.format("Column %s not found", columnName));
                if (!webElement.getAttribute("data-tip").contains(status))
                    throw new AutomationException(String.format("Expected %s to contain status as %s but found %s", columnName, status, webElement.getText()));
                break;
            case "REFILL":
                // column index in table is 15
                webElement = driverUtil.getWebElement(String.format(FIRST_MED_RECORD_COLUMN_VALUE, "15"));
                if (webElement == null)
                    throw new AutomationException(String.format("Column %s not found", columnName));
                if (!webElement.getAttribute("data-tip").contains(status))
                    throw new AutomationException(String.format("Expected %s to contain refill as %s but found %s", columnName, status, webElement.getText()));
                break;
            case "90DS":
                // added 14 because the column index in table is 14
                webElement = driverUtil.getWebElement(String.format(FIRST_MED_RECORD_COLUMN_VALUE, "16"));
                if (webElement == null)
                    throw new AutomationException(String.format("Column %s not found", columnName));
                if (!webElement.getAttribute("data-tip").contains(status))
                    throw new AutomationException(String.format("Expected %s to contain status as %s but found %s", columnName, status, webElement.getText()));
                break;
            case "HOME":
                // column index in table is 16
                webElement = driverUtil.getWebElement(String.format(FIRST_MED_RECORD_COLUMN_VALUE, "17"));
                if (webElement == null)
                    throw new AutomationException(String.format("Column %s not found", columnName));
                if (!webElement.getAttribute("data-tip").contains(status))
                    throw new AutomationException(String.format("Expected %s to contain status as %s but found %s", columnName, status, webElement.getText()));
                break;
            default:
                throw new AutomationException(String.format("Column name '%s' not covered in automation", columnName));
        }
    }

    public void verifyUpdatedDateForMeasure(String measureName) throws AutomationException {
        WebElement element = driverUtil.getWebElement(String.format(UPDATED_DATE_OF_MEASURE, measureName));
        if (element == null)
            throw new AutomationException("Updated column not displayed");
        String actualDate = element.getText();
        LocalDate localDate = LocalDate.now();
        String expectedDate = localDate.toString();
        if (!actualDate.equals(expectedDate))
            throw new AutomationException(String.format("Updated date not displayed as expected, we expected date to be %s but found %s", expectedDate, actualDate));
    }

    public void verifyMedicationTableHasColumn(String columnName) throws AutomationException {
        WebElement element = driverUtil.getWebElement(String.format(MED_RECORD_COLUMN_HEADER, columnName), 10);
        if (element == null)
            throw new AutomationException("Column header not present in medication table : " + columnName);
    }

    public void verifyMedicationTableNotHasColumn(String columnName) throws AutomationException {
        WebElement element = driverUtil.getWebElement(String.format(MED_RECORD_COLUMN_HEADER, columnName), 2);
        if (element != null)
            throw new AutomationException("Column header present in medication table but we expect it to be not present : " + columnName);
    }

    public void verifyMedRecordsWithOnlyPDCValueDisplayed() throws AutomationException {
        List<WebElement> webElementList = driverUtil.getWebElements(ALL_MED_RECORD_PDC_COLUMN_DATA);
        if (webElementList == null)
            throw new AutomationException("No med records displayed with pdc values");
        for (WebElement element : webElementList
        ) {
            if (element.getText().equals(""))
                throw new AutomationException("We expected med records with only pdc value should be displayed, but all med records displayed");
        }
    }

    public void applyFilterMedDetailTable() throws AutomationException {
        WebElement element = driverUtil.getWebElement(MED_DETAIL_TABLE_FILTER_NOT_APPLIED, 10);
        if (element != null)
            element.click();

        element = driverUtil.getWebElement(MED_DETAIL_TABLE_FILTER_APPLIED, 10);
        if (element == null)
            throw new AutomationException("We expected filter to be applied but it was probably not applied");
    }

    public void removeFilterMedDetailTable() throws AutomationException {
        WebElement element = driverUtil.getWebElement(MED_DETAIL_TABLE_FILTER_APPLIED, 10);
        if (element != null)
            element.click();

        element = driverUtil.getWebElement(MED_DETAIL_TABLE_FILTER_NOT_APPLIED, 10);
        if (element == null)
            throw new AutomationException("We expected filter to be not applied but it was probably applied");
    }
}
