package com.arine.automation.pages;

import com.arine.automation.exception.AutomationException;
import com.arine.automation.util.WebDriverUtil;
import cucumber.api.DataTable;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.Map;

public class ReportPage extends PatientPage {

    public static final String REPORTS_TAB = "Reports";
    private static final String DRP_TABLE = "//*[contains(@class,'headerTable') and .//*[text()='DRP']]/..";
    private static final String ADD_DRP_ICON = "//*[contains(@class,'headerTable') and .//*[text()='DRP']]//*[contains(@class,'iconAdd')]";
    private static final String DRP_INPUT = "//textarea[@id='drp']";
    private static final String PAT_ASMT_INPUT = "//textarea[@id='patAsmt']";
    private static final String PAT_REC_INPUT = "//textarea[@id='patRec']";
    private static final String PROV_ASMT_INPUT = "//textarea[@id='provAsmt']";
    private static final String PROV_REC_INPUT = "//textarea[@id='provRec']";

    private static final String DRP_RECORD = "//tr[.//*[contains(@class,'drp') and text()='%s']]";
    private static final String ALL_DRP_RECORD = "//tr[.//*[contains(@class,'drp')]]//span[contains(@class,'RecommendationTable-__drp')]";
    private static final String FIRST_DRP_RECORD = "//tr[.//*[contains(@class,'drp')]][1]//span[2]";
    private static String FIRST_DRP_RECORD_NAME = "";
    private static final String DRP_RECORD_RADIOBUTTON = "//tr[.//*[contains(@class,'drp') and text()='%s']]//span[contains(@class,'checkbox')]";
    private static final String DELETE_DRP_ICON = "//*[@data-tip='Delete selected DRP']";
    private static final String SHOW_DELETED_DRP_ICON = "//*[@data-tip='click to show deleted DRP']";
    private static final String HIDE_DELETED_DRP_ICON = "//*[@data-tip='click to hide deleted DRP']";
    private static final String RESTORE_DELETED_DRP_ICON = "//tr[.//*[contains(@class,'drp') and text()='%s']]//*[@data-icon='trash-restore']";
    private static final String MEDICATION_LIST_PML_POPUP = "//*[contains(@class,'PreviewReportMulti')]//table//tr[contains(@class,'PreviewReportMulti')]//td[1]";
    private static final String MEDICATION_LIST_LANDING_PAGE = "//*[contains(@class,'MedDetailTable') and contains(@class,'tableContainer')]//tr//td[1]";


    public void unarchiveReportIfArchived(String reportName) throws AutomationException {
        showArchivedReports();
        List<WebElement> webElements = driverUtil.getWebElements(REPORT_NAMES);
        boolean recordFound = false;
        for (WebElement element : webElements) {
            if (element.getText().contains(reportName)) {
                recordFound = true;
                break;
            }
        }
        WebElement restoreIcon = driverUtil.getWebElementAndScroll(String.format(RESTORE_ARCHIVED_REPORT, reportName));
        if (restoreIcon != null) {
            WebDriverUtil.waitForAWhile();
            restoreIcon.click();
            clickOnButton("unArchive");
        }
        hideArchivedReports();
    }

    public void addNewDrpIfNotPresent(DataTable dataTable) throws AutomationException {
        Map<String, String> inputDataMap = convertDataTableIntoMap(dataTable);
        String patient = inputDataMap.get(ReportPage.DRPData.PatientId.toString());
        searchPatient(patient);
        clickOnGlobalTab(REPORTS_TAB);
        clickOnButton("Make Reports");

        WebElement drpRecord = driverUtil.getWebElementAndScroll(String.format(DRP_RECORD, inputDataMap.get(ReportPage.DRPData.DrugRelatedProblem.toString())), WebDriverUtil.WAIT_5_SEC);
        if (drpRecord == null) {
            driverUtil.getWebElement(ADD_DRP_ICON, WebDriverUtil.WAIT_2_SEC, "Unable to locate create new med icon!").click();
            driverUtil.getWebElementAndScroll(DRP_INPUT, WebDriverUtil.WAIT_2_SEC, "Unable to find Drug Related Problem input!")
                    .sendKeys(inputDataMap.get(ReportPage.DRPData.DrugRelatedProblem.toString()));
            driverUtil.getWebElementAndScroll(PAT_ASMT_INPUT, WebDriverUtil.WAIT_1_SEC, "Unable to find Patient Assessment input!")
                    .sendKeys(inputDataMap.get(ReportPage.DRPData.PatientAssessment.toString()));
            driverUtil.getWebElementAndScroll(PAT_REC_INPUT, WebDriverUtil.WAIT_1_SEC, "Unable to find Patient Recommendation input!")
                    .sendKeys(inputDataMap.get(ReportPage.DRPData.PatientRecommendation.toString()));
            driverUtil.getWebElementAndScroll(PROV_ASMT_INPUT, WebDriverUtil.WAIT_1_SEC, "Unable to find Provider Assessment input!")
                    .sendKeys(inputDataMap.get(ReportPage.DRPData.ProviderAssessment.toString()));
            driverUtil.getWebElementAndScroll(PROV_REC_INPUT, WebDriverUtil.WAIT_1_SEC, "Unable to find Provider Recommendation input!")
                    .sendKeys(inputDataMap.get(ReportPage.DRPData.ProviderRecommendation.toString()));
            WebDriverUtil.waitForAWhile(5);
        }
        scrollToTop();
    }

    public void addNewDrp(DataTable dataTable) throws AutomationException {
        Map<String, String> inputDataMap = convertDataTableIntoMap(dataTable);
        String patient = inputDataMap.get(ReportPage.DRPData.PatientId.toString());
        searchPatient(patient);
        clickOnGlobalTab(REPORTS_TAB);
        clickOnButton("Make Reports");

        driverUtil.getWebElement(ADD_DRP_ICON, WebDriverUtil.WAIT_2_SEC, "Unable to locate create new med icon!").click();
        driverUtil.getWebElementAndScroll(DRP_INPUT, WebDriverUtil.WAIT_2_SEC, "Unable to find Drug Related Problem input!")
                .sendKeys(inputDataMap.get(ReportPage.DRPData.DrugRelatedProblem.toString()));
        driverUtil.getWebElementAndScroll(PAT_ASMT_INPUT, WebDriverUtil.WAIT_1_SEC, "Unable to find Patient Assessment input!")
                .sendKeys(inputDataMap.get(ReportPage.DRPData.PatientAssessment.toString()));
        driverUtil.getWebElementAndScroll(PAT_REC_INPUT, WebDriverUtil.WAIT_1_SEC, "Unable to find Patient Recommendation input!")
                .sendKeys(inputDataMap.get(ReportPage.DRPData.PatientRecommendation.toString()));
        driverUtil.getWebElementAndScroll(PROV_ASMT_INPUT, WebDriverUtil.WAIT_1_SEC, "Unable to find Provider Assessment input!")
                .sendKeys(inputDataMap.get(ReportPage.DRPData.ProviderAssessment.toString()));
        driverUtil.getWebElementAndScroll(PROV_REC_INPUT, WebDriverUtil.WAIT_1_SEC, "Unable to find Provider Recommendation input!")
                .sendKeys(inputDataMap.get(ReportPage.DRPData.ProviderRecommendation.toString()));
        WebDriverUtil.waitForAWhile(5);
        scrollToTop();
    }

    public void deselectAllExistingReports() throws AutomationException {
        WebElement selectDeselectAllcheckbox = driverUtil.getWebElement("//th//span[contains(@class,'checkbox')]");
        if (selectDeselectAllcheckbox != null ) {
            selectDeselectAllcheckbox.click();
            selectDeselectAllcheckbox.click();
        }
    }

    private static enum DRPData {
        PatientId,
        DrugRelatedProblem,
        PatientAssessment,
        PatientRecommendation,
        ProviderAssessment,
        ProviderRecommendation;

        public String getName(ReportPage.DRPData drp) {
            return drp.toString();
        }
    }
}
