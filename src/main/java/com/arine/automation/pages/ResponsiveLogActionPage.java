package com.arine.automation.pages;

import com.arine.automation.exception.AutomationException;
import com.arine.automation.glue.CommonSteps;
import com.arine.automation.util.WebDriverUtil;
import cucumber.api.DataTable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ResponsiveLogActionPage extends PatientPage {
    public static final String OPEN_TASKS_COLUMN = "//*[contains(@class,'PatientStoryInput-__tableHeader') and text()='OPEN TASKS']";
    public static final String PRACTICENERS_NAME_FROM_CARE_TEAM_TAB = "//*[contains(@class,'PractitionerTable-__tableContainer')]//tr[.//*[text()='%s']]";
    public static final String PRACTICENERS_NAME_FROM_LOG_ACTION_POP_UP = "//*[contains(@class,'PatientStoryInput-__tableHeader') and text()='STAKEHOLDER']/..//*[contains(text(),'%s')]";

    private static final String ACTION_COLUMN_DATA = "(//*[contains(@class,'PatientStoryInput-__tableHeader') and text()='ACTION']/..//*[contains(@class,'PatientStoryInput-__inputTextFixing')])[1]";
    private static final String TYPE_COLUMN_DATA = "(//*[contains(@class,'PatientStoryInput-__tableHeader') and text()='TYPE']/..//*[contains(@class,'PatientStoryInput-__inputTextFixing')])[1]";
    private static final String STACKHOLDER_COLUMN_DATA = "(//*[contains(@class,'PatientStoryInput-__tableHeader') and text()='STAKEHOLDER']/..//*[contains(@class,'PatientStoryInput-__inputTextFixing')])[1]";
//    private static final String STEPS_PERFORMED_COLUMN_DATA = "(//*[contains(@class,'PatientStoryInput-__tableHeader') and text()='STEP(S) PERFORMED']/..//*[contains(@class,'PatientStoryInput-__inputTextFixing')])[1]";
    private static final String STEPS_PERFORMED_COLUMN_DATA ="//*[contains(text(),'%s')]";
    private static final String STEPS_COLUMN_DATA = "//*[contains(@class,'PatientStoryInput-__tableHeader') and text()='STEP(S) PERFORMED']/..//*[text()='+ Cold Call Attempt']";
    private static final String OUTCOMES_COLUMN_DATA = "(//*[contains(@class,'PatientStoryInput-__tableHeader') and text()='OUTCOME']/..//*[contains(@class,'PatientStoryInput-__inputTextFixing')])[1]";
    public static final String NEW_LOG_ACTION_SUBMIT_BUTTON = "//button[@id='logAction']";
    public static final String CLOSE_LOGACTION_ERROR_BUTTON = "//button[text()='OK']";
    public static final String PCP_MTM_FROM_LOG_ACTION_POP_UP= "//*[contains(@class,'PatientStoryInput-__tableHeader') and text()='STEP(S) PERFORMED']/../..//*[contains(text(),'PCP_MTM')]";
    public static final String CASES_FROM_LOG_ACTION_POP_UP = "//*[contains(@class,'PatientStoryInput-__tableHeader') and text()='STEP(S) PERFORMED']/../..//*[contains(text(),'10000')]";
    public static final String CREATE_LOG_PCP_MTM_CASES_NAME_HEADER = "//*[contains(@class,'PatientStoryInput-__stepsList')]//*[contains(text(),'%s')]";
    public static final String NEW_LOG_ACTION_STEP_PERFORMED_PCP_MTM_CHECKBOX = "(//input[contains(@name,'%s')])[1]";

    public void verifyByDefaultActionColumnInLogActionPopup() throws AutomationException {
        WebElement typeColumn = driverUtil.getWebElement(TYPE_COLUMN_DATA);
        if (typeColumn != null)
            throw new AutomationException("Type column is being displayed by default in log action popUp!");

        WebElement stakHolderColumn = driverUtil.getWebElement(STACKHOLDER_COLUMN_DATA);
        if (stakHolderColumn != null)
            throw new AutomationException("StakHolder column is being displayed by default in log action popUp!");

        WebElement outcomeColumn = driverUtil.getWebElement(OUTCOMES_COLUMN_DATA);
        if (outcomeColumn != null)
            throw new AutomationException("Outcome column is being displayed by default in log action popUp!");

        WebElement stepsPerformedColumn = driverUtil.getWebElement(STEPS_COLUMN_DATA);
        if (stepsPerformedColumn != null)
            throw new AutomationException("Steps Performed column is being displayed by default in log action popUp!");

        WebElement actionColumn = driverUtil.getWebElement(ACTION_COLUMN_DATA);
        if (actionColumn == null)
            throw new AutomationException("No Action column is being displayed by default in log action popUp or it might taking too long time to load!");
    }


    public void verifyWhenSelectActionOtherColumnDisplayed() throws AutomationException {
        WebElement actionColumn = driverUtil.getWebElement(ACTION_COLUMN_DATA);
        if (actionColumn == null)
            throw new AutomationException("No Action column is being displayed in log action popUp or it might taking too long time to load!");
        actionColumn.click();

        WebDriverUtil.waitForAWhile();
        WebElement typeColumn = driverUtil.getWebElement(TYPE_COLUMN_DATA);
        if (typeColumn == null)
            throw new AutomationException("No Type column is being displayed in log action popUp or it might taking too long time to load!");
        typeColumn.click();

        WebDriverUtil.waitForAWhile();
        WebElement stakHolderColumn = driverUtil.getWebElement(STACKHOLDER_COLUMN_DATA);
        if (stakHolderColumn == null)
            throw new AutomationException("No StakHolder column is being displayed in log action popUp or it might taking too long time to load!");
        stakHolderColumn.click();

        WebDriverUtil.waitForAWhile();
        WebElement outcomeColumn = driverUtil.getWebElement(OUTCOMES_COLUMN_DATA);
        if (outcomeColumn == null)
            throw new AutomationException("No Outcome column is being displayed in log action popUp or it might taking too long time to load!");
        outcomeColumn.click();

        WebDriverUtil.waitForAWhile();
        WebElement stepsPerformedColumn = driverUtil.getWebElement(STEPS_COLUMN_DATA);
        if (stepsPerformedColumn == null)
            throw new AutomationException("No Steps Performed column is being displayed in log action popUp or it might taking too long time to load!");
        stepsPerformedColumn.click();
    }

    public void selectDataForVerifyPCP_MTMAndCasesPresentInLogActionPopUp(DataTable data) throws AutomationException{
        Map<String, String> taskDetails = new HashMap<>();
        List<List<String>> rows = data.asLists(String.class);
        for (int i = 1; i < rows.size(); i++) {
            List<String> row = rows.get(i);
            for (int j = 0; j < row.size(); j++) {
                taskDetails.put(rows.get(0).get(j), row.get(j));
            }
        }

        String value = taskDetails.get(NEW_LOG_ACTION);
        WebElement input = driverUtil.getWebElement(String.format(NEW_LOG_ACTION_RADIO_BUTTON, value));
        if (input == null)
            throw new AutomationException(String.format("No Action option for %s is being displayed or it might taking too long time to load!", value));
        input.click();

        value = taskDetails.get(NEW_LOG_ACTION_TYPE);
        input = driverUtil.getWebElement(String.format(NEW_LOG_ACTION_TYPE_RADIO_BUTTON, value));
        if (input == null)
            throw new AutomationException(String.format("No Type option for %s is being displayed or it might taking too long time to load!", value));
        input.click();

        value = taskDetails.get(NEW_LOG_ACTION_STAKEHOLDER);
        input = driverUtil.getWebElement(String.format(NEW_LOG_ACTION_STAKEHOLDER_RADIO_BUTTON, value));
        if (input == null)
            throw new AutomationException(String.format("No Stakeholder option for %s is being displayed or it might taking too long time to load!", value));
        input.click();

        value = taskDetails.get(NEW_LOG_ACTION_OUTCOME);
        input = driverUtil.getWebElement(String.format(NEW_LOG_ACTION_OUTCOME_RADIO_BUTTON, value));
        if (input == null)
            throw new AutomationException(String.format("No Outcome option for %s is being displayed or it might taking too long time to load!", value));
        input.click();

        value = taskDetails.get(NEW_LOG_ACTION_STEP_PERFORMED);
        input = driverUtil.getWebElement(String.format(STEPS_PERFORMED_COLUMN_DATA, value));
        if (input == null)
            throw new AutomationException(String.format("No Step performed option for %s is being displayed or it might taking too long time to load!", value));
        input.click();

//        input = driverUtil.getWebElement(String.format(STEPS_PERFORMED_COLUMN_DATA));
//        if (input == null)
//            throw new AutomationException(String.format("No Step performed option for %s is being displayed or it might taking too long time to load!", value));
//        input.click();
    }

    public void verifyNoPCP_MTMOptionDisplayedinLogActionPopUp() throws AutomationException {
        WebElement PCP_MTMOptionInStepsColumn = driverUtil.getWebElement(PCP_MTM_FROM_LOG_ACTION_POP_UP);
        if (PCP_MTMOptionInStepsColumn != null)
            throw new AutomationException("PCP_MTM Report option is Present in steps Performed column if PCP_MTM Report is Archived Then It don't want to display in logAction PopUp!");
    }

    public void verifyPCP_MTMOAndClassptionDisplayedinLogActionPopUp() throws AutomationException {
        WebElement PCP_MTMOptionInStepsColumn = driverUtil.getWebElement(PCP_MTM_FROM_LOG_ACTION_POP_UP);
        if (PCP_MTMOptionInStepsColumn == null)
            throw new AutomationException("No PCP-MTM Option is Present in Steps Performed column in log action popUp or it might taking too long time to load!");

        WebElement CasesptionInStepsColumn = driverUtil.getWebElement(CASES_FROM_LOG_ACTION_POP_UP);
        if (CasesptionInStepsColumn == null)
            throw new AutomationException("No PCP-MTM Option is Present in Steps Performed column in log action popUp or it might taking too long time to load!");
    }


    public void verifypractitionersNameDisplayedILogAction(String name) throws AutomationException {
        WebElement newLogActionPopup = driverUtil.getWebElement(CREATE_NEW_LOG_ACTION_POPUP);
        WebElement practitionersName;
        if (newLogActionPopup != null)
            practitionersName = driverUtil.getWebElement(String.format(PRACTICENERS_NAME_FROM_LOG_ACTION_POP_UP,name));
        else
            practitionersName = driverUtil.getWebElement(String.format(PRACTICENERS_NAME_FROM_CARE_TEAM_TAB,name));
        if (practitionersName == null)
            throw new AutomationException("Practitioner Name %s is not being displayed or it might taking too long time to load!");

        WebDriverUtil.waitForAWhile(2);
    }

    public void verifyNotAbleToCreateLogWithoutSelectingAnyAttribute() throws AutomationException {
        WebElement newLogActionSubmitButton = driverUtil.getWebElement(NEW_LOG_ACTION_SUBMIT_BUTTON);
        if (newLogActionSubmitButton == null)
            throw new AutomationException("No New Log Action Button is being displayed in log action popUp or it might taking too long time to load!");

        if (newLogActionSubmitButton != null) {
            WebElement typeColumn = driverUtil.getWebElement(TYPE_COLUMN_DATA);
            if (typeColumn != null)
                throw new AutomationException("Type column is being displayed by default in log action popUp!");

            WebElement stakHolderColumn = driverUtil.getWebElement(STACKHOLDER_COLUMN_DATA);
            if (stakHolderColumn != null)
                throw new AutomationException("StakHolder column is being displayed by default in log action popUp!");

            WebElement outcomeColumn = driverUtil.getWebElement(OUTCOMES_COLUMN_DATA);
            if (outcomeColumn != null)
                throw new AutomationException("Outcome column is being displayed by default in log action popUp!");

            WebElement stepsPerformedColumn = driverUtil.getWebElement(STEPS_PERFORMED_COLUMN_DATA);
            if (stepsPerformedColumn != null)
                throw new AutomationException("Steps Performed column is being displayed by default in log action popUp!");
        }
        newLogActionSubmitButton.click();
        driverUtil.waitForAWhile(WAIT_3_SECOND);
    }

    public void closeLogNewActionErrorPopUp() throws AutomationException {
        WebElement okButtonToCloseErrorPopUp = driverUtil.getWebElement(CLOSE_LOGACTION_ERROR_BUTTON);
        if (okButtonToCloseErrorPopUp == null)
            throw new AutomationException("No OK Button is Present in Error pop-up is being displayed or it might taking too long time to load!");
        okButtonToCloseErrorPopUp.click();
    }


    public void createLogActionWithOnlyComment(String comment) throws AutomationException {
        if (comment != null && !comment.isEmpty()) {
            addNewComment(comment);
        }

        WebElement submitButton = driverUtil.getWebElement(NEW_LOG_ACTION_SUBMIT_BUTTON);
        if (submitButton == null)
            throw new AutomationException("Log Action button is being displayed on Create new task popup or it might taking too long time to load!");
        if (submitButton.isDisplayed())
            submitButton.click();
        else
            throw new AutomationException("Unable to click on Log Action button as it is disabled!");
        driverUtil.waitForInvisibleElement(By.xpath(CREATE_NEW_LOG_ACTION_POPUP));
    }

    public static final String ADD_NEW_LOGACTION_ICON = " //*[contains(@class,'iconAdd')]";
    public static final String PATIENT_TAB_DEFAULT_BUTTON = "//*[contains(@class,'TaskInput')][text()='Log Action']";
    public static final String TASK_RECORD = "//*[@role='row']//*[contains(text(),'%s')]/../..";


    public void clickOnAddNewLogActionIcon() throws AutomationException {
        WebElement newLogActionIcon = driverUtil.getWebElementAndScroll(ADD_NEW_LOGACTION_ICON);
        if (newLogActionIcon == null)
            throw new AutomationException("Unable to find Add new Log Action icon or it might taking too long time to load!");
        newLogActionIcon.click();

    }

    public void clickOnTabButton(String text) throws AutomationException {
        WebElement button = driverUtil.getWebElementAndScroll(String.format(PATIENT_TAB_DEFAULT_BUTTON, text));
        if (button == null)
            throw new AutomationException(String.format("Unable to find %s button on patient tab or it might taking too long time to load!", text));
        //driverUtil.waitForElementClickable(By.xpath(String.format(PATIENT_TAB_BUTTON, text)));
        button.click();
    }

    public void selectNewlycreatedTask(String finder) throws AutomationException {
        WebElement task = driverUtil.getWebElement(String.format(TASK_RECORD, finder), WAIT_30_SECOND);
        CommonSteps.takeScreenshot();
        if (task == null)
            throw new AutomationException("No task found with given text: " + finder);
        task.click();
        WebDriverUtil.waitForAWhile(10);
    }

//    public void verifyNAisPresentinOpenTaskcolumn() throws AutomationException {
//        WebElement NAOptionInopenTaskColumn = driverUtil.getWebElement(NA_OPTION_IN_OPEN_TASK_COLUMN);
//        if (NAOptionInopenTaskColumn == null)
//            throw new AutomationException("No NA Present in Open Task column is being displayed or it might taking too long time to load!");
//    }
}
