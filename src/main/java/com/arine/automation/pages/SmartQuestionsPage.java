package com.arine.automation.pages;

import com.arine.automation.exception.AutomationException;
import com.arine.automation.glue.CommonSteps;
import com.arine.automation.util.WebDriverUtil;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class SmartQuestionsPage extends PatientPage {
    private static final String SMART_QUESTIONS_TAB = "Smart Questions";
    private static final String MED_DIAGNOSIS_TABLE = "//table[.//thead[.//*[contains(text(),'Med-Diagnosis')]]]";
    private static final String MED_RECORD = "//table[.//thead[.//*[contains(text(),'Smart Questions')]]]//td[text()='%s']";
    private static final String OTHER_ASSESSMENT = "//table[.//thead[.//*[contains(text(),'Other Assessments')]]]//td[text()='%s']";
    private static final String SMART_QUESTION_YES_CHECKBOX = "//*[text()='%s']/../div//input[@value=1]";
    private static final String SMART_QUESTION_NO_CHECKBOX = "//*[text()='%s']/../div//input[@value=2]";
    private static final String HEALTH_CONDITIONS = "//table[.//thead[.//*[contains(text(),'Health Conditions')]]]//td[text()='%s']";
    private static final String UPDATE_SMART_QUESTIONS_BUTTON = "//button[text()='Update Smart Questions']";
    private static final String REPORT_TAB_CONVERSATION_DATE = "//input[@name='conversationDate']";
    private static final String REPORT_TAB_PREVIOUS_DAY = "//div[contains(@class,'today')]/preceding-sibling::div[1]";
    private static final String SMART_QUESTIONS = "//*[text()='SmartQuestion']/ancestor::table//tbody/tr[2]";
    private static final String SMART_QUESTIONS_FIRST_RECORD_ANSWER = "//*[text()='SmartQuestion']/ancestor::table//tbody/tr[2]/td[2]";
    private static final String FIRST_RECORD_SMART_QUESTIONS_FORM_ANSWER_OPTIONS = "(//*[contains(@class,'SmartQuestionForm-__scrollbox')]//p)[1]/../div//input";
    private static final String DISABLED_NEXT_BUTTON = "//button[contains(text(),'Next >>') and @disabled]";
    private static final String ACTUAL_SCORE = "//*[contains(@title,'ACT score') or contains(@title,'PHQ-9 Score') or contains(@title,'PHQ-2 Score') or contains(@title,'mMRC score:')]/../td[2]";
    private static final String ACT_SCORE_QUESTION_SELECTED = "//*[contains(@class,'SmartQuestionAssessmentResult')]/parent::div[contains(@class,'selectedQuestion')]";
    private static final String ANSWER_RADIO_BUTTON = "//*[contains(@class,'selectedQuestion')]//input[@value=%s]";
    private static final String ACTUAL_TOTAL_PERCENTAGE = "//*[contains(@class,'pin')]";
    private static final String SELECTED_QUESTION_ANSWER_RADIO_BUTTONS = "//*[contains(@class,'selectedQuestion')]//input";
    private static final String QUESTIONS_IN_SELECTED_OTHER_ASSESSMENT_GROUP = "//*[contains(@class,'SmartQuestionTable-__table')]//tbody//tr[contains(@style,'normal')]";
    private static final String SELECTED_ANSWER_RADIO_BUTTON_LABEL = "//*[contains(@class,'selectedQuestion')]//input[@value=%s]/../../td[2]/span";
    private static final String DISABLED_OTHER_ASSESSMENT_GROUP = "//td[text()='%s']/parent::tr[contains(@class,'disable')]";
    private static final String ENABLED_OTHER_ASSESSMENT_GROUP = "//table[.//thead[.//*[contains(text(),'Other Assessments')]]]//td[text()='Mental Health - PHQ-9']/parent::tr[not(contains(@class,'disable'))]";

    public static String firstSmartQuestionCurrentAnswer = "";
    public static String firstSmartQuestionUpdatedAnswer = "";

    public void verifyMedDiagnosisTableDisplayed() throws AutomationException {
        clickOnGlobalTab(SMART_QUESTIONS_TAB);
        driverUtil.getWebElement(MED_DIAGNOSIS_TABLE, WebDriverUtil.WAIT_5_SEC,
                "Med-Diagnosis table is not being displayed or it might taking too long time to load!");
    }

    public void verifyMedicationIsDisplayed(String medName, boolean shouldDisplayed) throws AutomationException {
        clickOnGlobalTab(SMART_QUESTIONS_TAB);
        WebElement medRecord = driverUtil.getWebElement(String.format(MED_RECORD, medName));
        if (shouldDisplayed && medRecord == null)
            throw new AutomationException(String.format("We supposed Med: %s should be displayed but found there is no record is being displayed!", medName));
        else if (!shouldDisplayed && medRecord != null)
            throw new AutomationException(String.format("We supposed Med: %s should not be displayed but found med record is being displayed!", medName));
    }

    public void verifyOtherAssessmentIsDisplayed(String medName, boolean shouldDisplayed) throws AutomationException {
        clickOnGlobalTab(SMART_QUESTIONS_TAB);
        WebElement medRecord = driverUtil.getWebElement(String.format(OTHER_ASSESSMENT, medName));
        if (shouldDisplayed && medRecord == null)
            throw new AutomationException(String.format("We expected other assessment: %s should be displayed but found there is no record is being displayed!", medName));
        else if (!shouldDisplayed && medRecord != null)
            throw new AutomationException(String.format("We expected other assessment: %s should not be displayed but found med record is being displayed!", medName));
    }

    public void clickOnUpdateSmartQuestionsButton() throws AutomationException {
        clickOnGlobalTab(SMART_QUESTIONS_TAB);
        driverUtil.getWebElement(UPDATE_SMART_QUESTIONS_BUTTON, WebDriverUtil.WAIT_2_SEC,
                "Update Smart Questions button is not being displayed or it might taking too long time to load!").click();
        driverUtil.waitForLoadingPage();
    }

    public void verifyHealthConditionsAreDisplayed(String healthCondition, boolean shouldDisplayed) throws AutomationException {
        clickOnGlobalTab(SMART_QUESTIONS_TAB);
        WebElement medRecord = driverUtil.getWebElement(String.format(HEALTH_CONDITIONS, healthCondition));
        if (shouldDisplayed && medRecord == null)
            throw new AutomationException(String.format("We expected Health Condition: %s should be displayed but found there is no record is being displayed!", healthCondition));
        else if (!shouldDisplayed && medRecord != null)
            throw new AutomationException(String.format("We expected Health Condition: %s should not be displayed but found health condition record is being displayed!", healthCondition));
    }

    public void clickOnSmartQuestionMedication(String medName) throws AutomationException {
        clickOnGlobalTab(SMART_QUESTIONS_TAB);
        WebElement medRecord = driverUtil.getWebElement(String.format(MED_RECORD, medName));
        if (medRecord == null)
            throw new AutomationException(String.format("We supposed Med: %s should be displayed but found there is no record is being displayed!", medName));
        medRecord.click();
    }

    public static void verifySmartQuestion(String smartQuestionToVerify) throws AutomationException {
        WebElement element = driverUtil.findElement("//*[contains(text(),'" + smartQuestionToVerify + "')]");
        if (element == null)
            throw new AutomationException("Smart Question not displayed : " + smartQuestionToVerify);
    }

    public void clickOnSmartQuestion(String smartQuestion) throws AutomationException {
        WebElement element = driverUtil.findElement("//*[contains(text(),'" + smartQuestion + "')]");
        if (element == null)
            throw new AutomationException("Smart Question not displayed : " + smartQuestion);
        element.click();
    }

    public void verifySmartQuestionAndAnswer(String answer, String currentDate) throws AutomationException {
        //DateTimeFormatter format = DateTimeFormatter.ofPattern("MM/dd/yy");
        //String currentDate = LocalDateTime.now().format(format);
        int currentDateColIndex = 1;
        List<WebElement> webElements = driverUtil.getWebElements("//*[contains(@class,'SmartQuestionTable-__tableContainer')]//thead//div[text()!='SmartQuestion']");
        for (WebElement webElement : webElements) {
            if (webElement.getText().equals(currentDate)) {
                currentDateColIndex++;
                break;
            }
            currentDateColIndex++;
        }

        WebElement smartQuestionAnswerColumn = driverUtil.findElement(SMART_QUESTIONS_FIRST_RECORD_ANSWER);
        if (smartQuestionAnswerColumn != null) {
            String actualAnswer = smartQuestionAnswerColumn.getText();
            if (!actualAnswer.equalsIgnoreCase(answer))
                throw new AutomationException("We expected first smart question's answer column's value to be '" + answer + "' but found '" + actualAnswer);

            WebElement currentDateHistoricalColumnAnswer = driverUtil.findElement("//*[text()='SmartQuestion']/ancestor::table//tbody/tr[2]/td[" + currentDateColIndex + "]/div");
            actualAnswer = currentDateHistoricalColumnAnswer.getText();
            if (!actualAnswer.equalsIgnoreCase(answer))
                throw new AutomationException("We expected answer for first smart question's Historical Column '" + currentDate + "' value to be '" + answer + "' but found " + actualAnswer);
        }
    }

    public static void clickOnCompleteCMR() throws AutomationException {
        WebElement element = driverUtil.findElementByText("Complete  CMR");
        if (element == null)
            throw new AutomationException("Complete CMR Button not displayed");
        element.click();
        PageFactory.homePage().waitForLoadingPage();
        WebElement reportNotGenerated = driverUtil.findElementByText("Please generate reports before completing the CMR.");
        if (reportNotGenerated != null) {
            driverUtil.findElementByTextAndClick("OK");
            PageFactory.patientPage().clickOnButton("Make Reports");
            PageFactory.patientPage().selectReportTypes("CL");
            PageFactory.patientPage().clickOnSetCallDateButton();
            PageFactory.patientPage().clickOnButton("Generate Selected Reports");
            PageFactory.homePage().waitForLoadingPage();
            element = driverUtil.findElementByText("Complete CMR");
            if (element == null)
                throw new AutomationException("Complete CMR Button not displayed");
            element.click();
            PageFactory.homePage().waitForLoadingPage();
        }
        WebElement verifyTextOnCompleteCMRPopup = driverUtil.findElementByText("Are you sure you want to complete the CMR for this patient");
        WebElement verifyCMRCompletedPopup = driverUtil.findElementByText("The CMR has been marked completed");
        if (verifyTextOnCompleteCMRPopup != null) {
            WebElement completeCMRBtn = driverUtil.findElement("//*[text()='Complete CMR' and contains(@class,'confirm')]");
            if (completeCMRBtn == null)
                throw new AutomationException("Complete CMR button not displayed on CMR Confirmation popup");
            completeCMRBtn.click();
        } else if (verifyCMRCompletedPopup != null) {
            driverUtil.findElementByTextAndClick("OK");
        }
    }

    public static void userSetPastCallDate() throws AutomationException {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("MM/dd/yy");
        String date = LocalDateTime.now().minusDays(1).format(format);
        WebElement element = driverUtil.findElement(REPORT_TAB_CONVERSATION_DATE);
        if (element == null)
            throw new AutomationException("Date picker to set call date not displayed");
        element.click();
        //driverUtil.findElement(REPORT_TAB_PREVIOUS_DAY).click();
        element.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
        element.sendKeys(date);
    }

    public static void verifyHistoricalColumnNotDisplayed() throws AutomationException {
        List<WebElement> webElements = driverUtil.getWebElements("//*[contains(@class,'SmartQuestionTable-__tableContainer')]//thead//div[text()!='SmartQuestion']");
        boolean historicalColumnPresent = false;
        for (WebElement webElement : webElements
        ) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yy");
            dateFormat.setLenient(false);
            try {
                dateFormat.parse(webElement.getText());
                historicalColumnPresent = true;
                if (historicalColumnPresent)
                    throw new AutomationException("Historical Column Present with date : " + webElement.getText());
            } catch (ParseException pe) {
                //do nothing
            }
        }
    }

//    public static void userUpdateWithAnswer(String question) throws AutomationException {
//        WebElement yesCheckbox = driverUtil.getWebElement(String.format(SMART_QUESTION_YES_CHECKBOX, question));
//        if (yesCheckbox == null)
//            throw new AutomationException("Unable to find answer Yes checkbox for given questions : " + question);
//        if (yesCheckbox.isSelected()) {
//            driverUtil.getWebElement(String.format(SMART_QUESTION_NO_CHECKBOX, question)).click();
//            selectedAnswer = "No";
//        } else {
//            yesCheckbox.click();
//            selectedAnswer = "Yes";
//        }
//    }

    public void selectFirstSmartQuestion() throws AutomationException {
        List<WebElement> elementList = driverUtil.getWebElements(SMART_QUESTIONS);
        if (elementList.size() < 1)
            throw new AutomationException("No smart questions displayed");
        elementList.get(0).click();
        elementList = driverUtil.getWebElements(SMART_QUESTIONS);
        if (!elementList.get(0).getAttribute("class").contains("selected"))
            throw new AutomationException("We expected first smart question to be selected but it was probably not selected");
        CommonSteps.takeScreenshot();
    }


    public void updateFirstSmartQuestionAnswer() throws AutomationException {
        WebElement element = driverUtil.getWebElement(SMART_QUESTIONS_FIRST_RECORD_ANSWER);
        if (element == null)
            throw new AutomationException("No smart questions displayed");
        firstSmartQuestionCurrentAnswer = element.getText();

        List<WebElement> webElementList = driverUtil.getWebElements(FIRST_RECORD_SMART_QUESTIONS_FORM_ANSWER_OPTIONS);
        if (webElementList == null)
            throw new AutomationException("Could not find answer options for selected smart question");

        for (WebElement answerOption : webElementList
        ) {
            String type = answerOption.getAttribute("type");
            if (type.equalsIgnoreCase("radio")) {
                if (!answerOption.isSelected()) {
                    answerOption.click();
                    break;
                }
            } else if (type.equalsIgnoreCase("text")) {
                answerOption.clear();
                answerOption.sendKeys("Updated by automation_" + System.currentTimeMillis());
                answerOption.sendKeys(Keys.ENTER);
                break;
            } else if (type.equalsIgnoreCase("checkbox")) {
                answerOption.click();
                break;
            } else {
                throw new AutomationException("New input type added to smart questions, needs to be handled in automation");
            }
        }

        WebElement firstSmartQuestionUpdatedAnswerElement = driverUtil.getWebElement(SMART_QUESTIONS_FIRST_RECORD_ANSWER);
        if (firstSmartQuestionUpdatedAnswerElement == null)
            throw new AutomationException("No smart questions displayed");
        firstSmartQuestionUpdatedAnswer = firstSmartQuestionUpdatedAnswerElement.getText();

        if (firstSmartQuestionCurrentAnswer.equals(firstSmartQuestionUpdatedAnswer))
            throw new AutomationException("We expected the answer for first smart question to be updated, but it was probably not updated");
    }

    public void answerQuestionForOtherAssessmentAndValidateScore(String assessmentGroup) throws AutomationException {
        clickOnOtherAssessment(assessmentGroup);
        int expectedScore = 0;
        WebElement actScoreQuestionSelected = driverUtil.getWebElement(ACT_SCORE_QUESTION_SELECTED);
        while (actScoreQuestionSelected == null) {
            int randomInt = getRandomInteger(1, 3);
            WebElement answerRadioButton = driverUtil.getWebElement(String.format(ANSWER_RADIO_BUTTON, randomInt));
            expectedScore = expectedScore + randomInt;
            answerRadioButton.click();
            WebDriverUtil.waitForAWhile(2);
            actScoreQuestionSelected = driverUtil.getWebElement(ACT_SCORE_QUESTION_SELECTED);
        }

        WebElement actualActScore = driverUtil.getWebElement(ACTUAL_SCORE);
        if (actualActScore == null)
            throw new AutomationException("Actual Score not displayed in SQ table for other assessment group : " + assessmentGroup);
        int actualScore = Integer.parseInt(actualActScore.getText());
        if (actualScore != expectedScore)
            throw new AutomationException(String.format("We expected score to be %s but actual score displayed was %s", expectedScore, actualScore));
    }

    public static int getRandomInteger(int maximum, int minimum) {
        return ((int) (Math.random() * (maximum - minimum))) + minimum;
    }

    public void userAnswerOtherAssessmentToScore(String assessmentGroup, String expectedScore) throws AutomationException {
        //Select Assessment Group
        clickOnOtherAssessment(assessmentGroup);
        int expectedTotalScore = Integer.parseInt(expectedScore);

        // Mark All the answers to 0
        WebElement actScoreQuestionSelected = driverUtil.getWebElement(ACT_SCORE_QUESTION_SELECTED);
        while (actScoreQuestionSelected == null) {
            WebElement answerRadioButton = driverUtil.getWebElement(String.format(ANSWER_RADIO_BUTTON, 0));
            answerRadioButton.click();
            WebDriverUtil.waitForAWhile(2);
            actScoreQuestionSelected = driverUtil.getWebElement(ACT_SCORE_QUESTION_SELECTED);
        }
        clickOnOtherAssessment(assessmentGroup);
        WebDriverUtil.waitForAWhile(2);
        WebElement actualActScore = driverUtil.getWebElement(ACTUAL_SCORE);
        if (actualActScore == null)
            throw new AutomationException("Actual Score not displayed in SQ table for other assessment group : " + assessmentGroup);
        int actualTotalScore = Integer.parseInt(actualActScore.getText());
        if (actualTotalScore < expectedTotalScore) {
            int noOfQuestionsInSelectedGroup = driverUtil.getWebElements(QUESTIONS_IN_SELECTED_OTHER_ASSESSMENT_GROUP).size();
            int noOfQuestionsAnswered = 0;
            List<WebElement> listOfOptionsForSelectedQuestion = driverUtil.getWebElements(SELECTED_QUESTION_ANSWER_RADIO_BUTTONS);
            for (int i = 1; i < listOfOptionsForSelectedQuestion.size(); i++) {
                while (noOfQuestionsAnswered < noOfQuestionsInSelectedGroup) {
                    WebElement answerRadioButton = driverUtil.getWebElement(String.format(ANSWER_RADIO_BUTTON, i));
                    answerRadioButton.click();
                    WebDriverUtil.waitForAWhile(2);
                    actualTotalScore = Integer.parseInt(driverUtil.getWebElement(ACTUAL_SCORE).getText());
                    if (actualTotalScore >= expectedTotalScore)
                        break;
                    noOfQuestionsAnswered++;
                }
                noOfQuestionsAnswered = 0;
                if (actualTotalScore >= expectedTotalScore)
                    break;
                else {
                    WebDriverUtil.waitForAWhile(2);
                    clickOnOtherAssessment(assessmentGroup);
                    WebDriverUtil.waitForAWhile(2);
                }
            }
        }
    }

    public void userVerifyScoreScaleForPHQ9(String risk) throws AutomationException {
        WebElement actualActScore = driverUtil.getWebElement(ACTUAL_SCORE);
        if (actualActScore == null)
            throw new AutomationException("Actual Score not displayed in SQ table for other assessment group PHQ9");
        int actualTotalScore = Integer.parseInt(actualActScore.getText());
        WebElement actualTotalPercentage = driverUtil.getWebElement(ACTUAL_TOTAL_PERCENTAGE);
        if (actualTotalPercentage == null)
            throw new AutomationException("Percentage pin bar not set");
        String actualPercentage = actualTotalPercentage.getAttribute("style");
        DecimalFormat df = new DecimalFormat("0.0000");
        float expectedPercentage;
        switch (risk.toUpperCase()) {
            case "SEVERE":
                expectedPercentage = Float.parseFloat(df.format(((float) actualTotalScore / 27 * 100)));
                if (!(expectedPercentage >= 74.0741f))
                    throw new AutomationException("We expected percentage to be above 74.07 as the expected risk is " + risk);
                if (!actualPercentage.contains(df.format(((float) actualTotalScore / 27 * 100))))
                    throw new AutomationException("Actual and expected percentage not matching");
                break;

            case "MODERATELY SEVERE":
                expectedPercentage = Float.parseFloat(df.format(((float) actualTotalScore / 27 * 100)));
                System.out.println("Expected Percentage " + expectedPercentage);
                if (!(expectedPercentage <= 74.0741f && expectedPercentage >= 55.5556f))
                    throw new AutomationException("We expected percentage to be above or equal to 55.55 & less than 74.07 as the expected risk is " + risk);
                if (!actualPercentage.contains(df.format(((float) actualTotalScore / 27 * 100))))
                    throw new AutomationException("Actual and expected percentage not matching");
                break;

            case "MODERATE":
                expectedPercentage = Float.parseFloat(df.format(((float) actualTotalScore / 27 * 100)));
                System.out.println("Expected Percentage " + expectedPercentage);
                if (!(expectedPercentage <= 55.5556f && expectedPercentage >= 37.037f))
                    throw new AutomationException("We expected percentage to be above or equal to 55.55 & less than 74.07 as the expected risk is " + risk);
                if (!actualPercentage.contains(df.format(((float) actualTotalScore / 27 * 100))))
                    throw new AutomationException("Actual and expected percentage not matching");
                break;
            case "MILD":
                expectedPercentage = Float.parseFloat(df.format(((float) actualTotalScore / 27 * 100)));
                System.out.println("Expected Percentage " + expectedPercentage);
                if (!(expectedPercentage <= 37.037f && expectedPercentage >= 18.5185f))
                    throw new AutomationException("We expected percentage to be above or equal to 55.55 & less than 74.07 as the expected risk is " + risk);
                if (!actualPercentage.contains(df.format(((float) actualTotalScore / 27 * 100))))
                    throw new AutomationException("Actual and expected percentage not matching");
                break;
            case "MINIMAL":
                expectedPercentage = Float.parseFloat(df.format(((float) actualTotalScore / 27 * 100)));
                System.out.println("Expected Percentage " + expectedPercentage);
                if (!(expectedPercentage <= 18.5185f))
                    throw new AutomationException("We expected percentage to be above or equal to 55.55 & less than 74.07 as the expected risk is " + risk);
                if (!actualPercentage.contains(df.format(((float) actualTotalScore / 27 * 100))))
                    throw new AutomationException("Actual and expected percentage not matching");
                break;
        }
    }

    public void userVerifyScoreScaleForPHQ2(String risk) throws AutomationException {
        WebElement actualActScore = driverUtil.getWebElement(ACTUAL_SCORE);
        if (actualActScore == null)
            throw new AutomationException("Actual Score not displayed in SQ table for other assessment group PHQ9");
        int actualTotalScore = Integer.parseInt(actualActScore.getText());
        WebElement actualTotalPercentage = driverUtil.getWebElement(ACTUAL_TOTAL_PERCENTAGE);
        if (actualTotalPercentage == null)
            throw new AutomationException("Percentage pin bar not set");
        String actualPercentage = actualTotalPercentage.getAttribute("style");
        DecimalFormat df = new DecimalFormat("0.0000");
        float expectedPercentage;
        switch (risk.toUpperCase()) {
            case "NEGATIVE":
                expectedPercentage = Float.parseFloat(df.format(((float) actualTotalScore / 6 * 100)));
                if (!(expectedPercentage < 50.00f))
                    throw new AutomationException("We expected percentage to be below 50% as the expected risk is " + risk);
                if (!actualPercentage.contains(df.format(((float) actualTotalScore / 6 * 100))))
                    throw new AutomationException("Actual and expected percentage not matching");
                break;

            case "POSITIVE":
                expectedPercentage = Float.parseFloat(df.format(((float) actualTotalScore / 6 * 100)));
                System.out.println("Expected Percentage " + expectedPercentage);
                if (!(expectedPercentage >= 50.00f))
                    throw new AutomationException("We expected percentage to be above or equal to 55.55 & less than 74.07 as the expected risk is " + risk);
                if (!actualPercentage.contains(df.format(((float) actualTotalScore / 6 * 100))))
                    throw new AutomationException("Actual and expected percentage not matching");
                break;
        }
    }

    public void verifyOtherAssessmentIsDisabled(String otherAssessmentGroup) throws AutomationException {
        WebElement element = driverUtil.findElement(String.format(DISABLED_OTHER_ASSESSMENT_GROUP, otherAssessmentGroup));
        if (element == null)
            throw new AutomationException(String.format("We expected other assessment %s to be disabled but its probably enabled or not displayed", otherAssessmentGroup));
    }

    public void verifyOtherAssessmentIsEnabled(String otherAssessmentGroup) throws AutomationException {
        WebElement element = driverUtil.findElement(String.format(ENABLED_OTHER_ASSESSMENT_GROUP, otherAssessmentGroup));
        if (element == null)
            throw new AutomationException(String.format("We expected other assessment %s to be enabled but its probably disabled or not displayed", otherAssessmentGroup));
    }

    public void verifyAnswersForOtherAssessmentWithHistoricalColumnAnswers(String otherAssessmentGroup, String currentDate) throws AutomationException {
        clickOnOtherAssessment(otherAssessmentGroup);
        int currentDateColIndex = 1;
        List<WebElement> webElements = driverUtil.getWebElements("//*[contains(@class,'SmartQuestionTable-__tableContainer')]//thead//div[text()!='SmartQuestion']");
        for (WebElement webElement : webElements) {
            if (webElement.getText().equals(currentDate)) {
                currentDateColIndex++;
                break;
            }
            currentDateColIndex++;
        }

        //get all answers from historical column
        List<WebElement> historicalColumnList = driverUtil.getWebElements(String.format("//*[contains(@class,'SmartQuestionTable-__table')]//tbody//tr[contains(@style,'normal')]//td[%s]", currentDateColIndex));
        List<WebElement> answerColumnList = driverUtil.getWebElements("//*[contains(@class,'SmartQuestionTable-__table')]//tbody//tr[contains(@style,'normal')]//td[2]");

        List<String> expectedAnswersHistoricalColumn = new ArrayList<>();
        List<String> actualAnswersFromAnswersColumn = new ArrayList<>();

        for (WebElement element : historicalColumnList) {
            expectedAnswersHistoricalColumn.add(element.getText());
        }

        for (WebElement element : answerColumnList) {
            actualAnswersFromAnswersColumn.add(element.getText());
        }

        if (!expectedAnswersHistoricalColumn.equals(actualAnswersFromAnswersColumn))
            throw new AutomationException("Answers from historical column not matching with answers from answer column from SQ table");
    }

    public void clickOnOtherAssessment(String otherAssessmentGroup) throws AutomationException {
        WebElement assessmentGroupElement = driverUtil.getWebElement(String.format(OTHER_ASSESSMENT, otherAssessmentGroup));
        if (assessmentGroupElement == null)
            throw new AutomationException(String.format("Other Assessments section doesn't display group %s", otherAssessmentGroup));
        assessmentGroupElement.click();
    }

    public void verifyToolTip(String updatedBy) throws AutomationException {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("MM/dd/yy");
        String currentDate = LocalDateTime.now().format(format);
        int currentDateColIndex = 1;
        List<WebElement> webElements = driverUtil.getWebElements("//*[contains(@class,'SmartQuestionTable-__tableContainer')]//thead//div[text()!='SmartQuestion']");
        for (WebElement webElement : webElements) {
            if (webElement.getText().equals(currentDate)) {
                currentDateColIndex++;
                break;
            }
            currentDateColIndex++;
        }

        //get all answers from historical column
        List<WebElement> historicalColumnList = driverUtil.getWebElements(String.format("//*[contains(@class,'SmartQuestionTable-__table')]//tbody//tr[contains(@style,'normal')]//td[%s]", currentDateColIndex));
        List<String> toolTipForEachAnswer = new ArrayList<>();

        for (WebElement element : historicalColumnList) {
            toolTipForEachAnswer.add(element.getAttribute("title"));
        }

        WebElement actualActScore = driverUtil.getWebElement(ACTUAL_SCORE);
        if (actualActScore == null)
            throw new AutomationException("Actual Score not displayed in SQ table for other assessment group PHQ9");
        int actualTotalScore = Integer.parseInt(actualActScore.getText());

        String expectedToolTip = String.format("Score : %s Updated By : %s Updated Date : %s", actualTotalScore, updatedBy, LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        for (String actualToolTip : toolTipForEachAnswer) {
            if (!actualToolTip.contains(expectedToolTip))
                throw new AutomationException(String.format("We expected tool tip to be %s but found %s", expectedToolTip, actualToolTip));
        }
    }

}
