package com.arine.automation.pages;

import com.arine.automation.exception.AutomationException;
import com.arine.automation.glue.CommonSteps;
import com.arine.automation.models.TaskInfo;
import com.arine.automation.util.WebDriverUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.Logger;

import static com.arine.automation.glue.CommonSteps.takeScreenshot;

public class TaskEditedPage extends BasePage {
    private static final Logger LOGGER = Logger.getLogger(TaskEditedPage.class.getName());

    public static final String TASK_SELECT_ALL_TASK = "//*[@name='selectAll']";
    public static final String TASK_RECORD_SELECT = "//*[@role='row']//*[contains(text(),'%s')]/../..//span[contains(@class,'checkbox')]";
    //public static final String TASK_RECORD_SELECT = "//*[@role='row']//*[contains(text(),'%s')]/../..//input";
    public static final String EDIT_SELECTED_BUTTON = "//button[not(contains(@class,'disabled')) and .//*[text()='Edit Selected']]";
    public static final String EDIT_SELECTED_BUTTON_DISABLED = "//button[contains(@class,'disabled') and .//*[text()='Edit Selected']]";
    public static final String CLOSE_TASK_BUTTON = "//li[text()='Close']";
    public static final String DELETE_TASK_BUTTON = "//li[text()='Delete']";
    public static final String TASK_RECORD = "//*[@role='row']//*[contains(text(),'%s')]/../..";
    static final String TASK_RECORD_LAST_COLUMN = "(//*[@role='row']//*[contains(text(),'%s')]/../..)[1]/div[last()]/*";
    public static final String SET_IN_PROGRESS_TASK_BUTTON = "//li[text()='Set to \"In progress\"']";
    public static final String TASK_STATUS = "//*[@role='row']//*[contains(text(),'%s')]/../..//div[3]";
    public static final String NOTES_TEXT_BOX = "//*[contains(@class, 'textBoxInputWrapper') and *[@name='note']]";
    public static final String NOTES_COLUMN_SELECTED_TASK = "//*[@role='row' and contains(@class,'selected')]//*[@role='cell'][8]/div";
    public static final String LOG_ACTION_BUTTON = "//*[@id='logAction']";
    public static final String TASK_TABLE_HEADER = "//th[@id='%s']";

    public static final String MAKE_AS_URGENT_BUTTON = "//li[text()='Mark as \"Urgent\"']";
    public static final String SELECT_NEXT_DATE = "//*[contains(@class, 'day--today')]/following::div[1]";
    public static final String SET_DUE_DATE_BUTTON = "//li[text()='Set due date']";
    public static final String DATE_AND_TIME_INPUT_BOX = "//input[@name='due_date']";
    public static final String SELECT_TIME = "//li[text()='11:45']";
    public static final String DATE_UPDATE_BUTTON = "//button[text()='Update']";


    public void selectAllTasksCheckBox() throws AutomationException {
        removeSortOnPriorityColumn();
        WebElement AllTasksSelectCheckbox = driverUtil.getElementUsingJavascript(TASK_SELECT_ALL_TASK);
        if (AllTasksSelectCheckbox == null)
            throw new AutomationException("checkbox for Select all tasks not displayed on Task table");
        takeScreenshot();
        if (!AllTasksSelectCheckbox.isSelected()) {
            driverUtil.clickUsingJavaScript(TASK_SELECT_ALL_TASK);
            if (!AllTasksSelectCheckbox.isSelected())
                throw new AutomationException("All tasks were not Selected");
        }
    }

    public void DeselectAllTasks() throws AutomationException {
        removeSortOnPriorityColumn();
        WebElement AllTasksSelectCheckbox = driverUtil.getElementUsingJavascript(TASK_SELECT_ALL_TASK);
        if (AllTasksSelectCheckbox == null)
            throw new AutomationException("checkbox for Select all tasks not displayed on Task table");
        takeScreenshot();
        if (!AllTasksSelectCheckbox.isSelected())
            driverUtil.clickUsingJavaScript(TASK_SELECT_ALL_TASK);
        WebDriverUtil.waitForAWhile();
        driverUtil.clickUsingJavaScript(TASK_SELECT_ALL_TASK);
        WebDriverUtil.waitForAWhile(5);
    }


    public static void verifyEditSelectedButtonIsDisabled() throws AutomationException {
        WebElement element = driverUtil.findElement(EDIT_SELECTED_BUTTON_DISABLED);
        if (element == null)
            throw new AutomationException("Edit Selected button is not displayed, else it is enabled but we expected it to disabled");
    }

    public static void verifyEditSelectedButtonIsEnable() throws AutomationException {
        WebElement element = driverUtil.findElement(EDIT_SELECTED_BUTTON);
        if (element == null)
            throw new AutomationException("Edit Selected button is not displayed, else it is disabled but we expected it to enabled");
    }


    public void selectNewlycreatedTask(String finder) throws AutomationException {
        WebElement task = driverUtil.getWebElement(String.format(TASK_RECORD, finder), WAIT_30_SECOND);
        CommonSteps.takeScreenshot();
        if (task == null)
            throw new AutomationException("No task found with given text: " + finder);
    }


    public void verifyTaskNotDisplayed(String TaskName) throws AutomationException {
        List<WebElement> webElements = driverUtil.getWebElements(TASK_RECORD);
        boolean recordFound = false;
        for (WebElement element : webElements
        ) {
            if (element.getText().contains(TaskName)) {
                recordFound = true;
                break;
            }
        }
        if (recordFound)
            throw new AutomationException("we expected Task should not be displayed but it is visible : " + TaskName);
    }


    public void Delete_Close_Operations_On_task(String noteOfTask, String OperationName) throws AutomationException {
        WebElement taskRecord = driverUtil.getWebElement(String.format(TASK_RECORD, noteOfTask));
        if (taskRecord == null)
            throw new AutomationException("No task found having text: " + noteOfTask);
        removeSortOnPriorityColumn();
        WebElement taskSelect = driverUtil.getWebElement(String.format(TASK_RECORD_SELECT, noteOfTask), 60);
        if (taskSelect == null)
            throw new AutomationException(String.format("Checkbox to select task %s not found", noteOfTask));
        driverUtil.waitForElementToBeClickable(String.format(TASK_RECORD_SELECT, noteOfTask));
        taskSelect.click();
        takeScreenshot();
        WebElement EditSelectedButton = driverUtil.getWebElement(EDIT_SELECTED_BUTTON);
        if (EditSelectedButton == null)
            throw new AutomationException("Unable to find Edit Selected task button on page or it might taking too long time to load!");
        EditSelectedButton.click();
        WebDriverUtil.waitForAWhile(5);
        WebElement element = driverUtil.getWebElement("//li[text()='" + OperationName + "']", 3);
        if (element == null)
            throw new AutomationException("Unable to find " + OperationName + " task button on page or it might taking too long time to load!");
        WebDriverUtil.waitForAWhile(10);
        element.click();

        WebDriverUtil.waitForAWhile(10);
        PageFactory.homePage().scrollToMenu("Tasks");
    }

    public void Delete_Close_Operations_On_Multiple_tasks(String Task_1, String Task_2, String OperationName) throws AutomationException {
        driverUtil.waitForElement(By.xpath("//*[contains(@class,'syncIcon')]//*[(contains(@class,'syncOff'))]"),30);
        WebElement taskRecord_1 = driverUtil.getWebElement(String.format(TASK_RECORD, Task_1));
        if (taskRecord_1 == null)
            throw new AutomationException("No task found having text: " + Task_1);
        removeSortOnPriorityColumn();
        WebElement taskSelect_1 = driverUtil.getWebElement(String.format(TASK_RECORD_SELECT, Task_1), 60);
        if (taskSelect_1 == null)
            throw new AutomationException(String.format("Checkbox to select task %s not found", Task_1));
        taskSelect_1.click();
        takeScreenshot();

        WebElement taskRecord_2 = driverUtil.getWebElement(String.format(TASK_RECORD, Task_2), 60);
        if (taskRecord_2 == null)
            throw new AutomationException("No task found having text: " + Task_2);
        WebDriverUtil.waitForAWhile(10);
        WebElement taskSelect_2 = driverUtil.getWebElement(String.format(TASK_RECORD_SELECT, Task_2));
        if (taskSelect_2 == null)
            throw new AutomationException(String.format("Checkbox to select task %s not found", Task_2));
        taskSelect_2.click();
        takeScreenshot();

        WebElement EditSelectedButton = driverUtil.getWebElement(EDIT_SELECTED_BUTTON);
        if (EditSelectedButton == null)
            throw new AutomationException("Unable to find Edit Selected task button on page or it might taking too long time to load!");
        EditSelectedButton.click();
        WebDriverUtil.waitForAWhile(5);
        WebElement element = driverUtil.getWebElement("//li[text()='" + OperationName + "']", 3);
        if (element == null)
            throw new AutomationException("Unable to find " + OperationName + " task button on page or it might taking too long time to load!");
        WebDriverUtil.waitForAWhile(10);
        element.click();

        WebDriverUtil.waitForAWhile(10);
        PageFactory.homePage().scrollToMenu("Tasks");
    }

    public void Task_InProgress(String noteOfTask) throws AutomationException {
        WebElement taskRecord = driverUtil.getWebElement(String.format(TASK_RECORD, noteOfTask));
        if (taskRecord == null)
            throw new AutomationException("No task found having text: " + noteOfTask);
        removeSortOnPriorityColumn();
        WebElement taskSelect = driverUtil.getWebElement(String.format(TASK_RECORD_SELECT, noteOfTask), 60);
        if (taskSelect == null)
            throw new AutomationException(String.format("Checkbox to select task %s not found", noteOfTask));
        taskSelect.click();
        takeScreenshot();

        WebElement EditSelectedButton = driverUtil.getWebElement(EDIT_SELECTED_BUTTON);
        if (EditSelectedButton == null)
            throw new AutomationException("Unable to find Edit Selected task button on page or it might taking too long time to load!");
        EditSelectedButton.click();
        WebDriverUtil.waitForAWhile(5);

        WebElement DeleteTaskButton = driverUtil.getWebElement(SET_IN_PROGRESS_TASK_BUTTON);
        if (DeleteTaskButton == null)
            throw new AutomationException("Unable to find Set In-Progress task button on page or it might taking too long time to load!");
        WebDriverUtil.waitForAWhile(10);
        DeleteTaskButton.click();

        WebDriverUtil.waitForAWhile(10);
        PageFactory.homePage().scrollToMenu("Tasks");

    }

    public void Multiple_Task_InProgress(String noteOfTask_1, String noteOfTask_2) throws AutomationException {
        WebElement taskRecord_1 = driverUtil.getWebElement(String.format(TASK_RECORD, noteOfTask_1));
        if (taskRecord_1 == null)
            throw new AutomationException("No task found having text: " + noteOfTask_1);
        removeSortOnPriorityColumn();
        WebElement taskSelect_1 = driverUtil.getWebElement(String.format(TASK_RECORD_SELECT, noteOfTask_1), 60);
        if (taskSelect_1 == null)
            throw new AutomationException(String.format("Checkbox to select task %s not found", noteOfTask_1));
        taskSelect_1.click();
        takeScreenshot();

        WebElement taskRecord_2 = driverUtil.getWebElement(String.format(TASK_RECORD, noteOfTask_2), 60);
        if (taskRecord_2 == null)
            throw new AutomationException("No task found having text: " + noteOfTask_1);
        WebDriverUtil.waitForAWhile(10);
        WebElement taskSelect_2 = driverUtil.getWebElement(String.format(TASK_RECORD_SELECT, noteOfTask_2));
        if (taskSelect_2 == null)
            throw new AutomationException(String.format("Checkbox to select task %s not found", noteOfTask_2));
        taskSelect_2.click();
        takeScreenshot();
        WebDriverUtil.waitForAWhile(5);

        WebElement EditSelectedButton = driverUtil.getWebElement(EDIT_SELECTED_BUTTON);
        if (EditSelectedButton == null)
            throw new AutomationException("Unable to find Edit Selected task button on page or it might taking too long time to load!");
        EditSelectedButton.click();
        WebDriverUtil.waitForAWhile(5);

        WebElement DeleteTaskButton = driverUtil.getWebElement(SET_IN_PROGRESS_TASK_BUTTON);
        if (DeleteTaskButton == null)
            throw new AutomationException("Unable to find Set In-Progress task button on page or it might taking too long time to load!");
        WebDriverUtil.waitForAWhile(10);
        DeleteTaskButton.click();

        WebDriverUtil.waitForAWhile(10);
        PageFactory.homePage().scrollToMenu("Tasks");

    }


    public void verifyTaskStatus(String task, String TaskStatus) throws AutomationException {
        WebElement Status = driverUtil.findElement("//*[@role='row']//*[contains(text(),'" + task + "')]/../..//div[3]");
        String element = Status.getText();
        if (!element.equals(TaskStatus))
            throw new AutomationException("Unable to find '" + TaskStatus + "' Status Column!");
        takeScreenshot();
    }


    public void Set_Task_Priority(String noteOfTask) throws AutomationException {
        WebElement taskRecord = driverUtil.getWebElement(String.format(TASK_RECORD, noteOfTask));
        if (taskRecord == null)
            throw new AutomationException("No task found having text: " + noteOfTask);
//        selectAllTasksCheckBox();
//        DeselectAllTasks();
        removeSortOnPriorityColumn();
        WebElement taskSelect = driverUtil.getWebElement(String.format(TASK_RECORD_SELECT, noteOfTask), 60);
        if (taskSelect == null)
            throw new AutomationException(String.format("Checkbox to select task %s not found", noteOfTask));
        driverUtil.waitForElementToBeClickable(String.format(TASK_RECORD_SELECT, noteOfTask));
        driverUtil.clickUsingJavaScript(String.format(TASK_RECORD_SELECT, noteOfTask));
        takeScreenshot();

        WebElement EditSelectedButton = driverUtil.getWebElement(EDIT_SELECTED_BUTTON);
        if (EditSelectedButton == null)
            throw new AutomationException("Unable to find Edit Selected task button on page or it might taking too long time to load!");
        EditSelectedButton.click();
        WebDriverUtil.waitForAWhile(5);

        WebElement DeleteTaskButton = driverUtil.getWebElement(MAKE_AS_URGENT_BUTTON);
        if (DeleteTaskButton == null)
            throw new AutomationException("Unable to find Make As Urgent task button on page or it might taking too long time to load!");
        WebDriverUtil.waitForAWhile(10);
        DeleteTaskButton.click();

        WebDriverUtil.waitForAWhile(10);
        PageFactory.homePage().scrollToMenu("Tasks");
    }

    public void Set_Task_Priority_for_Multiple_tasks(String noteOfTask_1, String noteOfTask_2) throws AutomationException {
        WebElement taskRecord = driverUtil.getWebElement(String.format(TASK_RECORD, noteOfTask_1));
        if (taskRecord == null)
            throw new AutomationException("No task found having text: " + noteOfTask_1);
//        selectAllTasksCheckBox();
//        DeselectAllTasks();
        removeSortOnPriorityColumn();
        WebElement taskSelect = driverUtil.getWebElement(String.format(TASK_RECORD_SELECT, noteOfTask_1), 60);
        if (taskSelect == null)
            throw new AutomationException(String.format("Checkbox to select task %s not found", noteOfTask_1));
        driverUtil.waitForElementToBeClickable(String.format(TASK_RECORD_SELECT, noteOfTask_1));
        driverUtil.clickUsingJavaScript(String.format(TASK_RECORD_SELECT, noteOfTask_1));
        takeScreenshot();

        WebElement taskRecord_2 = driverUtil.getWebElement(String.format(TASK_RECORD, noteOfTask_2), 60);
        if (taskRecord_2 == null)
            throw new AutomationException("No task found having text: " + noteOfTask_2);
        WebElement taskSelect_2 = driverUtil.getWebElement(String.format(TASK_RECORD_SELECT, noteOfTask_2));
        if (taskSelect_2 == null)
            throw new AutomationException(String.format("Checkbox to select task %s not found", noteOfTask_2));
        driverUtil.waitForElementToBeClickable(String.format(TASK_RECORD_SELECT, noteOfTask_2));
        driverUtil.clickUsingJavaScript(String.format(TASK_RECORD_SELECT, noteOfTask_2));
        takeScreenshot();

        WebElement EditSelectedButton = driverUtil.getWebElement(EDIT_SELECTED_BUTTON);
        if (EditSelectedButton == null)
            throw new AutomationException("Unable to find Edit Selected task button on page or it might taking too long time to load!");
        EditSelectedButton.click();
        WebDriverUtil.waitForAWhile(5);

        WebElement DeleteTaskButton = driverUtil.getWebElement(MAKE_AS_URGENT_BUTTON);
        if (DeleteTaskButton == null)
            throw new AutomationException("Unable to find Make As Urgent task button on page or it might taking too long time to load!");
        WebDriverUtil.waitForAWhile(10);
        DeleteTaskButton.click();

        WebDriverUtil.waitForAWhile(10);
        PageFactory.homePage().scrollToMenu("Tasks");
    }


    public void verifyTaskPriority(String task, String Priority) throws AutomationException {
        WebElement Status = driverUtil.findElement("//*[@role='row']//*[contains(text(),'" + task + "')]/../..//div[2]");
        String element = Status.getText();
        if (!element.equals(Priority))
            throw new AutomationException("Unable to find '" + Priority + "' Status Column!");
        takeScreenshot();
    }


    public void SetDueDateAndTimeAndVerifyFromTab(String noteOfTask) throws AutomationException {
        WebElement taskRecord = driverUtil.getWebElement(String.format(TASK_RECORD, noteOfTask));
        if (taskRecord == null)
            throw new AutomationException("No task found having note as : " + noteOfTask);
//        selectAllTasksCheckBox();
//        DeselectAllTasks();
        removeSortOnPriorityColumn();
        WebElement taskSelect = driverUtil.getWebElement(String.format(TASK_RECORD_SELECT, noteOfTask), 60);
        if (taskSelect == null)
            throw new AutomationException(String.format("Checkbox to select task %s not found", noteOfTask));
        driverUtil.waitForElementToBeClickable(String.format(TASK_RECORD_SELECT, noteOfTask));
        driverUtil.clickUsingJavaScript(String.format(TASK_RECORD_SELECT, noteOfTask));
        takeScreenshot();
        WebDriverUtil.waitForAWhile(5);

        WebElement EditSelectedButton = driverUtil.getWebElement(EDIT_SELECTED_BUTTON);
        if (EditSelectedButton == null)
            throw new AutomationException("Unable to find Edit Selected task button on page or it might taking too long time to load!");
        EditSelectedButton.click();
        WebDriverUtil.waitForAWhile(5);

        WebElement SetDueDateButton = driverUtil.getWebElement(SET_DUE_DATE_BUTTON);
        if (SetDueDateButton == null)
            throw new AutomationException("Unable to find Make As Urgent task button on page or it might taking too long time to load!");
        SetDueDateButton.click();

        WebElement DateAndTimeInputBox = driverUtil.getWebElement(DATE_AND_TIME_INPUT_BOX);
        if (DateAndTimeInputBox == null)
            throw new AutomationException("Unable to Date and time InputBox on page or it might taking too long time to load!");
        DateAndTimeInputBox.click();

        WebElement SelectDate = driverUtil.getWebElement(SELECT_NEXT_DATE);
        if (SelectDate == null)
            throw new AutomationException("Unable to find Expected date on page or it might taking too long time to load!");
        SelectDate.click();

        WebElement SelectTime = driverUtil.getWebElement(SELECT_TIME);
        if (SelectTime == null)
            throw new AutomationException("Unable to find Expected Time on page or it might taking too long time to load!");
        SelectTime.click();

        WebElement DateAndTimeInputBoxValue = driverUtil.getWebElement(DATE_AND_TIME_INPUT_BOX);
        if (DateAndTimeInputBoxValue == null)
            throw new AutomationException("Unable to find Date and time InputBox on page or it might taking too long time to load!");
        String Selected_date_and_Time = DateAndTimeInputBoxValue.getAttribute("value");
        System.out.println("Date From Box===" + Selected_date_and_Time);

        WebElement UpdateButton = driverUtil.getWebElement(DATE_UPDATE_BUTTON);
        if (UpdateButton == null)
            throw new AutomationException("Unable to find Update Button on page or it might taking too long time to load!");
        UpdateButton.click();
        waitForLoadingPage();
        WebElement DueDate = driverUtil.findElement("//*[@role='row']//*[contains(text(),'" + noteOfTask + "')]/../..//div[1]/div[1]");
        String DueDateFromTable = DueDate.getText();
        System.out.println(DueDateFromTable);
        if (!DueDateFromTable.equals(Selected_date_and_Time))
            throw new AutomationException("Its not matched Previous date to updated date or it might taking too long time to load!");
        PageFactory.homePage().scrollToMenu("Tasks");
    }


    public void SetDueDateAndTimeforMultipletaskAndVerifyFromTab(String Task_1, String Task_2) throws AutomationException {
        WebElement taskRecord = driverUtil.getWebElement(String.format(TASK_RECORD, Task_1));
        if (taskRecord == null)
            throw new AutomationException("No task found having text: " + Task_1);
        removeSortOnPriorityColumn();
        WebElement taskSelect = driverUtil.getWebElement(String.format(TASK_RECORD_SELECT, Task_1), 60);
        if (taskSelect == null)
            throw new AutomationException(String.format("Checkbox to select task %s not found", Task_1));
        driverUtil.waitForElementToBeClickable(String.format(TASK_RECORD_SELECT, Task_1));
        driverUtil.clickUsingJavaScript(String.format(TASK_RECORD_SELECT, Task_1));
        takeScreenshot();

        WebElement taskRecord_2 = driverUtil.getWebElement(String.format(TASK_RECORD, Task_2));
        if (taskRecord_2 == null)
            throw new AutomationException("No task found having text: " + Task_2);
        WebElement taskSelect_2 = driverUtil.getWebElement(String.format(TASK_RECORD_SELECT, Task_2), 60);
        if (taskSelect_2 == null)
            throw new AutomationException(String.format("Checkbox to select task %s not found", Task_2));
        driverUtil.waitForElementToBeClickable(String.format(TASK_RECORD_SELECT, Task_2));
        driverUtil.clickUsingJavaScript(String.format(TASK_RECORD_SELECT, Task_2));
        takeScreenshot();


        WebElement EditSelectedButton = driverUtil.getWebElement(EDIT_SELECTED_BUTTON);
        if (EditSelectedButton == null)
            throw new AutomationException("Unable to find Edit Selected task button on page or it might taking too long time to load!");
        EditSelectedButton.click();
        WebDriverUtil.waitForAWhile(5);

        WebElement SetDueDateButton = driverUtil.getWebElement(SET_DUE_DATE_BUTTON);
        if (SetDueDateButton == null)
            throw new AutomationException("Unable to find Make As Urgent task button on page or it might taking too long time to load!");
        WebDriverUtil.waitForAWhile(10);
        SetDueDateButton.click();


        WebElement DateAndTimeInputBox = driverUtil.getWebElement(DATE_AND_TIME_INPUT_BOX);
        if (DateAndTimeInputBox == null)
            throw new AutomationException("Unable to Date and time InputBox on page or it might taking too long time to load!");
        DateAndTimeInputBox.click();

        WebElement SelectDate = driverUtil.getWebElement(SELECT_NEXT_DATE);
        if (SelectDate == null)
            throw new AutomationException("Unable to find Expected date on page or it might taking too long time to load!");
        SelectDate.click();

        WebElement SelectTime = driverUtil.getWebElement(SELECT_TIME);
        if (SelectTime == null)
            throw new AutomationException("Unable to find Expected Time on page or it might taking too long time to load!");
        SelectTime.click();

        WebElement DateAndTimeInputBoxValue = driverUtil.getWebElement(DATE_AND_TIME_INPUT_BOX);
        if (DateAndTimeInputBoxValue == null)
            throw new AutomationException("Unable to find Date and time InputBox on page or it might taking too long time to load!");
        String Selected_date_and_Time = DateAndTimeInputBoxValue.getAttribute("value");
        System.out.println("Date From Box===" + Selected_date_and_Time);

        WebElement UpdateButton = driverUtil.getWebElement(DATE_UPDATE_BUTTON);
        if (UpdateButton == null)
            throw new AutomationException("Unable to find Update Button on page or it might taking too long time to load!");
        UpdateButton.click();

        waitForLoadingPage();
        WebElement DueDate = driverUtil.findElement("//*[@role='row']//*[contains(text(),'" + Task_1 + "')]/../..//div[1]/div[1]");
        String DueDateFromTable = DueDate.getText();
        if (!DueDateFromTable.equals(Selected_date_and_Time))
            throw new AutomationException("Date was not updated or it might taking too long time to update!");
        WebDriverUtil.waitForAWhile(10);
        WebElement DueDate_2 = driverUtil.findElement("//*[@role='row']//*[contains(text(),'" + Task_2 + "')]/../..//div[1]/div[1]");
        String DueDateFromTable_2 = DueDate_2.getText();
        if (!DueDateFromTable_2.equals(Selected_date_and_Time))
            throw new AutomationException("Its not matched Previous date to updated date or it might taking too long time to load!");
        WebDriverUtil.waitForAWhile(10);
        PageFactory.homePage().scrollToMenu("Tasks");
    }


    public void verifyUpdatedTaskReflectInBelowTable(String TaskNote) throws AutomationException {
        removeSortOnPriorityColumn();
        WebElement taskSelect = driverUtil.getWebElement(String.format(TASK_RECORD_SELECT, TaskNote));
        if (taskSelect == null)
            throw new AutomationException(String.format("Checkbox to select task %s not found", TaskNote));
        taskSelect.click();
        takeScreenshot();

        WebDriverUtil.waitForAWhile(10);
        WebElement NotesTextBox = driverUtil.getWebElement(NOTES_TEXT_BOX);
        if (NotesTextBox == null)
            throw new AutomationException("Note Text Box not find or or it might taking too long time to load!");
        takeScreenshot();
        String NoteText = NotesTextBox.getText();
        WebDriverUtil.waitForAWhile(10);
        if (!NoteText.equals(TaskNote))
            throw new AutomationException("Note From Table and Note from text box is not matched or might taking too long time to load!");
        WebDriverUtil.waitForAWhile(5);
    }

    public void removeSortOnPriorityColumn() throws AutomationException {
        WebElement priorityColumnHeader = driverUtil.findElement("(//*[@role='columnheader' and @title])[2]");
        if (priorityColumnHeader == null)
            throw new AutomationException("Priority column not displayed or taking too long time to load");

        while (!(priorityColumnHeader.getText().length() == 9)) {
            priorityColumnHeader.click();
            priorityColumnHeader = driverUtil.findElement("(//*[@role='columnheader' and @title])[2]");
        }

    }

    public void selectTaskHavingNote(String taskNote) throws AutomationException {
        removeSortOnPriorityColumn();
        WebElement taskSelect = driverUtil.getWebElement(String.format(TASK_RECORD_SELECT, taskNote));
        if (taskSelect == null)
            throw new AutomationException(String.format("Checkbox to select task %s not found", taskNote));
        taskSelect.click();
        takeScreenshot();
    }

    public void logActionForSelectedTaskAndVerifyInNotesBox(String updatedByUserAndDate) throws AutomationException {

        driverUtil.findElementByTextAndClick("Log Action");
        WebDriverUtil.waitForAWhile(20);
        WebElement logActionPopup = driverUtil.findElementByText("Log New Action for Task Patient");
        if (logActionPopup == null)
            throw new AutomationException("Log Action Pop up not displayed");
        WebElement logActionBtn = driverUtil.findElement(LOG_ACTION_BUTTON);
        if (logActionBtn == null)
            throw new AutomationException("Log action button not displayed on pop up of log action for task");
        logActionBtn.click();
        WebDriverUtil.waitForAWhile(20);

        DateTimeFormatter format = DateTimeFormatter.ofPattern("M/d/yy");
        String currentDate = LocalDateTime.now().format(format);
        String CurrentNoteWithDate = null;
        try {
            CurrentNoteWithDate = (updatedByUserAndDate+" "+( currentDate)+".");
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Current Date ====="+CurrentNoteWithDate);

        WebElement notesColumn = driverUtil.getWebElement(NOTES_COLUMN_SELECTED_TASK);
        if (notesColumn == null)
            throw new AutomationException("No task selected or notes column not displayed");
        takeScreenshot();
        String NoteText = notesColumn.getText();
        if (!NoteText.contains(CurrentNoteWithDate))
            throw new AutomationException("Notes column does not displayed expected Updated by user and date");
        WebDriverUtil.waitForAWhile(5);
    }


    public void selectTask(String notes) throws AutomationException {
        WebElement taskRecord = driverUtil.getWebElement(String.format(TASK_RECORD, notes));
        if (taskRecord == null)
            throw new AutomationException("No task found having notes as : " + notes);
        removeSortOnPriorityColumn();
        WebElement taskSelect = driverUtil.getWebElement(String.format(TASK_RECORD_SELECT, notes), 60);
        if (taskSelect == null)
            throw new AutomationException(String.format("Checkbox to select task %s not found", notes));
        taskSelect.click();
        takeScreenshot();
    }

    private static String getCurrentDate() {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("M/d/yy");
        String currentDate = LocalDateTime.now().format(format);
        return currentDate;
    }

    @Override
    public String getName() {
        return "Task";
    }
}
