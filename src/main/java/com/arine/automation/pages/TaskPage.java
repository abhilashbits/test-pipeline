package com.arine.automation.pages;

import com.arine.automation.exception.AutomationException;
import com.arine.automation.glue.CommonSteps;
import com.arine.automation.models.TaskInfo;
import com.arine.automation.util.WebDriverUtil;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import static com.arine.automation.glue.CommonSteps.takeScreenshot;

public class TaskPage extends BasePage {
    private static final Logger LOGGER = Logger.getLogger(TaskPage.class.getName());
    public static final String TASK_RECORD_XPATH_BY_INDEX = "//*[contains(@class,'PatientTriageTable-__tableContainer')]/table//tr[%s]";
    public static final String TASK_DETAIL_BY_TITLE_LINK = "//*[contains(@class,'TaskInput') and text()='%s']/a";
    public static final String TASK_DETAIL_BY_TITLE = "//*[contains(@class,'TaskInput') and text()='%s']/span";
    public static final String TASK_FILTER_ASSIGNEES_ALL = "//*[@for='allAssignees']";
    public static final String TASK_FILTER_ASSIGNEES_MY_TASK = "//*[@for='myTasks']";
    public static final String TASK_FILTER_ASSIGNEES_CSS = "//*[@for='CSS']";
    public static final String TASK_FILTER_ASSIGNEES_PHARMACIST = "//*[@for='Pharmacist']";

    public static final String TASK_FILTER_STATUS_OPEN = "//*[@for='open']";
    public static final String TASK_FILTER_STATUS_IN_PROGRESS = "//*[@for='in progress']";
    public static final String TASK_FILTER_STATUS_ALL = "//*[@for='allStatus']";

    public static final String TASK_FILTER_TASK_FOR_PATIENT = "//*[@for='typePatient']";
    public static final String TASK_FILTER_TASK_FOR_PRACTITIONER = "//*[@for='typePractitioner']";
    public static final String TASK_FILTER_TASK_FOR_ALL = "//*[@for='allTypes']";

    public static final String TASK_TABLE_HEADER = "//th[@id='%s']";
    public static final String DELETE_TASK_CONFIRMATION_POPUP_DELETE_BUTTON = "//button[text()='Delete this task']";

    public static Map<String, String> TASKS_FILTER_XPATH_MAPPER = new HashMap<>();

    static {
        TASKS_FILTER_XPATH_MAPPER.put("Assigned to: Only My Tasks", TASK_FILTER_ASSIGNEES_MY_TASK);
        TASKS_FILTER_XPATH_MAPPER.put("Assigned to: CSS", TASK_FILTER_ASSIGNEES_CSS);
        TASKS_FILTER_XPATH_MAPPER.put("Assigned to: Pharmacist", TASK_FILTER_ASSIGNEES_PHARMACIST);
        TASKS_FILTER_XPATH_MAPPER.put("Assigned to: All", TASK_FILTER_ASSIGNEES_ALL);

        TASKS_FILTER_XPATH_MAPPER.put("Status: Open", TASK_FILTER_STATUS_OPEN);
        TASKS_FILTER_XPATH_MAPPER.put("Status: In Progress", TASK_FILTER_STATUS_IN_PROGRESS);
        TASKS_FILTER_XPATH_MAPPER.put("Status: All", TASK_FILTER_STATUS_ALL);

        TASKS_FILTER_XPATH_MAPPER.put("Task for: Patient", TASK_FILTER_TASK_FOR_PATIENT);
        TASKS_FILTER_XPATH_MAPPER.put("Task for: Practitioner", TASK_FILTER_TASK_FOR_PRACTITIONER);
        TASKS_FILTER_XPATH_MAPPER.put("Task for: All", TASK_FILTER_TASK_FOR_ALL);
    }

    public static final String TASK_RECORD = "//*[@role='row']//*[contains(text(),'%s')]/../..";
    public static final String TASK_RECORD_LAST_COLUMN = "(//*[@role='row']//*[contains(text(),'%s')]/../..)[1]/div[last()]/*";
    public static final String TASK_RECORD_DELETE = "//*[@role='row']//*[contains(text(),'%s')]/../..//*[contains(@class,'iconBin')]";

    public WebElement getTaskRecord(int index) throws AutomationException {
        /*We need to add 1 in index
        for example if we want to click on first task record their index will be 2
        as first index is fixed for filters.*/
        return driverUtil.getWebElement(String.format(TASK_RECORD_XPATH_BY_INDEX, index + 1));
    }

    public WebElement getTaskDetail(String attribute) throws AutomationException {
        WebElement element = driverUtil.getWebElementAndScrollWithoutWait(String.format(TASK_DETAIL_BY_TITLE_LINK, attribute));
        if (element == null)
            element = driverUtil.getWebElement(String.format(TASK_DETAIL_BY_TITLE, attribute));
        return element;
    }

    public TaskInfo getTaskInfo() throws AutomationException {
        TaskInfo info = new TaskInfo();
        info.taskName = getTaskDetail("Task").getText();
        info.patientName = getTaskDetail("Patient Name").getText();
        info.patientId = getTaskDetail("ID").getText();
        return info;
    }

    public void selectTaskFilter(String filterName) throws AutomationException {
        String filterXPATH = TASKS_FILTER_XPATH_MAPPER.get(filterName);
        if (filterXPATH == null)
            throw new AutomationException("No filter xpath defined in Task page!");
        driverUtil.getWebElement(filterXPATH).click();
    }

    public void selectTasksSubTab(String tabName) throws AutomationException {
        driverUtil.getWebElement(String.format(TASK_TABLE_HEADER, tabName)).click();
    }

    public void verifyTask(String finder) throws AutomationException {
        WebElement task = driverUtil.getWebElement(String.format(TASK_RECORD, finder), WAIT_30_SECOND);
        CommonSteps.takeScreenshot();
        if (task == null)
            throw new AutomationException("No task found with given text: " + finder);
    }

    public void deleteTask(String finder) throws AutomationException {
        WebElement taskRecord = driverUtil.getWebElementAndScroll(String.format(TASK_RECORD_LAST_COLUMN, finder));
        if (taskRecord == null)
            throw new AutomationException("No task found with given text: " + finder);
        while (true) {
            try {
                WebElement taskDelete = driverUtil.getWebElement(String.format(TASK_RECORD_DELETE, finder));
                if (taskDelete == null)
                    break;
                WebDriverUtil.waitForAWhile();
                taskDelete.click();
                WebDriverUtil.waitForAWhile();
                WebElement deleteConfirmationButton = driverUtil.getWebElement(DELETE_TASK_CONFIRMATION_POPUP_DELETE_BUTTON);
                if (deleteConfirmationButton == null)
                    throw new AutomationException("Unable to find delete task button on confirmation popup or it might taking too long time to load!");
                deleteConfirmationButton.click();
                WebDriverUtil.waitForAWhile(10);
            } catch (StaleElementReferenceException e){
                // do nothing
            }
        }
        PageFactory.homePage().scrollToMenu("Tasks");
        takeScreenshot();
    }

    @Override
    public String getName() {
        return "Task";
    }
}
