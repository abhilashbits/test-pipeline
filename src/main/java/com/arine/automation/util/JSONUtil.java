package com.arine.automation.util;

import com.arine.automation.constants.Constants;
import com.arine.automation.models.SigTranslation;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JSONUtil {
    public static final String ABBREVIATIONS_FILE_PATH = Constants.TEST_DATA_FILE_PATH + "abbreviations.json";
    public static final String SIG_TEXT_FILE_PATH = Constants.TEST_DATA_FILE_PATH + "get-sig-text-unit-test.json";

    public static Map<String, Object> readAbbreviations(String jsonFilePath) throws IOException {
        byte[] jsonData = Files.readAllBytes(Paths.get(jsonFilePath));
        HashMap<String, Object> mapping = new ObjectMapper().readValue(new String(jsonData), HashMap.class);
        return mapping;
    }

    public static List<SigTranslation> readSigText(String jsonFilePath) throws IOException {
        byte[] jsonData = Files.readAllBytes(Paths.get(jsonFilePath));
        List<SigTranslation> mapping = Arrays.asList(new ObjectMapper().readValue(new String(jsonData), SigTranslation[].class));
        return mapping;
    }

    public static void main(String[] args) throws IOException {
        //List<SigTranslation> mapping = readSigText(JSONUtil.SIG_TEXT_FILE_PATH);
       //System.out.println(mapping.size());
    }
}
