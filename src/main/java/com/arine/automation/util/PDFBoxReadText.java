package com.arine.automation.util;

import com.arine.automation.exception.AutomationException;
import com.arine.automation.glue.CommonSteps;
import org.apache.commons.io.FileUtils;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.text.PDFTextStripper;
import org.opencv.core.Core;
import org.sikuli.script.Finder;
import org.sikuli.script.Image;
import org.sikuli.script.ScreenImage;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;

public class PDFBoxReadText {
    public static String FILEPATH = "./extent-reports/screenshots/generated-patient-report.pdf";
    public static String IMAGE_LOCATION = "./extent-reports/screenshots/";

    public static List<String> readTextFromPdfFile(byte[] data) throws AutomationException {
        List<String> pages = new ArrayList<>();
        String encodedString = new String(data);
        encodedString = encodedString.substring(encodedString.indexOf(",")+1, encodedString.length());
        File reportFile = createFileFromByteArray(encodedString);
        try (PDDocument doc = PDDocument.load(reportFile)) {
            PDFTextStripper stripper = new PDFTextStripper();
            for(int i=1; i<=doc.getNumberOfPages(); i++) {
                stripper.setStartPage(i);
                stripper.setEndPage(i);
                String pageText = stripper.getText(doc);
                pages.add(pageText);
            }
        }catch(Exception e) {
            throw new AutomationException("Unable to read report pdf!\n Exception:"+e.getMessage());
        }
        return pages;
    }

    public static Map<String, String> readPMLReportAllMedications(byte[] data) throws AutomationException {
        String encodedString = new String(data);
        encodedString = encodedString.substring(encodedString.indexOf(",")+1, encodedString.length());
        File reportFile = createFileFromByteArray(encodedString);
        Map<String, String> medicationMapping = PMLReportUtil.getAllMedicationNoteMapping(reportFile.getPath());
        return medicationMapping;
    }

    public static Map<String, String> readMtmPatientPMLReportAllMedications(byte[] data) throws AutomationException {
        String encodedString = new String(data);
        encodedString = encodedString.substring(encodedString.indexOf(",")+1, encodedString.length());
        File reportFile = createFileFromByteArray(encodedString);
        Map<String, String> medicationMapping = PMLReportUtil.getAllMtmPatientMedicationMapping(reportFile.getPath());
        return medicationMapping;
    }

    public static void verifyReportLogo(String organization, byte[] reportData) throws AutomationException, IOException {
        try{
            System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
            String encodedString = new String(reportData);
            encodedString = encodedString.substring(encodedString.indexOf(",")+1, encodedString.length());
            File reportFile = createFileFromByteArray(encodedString);
            List<File> images = PDFBoxReadText.getAllImagesFromPdf(reportFile);
            boolean found = false;
            File expectedLogo = new File("./logo/"+organization+".png");
            BufferedImage bufferImage = toBufferedImage(expectedLogo);
            Rectangle rect = new Rectangle();
            rect.setSize(bufferImage.getWidth(),bufferImage.getHeight());
            File matchedImage = null;
            for(File imageFile: images) {
                matchedImage = imageFile;
                ScreenImage screenImage = new ScreenImage(rect, bufferImage);
                Finder f = new Finder(screenImage);
                Image image = Image.create(imageFile);
                f.find(image);
                if(f.hasNext()) {
                    found = true;
                    break;
                }
            }
            if(!found) {
                List<String> imageList = new ArrayList<>();
                if(matchedImage==null || images==null || images.isEmpty()) {
                    File reportLogoFile = new File(IMAGE_LOCATION+expectedLogo.getName());
                    FileUtils.copyFile(expectedLogo, reportLogoFile);
                    matchedImage = reportLogoFile;
                }
                imageList.add("screenshots/"+matchedImage.getName());
                throw new AutomationException(String.format("Unable to find %s logo on the report and found other logo!", organization),imageList);
            }
            CommonSteps.stepScreenshot.set("screenshots/"+matchedImage.getName());
        }catch (Throwable e){
            e.printStackTrace();
        }
    }

    public static List<File> getAllImagesFromPdf(File reportFile) {
        List<File> images = new ArrayList<>();
        try {
            PDDocument document = PDDocument.load(reportFile);
            PDPage pdfPage = document.getPage(0);
            int i = 1;
            PDResources pdResources = pdfPage.getResources();
            for (COSName c : pdResources.getXObjectNames()) {
                PDXObject o = pdResources.getXObject(c);
                if (o instanceof org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject) {
                    File file = new File(IMAGE_LOCATION + System.currentTimeMillis() + "-image-in-report.png");
                    i++;
                    ImageIO.write(((org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject) o).getImage(), "png", file);
                    images.add(file);
                }
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        return images;
    }

    public static List<File> getALlImagesFromPdf(byte[] data) {
        List<File> images = new ArrayList<>();
        try {
            String encodedString = new String(data);
            encodedString = encodedString.substring(encodedString.indexOf(",")+1, encodedString.length());
            File reportFile = createFileFromByteArray(encodedString);
            PDDocument document = PDDocument.load(reportFile);
            //int totalPage = document.getNumberOfPages();
            PDPage pdfPage = document.getPage(0);
            int i = 1;
            PDResources pdResources = pdfPage.getResources();
            for (COSName c : pdResources.getXObjectNames()) {
                PDXObject o = pdResources.getXObject(c);
                if (o instanceof org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject) {
                    File file = new File(i + ".png");
                    i++;
                    ImageIO.write(((org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject) o).getImage(), "png", file);
                    //ITesseract instance = new Tesseract();
                    //String result = instance.doOCR(file);
                    //System.out.println("Image text: "+result);
                    images.add(file);
                }
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        return images;
    }

    public static File createFileFromByteArray(String encodedString) {
        File file = new File(FILEPATH);
        try {
            file.createNewFile();
            byte[] decodedBytes = Base64.getDecoder().decode(encodedString.getBytes());
            FileOutputStream fos = new FileOutputStream(file.getPath());
            fos.write(decodedBytes);
            fos.flush();
            fos.close();
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        }
        return file;
    }

    public static BufferedImage toBufferedImage(File imgFile) {
        BufferedImage img = null;
        try {
            img = ImageIO.read(imgFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return img;
    }
}