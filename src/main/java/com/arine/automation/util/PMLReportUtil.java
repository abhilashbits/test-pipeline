package com.arine.automation.util;

import com.aspose.pdf.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PMLReportUtil {

    public static class Medication {
        String name;
        String sigText;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSigText() {
            return sigText;
        }

        public void setSigText(String sigText) {
            this.sigText = sigText;
        }
    }

    public static Map<String, String> getAllMedicationNoteMapping(String reportFilePath) {
        List<Medication> allMedications = getAllMedications(reportFilePath);
        Map<String, String> medicationMapping = new HashMap<>();
        for(Medication medication: allMedications) {
            medicationMapping.put(medication.getName(),medication.getSigText());
        }
        System.out.println(medicationMapping.toString());
        return medicationMapping;
    }

   public static Map<String, String> getAllMtmPatientMedicationMapping(String reportFilePath) {
        List<Medication> allMedications = getAllMtmPatientMedications(reportFilePath);
        Map<String, String> medicationMapping = new HashMap<>();
        for(Medication medication: allMedications) {
            if(!medication.getName().isEmpty())
                medicationMapping.put(medication.getName(),medication.getSigText());
        }
        System.out.println(medicationMapping.toString());
        return medicationMapping;
    }

    private static List<Medication> getAllMedications(String pdfFilePath) {
        List<Medication> medicationList = new ArrayList<>();
        Document pdfDocument = new Document(pdfFilePath);
        TableAbsorber absorber = new TableAbsorber();
        for (Page page : pdfDocument.getPages()) {
            absorber.visit(page);
            for (AbsorbedTable table : absorber.getTableList()) {
                int rowIndex = 0;
                for (AbsorbedRow row : table.getRowList()) {
                    rowIndex++;
                    if(rowIndex<=2)
                        continue;
                    Medication medication = new Medication();
                    int columnIndex = 0;
                    for (AbsorbedCell cell : row.getCellList()) {
                        columnIndex++;
                        if(columnIndex==1) {
                            StringBuilder sb = new StringBuilder();
                            for (TextFragment fragment : cell.getTextFragments()) {
                                for (TextSegment seg : fragment.getSegments())
                                    sb.append(seg.getText()+" ");
                            }
                            medication.setName(sb.toString());
                        } else if(columnIndex==7) {
                            StringBuilder sb = new StringBuilder();
                            for (TextFragment fragment : cell.getTextFragments()) {
                                for (TextSegment seg : fragment.getSegments())
                                    sb.append(seg.getText()+" ");
                            }
                            medication.setSigText(sb.toString());
                        }
                    }
                    medicationList.add(medication);
                }
            }
        }
        return medicationList;
    }

    private static List<Medication> getAllMtmPatientMedications(String pdfFilePath) {
        List<Medication> medicationList = new ArrayList<>();
        Document pdfDocument = new Document(pdfFilePath);
        TableAbsorber absorber = new TableAbsorber();
        for (Page page : pdfDocument.getPages()) {
            absorber.visit(page);
            for (AbsorbedTable table : absorber.getTableList()) {
                int rowIndex = 0;
                Medication medication = null;
                for (AbsorbedRow row : table.getRowList()) {
                    if(table.getRowList().size()<2)
                        break;
                    rowIndex++;
                    if(rowIndex>2)
                        break;
                    int columnIndex = 0;
                    for (AbsorbedCell cell : row.getCellList()) {
                        columnIndex++;
                        if(columnIndex==1) {
                            StringBuilder sb = new StringBuilder();
                            for (TextFragment fragment : cell.getTextFragments()) {
                                for (TextSegment seg : fragment.getSegments())
                                    sb.append(seg.getText()+" ");
                            }
                            String medicationDetails = sb.toString();
                            if(medicationDetails.contains("Medication: ")) {
                                medication = new Medication();
                                String medicationName = medicationDetails.replaceAll("Medication: ","");
                                medicationName = processMedicationName(medicationName);
                                medication.setName(medicationName);
                            } else if(medicationDetails.contains("How I use it: ")) {
                                medication.setSigText(medicationDetails.replaceAll("How I use it: ",""));
                            } else if(medicationDetails.contains("Medicamento: ")) {
                                medication = new Medication();
                                String medicationName = medicationDetails.replaceAll("Medicamento: ","");
                                medicationName = processMedicationName(medicationName);
                                medication.setName(medicationName);
                            } else if(medicationDetails.contains("Cómo lo toma: ")) {
                                medication.setSigText(medicationDetails.replaceAll("Cómo lo toma: ",""));
                            } else {
                                medication=null;
                            }
                        }
                    }
                }
                if(medication!=null)
                    medicationList.add(medication);
            }
        }
        return medicationList;
    }

    private static void extractMarkedTable() {
        // Load source PDF document
        String filePath = "<... enter path to pdf file here ...>";
        Document pdfDocument = new Document(filePath);
        Page page = pdfDocument.getPages().get_Item(1);

        AnnotationSelector annotationSelector = new AnnotationSelector(
                new SquareAnnotation(page, Rectangle.getTrivial()));

        java.util.List<Annotation> list = annotationSelector.getSelected();
        if (list.size() == 0) {
            System.out.println("Marked tables not found..");
            return;
        }

        SquareAnnotation squareAnnotation = (SquareAnnotation) list.get(0);

        TableAbsorber absorber = new com.aspose.pdf.TableAbsorber();
        absorber.visit(page);

        for (com.aspose.pdf.AbsorbedTable table : absorber.getTableList()) {
            {
                boolean isInRegion = (squareAnnotation.getRect().getLLX() < table.getRectangle().getLLX())
                        && (squareAnnotation.getRect().getLLY() < table.getRectangle().getLLY())
                        && (squareAnnotation.getRect().getURX() > table.getRectangle().getURX())
                        && (squareAnnotation.getRect().getURY() > table.getRectangle().getURY());

                if (isInRegion) {
                    for (AbsorbedRow row : table.getRowList()) {
                        {
                            for (AbsorbedCell cell : row.getCellList()) {
                                for (TextFragment fragment : cell.getTextFragments()) {
                                    StringBuilder sb = new StringBuilder();
                                    for (com.aspose.pdf.TextSegment seg : fragment.getSegments())
                                        sb.append(seg.getText());
                                    System.out.print(sb.toString() + "|");
                                }
                            }
                            System.out.println();
                        }
                    }
                }
            }
        }
    }

    private static void extractTableSaveCSV(String pdfFilePath, String csvFilePath) {
        // Load PDF document
        Document pdfDocument = new Document(pdfFilePath);
        // Instantiate ExcelSave Option object
        ExcelSaveOptions excelSave = new ExcelSaveOptions();
        excelSave.setFormat(ExcelSaveOptions.ExcelFormat.CSV);
        // Save the output in XLS format
        pdfDocument.save(csvFilePath, excelSave);
    }

    public static void main(String[] args) {
        //getAllMtmPatientMedicationMapping("./temp/report.pdf");
    }

    public static String processMedicationName(String medicationName) {
        if(medicationName.contains(" MG"))
            medicationName = medicationName.substring(0,medicationName.indexOf(" MG")+3);
        if(medicationName.contains(" MEQ"))
            medicationName = medicationName.substring(0,medicationName.indexOf(" MEQ")+4);
        if(medicationName.contains(" MCG"))
            medicationName = medicationName.substring(0,medicationName.indexOf(" MCG")+4);
        if(medicationName.contains(" IU"))
            medicationName = medicationName.substring(0,medicationName.indexOf(" IU")+3);
        if(medicationName.contains("RIVAROXABAN (XARELTO)"))
            medicationName = medicationName.replace("RIVAROXABAN (XARELTO)", "XARELTO");
        if(medicationName.contains("ALBUTEROL (PROAIR HFA)"))
            medicationName =  medicationName.replace("ALBUTEROL (PROAIR HFA)", "PROAIR HFA");
        return medicationName;
    }
}
