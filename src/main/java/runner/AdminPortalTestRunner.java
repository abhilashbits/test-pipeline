package runner;

import com.arine.automation.constants.Constants;
import com.arine.automation.glue.CommonSteps;
import com.arine.automation.reports.ExecutionResult;
import com.arine.automation.reports.ExtentManager;
import com.arine.automation.reports.ExtentTestManager;
import com.arine.automation.util.PropertyReader;
import com.arine.automation.util.TestDataExcelUtil;
import com.aventstack.extentreports.Status;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.CucumberFeatureWrapper;
import cucumber.api.testng.TestNGCucumberRunner;
import cucumber.runtime.model.CucumberFeature;
import org.testng.ITest;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.*;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@CucumberOptions(
		features = "features/admin-portal",
		glue = { "com.arine.automation.glue"},
		//tags = "@AdminPortal",
		//dryRun = true,
		monochrome = true)
public class AdminPortalTestRunner  implements ITest {
	private ThreadLocal<String> testName = new ThreadLocal<>();
	protected TestNGCucumberRunner testNGCucumberRunner;
	private static final String LOG_FILE = "log4j.properties";
	private static int PASSED_FEATURES = 0;
	private static int FAILED_FEATURES = 0;
	@BeforeClass(alwaysRun = true)
	public void init(ITestContext iTestContext) throws IOException {
		CommonSteps.REPORT_LOGGER.log("----------------------- Arine Test Automation -----------------------");
		testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());
	}

	@Test(description = "Runs Cucumber Feature", dataProvider = "features")
	public void feature(CucumberFeatureWrapper cucumberFeature) {
		try {
			TestNGCucumberRunner testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());
			CucumberFeature feature = cucumberFeature.getCucumberFeature();
			boolean isSkippedFeature = false;
			if(PropertyReader.groupingBy().equalsIgnoreCase(Constants.GROUPING_BY_EXCEL_MAPPING_DATA) &&
					!TestDataExcelUtil.isFeatureIncluded(TestDataExcelUtil.ADMIN_PORTAL_TEST_SCENARIO_MAPPING, PropertyReader.suiteType(), feature.getPath())) {
				isSkippedFeature = true;
			}
			if(!isSkippedFeature) {
				ExtentTestManager.setFeatureFileName(feature.getPath());
				CommonSteps.REPORT_LOGGER.log("Test Feature: "+feature.getPath());
				testNGCucumberRunner.runCucumber(feature);
			} else {
				CommonSteps.REPORT_LOGGER.log(feature.getPath() + " Execution Skipped as per the test data mapping!");
			}
			testNGCucumberRunner.finish();		
		}  catch(Throwable ex) {
            		//Do Nothing..
        	} finally {
            		PASSED_FEATURES++;
       		}
	}

	@Test(dataProvider = "testResults")
	public void testAnalysis(com.aventstack.extentreports.model.Test test) {
		if(test.getStatus().equals(Status.FAIL)) {
			throw new RuntimeException(test.getName());
		} else if(test.getStatus().equals(Status.SKIP)) {
			throw new SkipException(test.getName());
		}
	}

	@DataProvider(name="features", parallel=true)
	public Object[][] features() {
		List<CucumberFeature> features = testNGCucumberRunner.getFeatures();
		List<Object[]> featuresList = new ArrayList(features.size());
		Iterator var3 = features.iterator();

		while(var3.hasNext()) {
			CucumberFeature feature = (CucumberFeature)var3.next();
			if(PropertyReader.groupingBy().equalsIgnoreCase(Constants.GROUPING_BY_EXCEL_MAPPING_DATA) &&
					!TestDataExcelUtil.isFeatureIncluded(TestDataExcelUtil.ADMIN_PORTAL_TEST_SCENARIO_MAPPING, PropertyReader.suiteType(), feature.getPath())) {
				continue;
			}
			featuresList.add(new Object[]{new CucumberFeatureWrapper(feature)});
		}
		return (Object[][])featuresList.toArray(new Object[0][]);
	}

	@AfterClass(alwaysRun = true)
	public void tearDownClass() {
		testNGCucumberRunner.finish();
		ExtentManager.getExtentReports().flush();
		CommonSteps.REPORT_LOGGER.log("Test Execution Completed!");
	}

	@DataProvider(name="testResults", parallel=true)
	public Object[][] testResultDataProvider() {
		ExecutionResult result = ExtentTestManager.getResult();
		List<com.aventstack.extentreports.model.Test> results = result.testList;
		List<Object[]> testList = new ArrayList(results.size());
		int passedScenarioCount = result.passedTestCases - PASSED_FEATURES;
		int failedScenarioCount = result.failedTestCases - FAILED_FEATURES;
		int totalTestToKeep = passedScenarioCount + failedScenarioCount;
		int index = 0;
		while(testList.size()<passedScenarioCount) {
			com.aventstack.extentreports.model.Test test = results.get(index);
			if(!test.getName().contains("Launch Browser") && test.getStatus().equals(Status.PASS)) {
				testList.add(new Object[]{test});
			}
			index++;
			if(index>results.size()-1)
				break;
		}
		index = 0;
		while(testList.size()<passedScenarioCount) {
			com.aventstack.extentreports.model.Test test = results.get(index);
			if(test.getName().contains("Launch Browser") && test.getStatus().equals(Status.PASS)) {
				testList.add(new Object[]{test});
			}
			index++;
			if(index>results.size()-1)
				break;
		}
		index = 0;
		while(testList.size()<totalTestToKeep) {
			com.aventstack.extentreports.model.Test test = results.get(index);
			if(test.getStatus().equals(Status.FAIL)) {
				testList.add(new Object[]{test});
			}
			index++;
			if(index>results.size()-1)
				break;
		}
		for(com.aventstack.extentreports.model.Test test: results) {
			if(test.getStatus().equals(Status.SKIP)) {
				testList.add(new Object[]{test});
			}
		}
		return (Object[][])testList.toArray(new Object[0][]);
	}

	@BeforeMethod
	public void BeforeMethod(Method method, Object[] testData){
		Object data = testData[0];
		if(data instanceof com.aventstack.extentreports.model.Test)
			testName.set(((com.aventstack.extentreports.model.Test) data).getName());
		else if(data instanceof CucumberFeatureWrapper)
			testName.set(((CucumberFeatureWrapper) data).getCucumberFeature().getPath());
	}

	@Override
	public String getTestName() {
		return testName.get();
	}
}